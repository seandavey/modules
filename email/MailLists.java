package email;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import app.ClassTask;
import app.Files;
import app.Person;
import app.Request;
import app.Roles;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.DeleteHook;
import db.Filter;
import db.Form;
import db.InsertHook;
import db.ManyToMany;
import db.NameValuePairs;
import db.OneToMany;
import db.OrderBy;
import db.Rows;
import db.SQL;
import db.Select;
import db.UpdateHook;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.PersonColumn;
import db.column.RolesColumn;
import db.feature.ColumnFilter;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.TSVectorSearch;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.jdbc.TSVector;
import db.object.JSONField;
import db.section.Dividers;
import jakarta.mail.Address;
import jakarta.mail.Message;
import jakarta.mail.Message.RecipientType;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import jakarta.mail.util.SharedByteArrayInputStream;
import mail.Addresses;
import mail.Mail;
import pages.Page;
import ui.Option;
import ui.Table;
import util.Array;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JS;

public class MailLists extends SiteModule implements InsertHook, DeleteHook, UpdateHook {
	@JSONField
	private boolean				m_add_list_name_to_subject = true;
	@JSONField(after = "outside email addresses or domains")
	private String[]			m_accept_from;
	@JSONField
	private boolean				m_add_sender_to_from;
	Map<String,String>			m_aliases = new HashMap<>();
	@JSONField(admin_only=true)
	private String[]			m_debug_addresses;
	List<String>				m_errors = new ArrayList<>();
	@JSONField
	private String				m_forward_rejected_posts_to;
	@JSONField(label="before post")
	private boolean				m_header_before = true;
	@JSONField(label="add \"posted by\" line to posts",fields={"m_header_before"})
	private boolean 			m_insert_headers = true;
	List<MailHandlerFactory>	m_mail_handler_factories = new ArrayList<>();
	Map<String,MailHandler>		m_mail_handlers = new HashMap<>();
	@JSONField(admin_only=true)
	private boolean				m_log_sends;
//	private int					m_max_message_size = 19922944;
	@JSONField(admin_only=true)
	private boolean				m_send_individually;
	@JSONField
	private boolean				m_show_full_address_on_email_page;
	@JSONField
	private boolean				m_show_reply_links_in_archives = true;

	//--------------------------------------------------------------------------

	public
	MailLists() {
		m_description = "Email Lists with archives, nightly digests, automatic subscription and many options";
	}

	//--------------------------------------------------------------------------

	private boolean
	accept(String address) {
		if (m_accept_from != null)
			for (String s : m_accept_from)
				if (address.endsWith(s))
					return true;
		return false;
	}

	//--------------------------------------------------------------------------

	public MailLists
	addAlias(String from, String to) {
		m_aliases.put(from, to);
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	addAddresses(ArrayList<String> a, DBConnection db) {
		if (!isActive())
			return;
		List<String> addresses = db.readValues(new Select("name").from("mail_lists").where("active").orderBy("LOWER(name)"));
		for (String address : addresses)
			a.add(Site.site.getEmailAddress(address));
	}

	//--------------------------------------------------------------------------

//	public MailLists
//	addMailHandler(String list, MailHandler mail_handler) {
//		m_mail_handlers.put(list.toLowerCase(), mail_handler);
//		return this;
//	}

	//--------------------------------------------------------------------------

	public MailLists
	addMailHandlerFactory(MailHandlerFactory mail_handler_factory) {
		m_mail_handler_factories.add(mail_handler_factory);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Email", false) {
			@Override
			public void
			writeContent(Request r) {
				String list = r.getPathSegment(1);
				if (list != null) {
					int list_id = r.db.lookupInt(new Select("id").from("mail_lists").whereEquals("name", list), 0);
					if (list_id != 0)
						writeArchive(list_id, r);
					return;
				}
				HTMLWriter w = r.w;
				w.js("""
					function toggle_digest(c, list_id) {
						net.post(context + '/MailLists/' + list_id + '/digest/' + (c.checked ? 'true' : 'false'));
					}
					function toggle_subscription(c, list_id, archive, archive_public) {
						_.$('#digest'+list_id).style.display=c.checked?'':'none'
						if (archive && !archive_public) {
							_.$('#view'+list_id).style.display=c.checked?'':'none'
							_.$('#date'+list_id).style.display=c.checked?'':'none'
						}
						net.post(context + '/MailLists/' + list_id + '/subscribed/' + (c.checked ? 'true' : 'false'));
					}""");
				if (Site.settings.getBoolean("show send email to community members link"))
					w.write("<div style=\"padding:30px 0\">")
						.ui.aButton("Send email to individual community members", "send_email.jsp", null)
						.write("</div>");
				w.write("<div>");
				writeLists(r);
				w.write("</div>");
				if (r.getParameter("inactive_lists") == null && r.db.rowExists("mail_lists", "NOT active"))
					w.write("<div style=\"padding-top: 20px;\">")
						.ui.aButton("Show inactive lists", "Email?inactive_lists=show")
						.write("</div>");
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("mail_lists")
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("accept_from", Types.VARCHAR))
			.add(new JDBCColumn("allow_from_outside", Types.BOOLEAN).setDefaultValue(false))
			.add(new JDBCColumn("allow_from_outside_subscribers", Types.BOOLEAN).setDefaultValue(false))
			.add(new JDBCColumn("announce_only", Types.BOOLEAN))
			.add(new JDBCColumn("archive", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("archives_public", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("footer", Types.VARCHAR))
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("password", Types.VARCHAR))
			.add(new JDBCColumn("posting_role", Types.VARCHAR))
			.add(new JDBCColumn("send_to", Types.VARCHAR))
			.add(new JDBCColumn("show_subscribers", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("subscribing_role", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createManyToManyLinkTable("mail_lists", "people");
		db.createTable("mail_lists_digest", false, "mail_lists_id INTEGER REFERENCES mail_lists ON DELETE CASCADE,people_id INTEGER REFERENCES people ON DELETE CASCADE", null, false);
		db.createManyTable("mail_lists", "subscribers", "name VARCHAR,email VARCHAR");
		table_def = new JDBCTable("additional_emails")
			.add(new JDBCColumn("people"))
			.add(new JDBCColumn("email", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		createListTable(id, r.db);
		setupList(nvp.getString("name"), true);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String,Object> previous_values, Request r) {
		if (previous_values.containsKey("name"))
			setupList(nvp.getString("name"), true);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeDelete(String where, Request r) {
		String id = where.substring(3);
		String name = r.db.lookupString(new Select("name").from("mail_lists").where(where));

//		new File(Site.site.getBasePathBuilder().append("inbox").append(name + ".sh").toString()).delete();
		Files.deleteDirectory("inbox", name);
		Site.site.runScript("delete_email", name, Site.site.getDomain());
		r.db.dropTable("ml_" + id);
		Files.deleteDirectory("mail", id);
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String,Object> previous_values, Request r) {
		if (nvp.getBoolean("active"))
			r.db.changed("mail_lists", "name", id, nvp, previous_values);
		return null;
	}

	// --------------------------------------------------------------------------

	public void
	checkForMail(DBConnection db) {
		if (Site.site.isDevMode())
			return;
		readAndSend(db);
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public static void
	cleanup(DBConnection db) {
		List<String> tables = db.readValues(new db.Select("table_name").from("information_schema.tables").where("table_name ILIKE 'ml_%'"));
		for (String table : tables)
			if (!db.exists("mail_lists", "id=" + table.substring(3))) {
				System.out.println("dropping " + table);
				db.dropTable(table);
			}
		File[] dirs = Site.site.newFile("mail").listFiles();
		if (dirs != null)
			for (File d : dirs)
				if (d.isDirectory() && !db.exists("mail_lists", "id=" + d.getName())) {
					System.out.println("deleting " + d.getAbsolutePath());
					Files.deleteDirectory("mail", d.getName());
				}
	}

	// --------------------------------------------------------------------------

	private static void
	createListTable(int list_id, DBConnection db) {
		JDBCTable table_def = new JDBCTable("ml_" + list_id)
			.add(new JDBCColumn("arrived", Types.TIMESTAMP))
			.add(new JDBCColumn("sender", Types.VARCHAR))
			.add(new JDBCColumn("subject", Types.VARCHAR))
			.add(new TSVector());
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createIndex("ml_" + list_id, "arrived");
	}

	//--------------------------------------------------------------------------

	@ClassTask({"date", "mail list"})
	public void
	digestsSend(LocalDate date, int mail_lists_id, DBConnection db) {
		if (mail_lists_id != 0) {
			new MailList(mail_lists_id, db).sendDigest(date, false, this, db);
			return;
		}
		List<Integer> mail_lists = db.readValuesInt(new Select("mail_lists_id").distinct().from("mail_lists_digest"));
		for (Integer list_id : mail_lists)
			new MailList(list_id, db).sendDigest(date, false, this, db);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"date", "mail list"})
	public void
	digestsTest(LocalDate date, int mail_lists_id, DBConnection db) {
		if (mail_lists_id != 0) {
			new MailList(mail_lists_id, db).sendDigest(date, false, this, db);
			return;
		}
		List<Integer> mail_lists = db.readValuesInt(new Select("mail_lists_id").distinct().from("mail_lists_digest"));
		for (Integer list_id : mail_lists)
			new MailList(list_id, db).sendDigest(date, true, this, db);
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		int list_id = Integer.parseInt(path_segments[0]);
		switch(path_segments[1]) {
		case "archive":
			writeArchive(list_id, r);
			return true;
		case "subscribers":
			MailList mail_list = new MailList(list_id, r.db);
			MailHandler mail_handler = getMailHandler(list_id, r.db);
			boolean has_subscribers = mail_handler != null ? mail_handler.writeSubscribers(r) : mail_list.writeSubscribers(r);
			mail_list.writeAdditionalSubscribers(has_subscribers, r);
			return true;
		}
		int message_id = Integer.parseInt(path_segments[1]);
		switch(path_segments[2]) {
		case "message":
			writeMessage(list_id, message_id, r);
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_two = r.getPathSegment(2);
		if (segment_two == null)
			return false;
		int ml_id = r.getPathSegmentInt(1);
		int people_id = r.getUser().getId();
		switch (segment_two) {
		case "digest":
			if ("true".equals(r.getPathSegment(3))) {
				if (!r.db.exists("mail_lists_digest", "people_id=" + people_id + " AND mail_lists_id=" + ml_id))
					r.db.insert("mail_lists_digest", "people_id,mail_lists_id", people_id + "," + ml_id);
			} else
				r.db.delete("mail_lists_digest", "people_id=" + people_id + " AND mail_lists_id=" + ml_id, false);
			return true;
		case "subscribed":
			if ("true".equals(r.getPathSegment(3))) {
				if (!r.db.exists("mail_lists_people", "people_id=" + people_id + " AND mail_lists_id=" + ml_id))
					r.db.insert("mail_lists_people", "people_id,mail_lists_id", people_id + "," + ml_id);
			} else {
				r.db.delete("mail_lists_people", "mail_lists_id=" + ml_id + " AND people_id=" + people_id, false);
				r.db.delete("mail_lists_digest", "mail_lists_id=" + ml_id + " AND people_id=" + people_id, false);
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyHour(LocalDateTime now, DBConnection db) {
		checkForMail(db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyNight(LocalDateTime now, DBConnection db) {
		readAndSend(db);
		digestsSend(now.toLocalDate().minusDays(1), 0, db);
	}

	// --------------------------------------------------------------------------

	private void
	forwardRejectedPost(String list, Message message, String reason) throws MessagingException {
		if (m_forward_rejected_posts_to == null)
			return;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			message.writeTo(os);
		} catch (IOException e) {
			Site.site.log(e);
		}
		MimeMessage new_message = new MimeMessage(Site.site.getMailSession(), new ByteArrayInputStream(os.toString().getBytes()));
		new_message.setFrom(new InternetAddress(Site.site.getEmailAddress(list)));
		new_message.removeHeader("Return-Path");
		Mail.insertHeader(new_message, "Post to " + list + " rejected - " + reason);
		send(new_message, new InternetAddress[] { new InternetAddress(m_forward_rejected_posts_to) }, list);
	}

	// --------------------------------------------------------------------------

	private String
	getListName(int list_id, DBConnection db) {
		return db.lookupString(new Select("name").from("mail_lists").whereIdEquals(list_id));
	}

	// --------------------------------------------------------------------------

	MailHandler
	getMailHandler(int list_id, DBConnection db) {
		return getMailHandler(getListName(list_id, db), db);
	}

	// --------------------------------------------------------------------------

	public MailHandler
	getMailHandler(String list, DBConnection db) {
		if (list == null) {
			Site.site.log("getMailHandler: null list", false);
			return null;
		}
		MailHandler mail_handler = m_mail_handlers.get(list.toLowerCase());
		if (mail_handler != null)
			return mail_handler;
		String send_to = db.lookupString(new Select("send_to").from("mail_lists").where("name ILIKE '" + SQL.escape(list) + "'"));
		if (send_to != null)
			for (MailHandlerFactory mail_handler_factory : m_mail_handler_factories) {
				mail_handler = mail_handler_factory.getHandler(send_to, db);
				if (mail_handler != null) {
					m_mail_handlers.put(list.toLowerCase(), mail_handler);
					return mail_handler;
				}
			}
		return null;
	}

	//--------------------------------------------------------------------------

	public final List<String>
	getMailLists(int person_id, Request r) {
		ArrayList<String> lists = new ArrayList<>();
		Select query = new Select("mail_lists.name,mail_lists_people.mail_lists_id,mail_lists_digest.mail_lists_id")
				.from("mail_lists LEFT JOIN mail_lists_people ON (mail_lists_people.mail_lists_id=mail_lists.id AND mail_lists_people.people_id=" + person_id + ") LEFT JOIN mail_lists_digest ON (mail_lists_digest.mail_lists_id=mail_lists.id AND mail_lists_digest.people_id=" + person_id + ")")
				.where("mail_lists.active")
				.orderBy("LOWER(mail_lists.name)");
		Rows rows = new Rows(query, r.db);
		while (rows.next()) {
			String list = rows.getString(1);
			MailHandler mail_handler = getMailHandler(list, r.db);
			boolean subscribed = mail_handler != null ? mail_handler.isSubscribed(person_id, r.db) : rows.getInt(2) != 0;
			if (subscribed)
				if (rows.getInt(3) != 0)
					lists.add(list + " (digest)");
				else
					lists.add(list);
		}
		rows.close();
		return lists;
	}

	//--------------------------------------------------------------------------
	// return extension to use for renamed file

	private String
	handleMessage(String list, String filename, Message message, boolean archive, boolean include_digest_subscribers, DBConnection db) throws MessagingException {
		boolean from_site = message.getHeader("X-from-site") != null;
		if (from_site)
			archive = false;
		final String subject = message.getSubject();
		if (subject != null && subject.startsWith("Automatic reply:") || message.getHeader("X-ignore") != null || message.getHeader("auto-submitted") != null || message.getHeader("X-Autoreply") != null && message.getHeader("X-Autorespond") == null)
			return "autoreply";

		for (String from: m_aliases.keySet())
			if (from.equals(list))
				list = m_aliases.get(from);

		MailList mail_list = new MailList(list, db);
		if (!mail_list.isActive()) {
			Site.site.log("handleMessage:post to inactivelist", false);
			forwardRejectedPost(list, message, "posted to inactive list");
			return "inactive";
		}
		if (mail_list.announceOnly() && (message.getHeader("In-Reply-To") != null || message.getHeader("References") != null && message.getHeader("References").length > 1)) {
			Site.site.log("handleMessage:reply to announce only list " + list, false);
			forwardRejectedPost(list, message, "reply to announce only list");
			return "announce_only";
		}

		InternetAddress from = null;
		try {
			from = (InternetAddress)message.getFrom()[0];
		} catch (AddressException e) {
			String s = e.toString();
			int index = s.indexOf("<<");
			if (index != -1)
				try {
					from = new InternetAddress(s.substring(index + 2, s.indexOf(">>", index + 2)));
				} catch (AddressException e1) {
				}
			if (from == null) {
				Site.site.log(e);
				return "AddressException";
			}
		}

		MailHandler mail_handler = getMailHandler(list, db);
		if (!from_site && !mail_list.acceptPost(from, mail_handler, db)) {
			Site.site.log("email not accepted from " + from.toString(), false);
			return "not_accepted";
		}
		String name = null;
		String address = from.getAddress();
		int from_id = lookupPerson(address, db);
		if (from_id != 0)
			name = db.lookupString(new Select("first,last").from("people").whereIdEquals(from_id));
		if (name == null)
			name = mail_list.lookupName(from, db);
		if (name == null && accept(address))
			name = address;
		if (name == null && from_site)
			name = address;
		if (name == null) {
			forwardRejectedPost(list, message, "unknown sender " + message.getFrom()[0].toString());
			return "unknown_sender";
		}

//		if (mail_handler != null && mail_handler.handleMessage(message, db))
//			return true;

		final int list_id = mail_list.getId();
		if (list_id == 0) {
			Site.site.log("list not found: " + list, false);
			return "no_list";
		}

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			message.writeTo(os);
		} catch (IOException e) {
			Site.site.log(e);
		}
		String message_text = os.toString();

		MimeMessage new_message = new MimeMessage(Site.site.getMailSession(), new ByteArrayInputStream(message_text.getBytes()));
		new_message.setReplyTo(new Address[] { from });
		new_message.setSender(new InternetAddress(Site.site.getEmailAddress(list)));
		String personal = list;
		if (m_add_sender_to_from)
			personal += " - " + name;
		try {
			new_message.setFrom(new InternetAddress(Site.site.getEmailAddress(list), personal));
		} catch (UnsupportedEncodingException e) {
			Site.site.log(e);
		}
		new_message.removeHeader("DKIM-Signature");
//		new_message.removeHeader("Return-Path");
		new_message.removeHeader("Newsgroup");
		new_message.setHeader("List-Post", "<mailto:" + Site.site.getEmailAddress(list) + ">");
		new_message.setHeader("List-Unsubscribe", "<https://" + Site.site.getDomain() + Site.context + "/Email>");
		new_message.setHeader("Precedence", "list");
		if (subject == null)
			new_message.setSubject("[" + list + "]");
		else if (m_add_list_name_to_subject && subject.indexOf("[" + list + "]") == -1)
			new_message.setSubject("[" + list + "] " + subject);

		if (from.toString().startsWith("noreply@"))
			Mail.insertHeader(new_message, "This email was sent from a \"noreply\" account. You can reply to the list but replies to the \"noreply\" address will be discarded.");
		if (mail_list.announceOnly())
			Mail.insertHeader(new_message, "This email is from an \"announce only\" mail list. Do not reply to this email. Replies will be discarded.");
		if (m_insert_headers)
			if (m_header_before)
				Mail.insertHeader(new_message, "posted by " + name);
			else
				Mail.addFooter(new_message, "posted by " + name);
		mail_list.addFooter(new_message);

		int archive_id = 0;
		if (archive)
			archive_id = mail_list.archive(name, subject, message_text, db);

		Addresses addresses = mail_handler != null ? mail_handler.getAddresses(list_id, db) : mail_list.getAddresses(include_digest_subscribers, db);
		addresses.add(db.readValues(new Select("email").from("subscribers").where("email IS NOT NULL AND mail_lists_id=" + list_id)));
		if (addresses.size() == 0) {
			Site.site.log("no subscribers: " + list, false);
			//			new Mail().send(list + "@" + Site.site.getDomain(), message.getReplyTo()[0].toString(),  Site.site.getDisplayName() + ": mail error", "Your recent message to " + list + "@" + Site.site.getDomain() + " with subject \"" + message.getSubject() + "\" was archived but not sent because currently no one is signed up for that list.", false);
			return "no subscribers";
		}
//		try {
			addresses.remove(message.getRecipients(RecipientType.TO), db);
			addresses.remove(message.getRecipients(RecipientType.CC), db);
			if (from_id != 0 && db.lookupBoolean(new Select("remove_me_when_i_post_to_lists").from("people").whereIdEquals(from_id)))
				addresses.remove(from, db);
//		} catch (Exception e) {
//			if (archive_id != 0)
//				mail_list.deleteFromArchive(archive_id, db);
//			sendError(list, "handleMessage", null, message, e);
//			return "address_remove_error";
//		}
		try {
			if (!send(new_message, addresses.toArray(), list)) {
				if (archive_id != 0)
					mail_list.deleteFromArchive(archive_id, db);
				return "send_error";
			}
		} catch (Exception e) {
			System.out.println("Error with address for list " + list);
			throw e;
		}
		final Path path = Site.site.getPath("mail", Integer.toString(list_id), archive_id + ".gz");
		if (path.toFile().exists()) {
			final int id = archive_id;
			new Thread() {
				@Override
				public void run() {
					String text = Site.site.loadTokens(path);
					if (text != null) {
						if (subject != null)
							text = subject + "\n" + text;
						try {
							DBConnection dbc = new DBConnection();
							dbc.update("ml_" + list_id, "tsvector=to_tsvector(" + SQL.string(text) + ")", id);
							dbc.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
		}
		return "sent";
	}

	//--------------------------------------------------------------------------

//	@ClassTask({"list id","filename"})
//	public void
//	importSubscribers(int list_id, String filename, DBConnection db) {
//		try {
//			NameValuePairs nvp = new NameValuePairs();
//			nvp.set("mail_lists_id", list_id);
//			BufferedReader br = new BufferedReader(new FileReader(Site.site.getBasePathBuilder().append(filename).toString()));
//			String l = br.readLine();
//			while (l != null) {
//				String[] columns = l.split("\t");
//				nvp.set("email", columns[0]);
//				nvp.set("name", columns.length > 1 && columns[1].length() > 0 ? columns[1] : null);
//				db.insert("subscribers", nvp);
//				l = br.readLine();
//			}
//			br.close();
//		} catch (IOException e) {
//		}
//
//	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Roles.add("mail", "can edit the mail lists with this role");
		Site.site.addUserDropdownItem(new Page("Edit Mail Lists", this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("mail_lists", r).writeComponent();
			}
		}.addRole("mail"));
		readAndSend(db);
	}

	//--------------------------------------------------------------------------

	private static String
	loadFileTokens(Path path) throws IOException {
		String text = Site.site.loadTokens(path);
		StringBuilder s = new StringBuilder();
		BufferedReader br = new BufferedReader(new StringReader(text));
		boolean keep = true;
		String l = br.readLine();
		while (l != null) {
			if (l.startsWith("--")) {
				if (l.endsWith("--"))
					keep = true;
			} else if (l.startsWith("Content-Type: image") || l.startsWith("Content-Transfer-Encoding: base64"))
				keep = false;
			else if (keep)
				s.append(l).append("\n");
			l = br.readLine();
		}
		return s.toString();
	}

	//--------------------------------------------------------------------------

	private static void
	loadListTokens(int list_id, DBConnection db) {
		createListTable(list_id, db);
		String list_table = "ml_" + list_id;
		Rows messages = new Rows(new Select("id,sender,subject").from(list_table).where("tsvector IS NULL OR length(tsvector)<10").orderBy("id"), db);
		while (messages.next()) {
			int message_id = messages.getInt(1);
			Path path = Site.site.getPath("mail", Integer.toString(list_id), message_id + ".gz");
			if (!path.toFile().exists()) {
				System.out.println(path + " not found");
				continue;
			}
			try {
				String s = loadFileTokens(path);
				db.update(list_table, "tsvector=to_tsvector('" + SQL.escape(messages.getString(2) + "\n" + messages.getString(3) + "\n" + s.toString()) + "')", message_id);
			} catch (Exception e) {
				System.out.println("list " + list_id + " message " + message_id);
			}
		}
		messages.close();
	}

	//--------------------------------------------------------------------------

	@ClassTask({"list id"})
	public static void
	loadTokens(int list_id, DBConnection db) {
		cleanup(db);
		if (list_id != 0)
			loadListTokens(list_id, db);
		else {
			Rows mail_lists = new Rows(new Select("id").from("mail_lists").orderBy("id"), db);
			while (mail_lists.next())
				loadListTokens(mail_lists.getInt(1), db);
			mail_lists.close();
		}
	}

	//--------------------------------------------------------------------------

	static int
	lookupId(InternetAddress address, DBConnection db) {
		String email = address.getAddress();
		String where = "active";
		if (db.hasColumn("people", "can_post_to_mail_lists"))
			where += " AND can_post_to_mail_lists";
		int id = db.lookupInt("id", "people", where + " AND email ILIKE '" + SQL.escape(email) + "%'", 0);
		if (id == 0)
			id = db.lookupInt("people_id", "additional_emails", "email ILIKE '" + SQL.escape(email) + "%'", 0);
		return id;
	}

	//--------------------------------------------------------------------------

	private static int
	lookupPerson(String address, DBConnection db) {
		String where = "active";
		if (db.hasColumn("people", "can_post_to_mail_lists"))
			where += " AND can_post_to_mail_lists";
		int id = db.lookupInt("id", "people", where + " AND email ILIKE '" + SQL.escape(address) + "%'", 0);
		if (id == 0)
			id = db.lookupInt("people_id", "additional_emails", "email ILIKE '" + SQL.escape(address) + "%'", 0);
		return id;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("additional_emails"))
			return new ViewDef(name) {
					@Override
					protected void
					beforeList(View v, Request r) {
						r.w.ui.helpText("Enter other email addresses that you want to post from");
					}
				}
				.setDefaultOrderBy("email")
				.setRecordName("Additional email", true);
//				.setShowColumnHeads(false);
		if (name.equals("mail_lists"))
			return new ViewDef(name){
					@Override
					public String
					beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
						boolean new_active = nvp.getBoolean("active");
						boolean current_active = r.db.lookupBoolean(new Select("active").from("mail_lists").whereIdEquals(id));
						if (!new_active && current_active) {
							r.db.delete("mail_lists_people", "mail_lists_id", id, false);
							r.db.delete("mail_lists_digest", "mail_lists_id", id, false);
							Site.site.runScript("delete_email", name, Site.site.getDomain());
							Files.deleteDirectory("inbox", name);
						} else if (new_active && !current_active) {
							Site.site.makeDirectory("inbox", name);
							Site.site.runScript("new_email", name, Site.site.getDatabaseName(), Site.site.getDomain());
						}
						return super.beforeUpdate(id, nvp, previous_values, r);
					}
				}
				.setAccessPolicy(new RoleAccessPolicy("mail").add().delete().edit())
				.setAfterForm("<script>function mail_list_update(){var s=_.$('#Mail_List')['send_to'];var d=s.options[s.selectedIndex].value=='Subscribers'?'':'none';_.$('#subscribing_role_row').style.display=d;_.$('#mail_lists people_row').style.display=d;}mail_list_update()</script>")
				.addDeleteHook(this)
				.addInsertHook(this)
				.addUpdateHook(this)
				.setDefaultOrderBy("name")
				.setRecordName("Mail List", true)
				.setColumnNamesForm(new String[] { "active", "name", "description", "accept_from", "send_to", "announce_only", "archive", "footer", "subscribing_role", "show_subscribers", "mail_lists people", "mail_lists digest", "subscribers", "allow_from_outside_subscribers" })
				.setColumnNamesFormTable(new String[] { "name" })
				.setColumnNamesTable(new String[] { "name", "send_to", "active" })
				.setColumn(new AcceptFromColumn())
				.setColumn(new Column("allow_from_outside_subscribers").setDisplayName("accept posts from addresses on \"outside subscribers\" list"))
				.setColumn(new Column("announce_only").setDisplayName("announcements only, replies to this list will be ignored"))
				.setColumn(new Column("archive").setSubColumns(true, "archives_public"))
				.setColumn(new Column("archives_public").setDisplayName("archives viewable by site members not on list"))
				.setColumn(new Column("footer").setTitle("Optional text that will be added to the bottom of every post to this list"))
				.setColumn(new Column("name").setPostText("@" + Site.site.getDomain()).setDisplayName("address").setIsRequired(true).setIsUnique(true).setNoSpaces(true).setOrderBy("lower(name)"))
				.setColumn(new Column("password").setIsHidden(true))
				.setColumn(new Column("show_subscribers").setDisplayName("show subscribers button on Email page"))
				.setColumn(new RolesColumn("subscribing_role").setDisplayName("role required to subscribe/unsubscribe people to list"))
				.setColumn(new Column("send_to"){
					@Override
					public void
					writeInput(View v, Form f, boolean inline, Request r) {
						String value = null;
						if (v.getMode() == Mode.EDIT_FORM)
							value = v.data.getString("send_to");
						r.w.setAttribute("onchange", "mail_list_update()")
							.ui.selectOpen("send_to", true)
							.write("<option value=\"Subscribers\">Subscribers</option>");
						List<Option> options = new ArrayList<>();
						for (MailHandlerFactory mail_handler_factory : m_mail_handler_factories)
							mail_handler_factory.addSendToOptions(options, r.db);
						Collections.sort(options);
						for (Option option : options)
							r.w.option(option.getText(), option.getValue(), value == null ? false : value.equals(option.getValue()));
						r.w.tagClose();
					}
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						String send_to = v.data.getString("send_to");
						if ("Subscribers".equals(send_to))
							r.w.write(send_to);
						else {
							MailHandler mail_handler = getMailHandler(v.data.getString("name"), r.db);
							if (mail_handler != null)
								r.w.write(mail_handler.getDisplayName());
						}
						return true;
					}
				})
				.addRelationshipDef(new ManyToMany("mail_lists people", "mail_lists_people", "first,last").setHideOnForms(Mode.ADD_FORM))
				.addRelationshipDef(new ManyToMany("mail_lists digest", "mail_lists_digest", "first,last").setHideOnForms(Mode.ADD_FORM))
				.addRelationshipDef(new OneToMany("subscribers").setHideOnForms(Mode.ADD_FORM));
		if (name.equals("mail_lists digest"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setDeleteButtonText(Text.unsubscribe)
				.setDefaultOrderBy("first,last")
				.setFrom("people")
				.setRecordName("Digest Subscriber", true)
//				.setShowColumnHeads(false)
				.setColumnNamesTable("id")
				.setColumn(new PersonColumn("id", false));
		if (name.equals("mail_lists people"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setDeleteButtonText(Text.unsubscribe)
				.setDefaultOrderBy("first,last")
				.setFrom("people")
				.setRecordName("Subscriber", true)
//				.setShowColumnHeads(false)
				.setColumnNamesTable("id")
				.setColumn(new PersonColumn("id", false));
		if (name.equals("mail_lists_people"))
			return new ViewDef(name)
				.setColumn(new LookupColumn("mail_lists_id", "mail_lists", "name", new Select("id,name,send_to,active").from("mail_lists").orderBy("name")).setDisplayName("mail list").setFilter(new Filter(){
					@Override
					public boolean
					accept(Rows rows, Request r) {
						return rows.getBoolean("active") && "Subscribers".equals(rows.getString("send_to"));
					}
				}))
				.setColumn(new PersonColumn("people_id",false));
		if (name.startsWith("ml_"))
			return new ViewDef(name)
				.addFeature(new TSVectorSearch(null, Location.TOP_RIGHT))
				.addFeature(new MonthFilter(name, Location.TOP_LEFT).setColumn("arrived"))
				.addFeature(new YearFilter(name, Location.TOP_LEFT).setColumn("arrived"))
				.addFeature(new ColumnFilter("sender", null, Location.TOP_LEFT))
				.addSection(new Dividers("arrived", new OrderBy("arrived", true, false)).setFormatDate(true))
				.setAccessPolicy(new RoleAccessPolicy("mail").delete())
				.setDefaultOrderBy("arrived DESC")
				.setRecordName("Message", true)
				.setRowWindowSize(50)
				.setColumnNamesTable(new String[] { "arrived", "sender", "subject" })
				.setColumn(new Column("arrived").setIsSortable(false))
				.setColumn(new Column("sender").setIsSortable(false))
				.setColumn(new Column("subject"){
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						String subject = v.data.getString("subject");
						if (subject == null || subject.length() == 0)
							subject = "(no subject)";
						r.w.addStyle("text-decoration:none")
							.aOnClick(subject, "new Dialog({url:'" + apiURL(v.getViewDef().getName().substring(3), v.data.getString("id"), "message") + "',title:" + JS.string(subject) + "});");
						return true;
					}
				}.setIsSortable(false));
		if (name.equals("people mail_lists"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setAddButtonText(Text.subscribe)
				.setDefaultOrderBy("name")
				.setDeleteButtonText(Text.unsubscribe)
				.setFrom("mail_lists")
				.setRecordName("Mail List", true)
//				.setShowColumnHeads(false)
				.setColumnNamesTable(new String[] { "name" });
		if (name.equals("people mail_lists_digest"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setAddButtonText(Text.subscribe)
				.setDefaultOrderBy("name")
				.setDeleteButtonText(Text.unsubscribe)
				.setFrom("mail_lists")
				.setRecordName("Mail List Digest", true)
//				.setShowColumnHeads(false)
				.setColumnNamesTable(new String[] { "name" });
		if (name.equals("subscribers"))
			return new ViewDef(name) {
					@Override
					protected void
					beforeList(View v, Request r) {
						r.w.write("<div class=\"alert alert-info\">Outside subscribers are people who do not have an account on this website.</div>");
					}
				}
				.setDefaultOrderBy("name,email")
				.setRecordName("Outside Subscriber", true);
		return null;
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public synchronized void
	readAndSend(DBConnection db) {
		Session session = Session.getInstance(new Properties(), null);

		Site.site.newFile("inbox").listFiles(new FileFilter() {
				@Override
				public boolean
				accept(File file) {
					if (file.isDirectory() && file.getName().charAt(0) != '.')
						readAndSend(file, session, db);
					return false;
				}
			});
	}

	//--------------------------------------------------------------------------

	public synchronized void
	readAndSend(File dir, Session session, DBConnection db) {
		String list = dir.getName();
		if (list.indexOf('/') != -1)
			dir = new File(dir.getParent() + "/" + list.replace('/', '_'));
		File[] files = dir.listFiles(new FileFilter() {
			@Override
			public boolean
			accept(File file) {
				return file.getName().endsWith(".txt");
			}
		});
		if (files == null) {
			Site.site.log("Problem reading directory " + dir.getAbsolutePath(), false);
			return;
		}
		for (File file : files)
			try {
//				if (file.length() > m_max_message_size) {
//					Site.site.log(file.toString() + " too big", false);
//					file.delete();
//					continue;
//				}
				String result = handleMessage(list, file.getName(), new MimeMessage(session, new FileInputStream(file)), true, false, db);
				String p = file.getAbsolutePath();
				p = p.substring(0, p.length() - 4);
				new File(p).renameTo(new File(p + "." + result));
				file.delete();
			} catch (Exception e) {
				Site.site.log(e);
			}
	}

	//--------------------------------------------------------------------------

	public void
	removeMailHandlerFactory(String name) {
		for (int i=0; i<m_mail_handler_factories.size(); i++)
			if (m_mail_handler_factories.get(i) instanceof PeopleMailHandlerFactory && ((PeopleMailHandlerFactory)m_mail_handler_factories.get(i)).m_name.equals(name)) {
				m_mail_handler_factories.remove(i);
				return;
			}
	}

	//--------------------------------------------------------------------------

	public void
	removePerson(int id, DBConnection db) {
		db.delete("mail_lists_people", "people_id", id, false);
		db.delete("mail_lists_digest", "people_id", id, false);
	}

	//--------------------------------------------------------------------------

//	@AdminTask({"list","filename"})
//	public synchronized void
//	resend(String list, String filename, Request r) {
//		Session session = Session.getInstance(new Properties(), null);
//		try {
//			MimeMessage message = new MimeMessage(session, new FileInputStream(r.getBaseFilePath().append("inbox").append(list).append(filename).toString()));
//			handleMessage(list, message, false, false, r.db);
//		} catch (FileNotFoundException e) {
//			r.log(e);
//		} catch (MessagingException e) {
//			r.log(e);
//		}
//	}

	//--------------------------------------------------------------------------
	// returns true if send was successful

	synchronized final boolean
	send(Message message, InternetAddress[] recipients, String list) {
		if (Site.site.isDevMode())
			return true;
		try {
			if (recipients != null && recipients.length > 0) {
				final String smtp_username = Site.settings.getString("smtp username");
				final String smtp_password = Site.settings.getString("smtp password");
				if (m_send_individually)
					for (InternetAddress recipient : recipients) {
						if (m_debug_addresses != null && Array.indexOf(m_debug_addresses, recipient.getAddress()) != -1)
							try {
								FileWriter fw = new FileWriter(Site.site.newFile("mail.debug"), true);
								fw.write(recipient.getAddress() + " " + LocalDateTime.now().toString() + " " + message.getFrom()[0].toString() + " " + message.getSubject() + "\n");
								fw.close();
							} catch (IOException e) {
							}
						message.setHeader("X-to", recipient.toString());
						Transport.send(message, new InternetAddress[] { recipient }, smtp_username, smtp_password);
					}
				else
					Transport.send(message, recipients, smtp_username, smtp_password);
			}
			if (m_log_sends)
				Site.site.log(list + " - " + message.getSubject(), "mail.log");
			return true;
		} catch (MessagingException e) {
			if (e.getMessage().indexOf("file too big") != -1) {
				Site.site.log("big mail deleted for list " + list, false);
				return false;
			}
			Site.site.log(e.toString(), false);
			return false;
		}
	}

	//--------------------------------------------------------------------------

	@ClassTask({"list","from","subject","content"})
	public final void
	send(String list, String from, String subject, String content, boolean use_template, DBConnection db) {
		if (content == null || content.length() == 0) {
			Site.site.log("empty message", true);
			return;
		}
		Mail mail = new Mail(subject, content)
			.from(from);
		MimeMessage message = mail.getMessage(use_template);
		try {
			message.setRecipients(RecipientType.TO, new Addresses().add(Site.site.getEmailAddress(list)).toArray());
			message.setHeader("X-from-site", "true");
			message.saveChanges();
			handleMessage(list, null, message, false, true, db);
		} catch (MessagingException e) {
			Site.site.log(e);
		}
	}

//	//--------------------------------------------------------------------------
//
//	private void
//	sendError(String list, String method_name, String text, Message message, Exception e) {
//		for (String l : m_errors)
//			if (list.equals(l))
//				return;
//		m_errors.add(list);
//		String content;
//		if (e != null) {
//			if (e.toString().indexOf("* BYE JavaMail Exception") != -1)
//				return;
//			content = method_name + ": " + e.toString();
//		} else
//			content = method_name;
//		if (text != null)
//			content += "<br><br>" + text;
//		new Mail(Site.site.getDisplayName() + ": mail error",  content)
//			.from(list)
//			.to("mosaic")
//			.send();
//	}

	//--------------------------------------------------------------------------

	@ClassTask()
	public static void
	setPasswords(Request r) {
		Rows rows = new Rows(new Select("name").from("mail_lists"), r.db);
		while (rows.next())
			Site.site.runScript("pw", rows.getString(1), Site.site.getDatabaseName(), Site.site.getDomain(), Text.uuid());
		rows.close();
	}

	//--------------------------------------------------------------------------

	@ClassTask({"name","create email address"})
	public static void
	setupList(String name, boolean create_email_address) {
		String file_name = name.replace('/', '_');
		Site.site.newFile("inbox", file_name).mkdirs();
		if (create_email_address)
			Site.site.runScript("new_email", name, Site.site.getDatabaseName(), Site.site.getDomain());
	}

	//--------------------------------------------------------------------------

	@ClassTask({"create email address"})
	public static void
	setupLists(boolean create_email_address, Request r) {
		Rows rows = new Rows(new Select("name").from("mail_lists"), r.db);
		while (rows.next())
			setupList(rows.getString(1), create_email_address);
		rows.close();
	}

	//--------------------------------------------------------------------------

//	@ClassTask({"list"})
//	public void
//	viewInbox(String list, Request r) {
//		HTMLWriter w = r.w;
//		Table table = w.ui.table().addDefaultClasses();
//		File[] files = Site.site.newFile("inbox", list).listFiles();
//		if (files == null)
//			w.write("No files found in inbox/").write(list);
//		else {
//			Arrays.sort(files, Comparator.comparingLong(File::lastModified));
//			for (File file : files) {
//				String from = null;
//				String subject = null;
//				try {
//					BufferedReader br = new BufferedReader(new FileReader(file));
//					String line = br.readLine();
//					while (line != null) {
//						if (line.startsWith("From: ")) {
//							from = line.substring(6);
//							if (subject != null)
//								break;
//						} else if (line.startsWith("Subject: ")) {
//							subject = line.substring(9);
//							if (from != null)
//								break;
//						}
//						line = br.readLine();
//					}
//					br.close();
//				} catch (IOException e) {
//					System.out.println(e.toString());
//				}
//				table.tr().td();
//				w.write(app.Files.modified(file).toString());
//				table.td();
//				w.write(from);
//				table.td();
//				w.write(subject);
//			}
//		}
//		table.close();
//	}

	//--------------------------------------------------------------------------

	private void
	writeArchive(int list_id, Request r) {
		Person user = r.getUser();
		if (user == null && m_is_secured)
			return;
		MailHandler mail_handler = getMailHandler(list_id, r.db);
		boolean subscribed = user != null && (mail_handler != null ? mail_handler.isSubscribed(user.getId(), r.db) : r.db.exists("mail_lists_people", "mail_lists_id=" + list_id + " AND people_id=" + user.getId()));
		if (!r.db.lookupBoolean(new Select("archives_public").from("mail_lists").whereIdEquals(list_id)) && !subscribed && !r.userIsAdministrator())
			r.w.write("Archives for this list are private to the list subscribers");
		else {
			MailList mail_list = new MailList(list_id, r.db);
			if (mail_list.getId() == 0)
				return;
			r.w.write("<h3>").write(mail_list.getName()).write(" mail list archive</h3>");
			Site.site.newView("ml_" + list_id, r).writeComponent();
		}
	}

	//--------------------------------------------------------------------------

	final void
	writeLists(Request r) {
		Select query;
		Person user = r.getUser();
		if (user != null) {
			int user_id = user.getId();
			query = new Select("mail_lists.id,mail_lists.name,mail_lists.description,mail_lists.subscribing_role,mail_lists_people.mail_lists_id,mail_lists_digest.mail_lists_id,show_subscribers,archive,archives_public")
				.from("mail_lists LEFT JOIN mail_lists_people ON (mail_lists_people.mail_lists_id=mail_lists.id AND mail_lists_people.people_id=" + user_id + ") LEFT JOIN mail_lists_digest ON (mail_lists_digest.mail_lists_id=mail_lists.id AND mail_lists_digest.people_id=" + user_id + ")")
				.where("show".equals(r.getParameter("inactive_lists")) ? null : "mail_lists.active")
				.orderBy("LOWER(mail_lists.name)");
		} else
			query = new Select("mail_lists.id,mail_lists.name,mail_lists.description,NULL,0,0,false,archive,archives_public")
			.from("mail_lists")
			.where("show".equals(r.getParameter("inactive_lists")) ? null : "mail_lists.active")
			.orderBy("LOWER(mail_lists.name)");
		HTMLWriter w = r.w;

		Table table = w.ui.table().addDefaultClasses();
		table.th(user == null ? "list" : "send email to list").th("archives").th("latest post");
		if (user != null)
			table.th("subscribed").th("nightly digest").th("subscribers");
		Rows rows = new Rows(query, r.db);
		while (rows.next()) {
			int list_id = rows.getInt(1);
			String list = rows.getString(2);

			table.tr().td();
			if (user != null) {
				String domain = Site.settings.getString("mail list domain override");
				String address = domain == null ? Site.site.getEmailAddress(list) : list + "@" + domain;
				w.addStyle("text-decoration:none")
					.a(m_show_full_address_on_email_page ? address : list, "mailto:" + address);
			} else
				w.write(list);
			String description = rows.getString(3);
			if (description != null)
				w.tag("div", description);

			boolean subscribed = user == null ? false : getMailHandler(list, r.db) != null ? getMailHandler(list, r.db).isSubscribed(user.getId(), r.db) : rows.getInt(5) != 0;
			boolean archive = rows.getBoolean(8);
			boolean archive_public = rows.getBoolean(9);
			table.td();
			if (archive) {
				if (!archive_public && !subscribed)
					w.addStyle(r.userIsAdministrator() ? "outline:dashed red thin" : "display:none");
				w.setId("view" + list_id)
					.ui.buttonIconOnClick("archive", "app.go_nav(null," + JS.string(list) + ",'" + apiURL(list_id + "/archive") + "','#main')");
			}

			table.td();
			if (!archive_public && !subscribed && !r.userIsAdministrator())
				w.addStyle("display:none");
			w.setId("date" + list_id)
				.tagOpen("span");
			w.writeDate(r.db.lookupDate(new Select("max(arrived)").from("ml_" + list_id)))
				.tagClose();

			if (user != null) {
				table.td();
				String subscribing_role = rows.getString(4);
				if (getMailHandler(list, r.db) != null || subscribing_role != null && !r.userHasRole(subscribing_role)) {
					if (subscribed)
						w.write("&#10003;");
				} else
					w.setAttribute("onchange", "toggle_subscription(this," + list_id + (archive ? ",true" : ",false") + (archive_public ? ",true" : ",false") + ");return false")
						.ui.checkbox(null, null, null, subscribed, true, false);

				table.td();
				w.setId("digest" + list_id)
					.setAttribute("onchange", "toggle_digest(this," + list_id + ");return false");
				if (!subscribed)
					w.addStyle("display:none");
				w.ui.checkbox(null, null, null, rows.getInt(6) != 0, true, false);

				table.td();
				if (rows.getBoolean(7))
					w.ui.buttonIconOnClick("people", "new Dialog({ok:true,title:'" + list + " subscribers',url:'" + apiURL(list_id + "/subscribers") + "'})");
				else
					w.nbsp();
			}
		}
		rows.close();
		table.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeMessage(int list_id, int message_id, Request r) {
		HTMLWriter w = r.w;
		Rows rows = new Rows(new Select("sender,arrived").from("ml_" + list_id).whereIdEquals(message_id), r.db);
		if (rows.next()) {
			MailList mail_list = new MailList(list_id, r.db);
			String text = mail_list.loadMessage(message_id);
			if (text == null)
				w.write("Message file not found.");
			else {
				String sender = rows.getString(1);
				try {
					MimeMessage message = new MimeMessage(Site.site.getMailSession(), new ByteArrayInputStream(text.getBytes()));
					w.write("<div id=\"ml_message\" style=\"word-break:break-word;\">From: ").write(sender).write(" - ").write(Time.formatter.getDateTime(rows.getTimestamp(2))).br();
					if (m_show_reply_links_in_archives) {
						String subject = message.getSubject();
						if (subject != null && !subject.startsWith("Re:"))
							subject = "Re: " + subject;
						w.ui.aButton("Reply to " + mail_list.getName(), "mailto:" + Site.site.getEmailAddress(mail_list.getName()) + "?subject=" + subject);
						w.space();
						String[] header = message.getHeader("Return-Path");
						if (header != null)
							w.ui.aButton("Reply to " + sender, "mailto:" + header[0] + "?subject=" + subject);
					}
					w.hr();
					Object content = message.getContent();
					if (content instanceof String)
						w.write(HTMLWriter.breakNewlines((String)content));
					else if (content instanceof SharedByteArrayInputStream) {
						InputStreamReader isr = new InputStreamReader((SharedByteArrayInputStream)content, Charset.forName("UTF-8"));
					    int charsRead = 0;
					    StringBuilder s = new StringBuilder();
					    int bufferSize = 1024;
					    char[] buffer = new char[bufferSize];
					    while ((charsRead = isr.read(buffer)) != -1)
							s.append(Arrays.copyOf(buffer, charsRead));
					    w.write(s.toString());
					} else if (content instanceof com.sun.mail.util.BASE64DecoderStream) {
						InputStreamReader isr = new InputStreamReader((com.sun.mail.util.BASE64DecoderStream)content, Charset.forName("UTF-8"));
					    int charsRead = 0;
					    StringBuilder s = new StringBuilder();
					    int bufferSize = 1024;
					    char[] buffer = new char[bufferSize];
					    while ((charsRead = isr.read(buffer)) != -1)
							s.append(Arrays.copyOf(buffer, charsRead));
					    w.write(s.toString());
					} else if (content instanceof MimeMultipart)
						WebMail.writePart(message, list_id, message_id, (MimeMultipart)content, new ArrayList<>(), r);
					else
						r.log("unknown content class " + content.getClass().getCanonicalName(), false);
				} catch (MessagingException | IOException e) {
					rows.close();
					throw new RuntimeException(e);
				}
				w.write("</div>");
			}
		} else
			w.write("message not found");
		rows.close();
	}
}
