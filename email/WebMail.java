package email;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.Module;
import app.Request;
import app.Site;
import db.DBConnection;
import jakarta.mail.BodyPart;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.Part;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import jakarta.mail.internet.MimePart;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class WebMail extends Module {
	private static String
	buildURL(int list_id, int message_id, List<Integer> path) {
		StringBuilder url = new StringBuilder(Site.context).append("/WebMail/").append(list_id).append('/').append(message_id);
		url.append("?path=");
		for (int i=0; i<path.size(); i++) {
			if (i > 0)
				url.append(',');
			url.append(path.get(i));
		}
		return url.toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(HttpServletRequest http_request, String[] path_segments, HttpServletResponse http_response) throws IOException {
		if (http_request.getRemoteUser() == null)
			return false;
		http_response.setContentType("text/html");
		int ml_id = Integer.parseInt(path_segments[1]);
		DBConnection db = new DBConnection();
		MailList mail_list = new MailList(ml_id, db);
		try {
			String text = mail_list.loadMessage(Integer.parseInt(path_segments[2]));
			if (text == null)
				http_response.getWriter().write("Message file not found.");
			else {
				InputStream is = new ByteArrayInputStream(text.getBytes());
				MimeMessage message = new MimeMessage(Site.site.getMailSession(), is);
				String cid = http_request.getParameter("cid");
				if (cid != null) {
					MimeMultipart multipart = (MimeMultipart)message.getContent();
					writeFile(multipart.getBodyPart("<" + cid + ">"), http_response);
				} else if (http_request.getParameter("path") != null) {
					String[] indexes = http_request.getParameter("path").split(",");
					int[] p = new int[indexes.length];
					for (int j=0; j<indexes.length; j++)
						p[j] = Integer.parseInt(indexes[j]);
					writeFile(message, p, 0, http_response);
				}
			}
		} catch (IOException | MessagingException e) {
			Site.site.log(e);
		}
		db.close();
		return true;
	}

	//--------------------------------------------------------------------------

	private static MimeBodyPart
	getBodyPart(MimeMultipart multipart, String cid) throws MessagingException {
		for (int i=0,n=multipart.getCount(); i<n; i++) {
			BodyPart body_part = multipart.getBodyPart(i);
			if (body_part instanceof MimeBodyPart && cid.equals(((MimeBodyPart)body_part).getContentID()))
				return (MimeBodyPart)body_part;
			try {
				Object content = body_part.getContent();
				if (content instanceof MimeMultipart) {
					MimeBodyPart mime_body_part = getBodyPart((MimeMultipart)content, cid);
					if (mime_body_part != null)
						return mime_body_part;
				}
			} catch (IOException e) {
				System.out.println(e);
			}
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public static void
	writeFile(Part body_part, HttpServletResponse http_response) {
		if (body_part == null)
			return;
		try {
			String content_type = body_part.getContentType();
			int index = content_type.indexOf(';');
			if (index != -1)
				content_type = content_type.substring(0, index);
			http_response.setContentType(content_type);
			http_response.setHeader("Content-Disposition","attachment;filename=" + body_part.getFileName().replace(',', '_'));
			InputStream is = body_part.getInputStream();
			ServletOutputStream os = http_response.getOutputStream();
			byte[] bytes = new byte[4096];
			int n = is.read(bytes);
			while (n > 0) {
				os.write(bytes, 0, n);
				n = is.read(bytes);
			}
		} catch (IOException | MessagingException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	writeFile(Part part, int[] path, int depth, HttpServletResponse http_response) {
		try {
			Multipart multipart = (Multipart)part.getContent();
			BodyPart body_part = multipart.getBodyPart(path[depth]);
			if (depth < path.length - 1)
				writeFile(body_part, path, depth + 1, http_response);
			else
				writeFile(body_part, http_response);
		} catch (IOException | MessagingException e) {
			throw new RuntimeException("WebMail.writeFile", e);
		}
	}

	//--------------------------------------------------------------------------

	private static void
	writeHTML(Part part, int list_id, int message_id, MimeMultipart message, Request r) {
		try {
			StringBuilder s = new StringBuilder(part.getContent().toString());
			Pattern pattern = Pattern.compile(".*<\\s*body[^>]*>(.*)<\\s*/\\s*body\\s*>.*", Pattern.DOTALL);
			Matcher matcher = pattern.matcher(s.toString());
			if (matcher.find()) {
				s.setLength(0);
				s.append(matcher.group(1));
			}
			int index = s.indexOf("\"cid:");
			while (index != -1) {
				String cid = s.substring(index + 5, s.indexOf("\"", index + 5));
				MimeBodyPart body_part = getBodyPart(message, "<" + cid + ">");
				if (body_part != null) {
					String[] header = body_part.getHeader("Content-Transfer-Encoding");
					if ("base64".equals(header[0])) {
						InputStream raw_input_stream = body_part.getRawInputStream();
						StringBuilder sb = new StringBuilder();
						BufferedReader br = new BufferedReader(new InputStreamReader(raw_input_stream));
						String line;
						while((line = br.readLine()) != null)
							sb.append(line);
						br.close();
						String content_type = body_part.getContentType();
						int semicolon = content_type.indexOf(';');
						if (semicolon != -1)
							content_type = content_type.substring(0, semicolon);
						s.replace(index + 1, s.indexOf("\"", index + 5) + 1, "data:" + content_type + ";base64," + sb.toString() + "\" style=\"width:100%;\"");
					} else
						s.replace(index + 1, index + 5, Site.context + "/WebMail/" + list_id + '/' + message_id + "?cid=");
				} /*else
					request.log("cid " + cid + " not found in message " + message_id, false); */
				index = s.indexOf("\"cid:", index + 5);
			}
			r.w.write(s.toString());
		} catch (IOException | MessagingException e) {
			throw new RuntimeException("WebMail.writeHTML", e);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	writePart(Part part, int list_id, int message_id, MimeMultipart message, ArrayList<Integer> path, Request r) {
		try {
			String disposition = part.getDisposition();
			String type = part.getContentType().toLowerCase();

			if (type.startsWith("multipart")) {
				Multipart multipart = (Multipart)part.getContent();
				if (type.startsWith("multipart/alternative")) {
					for (int i=0,n=multipart.getCount(); i<n; i++) {
						BodyPart body_part = multipart.getBodyPart(i);
						type = body_part.getContentType().toLowerCase();
						if (type.startsWith("text/html")) {
							writeHTML(body_part, list_id, message_id, message, r);
							return;
						}
						if (type.startsWith("multipart/related")) {
							path.add(i);
							writePart(body_part, list_id, message_id, message, path, r);
							path.remove(path.size() - 1);
							return;
						}
					}
					for (int i=0,n=multipart.getCount(); i<n; i++) {
						BodyPart body_part = multipart.getBodyPart(i);
						type = body_part.getContentType().toLowerCase();
						if (type.startsWith("text/plain"))
							r.w.write("<pre>").write(body_part.getContent()).write("</pre>");
					}
					return;
				}
				for (int i=0,n=multipart.getCount(); i<n; i++) {
					path.add(i);
					writePart(multipart.getBodyPart(i), list_id, message_id, message, path, r);
					path.remove(path.size() - 1);
				}
			} else if (part instanceof InputStream)
				r.w.write("======InputStream: " + part.getClass().getSimpleName() + "======<br />");
			else if (disposition != null && disposition.toLowerCase().startsWith("attachment")) {
				if (type.startsWith("image"))
					r.w.addStyle("width:100%").img(buildURL(list_id, message_id, path));
				else
					r.w.br().write("attachment: ").setAttribute("download", part.getFileName()).a(part.getFileName(), buildURL(list_id, message_id, path));
			} else if (type.startsWith("image")) {
				MimePart mp = (MimePart)part;
				if (mp.getContentID() == null)
					r.w.addStyle("width:100%").img(buildURL(list_id, message_id, path));
			} else if (type.startsWith("text/plain"))
				r.w.write("<pre>").write(part.getContent().toString()).write("</pre>");
			else if (type.startsWith("text/html"))
				writeHTML(part, list_id, message_id, message, r);
			else
				r.w.br().write("attachment: ").setAttribute("download", part.getFileName()).a(part.getFileName(), buildURL(list_id, message_id, path));
		} catch (IOException | MessagingException e) {
			throw new RuntimeException("WebMail.writePart", e);
		}
	}
}
