package email;

import java.util.List;

import db.DBConnection;
import ui.Option;

public interface MailHandlerFactory {
	public MailHandler
	getHandler(String send_to, DBConnection db);

	public void
	addSendToOptions(List<Option> options, DBConnection db);
}
