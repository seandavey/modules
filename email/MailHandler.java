package email;

import app.Request;
import db.DBConnection;
import db.Rows;
import db.Select;
import mail.Addresses;
import util.Text;

public class MailHandler implements Comparable<MailHandler> {
	protected final String	m_display_name;
	private String			m_people_filter;
	private String			m_role;

	//--------------------------------------------------------------------------

	public
	MailHandler(String display_name) {
		m_display_name = display_name;
	}

	//--------------------------------------------------------------------------

	private void
	adjustQuery(Select query) {
		query.from("people").where("active AND email IS NOT NULL");
		if (m_people_filter != null)
			query.andWhere(m_people_filter);
		if (m_role != null)
			query.joinOn("user_roles", "people.user_name=user_roles.user_name").andWhere("role='" + m_role + "'");
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	compareTo(MailHandler mail_handler) {
		return getDisplayName().compareTo(mail_handler.getDisplayName());
	}

	//--------------------------------------------------------------------------

	protected String
	getDisplayName() {
		return m_display_name;
	}

	//--------------------------------------------------------------------------

	protected Addresses
	getAddresses(int list_id, DBConnection db) {
		Select query = new Select("email");
		adjustQuery(query);
		query.andWhere("NOT EXISTS(SELECT 1 FROM mail_lists_digest WHERE mail_lists_digest.people_id=people.id AND mail_lists_digest.mail_lists_id=" + list_id + ")");
		return new Addresses().add(db.readValues(query));
	}

	//--------------------------------------------------------------------------

//	protected final boolean
//	handleMessage(Message message, DBConnection db) {
//		return false;
//	}

	//--------------------------------------------------------------------------

	public boolean
	isSubscribed(int person_id, DBConnection db) {
		if (person_id == 0)
			return false;
		if (m_people_filter != null)
			return db.exists("people", "id=" + person_id + " AND (" + m_people_filter + ")");
		if (m_role != null)
			return db.exists("user_roles", "people_id=" + person_id + " AND role='" + m_role + "'");
		return true;
	}

	//--------------------------------------------------------------------------

	final MailHandler
	setPeopleFilter(String people_filter) {
		m_people_filter = people_filter;
		return this;
	}

	//--------------------------------------------------------------------------

	final MailHandler
	setRole(String role) {
		m_role = role;
		return this;
	}

	//--------------------------------------------------------------------------

	protected boolean
	writeSubscribers(Request r) {
		boolean first = true;
		Select query = new Select("first,last").orderBy("first,last");
		adjustQuery(query);
		Rows rows = new Rows(query, r.db);
		while (rows.next()) {
			if (first)
				first = false;
			else
				r.w.br();
			r.w.write(Text.join(" ", rows.getString(1), rows.getString(2)));
		}
		rows.close();
		return !first;
	}
}
