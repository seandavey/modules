package email;

import app.Request;
import app.Roles;
import db.Form;
import db.NameValuePairs;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.column.ColumnBase;

public class AcceptFromColumn extends ColumnBase<AcceptFromColumn> {
	public
	AcceptFromColumn() {
		super("accept_from");
		setDisplayName("accept posts from");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		String selected_option = null;
		String selected_role = null;
		if (v.getMode() == Mode.EDIT_FORM) {
			selected_option = v.data.getString("accept_from");
			int index = selected_option.indexOf(':');
			if (index != -1) {
				selected_role = selected_option.substring(index + 1);
				selected_option = selected_option.substring(0, index + 1);
			}
		}
		String[] options = new String[] {"Members of this site", "Anyone in the world (no spam protection)", "People with role:", "Subscribers to this list"};
		r.w.setAttribute("onchange", "this.nextElementSibling.style.visibility=this.selectedIndex==2?'visible':'hidden'")
			.ui.select("accept_from", options, selected_option, null, true)
			.nbsp();
		if (selected_role == null)
			r.w.addStyle("visibility:hidden");
		r.w.ui.select("db_accept_from", Roles.getRoleNames(), selected_role, null, true);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		if ("People with role:".equals(value))
			nvp.set("accept_from", value + r.getParameter("db_accept_from"));
		else
			nvp.set("accept_from", value);
	}
}
