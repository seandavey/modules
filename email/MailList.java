package email;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import app.Modules;
import app.Request;
import app.Site;
import db.DBConnection;
import db.NameValuePairs;
import db.Rows;
import db.SQL;
import db.Select;
import db.object.DBField;
import db.object.DBObject;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import mail.Addresses;
import mail.Mail;
import util.Text;
import util.Time;

class MailList {
	@DBField
	private String m_accept_from;
	@DBField
	private boolean	m_active;
	@DBField
	private boolean	m_allow_from_outside_subscribers;
	@DBField
	private boolean	m_announce_only;
	@DBField
	private boolean	m_archive;
	@DBField
	private boolean	m_archives_public;
	private File	m_dir;
	@DBField
	private String	m_footer;
	@DBField
	private int		m_id;
	@DBField
	private String	m_name;
	@DBField
	private String	m_send_to;
	@DBField
	private String	m_subscribing_role;

	//--------------------------------------------------------------------------

	public
	MailList(int id, DBConnection db) {
		DBObject.load(this, "mail_lists", id, db);
		m_dir = getDir();
	}

	//--------------------------------------------------------------------------

	MailList(String name, DBConnection db) {
		DBObject.load(this, "mail_lists", "name ILIKE " + SQL.string(name), db);
		m_dir = getDir();
	}

	// --------------------------------------------------------------------------

	final boolean
	acceptPost(InternetAddress address, MailHandler mail_handler, DBConnection db) {
		if (m_accept_from.startsWith("Subscribers")) {
			if (mail_handler != null && !mail_handler.isSubscribed(m_id, db))
				return false;
			int id = MailLists.lookupId(address, db);
			if (id == 0)
				return false;
			return db.exists("mail_lists_people", "mail_lists_id=" + m_id + " AND people_id=" + id);
		}
		// accept from people with role
		int index = m_accept_from.indexOf(':');
		if (index != -1) {
			String email = address.getAddress();
			String user_name = db.lookupString("user_name", "people", "active AND email ILIKE '" + SQL.escape(email) + "%'");
			if (user_name == null)
				user_name = db.lookupString("user_name", "people", "active AND id=" + db.lookupString("people_id", "additional_emails", "email ILIKE '" + SQL.escape(email) + "%'"));
			if (user_name == null)
				return false;
			return db.exists(new Select("1").from("user_roles").whereEquals("user_name", user_name).andWhere("role='" + m_accept_from.substring(index + 1) + "'"));
		}
		return true;
	}

	// --------------------------------------------------------------------------

	final boolean
	addFooter(MimeMessage message) {
		if (m_footer != null) {
			Mail.addFooter(message, m_footer);
			try {
				message.saveChanges();
			} catch (MessagingException e) {
				Site.site.log(e);
			}
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	final boolean
	announceOnly() {
		return m_announce_only;
	}

	// --------------------------------------------------------------------------

	final int
	archive(String name, String subject, String text, DBConnection db) {
		if (!m_archive)
			return 0;
		NameValuePairs nvp = new NameValuePairs()
			.set("sender", name)
			.set("subject", subject == null ? "(no subject)" : subject)
			.setTimestamp("arrived");
		int id = db.insert("ml_" + m_id, nvp).id;
		storeMessage(text, id);
		return id;
	}

	// --------------------------------------------------------------------------

	final void
	deleteFromArchive(int id, DBConnection db) {
		db.delete("ml_" + m_id, id, false);
	}

	// --------------------------------------------------------------------------

	private File
	getDir() {
		File dir = Site.site.newFile("mail", Integer.toString(m_id));
		if (!dir.exists()) {
			dir.mkdirs();
			dir.setWritable(true, false);
		}
		return dir;
	}

	//--------------------------------------------------------------------------

	final int
	getId() {
		return m_id;
	}

	//--------------------------------------------------------------------------

	final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	final Addresses
	getAddresses(boolean include_digest_subscribers, DBConnection db) {
		String where = "email IS NOT NULL AND people.active AND mail_lists_people.mail_lists_id=" + m_id;
		if (!include_digest_subscribers)
			where += " AND NOT EXISTS(SELECT 1 FROM mail_lists_digest WHERE mail_lists_digest.people_id=people.id AND mail_lists_digest.mail_lists_id=" + m_id + ")";
		return new Addresses().add(db.readValues(new Select("email").from("people").join("people", "mail_lists_people").where(where)));
	}

	//--------------------------------------------------------------------------

	final boolean
	isActive() {
		return m_active;
	}

	//--------------------------------------------------------------------------

	final String
	loadMessage(int message_id) {
		BufferedReader br = null;
		try {
			File file = new File(m_dir + File.separator + message_id);
			if (file.exists())
				br = new BufferedReader(new FileReader(file));
			else {
				file = new File(m_dir + File.separator + message_id + ".gz");
				if (file.exists())
					br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		if (br == null)
			return null;
		StringBuilder sb = new StringBuilder();
		try {
			String l = br.readLine();
			while (l != null) {
				if (sb.length() > 0)
					sb.append('\n');
				sb.append(l.replaceAll("\\\\r\\\\n", "\n").replaceAll("\\\\n", "\n").replaceAll("\\\\t", "\t"));
				l = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	final String
	lookupName(InternetAddress address, DBConnection db) {
		String email = address.getAddress();
		String name = null;
		if (m_allow_from_outside_subscribers)
			name = db.lookupString("email", "subscribers", "email ILIKE '" + SQL.escape(email) + "%'");
		if (name == null && m_accept_from.startsWith("Anyone"))
			name = email;
		return name;
	}

	//--------------------------------------------------------------------------

	public final void
	sendDigest(LocalDate date, boolean testing, MailLists mail_lists, DBConnection db) {
		List<Object[]> messages = db.readRowsObjects(new Select("id,sender,subject,arrived").from("ml_" + m_id).where("arrived::date='" + date.toString() + "'").orderBy("arrived"));
		if (messages.size() == 0) {
			if (testing)
				Site.site.log("no messages for " + m_name, false);
			return;
		}
		List<String> emails = db.readValues(new Select("email").from("people JOIN mail_lists_digest ON (mail_lists_digest.people_id=people.id)").where("mail_lists_id=" + m_id + " AND people.active AND email IS NOT NULL"));
		if (emails.size() == 0) {
			if (testing)
				Site.site.log("no emails for " + m_name, false);
			return;
		}
//		if (testing) {
//			Site.site.log(m_name + " digest", false);
//			for (Object[] m : messages) {
//				for (Object o : m)
//					if (o != null)
//						System.out.print(o.toString() + "|");
//				System.out.println();
//			}
//			System.out.println(emails.toString());
//			return;
//		}

		Session session = Site.site.getMailSession();
		MimeMultipart body = new MimeMultipart();
		StringBuilder html = new StringBuilder();
		try {
			for (int i=0; i<messages.size(); i++) {
				Object[] message = messages.get(i);
				String text = loadMessage((Integer)message[0]);
				if (text == null)
					continue;
				InputStream is = new ByteArrayInputStream(text.getBytes());
				MimeMessage m = new MimeMessage(session, is);
				String subject = (String)message[2];
				String reply_subject;
				if (subject == null) {
					subject = "(no subject)";
					reply_subject = "Re:";
				} else if (!subject.startsWith("Re:"))
					reply_subject = "Re: " + subject;
				else
					reply_subject = subject;
				html.append("<div style=\"line-height:19px;width:100%;background-color:white;padding:5px;border-bottom:solid 1px #ddd;\"><a href=\"#m")
					.append(i)
					.append("\" style=\"text-decoration:none\"><span style=\"width:30px;text-align:right;font-family:arial,helvetica,clean,sans-serif;color:rgb(34,34,34);font-size:12px;font-style:normal;font-weight:bold;\">")
					.append(i + 1)
					.append("</span><span style=\"padding-left:10px;color:#16a085;\">")
					.append(message[2])
					.append("</span> posted by ")
					.append(message[1])
					.append("</a></div>");
				MimeBodyPart body_part = new MimeBodyPart();
				Timestamp arrived = (Timestamp)message[3];
				String from = m.getFrom()[0].toString();
				InternetAddress ia = new InternetAddress(from);
				from = ia.getAddress();
				StringBuilder message_head = new StringBuilder();
				message_head.append("<div style=\"background-color:#eee;border:solid 1px #ccc;display:flex;flex-wrap:wrap;margin-bottom:20px;margin-top:20px;padding:5px;width:100%;\"><div><a name=\"m")
					.append(i)
					.append("\"><span style=\"color:rgb(34,34,34);font-weight:bold;font-family:arial,helvetica,clean,sans-serif;text-align:right;font-size:18px;font-style:normal;width:30px;\">")
					.append(i + 1)
					.append("</span></a><span style=\"padding-left:10px;font-size:18px;\">")
					.append(message[2])
					.append("</span><br>")
					.append(Time.formatter.getDateTime(arrived.toLocalDateTime()))
					.append(" posted by ")
					.append(message[1])
					.append("</div><div style=\"flex-grow:1;padding-right:10px;text-align:right;\"><a href=\"mailto:")
					.append(Site.site.getEmailAddress(m_name))
					.append("?subject=")
					.append(reply_subject)
					.append("\" style=\"float:right;margin-left:20px;text-decoration:none;\">reply to ")
					.append(m_name)
					.append("</a><a href=\"mailto:")
					.append(from)
					.append("?subject=")
					.append(reply_subject)
					.append("\" style=\"float:right;margin-left:20px;text-decoration:none;\">reply to ")
					.append(message[1])
					.append("</a></div></div>");
				body_part.setContent(message_head.toString(), "text/html");
				body.addBodyPart(body_part);
				body_part = new MimeBodyPart();
				body_part.setContent(m.getContent(), m.getContentType());
				body.addBodyPart(body_part);
				if (html.length() + text.length() + message_head.length() > 4000000) {
					sendDigests(emails, body, html, date);
					body = new MimeMultipart();
					html.setLength(0);
				}
			}
			if (body.getCount() > 0)
				sendDigests(emails, body, html, date);
		} catch (MessagingException e) {
			Site.site.log(e);
		} catch (IOException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	private void
	sendDigests(List<String> emails, MimeMultipart body, StringBuilder html, LocalDate date) {
		try {
			MimeBodyPart header = new MimeBodyPart();
			header.setContent(html.toString() + body.getCount() / 2 + " " + Text.pluralize("message", body.getCount() / 2), "text/html");
			body.addBodyPart(header, 0);
			MimeMessage digest = new MimeMessage(Site.site.getMailSession());
			digest.setContent(body);
			digest.setSubject("[" + m_name + "] " + Time.formatter.getDateMedium(date) + " Digest");
			digest.setFrom(Site.site.getEmailAddress(m_name));
			MailLists mail_lists = (MailLists)Modules.get("MailLists");
			mail_lists.send(digest, new Addresses().add(emails).toArray(), m_name);
		} catch (MessagingException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	private void
	storeMessage(String message, int message_id) {
		try {
			FileOutputStream fos = new FileOutputStream(m_dir + File.separator + message_id + ".gz");
			Writer w = new OutputStreamWriter(new GZIPOutputStream(fos));
			w.write(message);
			w.close();
			fos.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	final void
	writeAdditionalSubscribers(boolean has_subscribers, Request r) {
		boolean first = true;
		Rows rows = new Rows(new Select("email").from("subscribers").whereEquals("mail_lists_id", m_id), r.db);
		while (rows.next()) {
			if (first) {
				first = false;
				if (has_subscribers)
					r.w.br();
			} else
				r.w.br();
			r.w.write(rows.getString(1));
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	final boolean
	writeSubscribers(Request r) {
		boolean first = true;
		Rows rows = new Rows(new Select("first,last").from("people").joinOn("mail_lists_people", "people.id=mail_lists_people.people_id").whereEquals("mail_lists_id", m_id).orderBy("first,last"), r.db);
		while (rows.next()) {
			if (first)
				first = false;
			else
				r.w.br();
			r.w.write(Text.join(" ", rows.getString(1), rows.getString(2)));
		}
		rows.close();
		return !first;
	}
}
