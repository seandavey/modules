package email;

import java.util.List;

import db.DBConnection;
import ui.Option;

public class PeopleMailHandlerFactory implements MailHandlerFactory {
	private String	m_filter;
	final String	m_name;
	private String	m_role;

	//--------------------------------------------------------------------------

	public
	PeopleMailHandlerFactory(String name) {
		m_name = name;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addSendToOptions(List<Option> options, DBConnection db) {
		options.add(new Option(m_name, m_name));
	}

	//--------------------------------------------------------------------------

	@Override
	public MailHandler
	getHandler(String send_to, DBConnection db) {
		if (m_name.equals(send_to))
			return new MailHandler(m_name).setPeopleFilter(m_filter).setRole(m_role);
		return null;
	}

	//--------------------------------------------------------------------------

	public final PeopleMailHandlerFactory
	setFilter(String filter) {
		m_filter = filter;
		return this;
	}

	//--------------------------------------------------------------------------

	public final PeopleMailHandlerFactory
	setRole(String role) {
		m_role = role;
		return this;
	}
}
