package pictures;

import java.util.List;
import java.util.Map;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonValue;
import com.eclipsesource.json.ParseException;

import app.Request;
import app.Site;
import db.DBConnection;
import db.Form;
import db.InsertHook;
import db.NameValuePairs;
import db.SQL;
import db.Select;
import db.UpdateHook;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.column.ColumnBase;
import ui.Tags;

class TagsColumn extends ColumnBase<TagsColumn> implements InsertHook, UpdateHook {
	private final String m_table;

	//--------------------------------------------------------------------------

	public
	TagsColumn(String name, String table, ViewDef view_def) {
		super(name);
		m_table = table;
		view_def.addInsertHook(this);
		view_def.addUpdateHook(this);
	}

	//--------------------------------------------------------------------------
	// delete previous tags and insert new ones

	private void
	adjustTags(String tags, int id, DBConnection db) {
		String tags_table = m_table + "_tags";
		String many_many_table = m_table + "_" + tags_table;
		NameValuePairs nvp = new NameValuePairs();

		db.delete(many_many_table, m_table + "_id", id, false);
		if (tags != null && tags.length() > 0) {
			nvp.set(m_table + "_id", id);
			try {
				for (JsonValue object : Json.parse(tags).asArray()) {
					String tag = object.asObject().getString("value", null);
					int tag_id = db.lookupInt("id", tags_table, "tag=" + SQL.string(tag), -1);
					if (tag_id == -1)
						tag_id = db.insert(tags_table, "tag", SQL.string(tag)).id;
					nvp.set(tags_table + "_id", tag_id);
					db.insert(many_many_table, nvp);
				}
			} catch (ParseException e) {
				Site.site.log(e);
				System.out.println(tags);
			}
		}

		deleteUnusedTags(db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterDelete(ViewDef view_def, String where, Request r) {
		deleteUnusedTags(r.db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		String value = (String)r.getSessionAttribute(m_table + "_tags");
		adjustTags(value, id, r.db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		String value = (String)r.getSessionAttribute(m_table + "_tags");
		adjustTags(value, id, r.db);
	}

	//--------------------------------------------------------------------------

	private void
	deleteUnusedTags(DBConnection db) {
		String tags_table = m_table + "_tags";
		db.delete(tags_table, "NOT EXISTS (SELECT 1 FROM " + m_table + "_" + tags_table + " WHERE " + tags_table + "_id=" + tags_table + ".id)", false);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		r.setSessionAttribute(m_table + "_tags", value);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		String tags_table = m_table + "_tags";
		String many_many_table = m_table + "_" + tags_table;

		List<String> tags = r.db.readValues(new Select("tag").from(tags_table).orderBy("lower(tag)"));
		String[] choices = null;
		if (!tags.isEmpty())
			choices = tags.toArray(new String[tags.size()]);
		Mode mode = v.getMode();
		if (mode == View.Mode.ADD_FORM) {
			new Tags(m_name, null, choices).write(r.w);
			return;
		}
		tags = r.db.readValues(
				new Select("tag")
					.from(tags_table + " JOIN " + many_many_table + " ON " + many_many_table + "." + tags_table + "_id=" + tags_table + ".id")
					.where(many_many_table + "." + m_table + "_id=" + v.data.getString("id"))
					.orderBy("lower(tag)"));
		String[] items = null;
		if (!tags.isEmpty())
			items = tags.toArray(new String[tags.size()]);
		new Tags(m_name, items, choices).write(r.w);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		String tags_table = m_table + "_tags";
		String many_many_table = m_table + "_" + tags_table;
		String tags = r.db.lookupString(
				new Select("string_agg(tag,', ')")
					.from("(" + new Select("tag")
						.from(tags_table + " JOIN " + many_many_table + " ON " + many_many_table + "." + tags_table + "_id=" + tags_table + ".id")
						.where(many_many_table + "." + m_table + "_id=" + v.data.getString("id"))
						.groupBy("tag")
						.orderBy("lower(tag)").toString() + ") t"));
		if (tags != null)
			r.w.write(tags);
		return true;
	}
}
