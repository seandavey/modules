package pictures;

import java.time.LocalDateTime;
import java.util.ArrayList;

import app.Request;
import app.Site;
import db.DBConnection;
import db.Rows;
import db.Select;
import social.Comments;
import social.NewsItem;
import social.NewsProvider;
import util.Text;
import web.HTMLWriter;

class PicturesNewsItem extends NewsItem {
	private final ArrayList<Integer> m_item_ids	= new ArrayList<>();

	// --------------------------------------------------------------------------

	public
	PicturesNewsItem(NewsProvider provider, Rows rows) {
		super(provider, rows);
		m_item_ids.add(rows.getInt("item_id"));
	}

	// --------------------------------------------------------------------------

	public
	PicturesNewsItem(Pictures pictures, int item_id, int owner_id, LocalDateTime timestamp) {
		super(pictures, item_id, owner_id, timestamp);
		m_item_ids.add(item_id);
	}

	// --------------------------------------------------------------------------

	@Override
	protected String
	getAction(DBConnection db) {
		int num_pictures = m_item_ids.size();
		return "added " + num_pictures + ' ' + Text.pluralize("picture", num_pictures);
	}

	// --------------------------------------------------------------------------

	private void
	masonry(int num_columns, HTMLWriter w, DBConnection db) {
		StringBuilder images = new StringBuilder();
		w.write("<div class=\"news_item_pictures\" onclick=\"if(event.target!=this){Gallery.g.set_pics([");
		StringBuilder where = new StringBuilder("id IN (");
		for (int i = 0; i < m_item_ids.size(); i++) {
			if (i > 0)
				where.append(',');
			where.append(m_item_ids.get(i));
		}
		where.append(')');
		Rows rows = new Rows(new Select("id,file,width,height,caption,_owner_").from(m_provider.getTable()).where(where.toString()), db);
		boolean first = true;
		while (rows.next()) {
			String file = rows.getString(2);
			if (file == null || file.length() == 0)
				continue;
			int id = rows.getInt(1);
			if (first)
				first = false;
			else
				w.comma();
			w.write("{id:").write(id)
				.write(",file:").jsString(file)
				.write(",title:").jsString(((Pictures)m_provider).buildCaption(id, rows.getString(5), Site.site.lookupName(rows.getInt(6), db), false, rows.getInt(6), null).replace("\"", "&quot;"))
				.write(",can_edit:false}");
			images.append("<img style=\"cursor:pointer;\" src=\"")
				.append(Site.context)
				.append("/")
				.append(m_provider.getTable())
				.append("/thumbs/")
				.append(file)
				.append("\" />");
		}
		rows.close();
		w.write("],'pictures');Gallery.g.open(Array.prototype.indexOf.call(this.children,event.target));}\">")
			.write(images.toString())
			.write("</div>");
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	merge(NewsProvider provider, Rows rows) {
		if (m_provider == provider && m_owner_id == rows.getInt("_owner_")) {
			m_item_ids.add(rows.getInt("item_id"));
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	write(LocalDateTime now, boolean for_mail, HTMLWriter w, DBConnection db, Request r) {
		w.write("<div id=\"ni").write(m_news_id).write("\" class=\"news_item\"><div>");
		writeHeader(now, for_mail, w, db, r);
		int num_pictures = m_item_ids.size();
		if (num_pictures > 1)
			masonry(2, w, db);
		else {
			int item_id = m_item_ids.get(0);
			String[] image = db.readRow(new Select("file,caption,_owner_").from("pictures").whereIdEquals(item_id));
			if (image != null) {
				int owner_id = 0;
				String owner = null;
				if (image[2] != null) {
					owner_id = Integer.parseInt(image[2]);
					owner = Site.site.lookupName(owner_id, db);
				}
				w.write("\n<img src=\"").write(Site.site.absoluteURL("pictures", "thumbs", image[0])).write("\" onclick=\"Gallery.g.set_pics([{file:").jsString(image[0]).write(",id:").write(item_id).write(",title:")
						.jsString(((Pictures)m_provider).buildCaption(item_id, image[1], owner, false, owner_id, null).replace("\"", "&quot;"))
						.write("}],'pictures');Gallery.g.open(0);\" style=\"cursor:pointer;\"/>");
			}
		}
		w.write("</div>");
		if (for_mail)
			w.a(Site.site.absoluteURL("Pictures").set("view", m_item_ids.get(0)).toString());
		else
			Comments.writeComments(m_provider.getTable(), m_item_id, r);
		w.write("</div>");
	}
}
