class Gallery {
	constructor(options) {
		if (Gallery.g)
			Gallery.g._destroy()
		Gallery.g = this
		this.options = options || {}
		this.closed = true

		this.overlay = $div({classes:['gallery_overlay'], events:{click:this._close.bind(this)}, parent:document.body})
		this.modal = $div({classes:['gallery_modal'], parent:document.body})
		if (this.options.show_comments)
			this.comments = $div({id:'c_gallery_comments', classes:['comments','gallery_comments'], parent:document.body, styles:{display:'none', opacity:0, overflow:'auto'}})
		this.image = $div({id:'gallery_image', events:{click:this._close.bind(this)}, parent:this.modal})
		let bottom = $div({id:'gallery_bottom', parent:this.modal})
		this.navigation = $div({id:'gallery_navigation', parent:bottom})
		this.description = $div({id:'gallery_description', parent:bottom})
		$div({classes:'clear', parent:bottom})
		this.close_link = $a({id:'gallery_close_link', events:{click:this._close.bind(this)}, parent:this.navigation})
		this.next_link = $a({id:'gallery_next_link', events:{click:this._next.bind(this)}, parent:this.navigation, styles:{display:'none'}})
		this.prev_link = $a({id:'gallery_prev_link', events:{click:this._prev.bind(this)}, parent:this.navigation, styles:{display:'none'}})
		$div({classes:'clear', parent:this.navigation})
		this.count = $span({id:'gallery_count', parent:this.navigation, styles:{display:'none'}})

		if (this.options.el)
			this._fetch()
		if (this.options.infinite_scroll)
			document.addEventListener('scroll', function() {
				if (!this._fetching)
					if (_.dom.at_bottom())
						this._fetch_next()
			}.bind(this))
		app.subscribe('pictures', this)
	}

	_add(i) {
		let p = this.options.pics[i]
		let f = $figure({classes:['figure'], id:'pic'+p.id, parent:this.options.el})
		$img({classes:['figure-img','img-fluid','rounded'], events:{click:function(){this.open(i)}.bind(this)}, loading:'lazy', parent:f, src:this._path('thumbs/'+p.file)})
		let fc = $figcaption({classes:['figure-caption'], parent:f})
		let caption = null
		if (p.title.indexOf('posted') != 0) { 
			caption = p.title.substring(0, p.title.indexOf('<br'))
			fc.append(caption)
		}
		if (p.can_edit)
			$img({events:{click:function(){this._edit(this, i)}.bind(this)}, parent:fc, src:context+'/images/pencil.png', styles:{margin:'0 10px'}})
	}

	_close() {
		_.dom.set_styles(this.image,{'opacity':0, 'width':'', 'height':''})
 		_.dom.empty(this.image)
 		this.modal.style.display='none'
		if (this.comments)
 			this.comments.style.display='none'
 		this.overlay.style.display='none'
		app.z_index--
		this.closed = true
	}

	delete_record(id) {
		Dialog.confirm(null, 'Delete this picture?', _.text.delete, function() {
			net.post(context + '/Views/pictures/delete', "id="+id, function() {
				Dialog.top().close()
				_.$('#pic'+id).remove()
				this.options.pics.splice(this.index(id), 1)
			}.bind(this))
		})
	}
	
	_destroy() {
		app.unsubscribe('pictures', this)
	}

	_edit(e, i) {
		new Dialog({cancel:true, ok:{text:'Save'}, title:'Edit Picture', url:context + '/Views/pictures/component?db_mode=EDIT_FORM&db_key_value=' + this.options.pics[i].id, owner:_.c(e)})
	}

	_fetch() {
		let n = Math.min(30, this.options.pics.length)
		for (let i=0; i<n; i++)
			this._add(i)
	}

	_fetch_next() {
		this._fetching = true
		if (this.options.el.children.length < this.options.pics.length) {
			let n = Math.min(30, this.options.pics.length - this.options.el.children.length)
			for (let i=0; i<n; i++)
				this._add(this.options.el.children.length)
		}
		this._fetching = false
	}
	
	_file(i) {
		if (this.options.pics)
			return this.options.pics[i].file
		let img = this.images[i]
		if (img.tagName === 'FIGURE')
			img = img.firstChild
		return img.src.substring(img.src.lastIndexOf('/') + 1)
	}

	filter(f) {
		net.post(context+'/ViewStates/pictures','filter=' + f, function(){net.replace(this.options.el.closest('.component'), context + '/Views/pictures?db_mode=LIST');this.close();}.bind(this));
	}

	_go(i) {
		if (this.comments)
			this.comments.style.display = 'none'
		_.dom.empty(this.image)
		this.image.style.opacity = 0
		_.dom.empty(this.description)
		_.dom.empty(this.count)
		this.modal.classList.add('gallery_loading')
		this._load_image(this._file(i))
	}

	index(id) {
		if (this.options.pics) {
			let p = this.options.pics
			for(let i=0; i<p.length; i++)
				if (p[i].id == id)
					return i
		}
		return -1
	}

	_load_image(file) {
		let g = this
		$img({events: {load:function(){g._show_image(this)}}, src: this._path(file)})
	}

	_next() {
		this._go(this.image_index != this._num_images()-1 ? this.image_index += 1 : this.image_index = 0)
	}

	_num_images() {
		if (this.options.pics)
			return this.options.pics.length
		return this.images.length
	}

	open(i) {
		if (typeof i === 'number')
			this.image_index = i
		else {
			let c = i
			let p = i.parentNode
			if (p.tagName == 'FIGURE') {
				c = p
				p = p.parentNode
			}
			this.images = p.children
			this.image_index = Array.prototype.indexOf.call(p.children, c)
		}
		let z_index = app.z_index++
		this.overlay.style.zIndex = z_index
		this.overlay.style.display = 'block'
		this.modal.style.zIndex = z_index
		this.modal.classList.add('gallery_loading')
		_.dom.set_styles(this.modal, {display:'block', opacity:1, top:window.scrollY+80})
		let border = parseInt(window.getComputedStyle(this.modal).borderRightWidth)
		let w
		if (this._num_images() === 1) {
			this.prev_link.style.display = 'none'
			this.next_link.style.display = 'none'
			this.count.style.display = 'none'
			w = _.dom.size(this.close_link).x + border
		} else {
			this.prev_link.style.display = 'block'
			this.next_link.style.display = 'block'
			this.count.style.display = 'block'
			w = _.dom.size(this.prev_link).x + _.dom.size(this.next_link).x + _.dom.size(this.close_link).x + border
		}
		this.navigation.style.width = w + 'px'
		this.description.style.marginRight = w + 'px'
		this.closed = false
		this._load_image(typeof i === 'number' ? this.options.pics[i].file : i.src.substring(i.src.lastIndexOf('/') + 1))
	}

	_path(file) {
		let p = context + '/'
		if (this.options.dir)
			p += this.options.dir + '/'
		return p + file
	}

	_prev() {
		this._go(this.image_index != 0 ? this.image_index -= 1 : this.image_index = this._num_images()-1)
	}
	
	publish(event, data) {
		if (event === 'delete')
			return;
		if (event === 'img src change') {
			for (let i=0; i<this.options.pics.length; i++) {
				let src = data.src.substring(data.src.lastIndexOf('/') + 1)
				if (this.options.pics[i].file === src) {
					this.options.pics[i].file = data.new_src.substring(data.new_src.lastIndexOf('/') + 1)
					break
				}
			}
			return
		}
		net.post(context+'/ViewStates/pictures','filter=',function(){let p=this.options.el.closest('.component');if(p)net.replace(p)}.bind(this))
	}

	set_pics(pics) {
		this.options.pics = pics
		this.image_index = null
	}

	_show_image(image) {
		if (this.closed)
			return
		let n = this._num_images()
		if (this.options.pics && this.options.pics[this.image_index].title)
			this.description.innerHTML = this.options.pics[this.image_index].title
		if (n > 0)
		 	this.count.innerText = (this.image_index+1) + ' of ' + n
		let w = image.width
		let h = image.height
		let max = _.dom.size(window).x - 320
		if (w > max) {
			h *= max / w
			w = max
			_.dom.set_styles(image, {width:w, height:h})
		}
		max = _.dom.size(window).y - this.navigation.clientHeight - 120
		if (h > max) {
			w *= max / h
			h = max
			_.dom.set_styles(image, {width:w, height:h})
		}
 		_.dom.set_styles(this.image, {width:w, height:h})
		this.modal.classList.remove('gallery_loading')
 		_.dom.set_styles(this.modal, {width:w+20, left:window.innerWidth / 2 - (w / 2) - 10, height:h + this.navigation.clientHeight + 25})
		this.image.style.width = (_.dom.size(this.modal).x - 20) + 'px'
		this.image.appendChild(image)
		this.image.style.opacity = 1
		if (this.options.show_comments) {
			let s = _.dom.size(this.modal)
			_.dom.set_styles(this.comments, {display:'', left:_.dom.pos(this.modal).x, 'max-height':s.y, opacity:0, top:_.dom.pos(this.modal).y+h+this.navigation.clientHeight+20, width:_.dom.size(this.modal).x, zIndex:app.z_index})
			this.comments.style.opacity = 1
			let url=context + '/pictures_comments?id='+this.options.pics[this.image_index].id
			net.replace(this.comments, url, {ignore:true})
		}
	}
}
