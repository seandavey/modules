package pictures;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import app.ClassTask;
import app.ImageMagick;
import app.Images;
import app.Request;
import app.Roles;
import app.Site;
import db.DBConnection;
import db.ManyToMany;
import db.NameValuePairs;
import db.Options;
import db.Relationship;
import db.Rows;
import db.SQL;
import db.Select;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.RecordOwnerAccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.DateColumn;
import db.column.PersonColumn;
import db.column.PictureColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import pages.Page;
import social.NewsItem;
import social.NewsProvider;
import ui.SelectOption;
import web.HTMLWriter;
import web.JS;

public class Pictures extends NewsProvider {
	@JSONField
	private int		m_display_size			= 100;
	Options			m_galleries;
	@JSONField(after = "larger uploaded pictures will be resized down")
	private int		m_max_size				= 1024;
	@JSONField(after = "if set, only people with this role can add/edit/delete pictures", type = "role")
	private String	m_role;
	@JSONField
	boolean			m_start_with_galleries;
	@JSONField
	private int		m_thumb_size			= 470;

	//--------------------------------------------------------------------------

	public
	Pictures() {
		super("pictures", "picture");
		m_description = "A photo gallery for community pictures; interfaces with NewsFeed module";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Pictures", this, false){
			@Override
			public void
			writeContent(Request r) {
				HTMLWriter w = r.w;
				w.styleSheet("gallery-min").style(".nav li { white-space:nowrap; }");
				w.write("<div>");
				w.js("var gallery;app.subscribe('pictures',function(object,message){if(message==='delete')document.location=document.location});");
				if (m_start_with_galleries && m_galleries.getObjects().size() != 0 && r.getInt("view", 0) == 0) {
					w.write("<div><h2>Galleries</h2>");
					w.tagOpen("h4");
					w.ui.buttonOutlineOnClick("All Pictures", "net.replace(this.closest('div').parentNode,context+'/Pictures/galleries/all');");
					w.tagClose();
					for (SelectOption o : m_galleries) {
						w.tagOpen("h4");
						w.ui.buttonOutlineOnClick(o.getText(), "net.replace(this.closest('div').parentNode,context+'/Pictures/galleries/" + JS.escape(o.getValue()) + "');");
						w.tagClose();
					}
					w.write("</div>");
				} else {
					ViewState.setBaseFilter("pictures", null, r);
					ViewState.setFilter("pictures", null, r);
					r.removeSessionAttribute("pictures_galleries_id");
					new PicturesView(Site.site.getViewDef("pictures", r.db), r).writeComponent();
				}
				w.write("</div>");
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("pictures")
			.add(new JDBCColumn("file", Types.VARCHAR))
			.add(new JDBCColumn("caption", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP))
			.add(new JDBCColumn("pictures_galleries").setOnDeleteSetNull(true))
			.add(new JDBCColumn("width", Types.INTEGER))
			.add(new JDBCColumn("height", Types.INTEGER));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("pictures_galleries")
			.add(new JDBCColumn("text", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createTable("pictures_tags", "tag VARCHAR");
		db.createManyToManyLinkTable("pictures", "pictures_tags");
	}

	//--------------------------------------------------------------------------

	String
	buildCaption(int id, String caption, String owner, boolean filter_gallery, int owner_id, DBConnection db) {
		StringBuilder s = new StringBuilder();
		if (caption != null && caption.length() > 0) {
			s.append(HTMLWriter.breakNewlines(caption));
			s.append("<br/>");
		}
		if (m_role == null && owner != null) {
			s.append("posted by: ");
			if (filter_gallery)
				s.append("<a onclick=\"Gallery.g.filter('_owner_=").append(owner_id).append("')\">");
			s.append(owner);
			if (filter_gallery)
				s.append("</a>");
		}
		if (db != null)
			try (Rows rows = new Rows(new Select("tag,pictures_tags_id").from("pictures_tags").join("pictures_tags", "pictures_pictures_tags").whereEquals("pictures_id", id).orderBy("lower(tag)"), db)) {
				if (rows.isBeforeFirst()) {
					s.append("<br/>tags: ");
					rows.next();
					if (filter_gallery)
						s.append("<a onclick=\"Gallery.g.filter('EXISTS(SELECT 1 FROM pictures_pictures_tags WHERE pictures.id=pictures_id AND pictures_tags_id=").append(rows.getInt(2)).append(")')\">");
					s.append(rows.getString(1));
					if (filter_gallery)
						s.append("</a>");
					while (rows.next()) {
						s.append(", ");
						if (filter_gallery)
							s.append("<a onclick=\"Gallery.g.filter('EXISTS(SELECT 1 FROM pictures_pictures_tags WHERE pictures.id=pictures_id AND pictures_tags_id=").append(rows.getInt(2)).append(")')\">");
						s.append(rows.getString(1));
						if (filter_gallery)
							s.append("</a>");
					}
				}
			}
		return s.toString();
	}

	//--------------------------------------------------------------------------

	@ClassTask({ "dir" })
	public static void
	deleteUnneededThumbs(Request r) {
		Images.deleteUnneededThumbs(Site.site.newFile("pictures"));
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		if ("galleries".equals(r.getPathSegment(1))) {
			String gallery_id = r.getPathSegment(2);
			if ("all".equals(gallery_id))
				ViewState.setFilter("pictures", null, r);
			else
				ViewState.setFilter("pictures", "pictures_galleries_id=" + gallery_id, r);
			new PicturesView(Site.site.getViewDef("pictures", r.db), r).writeComponent();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@ClassTask()
	public void
	getDatesFromFiles(Request r) {
		Path path = Site.site.getPath("pictures");
		ViewDef view_def = Site.site.getViewDef("pictures", r.db);
		Rows rows = new Rows(new Select("id,file").from("pictures"), r.db);
		while (rows.next()) {
			String file = rows.getString(2);
			if (file != null)
				Images.setTimestampToExifDate(path.resolve(file), view_def, rows.getInt(1), r);
		}
		rows.close();
	}

	// --------------------------------------------------------------------------

	public Options
	getGalleries() {
		return m_galleries;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		return null;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return absoluteURL().set("db_key_value", item_id).toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		m_galleries = new Options(new Select("*").from("pictures_galleries").orderBy("lower(text)")).setAllowEditing(true);
		Site.site.addObjects(m_galleries);
		Site.site.addUserDropdownItem(new Page("Edit Pictures", this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("edit pictures", r).writeComponent();
			}
		}.addRole("pictures"));
		Roles.add("pictures", "can edit pictures with this role");
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public void
	listMissingPictures(Request r) {
		Path path = Site.site.getPath("pictures");
		Rows pictures = new Rows(new Select("file,id").from(m_table).orderBy("file"), r.db);
		while (pictures.next()) {
			String picture = pictures.getString(1);
			if (picture == null)
				r.w.write("picture id " + pictures.getInt(2) + " has null file field").br();
			else if (!path.resolve(picture).toFile().exists())
				r.w.write(picture).br();
		}
		pictures.close();
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public void
	listPicturesNotInDB(Request r) {
		String[] files = Site.site.newFile("pictures").list();
		if (files != null)
			for (String file : files)
				if (!r.db.exists(m_table, "file=" + SQL.string(file))) {
					r.w.aOpen(Site.context + "/pictures/" + file);
					r.w.img("pictures", file);
					r.w.tagClose();
					r.w.br();
				}
	}

	// --------------------------------------------------------------------------

	@ClassTask({ "directory", "size", "only missing thumbs" })
	public static void
	makeThumbs(String directory, int size, boolean only_missing_thumbs, Request r) {
		ImageMagick.makeThumbs(Site.site.getPath(directory), size, only_missing_thumbs);
	}

	//--------------------------------------------------------------------------

	@Override
	public final NewsItem
	newItem(Rows rows) {
		return new PicturesNewsItem(this, rows);
	}

	// --------------------------------------------------------------------------

	@Override
	public final NewsItem
	newItem(int item_id, int owner_id, LocalDateTime timestamp) {
		return new PicturesNewsItem(this, item_id, owner_id, timestamp);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("edit pictures")) {
			ViewDef view_def = new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("pictures").add().delete().edit())
				.setDefaultOrderBy("_timestamp_ DESC")
				.setFrom("pictures")
				.setRecordName("Picture", true)
				.setTimestampRecords(true);
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("file");
			column_names.add("caption");
			column_names.add("pictures_galleries_id");
			column_names.add("_owner_");
			column_names.add("_timestamp_");
			column_names.add("tags");
			addColumnNames(column_names);
			if (m_add_new_items_to_news_feed)
				column_names.add("show_on_news_feed");
			view_def.setColumnNamesForm(column_names)
				.setColumnNamesTable(new String[] { "file", "pictures_galleries_id", "caption", "_owner_", "_timestamp_", "tags" })
				.setColumn(new PictureColumn("file", "pictures", "pictures", m_thumb_size, m_max_size).setImageSize(m_display_size).setShowRotateButtons(true).setSizeIsMaxSide(false).setUseImageMagick(true).setMultiple(true).setIsRequired(true))
				.setColumn(m_galleries.newColumn("pictures_galleries_id").setDefaultToSessionAttribute().setDisplayName("gallery"))
				.setColumn(new TagsColumn("tags", "pictures", view_def))
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by"))
				.addRelationshipDef(new ManyToMany("pictures_tags", "pictures_pictures_tags", "tag").setManyTableColumn("tag").setHideOnForms(Mode.ADD_FORM, Mode.EDIT_FORM, Mode.READ_ONLY_FORM))
				.setColumn(new DateColumn("_timestamp_").setDisplayName("date"));
			return addHooks(view_def);
		}
		if (name.equals("pictures")) {
			ViewDef view_def = new ViewDef(name) {
				@Override
				public void
				afterInsert(int id, NameValuePairs nvp, Request r) {
					ViewState.setFilter(m_name, null, r);
					super.afterInsert(id, nvp, r);
				}
				@Override
				protected String
				getFormDeleteOnClick(Rows data, Relationship relationship, boolean dialog, Request r) {
					return "Gallery.g.delete_record(" + data.getInt("id") + ")";
				}
			};
			view_def.setAccessPolicy((m_role == null ? new RecordOwnerAccessPolicy() : new RoleAccessPolicy(m_role)).add().delete().edit())
				.setDefaultOrderBy("_timestamp_ DESC")
				.setRecordName("Picture", true)
				.setTimestampRecords(true)
				.setViewClass(PicturesView.class);
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("file");
			column_names.add("caption");
			column_names.add("pictures_galleries_id");
			column_names.add("_owner_");
			column_names.add("tags");
			addColumnNames(column_names);
			addColumnSendEmail(column_names, view_def);
			addColumnShowOnNewsFeed(column_names, view_def);
			view_def.setColumnNamesForm(column_names)
				.setColumnNamesFormTable(new String[] { "caption" })
				.setColumn(new PictureColumn("file", "pictures", "pictures", m_thumb_size, m_max_size).setShowRotateButtons(true).setSizeIsMaxSide(false).setUseImageMagick(true).setMultiple(true).setDisplayName("picture").setIsRequired(true))
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by"))
				.setColumn(m_galleries.newColumn("pictures_galleries_id").setDefaultToSessionAttribute().setDisplayName("gallery"))
				.setColumn(new TagsColumn("tags", "pictures", view_def))
				.addRelationshipDef(new ManyToMany("pictures_tags", "pictures_pictures_tags", "tag").setManyTableColumn("tag").setHideOnForms(Mode.ADD_FORM, Mode.EDIT_FORM, Mode.READ_ONLY_FORM));
			return addHooks(view_def);
		}
		if (name.equals("pictures_galleries"))
			return m_galleries.newViewDef()
				.setRecordName("Gallery", true);
		return null;
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public void
	recordAllSizes(Request r) {
		Rows rows = new Rows(new Select("id,file").from("pictures"), r.db);
		while (rows.next())
			try {
				BufferedImage image = ImageIO.read(Site.site.newFile("pictures", rows.getString("file")));
				r.db.update("pictures", "width=" + image.getWidth() + ",height=" + image.getHeight(), rows.getInt("id"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		rows.close();
	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	@Override
	public void
	writeAddButton(Request r) {
		writeAddButton("camera", "pictures", r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
	}
}
