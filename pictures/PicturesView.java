package pictures;

import app.Modules;
import app.Person;
import app.Request;
import app.Site;
import db.DBConnection;
import db.Options;
import db.OrderBy;
import db.RowsSelect;
import db.View;
import db.ViewDef;
import ui.Option;
import ui.Select;

public class PicturesView extends View {
	private String	m_dir;

	//--------------------------------------------------------------------------

	public
	PicturesView(ViewDef view_def, Request r) {
		super(view_def, r);
		m_pager.setWriteDropdown(false);
		m_dir = Site.settings.getString("pictures/dir");
		if (m_dir == null)
			m_dir = "pictures";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	write() {
		View.Mode mode = m_r.getEnum("db_mode", View.Mode.class);
		if (mode != null && mode != View.Mode.LIST) {
			super.write();
			return;
		}
		m_w.addClass("mb-3")
			.addStyle("display:flex")
			.tagOpen("div");
		m_w.write("<div style=\"display:flex;flex-grow:2;flex-wrap:wrap;gap:20px;white-space:nowrap;\">");
		writeGallerySelect();
		writeOwnerSelect();
		writeTagsSelect();
		writeYearSelect();
		writeSortSelect();
		m_w.write("</div><div style=\"padding-left:20px;text-align:right;\">");
		if (m_view_def.getAccessPolicy().showAddButton(this, m_r))
			m_w.ui.buttonOnClick("Add&nbsp;Picture", getOnClickForRow(0, Mode.ADD_FORM));
		m_w.write("</div></div>");
		m_w.write("<div id=\"gallery\" style=\"align-items:flex-start;display:flex;flex-wrap:wrap;justify-content:space-around;\"></div>")
			.captureStart()
			.write("new Gallery({dir:'").write(m_dir).write("',el:_.$('#gallery'),infinite_scroll:true,show_comments:true");
		writePics(m_r.getUser(), m_r.db);
		m_w.write("})");
		int view = m_r.getInt("view", 0);
		if (view != 0)
			m_w.write(";Gallery.g.open(Gallery.g.index(" + view + "))");
		m_w.script("gallery-min", true, m_w.captureEnd());
		writeViewJSON();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeComponent() {
		if (!m_request_processed) {
			processRequest(this, m_r).writeComponent();
			return;
		}

		componentOpen();
		write();
		m_w.tagClose();
		int id = m_r.getInt("db_key_value", 0);
		if (m_mode == Mode.LIST && id != 0)
			m_w.js("var i=Gallery.g.index(" + id + ");if(i!=-1)Gallery.g.open(i);");
	}

	//--------------------------------------------------------------------------

	private void
	writePics(Person user, DBConnection db) {
		Pictures pictures = (Pictures)Modules.get("Pictures");
		m_w.write(",pics:[");
		select();
		boolean first = true;
		while (next()) {
			String file = data.getString("file");
			if (file == null || file.length() == 0)
				continue;
			int id = data.getInt("id");
			if (first)
				first = false;
			else
				m_w.comma();
			m_w.write("{id:").write(id)
				.write(",file:").jsString(file)
				.write(",title:").jsString(pictures.buildCaption(id, data.getString("caption"), getColumnHTML("_owner_"), true, data.getInt("_owner_"), db))
				.write(",can_edit:").write(m_view_def.getAccessPolicy().showEditButtonForRow(data, m_r) || m_r.userIsAdministrator() ? "true}" : "false}");
		}
		m_w.write("]");
	}

	//--------------------------------------------------------------------------

	private void
	writeGallerySelect() {
		Options galleries = (Options)Site.site.getObjects("pictures_galleries");
		Select select = new Select("galleries", galleries)
			.addFirstOption(new Option("All Pictures", ""))
			.setInline(true)
			.setOnChange("var self=this;var v=this.options[this.selectedIndex].value;net.post(context+'/ViewStates/pictures',v?'filter=pictures_galleries_id='+v:'filter=',function(){net.replace(_.c(self),context + '/Views/pictures?db_mode=" + View.Mode.LIST + "');})");
		String filter = getState().getFilter();
		if (filter != null) {
			int index = filter.indexOf("pictures_galleries_id");
			if (index != -1)
				if (filter.charAt(index + 21) == '=')
					select.setSelectedOption(null, filter.substring(index + 22));
				else
					select.setSelectedOption(null, "null");
		}
		if (select.size() > 0) {
			m_w.write("<div>Gallery:<br>");
			select.write(m_w);
			m_w.write("</div>");
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeOwnerSelect() {
		db.Select query = new db.Select("people.id,first || ' ' || last AS name").distinct().from("people").joinOn("pictures", "people.id=pictures._owner_");
		String base_filter = m_state.getBaseFilter();
		if (base_filter != null)
			query.andWhere(base_filter);
		query.orderBy("2");
		Select select = new RowsSelect(null, query, "name", "id", m_r)
			.setAllowNoSelection(true)
			.setInline(true)
			.setOnChange("var self=this;var v=this.options[this.selectedIndex].value;net.post(context+'/ViewStates/pictures',v?'filter=_owner_='+v:'filter=',function(){net.replace(_.c(self),context + '/Views/pictures?db_mode=" + View.Mode.LIST + "');})");
		String filter = getState().getFilter();
		if (filter != null) {
			int index = filter.indexOf("_owner_");
			if (index != -1)
				if (filter.charAt(index + 7) == '=')
					select.setSelectedOption(null, filter.substring(index + 8));
				else
					select.setSelectedOption(null, "null");
		}
		if (((RowsSelect)select).size() > 2) {
			m_w.write("<div>Posted by:<br>");
			select.write(m_w);
			m_w.write("</div>");
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeSortSelect() {
		m_w.write("<div>Sort:<br>");
		OrderBy o = m_state.getOrderBy();
		String[] options = new String[] {"by Caption ↑", "by Caption ↓", "by Date ↑", "by Date ↓"};
		new Select(null, options)
			.setSelectedOption(options[o.columnName(0).equals("_timestamp_") ? o.descending(0) ? 3 : 2 : o.descending(0) ? 1 : 0], null)
			.setOnChange("var self=this;var i=this.selectedIndex;net.post(context+'/ViewStates/pictures','db_orderby='+(i==0?'LOWER(caption)':i==1?'LOWER(caption) DESC':i==2?'_timestamp_':'_timestamp_ DESC'),function(){net.replace(_.c(self),context + '/Views/pictures?db_mode=" + View.Mode.LIST + "');})")
			.write(m_w);
		m_w.write("</div>");
	}

	//--------------------------------------------------------------------------

	private void
	writeTagsSelect() {
		db.Select query = new db.Select("pictures_tags.id,tag").distinctOn("tag").from("pictures_tags").joinOn("pictures_pictures_tags", "pictures_tags.id=pictures_pictures_tags.pictures_tags_id").joinOn("pictures", "pictures_pictures_tags.pictures_id=pictures.id");
		String base_filter = m_state.getBaseFilter();
		if (base_filter != null)
			query.andWhere(base_filter);
		query.orderBy("lower(tag)");
		Select select = new RowsSelect(null, query, "tag", "id", m_r)
			.setAllowNoSelection(true)
			.setInline(true)
			.setOnChange("var self=this;var v=this.options[this.selectedIndex].value;net.post(context+'/ViewStates/pictures',v?'filter=EXISTS(SELECT 1 FROM pictures_pictures_tags WHERE pictures_id=pictures.id AND pictures_tags_id='+v+')':'filter=',function(){net.replace(_.c(self),context + '/Views/pictures?db_mode=" + View.Mode.LIST + "');})");
		String filter = getState().getFilter();
		if (filter != null && filter.startsWith("EXISTS")) {
			int index = filter.lastIndexOf('=');
			if (index != -1)
				select.setSelectedOption(null, filter.substring(index + 1, filter.length() - 1));
			else
				select.setSelectedOption(null, "null");
		}
		if (select.size() > 0) {
			m_w.write("<div>Tagged:<br>");
			select.write(m_w);
			m_w.write("</div>");
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeYearSelect() {
		db.Select query = new db.Select("EXTRACT(year from _timestamp_) AS year").distinct().from("pictures");
		String base_filter = m_state.getBaseFilter();
		if (base_filter != null)
			query.andWhere(base_filter);
		query.orderBy("1");
		Select select = new RowsSelect(null, query, "year", null, m_r)
			.setAllowNoSelection(true)
			.setInline(true)
			.setOnChange("var self=this;var v=this.options[this.selectedIndex].value;net.post(context+'/ViewStates/pictures',v?'filter=extract(year from _timestamp_)='+v:'filter=',function(){net.replace(_.c(self),context + '/Views/pictures?db_mode=" + View.Mode.LIST + "');})");
		String filter = getState().getFilter();
		if (filter != null) {
			int index = filter.indexOf("extract(year from _timestamp_)");
			if (index != -1)
				if (filter.charAt(index + 30) == '=')
					select.setSelectedOption(null, filter.substring(index + 31));
				else
					select.setSelectedOption(null, "null");
		}
		if (((RowsSelect)select).size() > 2) {
			m_w.write("<div>Year:<br>");
			select.write(m_w);
			m_w.write("</div>");
		}
	}
}
