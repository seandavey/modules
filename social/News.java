package social;

import java.io.IOException;
import java.sql.Types;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import app.Module;
import app.Modules;
import app.Person;
import app.Request;
import app.Site;
import app.Site.Message;
import app.Subscriber;
import db.DBConnection;
import db.NameValuePairs;
import db.Result;
import db.Rows;
import db.Select;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import mail.Mail;
import util.Time;
import web.HTMLWriter;

public class News extends Module implements Subscriber {
	private LocalDateTime					m_earliest_update;
	@JSONField(admin_only = true)
	private int								m_last_item_sent;
	@JSONField(admin_only = true)
	private boolean							m_new_items;
	private final Map<String, NewsProvider>	m_providers = new TreeMap<>();

	public static News news;

	//--------------------------------------------------------------------------

	public
	News() {
		news = this;
	}

	//--------------------------------------------------------------------------

	public final synchronized int
	addNewsItem(NewsProvider provider, int item_id, NameValuePairs nvp, Request r) {
		Person user = r.getUser();
		if (user == null)
			return 0;
		String timestamp = Time.newDateTime().toString();
		NameValuePairs nvp2 = new NameValuePairs()
			.set("provider", provider.getName())
			.set("item_id", item_id)
			.set("_owner_", user.getId())
			.set("_timestamp_", timestamp)
			.set("last_update", timestamp);
		Result result = r.db.insert("news", nvp2);
		m_new_items = true;
		store(r.db);
		return result.id;
	}

	//--------------------------------------------------------------------------

	public final void
	addProvider(NewsProvider news_provider, DBConnection db) {
		m_providers.put(news_provider.getName(), news_provider);
		Modules.add(new Comments(news_provider), db);
		Site.site.subscribe(news_provider.getTable(), this);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("news")
			.add(new JDBCColumn("channel", Types.INTEGER))
			.add(new JDBCColumn("provider", Types.VARCHAR))
			.add(new JDBCColumn("item_id", Types.INTEGER))
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP))
			.add(new JDBCColumn("last_update", Types.TIMESTAMP));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	private void
	deleteItem(int id, Request r) {
		Person user = r.getUser();
		String[] item = r.db.readRow(new Select("provider,item_id,_owner_").from("news").whereIdEquals(id));
		if (item == null)
			return;
		if (user.getId() == Integer.parseInt(item[2]) || r.userIsAdmin()) {
			r.db.delete("news", id, false);
			r.db.delete(m_providers.get(item[0]).getTable(), Integer.parseInt(item[1]), false);
		}
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch (path_segments[0]) {
		case "edit":
			editItem(Integer.parseInt(path_segments[1]), r);
			return true;
		case "item":
			Rows rows = new Rows(new Select("*").from("news").whereIdEquals(Integer.parseInt(path_segments[1])), r.db);
			if (rows.next()) {
				NewsProvider news_provider = m_providers.get(rows.getString("provider"));
				if (news_provider != null)
					news_provider.newItem(rows).write(Time.newDateTime(), false, r.w, r.db, r);
			}
			rows.close();
			return true;
		case "new":
			writeItems(0, new Rows(new Select("*").from("news").where("last_update>'" + path_segments[1] + "'").orderBy("last_update DESC"), r.db), r);
			return true;
		case "next":
			if (m_earliest_update == null)
				m_earliest_update = r.db.lookupTimestamp(new Select("MIN(last_update)").from("news"));
			LocalDateTime earliest_update = writeItems(20, new Rows(new Select("*").from("news").where("last_update<'" + path_segments[1] + "'").orderBy("last_update DESC").limit(21), r.db), r);
			if (earliest_update.equals(m_earliest_update))
				r.w.js("var e=_.$('#see_more');if(e)e.remove()");
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doAPIPost(String[] path_segments, Request r) {
		if (super.doAPIPost(path_segments, r))
			return true;
		switch (path_segments[0]) {
		case "delete":
			deleteItem(Integer.parseInt(path_segments[1]), r);
			return true;
		case "remove":
			removeItem(Integer.parseInt(path_segments[1]), r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	private void
	editItem(int id, Request r) {
		String[] item = r.db.readRow(new Select("provider,item_id").from("news").whereIdEquals(id));
		NewsProvider provider = m_providers.get(item[0]);
		if (provider != null)
			provider.writeNewsItemEditForm(Integer.parseInt(item[1]), id, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyFiveMinutes(LocalDateTime now, DBConnection db) {
		sendEmails(db);
	}

	//--------------------------------------------------------------------------

	private static List<String>
	getEmails(DBConnection db) {
		return db.readValues(new Select("email").from("people").where("send_email_when_someone_posts_to_the_site AND active AND email IS NOT NULL"));
	}

	//--------------------------------------------------------------------------

	public final Map<String, NewsProvider>
	getProviders() {
		return m_providers;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		if (m_last_item_sent == 0)
			m_last_item_sent = db.lookupInt(new Select("MAX(id)").from("news"), 0);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		if (message == Message.DELETE)
			for (NewsProvider np : m_providers.values())
				if (((String)object).equals(np.getTable())) {
					String where = (String)data;
					where = where.substring(where.indexOf('=') + 1);
					if (where.charAt(0) == '\'')
						where = where.substring(1, where.length() - 1);
					db.delete("news", "provider='" + np.getName() + "' AND item_id=" + where, false);
					return;
				}
	}

	//--------------------------------------------------------------------------

	private void
	removeItem(int id, Request r) {
		if (r.userIsAdmin() || r.getUser().getId() == r.db.lookupInt(new Select("_owner_").from("news").whereIdEquals(id), -1))
			r.db.delete("news", id, false);
	}

	//--------------------------------------------------------------------------

	static void
	sendEmail(NewsItem item, int news_id, DBConnection db) {
		List<String> emails = getEmails(db);
		if (emails.size() == 0)
			return;
		StringBuilder s = new StringBuilder();
		HTMLWriter w = new HTMLWriter(s);
		item.write(null, true, w, db, null);
		w.br().br().a("View post on site", Site.site.absoluteURL(Site.site.getHomePage()).set("v", news_id).toString());
		String content = s.toString();
		Person owner = Site.site.newPerson(item.m_owner_id, db);
		new Mail("[" + Site.site.getDisplayName() + "] New post by " + owner.getName(), content)
			.replyTo(owner.getEmail())
			.to(emails)
			.send();
	}

	//--------------------------------------------------------------------------

	private synchronized void
	sendEmails(DBConnection db) {
		if (!m_new_items)
			return;
		List<String> emails = getEmails(db);
		if (emails.size() == 0)
			return;
		Rows items = new Rows(new Select("provider,id,item_id,_owner_,last_update,_timestamp_").from("news").where("id > " + m_last_item_sent).andWhere("provider!='NewsFeed'").orderBy("id"), db);
		if (!items.isBeforeFirst()) {
			items.close();
			return;
		}
		NewsItem item = null;
		int news_id = 0;
		StringBuilder s = new StringBuilder("""
			<style>
			.news_item {
			    background: white;
			    border: 1px solid;
			    border-color: #e9eaed #dfe0e4 #d0d1d5;
			    border-radius: 3px;
			    color: #141823;
			    margin-top: 10px;
			}
			.news_item_header {
			    color: #9197a3;
			    font-size: 12px;
			    margin-bottom: 11px;
			    position: relative;
			}
			.news_item_person {
			    color: rgb(59, 89, 152);
			    font-weight: bold;
			}
			</style>""");
		HTMLWriter w = new HTMLWriter(s);
		while (items.next()) {
			if (ChronoUnit.MINUTES.between(items.getTimestamp("last_update"), Time.newDateTime()) < 5) {
				items.close();
				return;
			}
			news_id = items.getInt("id");
			m_last_item_sent = news_id;
			NewsProvider provider = m_providers.get(items.getString("provider"));
			if (provider == null)
				continue;
			else if (item == null)
				item = provider.newItem(items);
			else if (!item.merge(provider, items)) {
				item.write(null, true, w, db, null);
				w.br().br().a("view post on site", Site.site.absoluteURL(Site.site.getHomePage()).set("v", news_id).toString());
				item = provider.newItem(items);
			}
		}
		if (item != null) {
			item.write(null, true, w, db, null);
			w.br().br().a("view post on site", Site.site.absoluteURL(Site.site.getHomePage()).set("v", news_id).toString());
		}
		new Mail("[" + Site.site.getDisplayName() + "] Recent Posts", s.toString())
			.to(emails)
			.send();
		items.close();
		m_new_items = false;
		store(db);
	}

	//--------------------------------------------------------------------------

	public final void
	writeItems(Request r) {
		HTMLWriter w = r.w;
		w.js("JS.get('" + Site.site.getResources().get("js", "news-min") + "')");
		w.write("<div id=\"news_items\" class=\"news_items\">");
		Rows rows = new Rows(new Select("*").from("news").orderBy("last_update DESC").limit(21), r.db);
		LocalDateTime earliest_update = writeItems(20, rows, r);
		w.write("</div>");
//		if (m_earliest_update == null)
			m_earliest_update = r.db.lookupTimestamp(new Select("MIN(last_update)").from("news"));
		if (m_earliest_update != null && earliest_update.isAfter(m_earliest_update)) {
			w.write("<div id=\"see_more\" style=\"text-align:center\">");
			w.ui.buttonOutlineOnClick("See More", "News.fetch_next()");
			w.write("</div>");
		}
		int item_id = r.getInt("v", 0);
		if (item_id != 0)
			w.js("new Dialog({url:context+'/News/item/" + item_id + "'}).show()");
	}

	//--------------------------------------------------------------------------
	/**
	 * @param num_items max items to write, 0 means write only ones sent
	 * @param rows
	 * @param r
	 * @throws IOException
	 */

	private LocalDateTime
	writeItems(int num_items, Rows rows, Request r) {
		LocalDateTime earliest = null;
		LocalDateTime now = Time.newDateTime();
		NewsItem item = null;
		for (int i=0; num_items==0||i<num_items; i++) {
			if (!rows.next())
				break;
			earliest = rows.getTimestamp("last_update").withNano(0);
			if (i == 0 && earliest != null)
				r.w.js("JS.with('News', function(){News.set_latest('" + earliest.toString() + "')})");
			String provider_name = rows.getString("provider");
			NewsProvider provider = m_providers.get(provider_name);
			if (provider == null)
				r.log("no provider named " + provider_name, false);
			else if (item == null)
				item = provider.newItem(rows);
			else if (item.merge(provider, rows))
				--i;
			else {
				item.write(now, false, r.w, r.db, r);
				item = provider.newItem(rows);
			}
		}
		if (item != null) {
			item.write(now, false, r.w, r.db, r);
			r.w.js("JS.with('News', function(){News.set_earliest('" + earliest.toString() + "')})");
		}
		rows.close();
		return earliest;
	}
}