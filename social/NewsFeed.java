package social;

import java.sql.Types;
import java.time.LocalDateTime;

import app.Request;
import app.Site;
import db.DBConnection;
import db.NameValuePairs;
import db.Select;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import ui.RichText;
import util.Time;
import web.HTMLWriter;

public class NewsFeed extends NewsProvider {
	@JSONField(admin_only = true)
	private boolean	m_slack_style;
	@JSONField
	private String	m_status_input_placeholder = "What's on your mind?";

	//--------------------------------------------------------------------------

	public
	NewsFeed() {
		super("news_feed", "post");
		m_description = "A list showing user posts; users can post text and pictures, start discussions, post recipes and more; posts can be commented on";
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canDelete() {
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canEdit() {
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canRemove() {
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doAPIPost(String[] path_segments, Request r) {
		if (super.doAPIPost(path_segments, r))
			return true;
		switch (path_segments[0]) {
		case "add":
			NameValuePairs nvp = new NameValuePairs();
			nvp.set("text", r.getParameter("text"));
			int owner_id = r.getUser().getId();
			nvp.set("_owner_", owner_id);
			LocalDateTime timestamp = Time.newDateTime();
			nvp.set("_timestamp_", timestamp.toString());
			int id = r.db.insert(m_table, nvp).id;
			int news_id = News.news.addNewsItem(this, id, nvp, r);
			if (m_send_emails_on_new_items) {
				NewsItem item = new NewsItem(this, id, owner_id, timestamp);
				News.sendEmail(item, news_id, r.db);
			}
			return true;
		case "update":
			nvp = new NameValuePairs();
			nvp.set("text", r.getParameter("text"));
			nvp.setTimestamp("_timestamp_");
			id = Integer.parseInt(path_segments[1]);
			r.db.update(m_table, nvp, id);
			updateNewsItem(id, true, r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		return null;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return Site.site.absoluteURL(Site.site.getHomePage()).set("v", item_id).toString();
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		JDBCTable table_def = new JDBCTable(m_table)
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("text", Types.VARCHAR))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	public final void
	write(Request r) {
		if (m_slack_style) {
			r.w.write("<div id=\"news_outer\" style=\"overflow-y:scroll\">");
			News.news.writeItems(r);
			r.w.write("</div>");
			writeStatusForm(r);
			r.w.js("_.dom.set_style('#news_outer','height',window.innerHeight-_.$('#top_menu').offsetHeight-40-_.$('#status').offsetHeight)");
		} else {
			writeStatusForm(r);
			News.news.writeItems(r);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItemEditForm(int item_id, int news_id, Request r) {
		r.w.write("<div id=\"edit_news_item\" class=\"status\">")
			.textAreaExpanding("post", "width:100%", r.db.lookupString(new Select("text").from(m_table).whereIdEquals(item_id)))
			.js("Dialog.top().add_ok('Save',function(){net.post('" + apiURL("update", item_id) + "',{text:_.$('#edit_news_item').firstChild.value},function(){News.replace_news_item(" + news_id + ")})});");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		String text = db.lookupString("text", m_table, item_id);
		w.write("<div style=\"max-height:450px;overflow:auto;\">");
		if (for_mail)
			w.write(text);
		else
			w.writeWithLinks(text);
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, boolean hide_active, Request r) {
		if (column_names == null)
			column_names = new String[] { "m_name", "m_status_input_placeholder" };
		super.writeSettingsForm(column_names, in_dialog, hide_active, r);
	}

	//--------------------------------------------------------------------------

	private void
	writeStatusForm(Request r) {
		if (r.getUser() == null || r.userIsGuest())
			return;
		HTMLWriter w = r.w;
		w.addClass("status")
			.tagOpen("div");
		w.write("<div class=\"status_text\">");
		if (m_status_input_placeholder != null)
			w.setAttribute("placeholder", m_status_input_placeholder);
		new RichText().write("status", null, "textarea", false, "small", w);
		w.write("</div><div class=\"post\"><table style=\"background-color:inherit;width:100%;\"><tr><td>");
		for (NewsProvider provider : News.news.getProviders().values())
			if (provider.isActive())
				provider.writeAddButton(r);
		w.write("</td><td style=\"text-align:right;\"><button class=\"btn btn-primary btn-sm\" onclick=\"News.post_status()\">Post</button></td></tr></table></div>")
			.tagClose();
	}
}
