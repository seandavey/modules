var News = {
	channel: null,
	earliest: null,
	earliest_str: null,
	latest: null,
	latest_str: null,
	api_url() {
		return app.api_url('News', ...arguments)
	},
	fetch_new() {
		if (!News.latest_str)
			return
		News.fetching = true
		let url = News.api_url('new', News.latest_str)
		if (News.channel)
			url += '?channel=' + News.channel
		net.get_html(url, function(o) {
			let t = News.to_elements(o.html)
			let ni = _.$('#news_items')
			for (let i = t.length-1;i>=0;i--) {
				let item = _.$('#' + t[i].id)
				if (item)
					item.remove()
				ni.insertBefore(t[i], ni.firstChild)
			}
			_.dom.add_script(o.js)
			News.fetching = false
		}, true)
	},
	fetch_next() {
		if (!News.earliest_str)
			return
		News.fetching = true
		net.get_html(News.api_url('next', News.earliest_str), function(o) {
			let t = News.to_elements(o.html)
			let ni = _.$('#news_items')
			while (t.length > 0)
				ni.appendChild(t[0])
			_.dom.add_script(o.js)
			News.fetching = false
		}, true)
	},
	item_dropdown(b, id, t, e, d, r) {
		let dd = new Dropdown()
		if (d)
			dd.add('Delete '+t, function() {
				app.hide_popups()
				Dialog.confirm(null, 'Delete '+t+'?', _.text.delete, function() {
					net.post(News.api_url(id, 'delete'), null, function() {
						b.closest('div.news_item').remove()
					})
				})
			})
		if (e)
			dd.add('Edit '+t,function(){
				app.hide_popups()
				new Dialog({url:News.api_url('edit', id), title:'Edit Post', dialog_class:'news_item_dialog', e:b, cancel:'Cancel'})
			})
		if (r)
			dd.add('Remove '+t+' from news feed', function() {
				app.hide_popups()
				Dialog.confirm(null, 'Remove '+t+' from the news feed?', 'remove', function() {
					net.post(News.api_url(id, 'remove'), null, function() {
						b.closest('div.news_item').remove()
					})
				})
			})
		dd.show(b)
	},
	post_status() {
		let d = _.$('#status')
		_.rich_text.update_source_element(d)
		if (d.innerHTML)
			net.post(app.api_url('NewsFeed', 'add'), 'text='+encodeURIComponent(d.innerHTML), function() {
				_.rich_text.destroy(d)
				d.value = ''
				News.fetch_new()
			})
	},
	replace_news_item(id) {
		let ni = Dialog.top().options.e.closest('div.news_item')
		Dialog.top().close()
		net.get_html(News.api_url('item', id), function(o) {
			ni.parentNode.replaceChild(News.to_elements(o.html)[0], ni)
			_.dom.add_script(o.js)
		})
	},
	set_earliest(earliest) {
		if (!earliest)
			return
		t = Date.parse(earliest)
		if (!News.earliest || News.earliest > t) {
			News.earliest = t
			News.earliest_str = earliest
		}
	},
	set_latest(latest) {
		if (!latest)
			return
		t = Date.parse(latest)
		if (!News.latest || News.latest < t) {
			News.latest = t
			News.latest_str = latest
		}
	},
	to_elements(html) {
		let d = document.createElement('div')
		d.innerHTML = html
		return d.childNodes
	}
}
/*document.addEventListener('scroll', function() {
	if (document.body.clientWidth > 576 && !News.fetching)
		if (_.dom.at_bottom())
			News.fetch_next()
		else if (window.pageYOffset < 100)
			News.fetch_new()
})*/
