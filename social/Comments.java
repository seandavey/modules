package social;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import app.Module;
import app.Modules;
import app.Person;
import app.Request;
import app.Site;
import db.DBConnection;
import db.FormHook;
import db.NameValuePairs;
import db.Rows;
import db.Select;
import db.View;
import db.ViewDef;
import mail.Addresses;
import mail.Mail;
import ui.RichText;
import util.Text;
import util.Time;
import web.HTMLWriter;

public class Comments extends Module implements FormHook {
	private String			m_items_table;
	private String			m_max_height;
	private NewsProvider	m_provider;

	//--------------------------------------------------------------------------

	public
	Comments(String table) {
		super(table + "_comments");
		m_items_table = table;
	}

	//--------------------------------------------------------------------------

	public
	Comments(NewsProvider provider) {
		this(provider.getTable());
		m_provider = provider;
	}

	//--------------------------------------------------------------------------

	public static ViewDef
	addFormHook(ViewDef view_def) {
		view_def.addFormHook((Comments)Modules.get(view_def.getFrom() + "_comments"));
		return view_def;
	}

	//--------------------------------------------------------------------------
	// FormHook

	@Override
	public void
	afterForm(int id, Rows data, ViewDef view_def, View.Mode mode, boolean printer_friendly, Request r) {
		if (mode == View.Mode.ADD_FORM)
			return;

		r.w.write("<tr><td>");
		writeComments(view_def.getFrom(), data.getInt("id"), m_max_height, r);
		r.w.write("</td></tr>");
	}

	//--------------------------------------------------------------------------

	private void
	deleteItem(int id, Request r) {
		Person user = r.getUser();
		String[] item = r.db.readRow(new Select("_owner_").from(m_name).whereIdEquals(id));
		if (item == null)
			return;
		if (user.getId() == Integer.parseInt(item[0]) || r.userIsAdmin())
			r.db.delete(m_name, id, false);
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if ("edit".equals(r.getPathSegment(-1))) {
			writeEditForm(r.getPathSegment(-2), r);
			return true;
		}
		writeComments2(m_items_table, r.getInt("id", 0), r);
		return true;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String action = r.getPathSegment(-1);
		if (action == null)
			return false;
		switch(action) {
		case "add":
			NameValuePairs nvp = new NameValuePairs();
			int item_id = r.getInt("item_id", 0);
			nvp.set(m_items_table + "_id", item_id);
			String comment = r.getParameter("comment");
			nvp.set("comment", comment);
			int commenter_id = r.getUser().getId();
			nvp.set("_owner_", commenter_id);
			int id = r.db.insert(m_name, nvp).id;
			if (m_provider != null)
				m_provider.updateNewsItem(item_id, false, r);
			sendEmail(item_id, commenter_id, comment, r);
			return true;
		case "delete":
			deleteItem(r.getPathSegmentInt(-2), r);
			return true;
		case"update":
			nvp = new NameValuePairs();
			nvp.set("comment", r.getParameter("comment"));
			nvp.setTimestamp("_timestamp_");
			id = r.getPathSegmentInt(-2);
			r.db.update(m_name, nvp, id);
			item_id = r.db.lookupInt(new Select(m_items_table + "_id").from(m_name).whereIdEquals(id), 0);
			if (m_provider != null)
				m_provider.updateNewsItem(item_id, false, r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		db.createManyTable(m_items_table, m_name, "reply_to INTEGER,comment VARCHAR", "people", true);
	}

	//--------------------------------------------------------------------------

	private void
	sendEmail(int item_id, int commenter_id, String comment, Request r) {
		if (m_provider == null)
			return;
		int poster = r.db.lookupInt(new Select("_owner_").from(m_items_table).whereIdEquals(item_id), 0);
		if (!r.db.lookupBoolean(new Select("email_updates_to_my_conversations").from("people").whereIdEquals(poster)))
			return;
		Addresses addresses = new Addresses();
		if (poster != commenter_id)
			addresses.add(r.db.lookupString(new Select("email").from("people").whereIdEquals(poster)));
		Rows rows = new Rows(new Select("email").distinct().from(m_items_table + "_comments").joinOn("people", "_owner_!=" + commenter_id + " AND people.id=_owner_").whereEquals(m_items_table + "_id", item_id), r.db);
		while (rows.next())
			addresses.add(rows.getString(1));
		rows.close();
		if (addresses.size() == 0)
			return;
		Person commenter = new Person(commenter_id, r.db);
		String name = commenter.getName();
		String content = name + " commented on something you posted or commented on.<blockquote>" + comment + "</blockquote>click <a href=\"" + m_provider.getItemURL(item_id, r) + "\">here</a> to view";
		new Mail("[" + Site.site.getDisplayName() + "] " + name + " commented on a post", content)
			.replyTo(commenter.getEmail())
			.to(addresses)
			.send();
	}

	//--------------------------------------------------------------------------

	public final Comments
	setMaxHeight(String max_height) {
		m_max_height = max_height;
		return this;
	}

	//--------------------------------------------------------------------------

	private static void
	writeComment(String table, Object[] values, List<Object[]> rows, int depth, LocalDateTime now, Request r) {
		HTMLWriter w = r.w;
		w.write("<div class=\"comment\"");
		if (depth > 0)
			w.write(" style=\"padding-left:").write(depth * 15).write("px;\"");
		w.write('>');
		Person user = r.getUser();
		boolean can_delete = user != null && (((Integer)values[3]).intValue() == user.getId() || r.userIsAdmin());
		boolean can_edit = user != null && ((Integer)values[3]).intValue() == user.getId();
		if (can_edit || can_delete) {
			w.setAttribute("onclick", "_.comments.dropdown(this,'" + table + "_comments'," + values[0] + "," + (can_edit ? "true" : "false") + "," + (can_delete ? "true" : "false") + ");return false;")
				.addStyle("float:right")
				.tagOpen("span");
			w.ui.icon("chevron-down")
				.tagClose();
		}
		if (values[8] != null) {
			w.write("<div style=\"float:left;\">");
			if (r.getUser() != null)
				w.addStyle("cursor:pointer")
					.setOnClick("Person.popup(" + values[5] + ")");
			w.img("people", values[5].toString(), values[8].toString())
				.write("</div>");
		}
		w.write("<div class=\"comment_text\">");
		w.addClass("comment_person");
		if (r.getUser() != null)
			w.addStyle("cursor:pointer")
				.setOnClick("Person.popup(" + values[5] + ")");
		w.tag("span", Text.join(" ", (String)values[6], (String)values[7]));
		w.write(" <span id=\"comment")
			.write(values[0])
			.write("\">");
		if (values[1] != null)
			w.writeWithLinks(values[1].toString());
		w.write("</span><div class=\"comment_time\">")
			.write(Time.formatter.getTimeAgo(((Timestamp)values[4]).toLocalDateTime(), now))
			.write("</div></div></div>");
		Integer id = (Integer)values[0];
		for (Object[] row_values : rows)
			if (id.equals(row_values[2]))
				writeComment(table, row_values, rows, depth + 1, now, r);
	}

	//--------------------------------------------------------------------------

	private static void
	writeCommentInput(String table, int item_id, Request r) {
		Person user = r.getUser();
		if (user == null)
			return;
		if (r.userIsGuest())
			return;
		HTMLWriter w = r.w;
		w.ui.helpText("Add a comment")
			.write("<div class=\"comment_input\"><table style=\"width:100%\"><tbody><tr>");
		String picture = user.getPicture();
		if (picture != null)
			w.write("<td style=\"width:40px\"><img src=\"")
				.write(picture)
				.write("\" alt=\"\" /></td>");
		String id = "commentinput" + w.newID();
		w.write("<td><div>")
			.setAttribute("data-table", table)
			.setAttribute("data-item", item_id);
		new RichText()
			.setOnClick("setTimeout(function(){_.c(this).scrollTo(0,9999)}.bind(this),100)")
			.setOnSave("_.comments.post")
			.write(id, null, "div", false, "small", w);
		w.write("</div></td></tr></tbody></table></div>")
			.js("_.c('#" + id + "').scrollTo(0,9999);_.$('#" + id + "').removeAttribute('id')");
	}

	//--------------------------------------------------------------------------

	public static void
	writeComments(String table, int id, Request r) {
		writeComments(table, id, null, r);
	}

	//--------------------------------------------------------------------------

	private static void
	writeComments(String table, int id, String max_height, Request r) {
		HTMLWriter w = r.w;
		w.write("<div class=\"comments_divider\"></div>")
			.addClass("comments");
		if (max_height != null)
			w.addStyle("max-height:" + max_height);
		w.componentOpen(Site.context + "/" + table + "_comments?id=" + id);
		((Comments)Modules.get(table + "_comments")).writeComments2(table, id, r);
		w.tagClose();
	}

	//--------------------------------------------------------------------------

	private void
	writeComments2(String table, int id, Request r) {
		List<Object[]> rows = r.db.readRowsObjects(new Select(table + "_comments.id,comment,reply_to,_owner_,_timestamp_,people.id person,first,last,picture")
				.from(table + "_comments LEFT JOIN people ON people.id=" + table + "_comments._owner_")
				.where(table + "_id=" + id)
				.orderBy("_timestamp_"));

		LocalDateTime now = Time.newDateTime();
		if (rows.size() > 0)
			for (Object[] values : rows)
				if (values[2] == null)
					writeComment(table, values, rows, 0, now, r);
		if (m_provider == null || !m_provider.commentsClosed(id, r.db))
			writeCommentInput(table, id, r);
	}

	//--------------------------------------------------------------------------

	private void
	writeEditForm(String comment_id, Request r) {
		HTMLWriter w = r.w;
		w.write("<div id=\"edit_comment\" class=\"status\">");
		new RichText()
			.setOnClick("setTimeout(function(){_.c(this).scrollTo(0,9999)}.bind(this),100)")
			.write("post", r.db.lookupString(new Select("comment").from(m_name).whereIdEquals(comment_id)), "textarea", true, "small", w);
		w.write("<script>Dialog.top().add_ok('Save',function(){" +
			"var v=_.$('#edit_comment').firstChild.value;if(!v)return;net.post(context+'/").write(m_name).write("/").write(comment_id).write("/update',{comment:v}");
		if (m_provider != null) {
			r.db.lookupInt(new Select(m_items_table + "_id").from(m_name).whereIdEquals(comment_id), -1);
			w.write(",function(){_.$('#comment").write(comment_id).write("').innerHTML=v;Dialog.top().close()}");
		}
		w.write(")})</script>");
	}
}