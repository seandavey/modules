package social;

import java.sql.Types;
import java.time.LocalDate;

import app.Request;
import app.Site;
import attachments.Attachments;
import attachments.ViewDefWithAttachments;
import db.Categories;
import db.DBConnection;
import db.OneToMany;
import db.OrderBy;
import db.Select;
import db.View.Mode;
import db.ViewDef;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.DateColumn;
import db.column.TextAreaColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Dividers;
import pages.Page;
import util.Time;
import web.HTMLWriter;

public class Discussions extends NewsProvider {
	private final Attachments m_attachments = new Attachments("discussions");
	private Categories	m_categories;

	// --------------------------------------------------------------------------

	public
	Discussions() {
		super("discussions", "discussion");
		m_description = "Online discussions/forums; interfaces with NewsFeed module";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Discussions", this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("discussions", r).writeComponent();
				int v = r.getInt("v", 0);
				if (v != 0)
					r.w.js("window.addEventListener('load',function(){_.table.dialog_view(this,'discussions',context+'/Views/discussions/component?db_mode=READ_ONLY_FORM&db_key_value=" + v + "')})");
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable(m_table)
			.add(new JDBCColumn("close_date", Types.DATE))
			.add(new JDBCColumn("text", Types.VARCHAR))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		m_categories.addJDBCColumn(table_def);
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		m_categories.adjustTables(db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	commentsClosed(int id, DBConnection db) {
		LocalDate close_date = db.lookupDate(new Select("close_date").from("discussions").whereIdEquals(id));
		if (close_date != null && !close_date.isAfter(Time.newDate()))
			return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		return "started a discussion";
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return absoluteURL().set("v", item_id).toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		m_categories = new Categories(m_table, false)
			.setAllowEditing(true);
		super.init(was_added, db);
		m_attachments.init(db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("discussions")) {
			RecordOwnerAccessPolicy record_owner_access_policy = (RecordOwnerAccessPolicy)new RecordOwnerAccessPolicy().add().delete().edit().view();
			String categories_column_name = m_categories.getColumnName();
			return addHooks(new ViewDefWithAttachments(name, m_attachments)
				.addSection(new Dividers(categories_column_name, new OrderBy("(SELECT LOWER(text) FROM discussions_categories WHERE id=" + categories_column_name + ")", false, true)).setNullValueLabel(null))
				.setAccessPolicy(record_owner_access_policy)
				.setDefaultOrderBy("LOWER(title)")
				.setRecordName("Discussion", true)
				.setSelect(new Select(m_table + ".*,count(" + m_table + "_comments.id) AS comments,greatest(" + m_table + "._timestamp_,max(" + m_table + "_comments._timestamp_)) AS latest").from(m_table).leftJoin(m_table, m_table + "_comments").groupBy(m_table + ".id"))
				.setTimestampRecords(true)
				.setColumnNamesForm(m_add_new_items_to_news_feed ? new String[] { categories_column_name, "title", "text", "show_on_news_feed", "_owner_", "close_date" } : new String[] { categories_column_name, "title", "text", "_owner_" })
				.setColumnNamesTable(new String[] { "title", "comments", "latest" })
				.setColumn(m_categories.getColumn())
				.setColumn(new DateColumn("latest"))
				.setColumn(record_owner_access_policy.getColumn("posted by"))
				.setColumn(new Column("show_on_news_feed").setHideOnForms(Mode.READ_ONLY_FORM))
				.setColumn(new TextAreaColumn("text").setIsRequired(true).setIsRichText(true, false))
				.setColumn(new Column("_timestamp_").setDisplayName("when")))
				.setColumn(new Column("title").setIsRequired(true))
				.addRelationshipDef(new OneToMany(name + "_attachments"));
		}
		if (name.equals(m_table + "_attachments"))
			return m_attachments._newViewDef(name);

		ViewDef view_def = m_categories.newViewDef(name);
		if (view_def != null)
			return view_def;

		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeAddButton(Request r) {
		writeAddButton("chat-text", "discussions", r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		Object[] row = db.readRowObjects(new Select("title,text").from(m_table).whereIdEquals(item_id));
		w.write("<h5>").write((String)row[0]).write("</h5><div style=\"max-height:450px;overflow:auto;\">").writeWithLinks((String)row[1]).write("</div>");
		m_attachments.write(item_id, w, db);
	}
}
