package social;

import java.sql.Types;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;

import app.Request;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.Form;
import db.InsertHook;
import db.NameValuePairs;
import db.Rows;
import db.RowsSelect;
import db.Select;
import db.UpdateHook;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.column.Column;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import mail.Mail;
import util.Text;
import util.Time;
import web.HTMLWriter;

public abstract class NewsProvider extends SiteModule implements InsertHook, UpdateHook {
	@JSONField
	protected boolean	m_add_new_items_to_news_feed = true;
	@JSONField(after="leave blank to let user choose",type="mail list")
	private String		m_send_announcement_to;
	@JSONField
	protected boolean	m_send_emails_on_new_items = true;
	@JSONField(fields="m_send_announcement_to")
	protected boolean	m_support_announcing_new_items;
	protected String	m_table;
	protected String	m_type;

	//--------------------------------------------------------------------------

	public
	NewsProvider(String table, String type) {
		m_table = table;
		m_type = type;
	}

	//--------------------------------------------------------------------------

	public final boolean
	addColumnSendEmail(ArrayList<String> column_names_form, ViewDef view_def) {
		if (!m_support_announcing_new_items || m_send_announcement_to != null)
			return false;
		if (column_names_form != null)
			column_names_form.add("send to");
		view_def.setColumn(new Column("send to") {
			@Override
			public void
			writeInput(View v, Form f, boolean inline, Request r) {
				new RowsSelect("db_send_to", new Select("name").from("mail_lists").where("active").orderBy("name"), "name", null, r).setAllowNoSelection(true).write(r.w);
			}
		}.setDisplayName("send email to").setHideOnForms(Mode.EDIT_FORM, Mode.READ_ONLY_FORM));
		return true;
	}

	//--------------------------------------------------------------------------

	public final boolean
	addColumnShowOnNewsFeed(ArrayList<String> column_names_form, ViewDef view_def) {
		if (!m_add_new_items_to_news_feed)
			return false;
		if (column_names_form != null)
			column_names_form.add("show_on_news_feed");
		view_def.setColumn(new Column("show_on_news_feed").setHideOnForms(Mode.READ_ONLY_FORM));
		return true;
	}

	//--------------------------------------------------------------------------

	protected final ViewDef
	addHooks(ViewDef view_def) {
		view_def.addInsertHook(this);
		view_def.addUpdateHook(this);
		Comments.addFormHook(view_def);
		return view_def;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	addJDBCColumns(JDBCTable table_def) {
		super.addJDBCColumns(table_def);
		table_def.add(new JDBCColumn("show_on_news_feed", Types.BOOLEAN).setDefaultValue(true));
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		if (m_add_new_items_to_news_feed) {
			boolean show_on_news_feed = !nvp.containsName("show_on_news_feed") || nvp.getBoolean("show_on_news_feed");
			if (show_on_news_feed)
				News.news.addNewsItem(this, id, nvp, r);
		}
		if (m_support_announcing_new_items)
			sendAnnouncement(id, nvp.getInt("_owner_", 0), nvp.getDateTime("_timestamp_"), r);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		if (nvp.valueChanged("show_on_news_feed", m_table, id, r.db))
			if (m_add_new_items_to_news_feed && nvp.getBoolean("show_on_news_feed"))
				News.news.addNewsItem(this, id, nvp, r);
			else
				r.db.delete("news", "provider='" + m_name + "' AND item_id=" + id, false);
		return null;
	}

	//--------------------------------------------------------------------------

	public boolean
	canDelete() {
		return false;
	}

	//--------------------------------------------------------------------------

	public boolean
	canEdit() {
		return false;
	}

	//--------------------------------------------------------------------------

	public boolean
	canRemove() {
		return true;
	}

	//--------------------------------------------------------------------------

	protected boolean
	commentsClosed(int id, DBConnection db) {
		return false;
	}

	//--------------------------------------------------------------------------

	public abstract String
	getItemAction(int id, DBConnection db);

	//--------------------------------------------------------------------------

	public abstract String
	getItemURL(int item_id, Request r);

	//--------------------------------------------------------------------------

	public final String
	getTable() {
		return m_table;
	}

	//--------------------------------------------------------------------------

	public NewsItem
	newItem(Rows rows) {
		return new NewsItem(this, rows);
	}

	// --------------------------------------------------------------------------

	public NewsItem
	newItem(int item_id, int owner_id, LocalDateTime timestamp) {
		return new NewsItem(this, item_id, owner_id, timestamp);
	}

	// --------------------------------------------------------------------------

	private void
	sendAnnouncement(int item_id, int owner_id, LocalDateTime timestamp, Request r) {
		String address = m_send_announcement_to;
		if (address == null)
			address = r.getParameter("db_send_to");
		if (address == null || address.length() == 0)
			return;
		r.w.captureStart();
		newItem(item_id, owner_id, timestamp).write(null, true, r.w, r.db, r);
		String owner_email = Site.site.newPerson(owner_id, r.db).getEmail();
		new Mail(Text.capitalize(m_type) + " Added", r.w.captureEnd())
			.replyTo(owner_email)
			.from(address)
			.to(address)
			.send();
	}

	//--------------------------------------------------------------------------

	final void
	updateNewsItem(int item_id, boolean update_timestamp, Request r) {
		String where = "provider='" + m_name + "' AND item_id=" + item_id;
		if (r.db.exists("news", where)) {
			NameValuePairs nvp = new NameValuePairs();
			String timestamp = Time.formatter.getDateTime(Time.newDateTime());
			if (update_timestamp)
				nvp.set("_timestamp_", timestamp);
			nvp.set("last_update", timestamp);
			r.db.update("news", nvp, where);
		}
	}

	//--------------------------------------------------------------------------

	public void
	writeAddButton(Request r) {
	}

	//--------------------------------------------------------------------------

	protected final void
	writeAddButton(String icon, String view_def_name, Request r) {
		String record_name = Site.site.getViewDef(view_def_name, r.db).getRecordName();
		String on_click = "new Dialog({cancel:true,ok:{text:'add',click:function(){_.form.submit(Dialog.top().modal.querySelector('form'),{on_complete:function(){News.fetch_new();Dialog.top().close();},validate:true})}},title:'Add a " +
			record_name + "',url:context+'/Views/" + view_def_name + "/component?db_mode=" + View.Mode.ADD_FORM.toString() + "'})";
		r.w.addStyle("margin-right:10px")
			.setAttribute("title", "add " + record_name.toLowerCase())
			.ui.buttonIconOnClick(icon, on_click);
	}

	//--------------------------------------------------------------------------
	// return true to allow new comments to be added

	public abstract void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db);

	//--------------------------------------------------------------------------

	public void
	writeNewsItemEditForm(int item_id, int news_id, Request r) {
	}
}
