package social;

import java.time.LocalDateTime;

import app.Person;
import app.Request;
import app.Site;
import db.DBConnection;
import db.Rows;
import db.Select;
import util.Text;
import util.Time;
import web.HTMLWriter;

public class NewsItem {
	protected final int				m_item_id;
	protected final LocalDateTime	m_last_update;
	protected final int				m_news_id;
	protected final int				m_owner_id;
	protected final NewsProvider	m_provider;
	protected final LocalDateTime	m_timestamp;

	//--------------------------------------------------------------------------

	public
	NewsItem(NewsProvider provider, int item_id, int owner_id, LocalDateTime timestamp) {
		m_provider = provider;
		m_item_id = item_id;
		m_owner_id = owner_id;
		m_last_update = timestamp;
		m_news_id = 0;
		m_timestamp = timestamp;
	}

	//--------------------------------------------------------------------------

	public
	NewsItem(NewsProvider provider, Rows rows) {
		m_provider = provider;
		m_item_id = rows.getInt("item_id");
		m_last_update = rows.getTimestamp("last_update");
		m_news_id = rows.getInt("id");
		m_owner_id = rows.getInt("_owner_");
		m_timestamp = rows.getTimestamp("_timestamp_");
	}

	//--------------------------------------------------------------------------

	protected String
	getAction(DBConnection db) {
		return m_provider.getItemAction(m_item_id, db);
	}

	//--------------------------------------------------------------------------

	public boolean
	merge(NewsProvider provider, Rows rows) {
		return false;
	}

	//--------------------------------------------------------------------------

	public void
	write(LocalDateTime now, boolean for_mail, HTMLWriter w, DBConnection db, Request r) {
		w.write("<div id=\"ni").write(m_news_id).write("\" class=\"news_item\"><div>");
		writeHeader(now, for_mail, w, db, r);
		m_provider.writeNewsItem(m_item_id, for_mail, w, db);
		w.write("</div>");
		if (!for_mail)
			Comments.writeComments(m_provider.getTable(), m_item_id, r);
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	protected final void
	writeHeader(LocalDateTime now, boolean for_mail, HTMLWriter w, DBConnection db, Request r) {
		String[] person = db.readRow(new Select("first,last,picture").from("people").whereIdEquals(m_owner_id));
		w.write("<div class=\"news_item_header\">");
		if (!for_mail && person != null) {
			Person user = r.getUser();
			boolean owner = user != null && (m_owner_id == user.getId() || r.userIsAdmin());
			boolean can_delete = owner && m_provider.canDelete();
			boolean can_edit = owner && m_provider.canEdit();
			boolean can_remove = owner && m_provider.canRemove();
			if (can_delete || can_edit || can_remove) {
				w.setAttribute("onclick", "News.item_dropdown(this," + m_news_id + ",'" + m_provider.m_type + "'," + (can_edit ? "true," : "false,") + (can_delete ? "true," : "false,") + (can_remove ? "true" : "false") + ");return false;");
				w.tagOpen("span");
				w.ui.icon("chevron-down");
				w.tagClose();
			}
			if (person[2] != null)
				w.write("<img src=\"" + Site.context + "/people/" + m_owner_id + "/" + person[2] + "\" onclick=\"Person.popup(").write(m_owner_id).write(");return false;\" style=\"cursor:pointer\"/>");
		}
		w.write("<div style=\"display:inline-block;vertical-align:middle;\">")
			.write("<h5 style=\"color:inherit;\">");
		if (person != null) {
			w.addClass("news_item_person");
			if (!for_mail && r.getUser() != null)
				w.addStyle("cursor:pointer")
					.setOnClick("Person.popup(" + m_owner_id + ")");
			w.tag("span", Text.join(" ", person[0], person[1]))
				.space();
		}
		w.write(getAction(db));
		w.write("</h5>");
		if (!for_mail && m_timestamp != null)
			w.write(Time.formatter.getTimeAgo(m_timestamp, now));
		w.write("</div></div>");
	}
}
