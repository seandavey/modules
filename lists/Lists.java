package lists;

import java.sql.Types;

import app.Module;
import app.Request;
import app.Site;
import bootstrap5.ButtonNav;
import bootstrap5.ButtonNav.Panel;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.View;
import db.ViewDef;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import pages.Page;

public class Lists extends Module {
	private String m_one_table;

	//--------------------------------------------------------------------------

	public
	Lists(String one_table) {
		super();
		m_one_table = one_table;
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	addPages(DBConnection db) {
		Site.site.addUserDropdownItem(new Page("Edit Lists", this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("edit lists", r).writeComponent();
			}
		}.addRole("administrator"));
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("lists")
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("record_name", Types.VARCHAR));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		int list_id = Integer.parseInt(path_segments[0]);
		Site.site.newView("list" + list_id, r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("edit lists"))
			return new ViewDef(name)
				.setFrom("lists");
		if (name.equals("lists"))
			return new ViewDef(name);
		if (name.startsWith("list")) {
			DBConnection db = new DBConnection();
			ViewDef view_def = new ViewDef(name)
				.setColumnHeadsCanWrap(true)
				.setRecordName(db.lookupString(new Select("record_name").from("lists").whereIdEquals(name.substring(4))), true)
				.setShowDoneLink(true)
				.setShowTableColumnPicker(true, null)
				.setColumnNamesForm() // get from mods in _settings_
				.setColumnNamesTable();
			db.close();
			return view_def;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public final void
	write(int one_id, Request r) {
		Select query = new Select("id,name").from("lists").orderBy("name");
		if (m_one_table != null)
			query.whereEquals(m_one_table + "_id", one_id);
		ButtonNav bn = new ButtonNav("button_nav", m_one_table != null ? 3 : 2, r);
		bn.setOutline(true);
		try (Rows rows = new Rows(query, r.db)) {
			while (rows.next())
				bn.add(new Panel(rows.getString(2), null, apiURL(rows.getString(1))));
		}
		bn.close();
	}
}
