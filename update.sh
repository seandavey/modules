for dir in */; do
	d=${dir%*/}
	d=${d##*/}
	if [ $d == "WEB-INF" ]; then
		continue
	fi
#	if [ -e "$d/css" ]; then
#	fi
	if [ -e "$d/js" ]; then
		built=false
		for f in $d/js/*.js; do
			if [[ $f == *min.js ]]; then
				continue
			fi
			b="${f%.*}"
			if [ $f -nt $b-min.js ]; then
				esbuild "$b.js" --minify --target=es2020 > "$b-min.js"
				built=true
			fi
		done
		if [ $built = true ]; then
			../sites/modules_update $d js
		fi
	fi
done
