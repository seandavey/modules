package todos;

import java.util.ArrayList;
import java.util.Map;

import app.Module;
import app.Request;
import db.DBConnection;
import db.Filter;
import db.OrderBy;
import db.Rows;
import db.View;
import db.ViewDef;
import db.column.Column;
import db.column.LookupColumn;
import db.section.Dividers;
import web.HTMLWriter;

public class ToDos extends Module implements Filter {
	private boolean	m_assign_tasks;
	private boolean	m_group_by_project;
	private String	m_one_table;

	//--------------------------------------------------------------------------

	@Override
	public boolean
	accept(Rows rows, Request r) {
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		StringBuilder columns = new StringBuilder("task VARCHAR(60),notes VARCHAR");
		if (m_assign_tasks)
			columns.append(",assigned_to INT");
		if (m_group_by_project)
			columns.append(",project VARCHAR");
		if (m_one_table != null)
			db.createManyTable(m_one_table, "todos", columns.toString());
		else
			db.createTable("todos", columns.toString());
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("todos")) {
			ViewDef view_def = new ViewDef(name);
			view_def.setDefaultOrderBy("task");
			view_def.setRecordName("Task", true);
			ArrayList<String> column_names_form = new ArrayList<>();
			column_names_form.add("task");
			column_names_form.add("notes");
			if (m_group_by_project) {
				column_names_form.add("project");
				view_def.setColumn(new Column("project").setPostText("(optional)"));
				view_def.addSection(new Dividers("project", new OrderBy("project")));
			}
			if (m_assign_tasks) {
				column_names_form.add("assigned_to");
				view_def.setColumn(new LookupColumn("assigned_to", "people", "first,last").setFilter(this).setDefaultToUserId());
			}
			if (m_one_table != null) {
				column_names_form.add(m_one_table + "_id");
				view_def.setColumn(new Column(m_one_table + "_id").setDefaultToSessionAttribute().setIsHidden(true));
			}
			view_def.setColumnNamesForm(column_names_form);
			view_def.setColumnNamesTable("task");
			view_def.setColumn(new Column("task") {
				@Override
				public boolean
				writeValue(View v, Map<String,Object> data, Request r) {
					String notes = v.data.getString("notes");
					if (notes != null)
						r.w.write("<div onmouseenter=\"this.lastElementChild.style.display=''\" onmouseleave=\"this.lastElementChild.style.display='none'\">");
					r.w.write(v.data.getString("task"));
					if (notes != null) {
						r.w.write("<div style=\"background-color:white;padding:5px;font-size:smaller;display:none\">");
						r.w.write(HTMLWriter.breakNewlines(notes));
						r.w.write("</div></div>");
					}
					return true;
				}
			});
			return view_def;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public ToDos
	setAssignTasks(boolean assign_tasks) {
		m_assign_tasks = assign_tasks;
		return this;
	}

	//--------------------------------------------------------------------------

	public ToDos
	setGroupByProject(boolean group_by_project) {
		m_group_by_project = group_by_project;
		return this;
	}

	//--------------------------------------------------------------------------

	public ToDos
	setOneTable(String one_table) {
		m_one_table = one_table;
		return this;
	}
}
