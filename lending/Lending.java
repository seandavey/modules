package lending;

import java.sql.Types;

import app.Request;
import app.Site;
import app.SiteModule;
import db.Categories;
import db.DBConnection;
import db.ViewDef;
import db.access.RecordOwnerAccessPolicy;
import db.column.PersonColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Tabs;
import pages.Page;

public class Lending extends SiteModule {
	private Categories	m_categories = new Categories("lending_items", false);

	//--------------------------------------------------------------------------

	@Override
	public void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Lending", this, false) {
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("lending_items", r).writeComponent();
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("lending_items")
			.add(new JDBCColumn("item", Types.VARCHAR))
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("leant_to", "people"))
			.add(new JDBCColumn("date_borrowed", Types.DATE))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		m_categories.addJDBCColumn(table_def);
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, true, db);
		m_categories.adjustTables(db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("lending_items"))
			return new ViewDef(name)
				.addSection(new Tabs(m_categories.getColumnName(), m_categories.getOrderBy()))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit().view())
				.setDefaultOrderBy("lower(item)")
				.setRecordName("Item", true)
				.setColumnNamesForm("item", "description", m_categories.getColumnName(), "leant_to", "date_borrowed", "_owner_", "notes")
				.setColumnNamesTable("item", "_owner_", "date_borrowed")
				.setColumn(m_categories.getColumn())
				.setColumn(new PersonColumn("leant_to", false))
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId());
		ViewDef view_def = m_categories.newViewDef(name);
		if (view_def != null)
			return view_def;
		return null;
	}
}
