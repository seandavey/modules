package surveys;

import java.time.LocalDate;

import app.Request;
import db.ViewDef;
import util.Time;

public class View extends db.View {
	public
	View(ViewDef view_def, Request r) {
		super(view_def, r);
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	writeRow(String[] columns) {
		m_w.write("<div style=\"min-width:600px;padding-bottom:10px;\"><span style=\"float:right\">");
		writeButtons();
		m_w.write("</span>")
			.h3(data.getString("title"));
		String description = data.getString("description");
		if (description != null)
			m_w.write(description)
			.br();
		LocalDate close_date = data.getDate("close_date");
		if (!data.getBoolean("closed") && (close_date == null || close_date.isAfter(Time.newDate()))) {
			writeColumnHTML("take");
			m_w.write(" - ");
		}
		writeColumnHTML("show_answers");
		m_w.write("</div>");
		++m_num_rows;
	}
}
