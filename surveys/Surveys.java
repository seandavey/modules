package surveys;

import java.sql.Types;

import app.Person;
import app.Request;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.Form;
import db.LinkValueRenderer;
import db.NameValuePairs;
import db.OneToMany;
import db.Reorderable;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.ColumnBase;
import db.column.OptionsColumn;
import db.column.PersonColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import pages.Page;
import util.Text;

public class Surveys extends SiteModule {
	public
	Surveys() {
		m_description = "Create and take surveys with several types of questions and collated answers";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Surveys", this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("surveys", r).writeComponent();
				r.w.js("""
					const surveys = {
						update_type() {
							var i = _.$('#type').selectedIndex
							var pre = _.$('#pre_text_row')
							pre.style.display = i > 5 ? 'none' : ''
							var post = _.$('#post_text_row')
							post.style.display = i > 5 ? 'none' : ''
							_.$('#required_row').style.display = i > 5 ? 'none' : ''
							_.$('#text_row').style.display = i > 5 ? 'none' : ''
							var tdr = _.$('#type_data_row')
							tdr.style.display = i == 3 || i == 4 || i == 5 || i == 7 ? '' : 'none'
							var l = tdr.querySelector('label')
							l.innerText = i == 3 || i == 4 ? 'choices' : i == 5 ? 'number of options' : i == 7 ? 'text/HTML' : ''
							if (tdr.lastChild.children.length == 2) {
								var n = tdr.lastChild.firstElementChild
								n.name = i == 5 ? 'type_data' : ''
								n.style.display = i == 5 ? '' : 'none'
								if (!n.value)
									n.value = 5
								if (i == 5) {
									n.setAttribute('required', 'yes')
									n.setAttribute('data-title', l.innerText)
								} else
									n.removeAttribute('required')
								var d = tdr.lastChild.lastElementChild
								d.name = i == 5 ? '' : 'type_data'
								d.style.display = i == 5 ? 'none' : ''
							}
							_.rich_text.destroy(surveys.el)
							if (i == 7)
								surveys.el = _.rich_text.create(tdr.querySelector('textarea'),{toolbar:'full'})
						}
					}""");
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("surveys")
			.add(new JDBCColumn("closed", Types.BOOLEAN))
			.add(new JDBCColumn("close_date", Types.DATE))
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("number_questions", Types.BOOLEAN))
			.add(new JDBCColumn("show_answers", Types.BOOLEAN))
			.add(new JDBCColumn("show_names", Types.BOOLEAN))
			.add(new JDBCColumn("title", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, true, db);
		table_def = new JDBCTable("survey_items")
			.add(new JDBCColumn("surveys"))
			.add(new JDBCColumn("type", Types.VARCHAR))
			.add(new JDBCColumn("type_data", Types.VARCHAR))
			.add(new JDBCColumn("text", Types.VARCHAR))
			.add(new JDBCColumn("pre_text", Types.VARCHAR))
			.add(new JDBCColumn("post_text", Types.VARCHAR))
			.add(new JDBCColumn("required", Types.BOOLEAN))
			.add(new JDBCColumn("_order_", Types.INTEGER));
		JDBCTable.adjustTable(table_def, true, true, db);
		table_def = new JDBCTable("survey_answers")
			.add(new JDBCColumn("survey_items"))
			.add(new JDBCColumn("answer", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		JDBCTable.adjustTable(table_def, true, true, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
//		int csv_survey_id = r.getInt("csv", 0);
//		if (csv_survey_id != 0) {
//			new Survey(csv_survey_id, r.db).writeCSV(r);
//			return true;
//		}

		int id = r.getPathSegmentInt(1);
		if (id == 0)
			return false;
		String segment_two = r.getPathSegment(2);
		if (segment_two != null)
			switch(segment_two) {
			case "answers":
				new Survey(id, r.db).writeAnswers(r);
				return true;
			case "take":
				r.setSessionInt("survey", id);
				new Survey(id, r.db).setPageNum(0, r).writeForm(r);
				return true;
			}
		Survey survey = new Survey(id, r.db);
		int page_num = survey.getPageNum(r);
		if (page_num != -1)
			survey.setPageNum(page_num + 1, r);
		survey.writeForm(r);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		int id = r.getPathSegmentInt(1);
		if (id == 0)
			return false;
		int user_id = r.getUser().getId();
		NameValuePairs nvp = new NameValuePairs();
		nvp.set("_owner_", user_id);
		Rows items = new Rows(new Select("id,type").from("survey_items").whereEquals("surveys_id", r.getInt("surveys_id", 0)).orderBy("_order_"), r.db);
		while (items.next()) {
			int item_id = items.getInt(1);
			String type = items.getString(2);
			if ("Multiple Choice".equals(type)) {
				String[] values = r.getParameterValues("a" + item_id);
				nvp.set("answer", Text.join("\n", values));
			} else {
				String value = r.getParameter("a" + item_id);
				if (value != null)
					nvp.set("answer", value);
			}
			nvp.set("survey_items_id", item_id);
			r.db.updateOrInsert("survey_answers", nvp, "survey_items_id=" + item_id + " AND _owner_=" + user_id);
		}
		items.close();
		return true;
	}

	//----------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("survey_items"))
			return new ViewDef(name)
				.setAfterForm("<script>surveys.update_type()</script>")
				.setRecordName("Item", true)
				.setReorderable(new Reorderable())
				.setColumnNamesForm(new String[] { "type", "type_data", "text", "pre_text", "post_text", "required" })
				.setColumnNamesTable(new String[] { "type", "text", "required" })
				.setColumn(new Column("pre_text").setDisplayName("text before input"))
				.setColumn(new Column("post_text").setDisplayName("text after input"))
				.setColumn(new Column("text").setDisplayName("question text"))
				.setColumn(new OptionsColumn("type", "Short Answer", "Long Answer", "Number", "Multiple Choice", "Single Choice", "Rating", "Divider", /*"Page Break",*/ "Text/HTML").setOnChange("surveys.update_type()"))
				.setColumn(new Column("type_data"){
					@Override
					public void
					writeInput(View v, Form f, boolean inline, Request r) {
						if (v.getMode() == Mode.ADD_FORM) {
							r.w.addStyle("display:none")
								.setAttribute("required", "yes")
								.numberInput(null, "2", null, "1", getValue(v, r), false);
							super.writeInput(v, f, inline, r);
						} else if (v.data != null && "Rating".equals(v.data.getString("type")))
							r.w.setAttribute("data-title", "number of options")
								.setAttribute("required", "yes")
								.numberInput(m_name, "2", null, "1", getValue(v, r), false);
						else
							super.writeInput(v, f, inline, r);
					}
				}.setDisplayName(""));
		if (name.equals("surveys"))
			return new ViewDef(name)
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit())
				.setDefaultOrderBy("title")
				.setRecordName("Survey", true)
//				.setShowColumnHeads(false)
				.setViewClass(surveys.View.class)
				.setColumnNamesForm(new String[] { "title", "description", "number_questions", "show_answers", "show_names", "closed", "close_date", "_owner_" })
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(new Column("close_date").setHelpText("if set, the survey will automatically close on this date"))
				.setColumn( new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by").setViewRole("admin"))
				.setColumn(new Column("show_answers")
					.setDisplayName("allow participants to see each other's answers")
					.setValueRenderer(new LinkValueRenderer("Surveys/$(id)/answers", "view answers"){
						@Override
						public void
						writeValue(View v, ColumnBase<?> column, Request r) {
							Person user = r.getUser();
							if (user != null && v.data.getInt("_owner_") == user.getId() || v.data.getBoolean("show_answers"))
								super.writeValue(v, column, r);
						}
					}.setUseDialog("$(title)", null), false))
				.setColumn(new Column("show_names") {
					@Override
					public boolean
					isReadOnly(View v, Request r) {
						return v.getMode().equals(Mode.EDIT_FORM) && !v.data.getBoolean("show_names") && !r.userIsAdministrator();
					}
				}.setDisplayName("show participants' names with their answers").setHelpText("can't be turned on once it has been turned off"))
				.setColumn(new Column("take").setValueRenderer(new LinkValueRenderer("Surveys/$(id)/take", "take survey").setUseDialog("$(title)", "Save"), false))
				.setColumn(new Column("title").setIsRequired(true))
				.addRelationshipDef(new OneToMany("survey_items"));
		return null;
	}
}
