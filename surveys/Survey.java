package surveys;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import app.Request;
import app.Site;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.object.DBField;
import db.object.DBObject;
import ui.Form;
import ui.FormBase.Location;
import ui.Select.Type;
import util.Text;
import web.HTMLWriter;

public class Survey {
	@DBField
	private String		m_description;
	private Form		m_form;
	private int			m_id;
	@DBField
	private boolean		m_number_questions;
	@DBField
	private boolean		m_show_answers;
	@DBField
	private boolean		m_show_names;
	@DBField
	private String		m_title;

	//----------------------------------------------------------------------

	Survey(int surveys_id, DBConnection db) {
		m_id = surveys_id;
		DBObject.load(this, "surveys", surveys_id, db);
	}

	//----------------------------------------------------------------------

	private void
	formClose(Rows items, Request r) {
		HTMLWriter w = r.w;
		w.tagClose();
		if (!items.isAfterLast()) {
			w.write("<p style=\"text-align:center\">");
			m_form.setSubmitText("Next").writeSubmit();
			w.write("</p>");
		} else
			setPageNum(-1, r);
		m_form.close();
		if (getPageNum(r) == 0)
			w.write("</td></tr></table>");
	}

	//----------------------------------------------------------------------

	private void
	formOpen(Request r) {
		HTMLWriter w = r.w;
		if (getPageNum(r) == 0) {
			w.write("<table style=\"margin: 0px auto;width:600px;\"><tr><td>");
			if (m_description != null)
				w.write("<p style=\"width:600px\">").write(m_description).write("</p>");
			w.write("<p style=\"width:600px\">");
			if (r.db.exists(new Select().from("survey_answers").whereEquals("_owner_", r.getUser().getId())))
				w.write("These are your recorded responses. They can be edited until the end of the survey. ");
			else
				w.write("After you save your answers you may edit them again later. ");
			if (!m_show_answers)
				w.write("Your answers will not be viewable by others. ");
			else {
				w.write("Your answers will be viewable by others. ");
				if (m_show_names)
					w.write("Your name will be shown with your answers, you do not need to type it. ");
				else
					w.write("Your answers are anonymous, your name will not be shown with your answers. ");
			}
			w.write("</p>");
			w.write("</td></tr><tr><td id=\"survey_page\" survey_id=\"").write(m_id).write("\">");
		}
		m_form = new Form(null, Site.context + "/Surveys/" + m_id, r.w).setButtonsLocation(Location.NONE);
		m_form.open();
		w.hiddenInput("surveys_id", Integer.toString(m_id));
		w.hiddenInput("_owner_", Integer.toString(r.getUser().getId()));
		w.addStyle("padding:0 10px");
		w.tagOpen("div");
	}

	//----------------------------------------------------------------------

	final int
	getPageNum(Request r) {
		Integer i = (Integer)r.getSessionAttribute("survey_page");
		if (i != null)
			return i.intValue();
		return 0;
	}

	//----------------------------------------------------------------------

	final Survey
	setPageNum(int page_num, Request r) {
		r.setSessionInt("survey" + m_id + "_page", page_num);
		return this;
	}

	//----------------------------------------------------------------------

	final void
	writeAnswers(Request r) {
		HTMLWriter w = r.w;
		Rows rows = new Rows(new Select("title,show_names,number_questions").from("surveys").whereIdEquals(m_id), r.db);
		rows.next();
		w.h3(rows.getString(1));
		boolean show_names = rows.getBoolean(2) || r.userIsAdministrator();
		boolean number_questions = rows.getBoolean(3);
		rows.close();

		String columns = "survey_items.id,text,type,type_data,answer";
		if (show_names)
			columns += ",first,last";
		Select query = new Select(columns).from("survey_items").join("survey_items", "survey_answers").whereEquals("surveys_id", m_id).andWhere("type != 'Divider' AND type != 'Text/HTML' AND type != 'Page Break'").orderBy("_order_");
		if (show_names)
			query.joinOn("people", "_owner_=people.id");
		rows = new Rows(query, r.db);
		int question_num = 0;
		int previous_item = 0;
		Map<String,Integer> choices = new TreeMap<>();
		int[] ratings = new int[5];
		String type = null;
		while (rows.next()) {
			int item_id = rows.getInt(1);
			if (item_id != previous_item) {
				if (previous_item != 0)
					writeTotals(type, choices, ratings, w);
				choices.clear();
				Arrays.fill(ratings, 0);
				if (question_num > 0)
					w.hr();
				w.write("<p>");
				if (number_questions)
					w.write(++question_num).write(". ");
				w.write(rows.getString(2)).br();
			}
			String answer = rows.getString(5);
			type = rows.getString(3);
			if (type.equals("Multiple Choice")) {
				if (answer != null) {
					String[] answers = answer.split("\n");
					for (String a : answers) {
						Integer i = choices.get(a);
						if (i == null)
							choices.put(a, 1);
						else
							choices.put(a, i.intValue() + 1);
					}
					answer = HTMLWriter.breakNewlines(answer);
				}
			} else if (type.equals("Rating")) {
				if (answer != null)
					ratings[Integer.parseInt(answer) - 1]++;
			} else if (type.equals("Single Choice"))
				if (answer != null) {
					Integer i = choices.get(answer);
					if (i == null)
						choices.put(answer, 1);
					else
						choices.put(answer, i.intValue() + 1);
				}
			if (answer != null && answer.length() > 0) {
				if (show_names)
					w.write("<span style=\"font-weight:bold\">").write(rows.getString("first")).space().write(rows.getString("last")).write("</span><br />");
				w.write(answer).br();
			}
			previous_item = item_id;
		}
		rows.close();
		if (previous_item != 0)
			writeTotals(type, choices, ratings, w);
	}

	//----------------------------------------------------------------------

	final void
	writeForm(Request r) {
		Rows items = new Rows(new Select("*").from("survey_items").whereEquals("surveys_id", m_id).orderBy("_order_"), r.db);
		if (!items.isBeforeFirst()) {
			items.close();
			r.w.write("There are no questions for this survey.");
			return;
		}
		formOpen(r);
		int question_num = 1;
		int page_num = getPageNum(r);
		int num_pages = 0;
		while (num_pages < page_num && items.next()) {
			String type = items.getString("type");
			if (type.equals("Page Break"))
				num_pages++;
			else if (!type.equals("Divider") && !type.equals("Text/HTML"))
				question_num++;
		}
		while (items.next()) {
			String type = items.getString("type");
			if ("Page Break".equals(type))
				break;
			writeItem(question_num, type, items, r);
			if (!type.equals("Divider") && !type.equals("Text/HTML"))
				question_num++;
		}
		formClose(items, r);
		items.close();
	}

	//----------------------------------------------------------------------

	private void
	writeItem(int question_num, String type, Rows items, Request r) {
		HTMLWriter w = r.w;
		switch (type) {
		case "Divider":
			w.hr();
			return;
		case "Text/HTML":
			w.write(items.getString("type_data"));
			return;
		}
		w.tagOpen("p");
		w.tagOpen("div");
		if (m_number_questions) {
			w.write(question_num);
			w.write(". ");
		}
		int id = items.getInt("id");
		String question = items.getString("text");
		if (question != null)
			w.write(question);
		w.tagClose();
		String name = "a" + id;
		String value = r.db.lookupString(new Select("answer").from("survey_answers").whereEquals("survey_items_id", id).andWhere("_owner_=" + r.getUser().getId()));
		String pre_text = items.getString("pre_text");
		if (pre_text != null)
			w.ui.helpText(pre_text);
		switch (type) {
		case "Long Answer":
			w.textArea(name, value);
			break;
		case "Multiple Choice":
			String[] choices = items.getString("type_data").replaceAll("\r", "").split("\n");
			for (String choice : choices)
				w.ui.checkbox(name, choice, choice, null, choice.equals(value), false, false);
			break;
		case "Number":
			w.numberInput(name, null, null, null, value, false);
			break;
		case "Rating":
			for (int i=1; i<=items.getInt("type_data"); i++) {
				String num = Integer.toString(i);
				w.ui.radioButtonInline(name, null, num, num, num.equals(value), null);
			}
			break;
		case "Short Answer":
			w.setAttribute("autocomplete", "off").textInput(name, 0, value);
			break;
		case "Single Choice":
			String options = items.getString("type_data");
			if (options == null) {
				r.w.alert("danger", "There are no choices defined for this question");
				break;
			}
			ui.Select select = new ui.Select(name, options.replaceAll("\r", "").split("\n")).setType(Type.RADIO);
			if (value != null)
				select.setSelectedOption(value, null);
			if (items.getBoolean("required"))
				select.setIsRequired(true);
			select.write(r.w);
			break;
		}
		String post_text = items.getString("post_text");
		if (post_text != null)
			w.ui.helpText(post_text);
		w.tagClose();
	}

	//----------------------------------------------------------------------

	private void
	writeTotals(String type, Map<String,Integer> choices, int[] ratings, HTMLWriter w) {
		if (type.equals("Multiple Choice") || type.equals("Single Choice")) {
			w.write("totals: ");
			boolean first = true;
			for (String key : choices.keySet()) {
				if (first)
					first = false;
				else
					w.write(", ");
				w.write(key);
				w.write(": ");
				w.write(choices.get(key).intValue());
			}
		} else if (type.equals("Rating")) {
			int num = 0;
			int total = 0;
			w.write("totals: ");
			for (int i=0; i<ratings.length; i++) {
				if (i > 0)
					w.write(", ");
				w.write(i + 1);
				w.write(": ");
				w.write(ratings[i]);
				num += ratings[i];
				total += ratings[i] * (i + 1);
			}
			w.write("<br />average: ");
			w.write(Text.two_digits_max.format((double)total / num));
		}
		w.write("</p>");

	}

}
