package calendar;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;

import app.Request;
import app.Site;
import app.Stringify;
import attachments.Attachments;
import attachments.ViewDefWithAttachments;
import db.FormHook;
import db.NameValuePairs;
import db.Relationship;
import db.Rows;
import db.Select;
import db.View;
import db.ViewDef;
import mail.Mail;
import util.Time;
import web.JS;

@Stringify
public class EventViewDef extends ViewDefWithAttachments implements FormHook {
	protected final EventProvider m_ep;

	// --------------------------------------------------------------------------

	public
	EventViewDef(EventProvider event_provider, Attachments attachments) {
		super(event_provider.getName(), attachments);
		m_ep = event_provider;
		addFormHook(this);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	afterForm(int id, Rows data, ViewDef view_def, View.Mode mode, boolean printer_friendly, Request r) {
		m_ep.writeAutomaticReminders(r);
		r.w.js("var r=_.$('#repeat');if(r)_.$('#end_date_row').style.display=r.selectedIndex==0?'none':null;");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		super.afterInsert(id, nvp, r);
		if (m_ep.m_support_multiple_locations)
			Locations.setEventLocations(m_ep.m_events_table, id, r);
		checkWaitingList(id, r);
		if (m_ep.supportsAnnouncingNewEvents())
			sendAnnouncement(id, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String,Object> previous_values, Request r) {
		super.afterUpdate(id, nvp, previous_values, r);
		if (m_ep.m_support_multiple_locations)
			Locations.setEventLocations(m_ep.m_events_table, id, r);
		checkWaitingList(id, r);
	}

	//--------------------------------------------------------------------------
	
	private void
	checkWaitingList(int id, Request r) {
		if (!m_ep.m_support_waiting_list)
			return;
		if (m_ep.overlaps(r.getDate("date"), r.getDate("end_date"), r.getTime("start_time"), r.getTime("end_time"), Locations.getLocations(m_ep.m_support_multiple_locations, r), id, r.db) != null)
			r.response.addMessage(m_ep.m_waiting_list_message);
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	getFormDeleteOnClick(Rows row, Relationship relationship, boolean dialog, Request r) {
		StringBuilder s = new StringBuilder("Calendar.$().confirm_delete(").append(JS.string(getName())).append(",").append(JS.string(getRecordName())).append(",").append(row.getString("id"));
		if (m_ep.m_events_can_repeat && m_ep.repeatingEventsCanBeSplit() && !"never".equals(row.getString("repeat")))
			s.append(",true");
		s.append(')');
		return s.toString();
	}

	// --------------------------------------------------------------------------

	protected void
	sendAnnouncement(int id, Request r) {
		String address = m_ep.getSendAnnouncementTo();
		if (address == null)
			address = r.getParameter("db_send_to");
		if (address == null || address.length() == 0)
			return;
		Rows rows = new Rows(new Select("*").from(m_ep.m_events_table).whereIdEquals(id), r.db);
		if (!rows.next()) {
			rows.close();
			return;
		}
		Event event = m_ep.loadEvent(rows);
		rows.close();

		String calendar = m_ep.getDisplayName(r);
		StringBuilder message = new StringBuilder("<p>An event has been added to the ").append(calendar);
		if (!calendar.endsWith("alendar"))
			message.append(" Calendar");
		message.append(" with the following details:</p>date: ").append(Time.formatter.getDateMedium(event.getStartDate())).append("<br />");
		if (event.isRepeating())
			message.append("repeating: ").append(event.getRepeat()).append("<br />");
		event.appendItem("time", true, false, false, null, message, r);
		message.append("<br />");
		event.appendItem("location", true, false, false, null, message, r);
		message.append("<br />");
		event.appendItem("event", true, false, false, null, message, r);
		message.append("<br />");
		event.appendItem("notes", true, false, false, null, message, r);
		message.append("<br />");
		event.appendItem("posted by", true, false, false, null, message, r);
		message.append("<br />");
		message.append("<p>Click <a href=\"")
			.append(Site.site.absoluteURL("Calendars").set("edit", "'" + calendar + "'," + id))
			.append("\">here</a> to view the event on the website.</p>");
		new Mail(calendar + " Announcement", message.toString())
			.to(address)
			.send();
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	validate(int id, Request r) {
		String error = super.validate(id, r);
		if (error != null)
			return error;

		LocalTime start_time = r.getTime("start_time");
		LocalTime end_time = r.getTime("end_time");
		error = m_ep.validateTime(start_time, end_time);
		if (error != null)
			return error;

		if (m_ep.m_check_overlaps && !m_ep.m_support_waiting_list) {
			String repeat = r.getParameter("repeat");
			if (repeat != null && !repeat.equals("never")) // can't check repeating events
				return null;
			LocalDate date = r.getDate("date");
			LocalDate end_date = r.getDate("end_date");
			EventProvider ep = m_ep.overlaps(date, end_date, start_time, end_time, Locations.getLocations(m_ep.m_support_multiple_locations, r), id, r.db);
			if (ep != null) {
				String calendar = ep.getDisplayName(r);
				if (!calendar.endsWith("alendar"))
					calendar += " Calendar";
				return "There is another event on the " + calendar + " that overlaps with this one.";
			}
		}

		return null;
	}
}
