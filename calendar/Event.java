package calendar;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.TreeMap;

import org.jsoup.Jsoup;

import app.Request;
import app.Site;
import biweekly.component.VEvent;
import biweekly.property.RawProperty;
import biweekly.util.DayOfWeek;
import biweekly.util.Duration;
import biweekly.util.Frequency;
import biweekly.util.Recurrence;
import db.DBConnection;
import db.Rows;
import db.Select;
import ui.SelectOption;
import util.Text;
import util.Time;
import web.HTMLWriter;

public class Event implements Cloneable, Comparable<Event> {
	private Category				m_category;
	private String					m_color;
	protected LocalDate				m_end_date;
	protected LocalTime				m_end_time;
	private Map<LocalDate,Event>	m_exceptions;
	protected String				m_event_text;
	protected final EventProvider	m_ep;
	protected int					m_id;
	private SelectOption[]			m_locations;
	protected String				m_notes;
	private String					m_other_locations;
	private EventProvider			m_overlaps_with;
	protected int					m_owner;
	private String					m_repeat;
	protected LocalDate				m_start_date;
	protected LocalTime				m_start_time;
	protected LocalDateTime			m_timestamp;
	private boolean					m_waiting_list;

	//--------------------------------------------------------------------------

	public
	Event(EventProvider ep, LocalDate start, String event_text, int id) {
		m_ep = ep;
		m_start_date = start;
		m_event_text = event_text;
		m_id = id;
		if (ep.m_calendar_location_id != 0)
			m_locations = new SelectOption[] { Locations.getOptionByValue(ep.m_calendar_location_id) };
	}

	//--------------------------------------------------------------------------

	public
	Event(EventProvider event_provider, Rows rows) {
		m_ep = event_provider;
		load(rows);
	}

	//--------------------------------------------------------------------------

	public final void
	addException(Event exception) {
		if (m_exceptions == null)
			m_exceptions = new TreeMap<>();
		m_exceptions.put(exception.getStartDate(), exception);
	}

	//--------------------------------------------------------------------------

	public void
	adjustTimezone(ZoneId zone_id) {
		if (m_start_time != null) {
			ZonedDateTime zoned_date_time = ZonedDateTime.of(m_start_date, m_start_time, Time.zone_id);
			zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
			m_start_date = zoned_date_time.toLocalDate();
			m_start_time = zoned_date_time.toLocalTime();
		}
		if (m_end_time != null) {
			ZonedDateTime zoned_date_time = ZonedDateTime.of(m_start_date, m_end_time, Time.zone_id);
			zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
			m_end_time = zoned_date_time.toLocalTime();
		}
		if (m_end_date != null && m_start_time != null) {
			ZonedDateTime zoned_date_time = ZonedDateTime.of(m_end_date, m_start_time, Time.zone_id);
			zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
			m_end_date = zoned_date_time.toLocalDate();
		}
	}

	//--------------------------------------------------------------------------

	protected void
	appendEventHTML(LocalDate date, StringBuilder s, boolean black_text, Request r) {
		String[] items = m_ep.getDisplayItems();
		if (items != null)
			for (String item : items)
				appendItem(item, black_text, true, false, date, s, r);
		if (m_overlaps_with != null) {
			String calendar = m_overlaps_with.getDisplayName(r);
			s.append("<span style=\"font-size:smaller;\">this event ");
			if (m_waiting_list)
				s.append("is on the waiting list since it ");
			s.append("overlaps with an event on the ").append(calendar);
			if (!calendar.endsWith("alendar"))
				s.append(" Calendar");
			s.append("</span>");
		}
	}

	//--------------------------------------------------------------------------

	protected final void
	appendItem(String label, String item, boolean div, String span_class, boolean black_text, boolean html, boolean ics, StringBuilder s) {
		if (!html && s.length() > 0)
			s.append(ics ? " " : "\n");
		boolean show_label = label != null && !ics && (!html && m_ep.m_tooltip_item_labels || html && m_ep.m_display_item_labels);
		if (html && (div || show_label))
			s.append("<div>");
		if (show_label)
			if (html)
				s.append("<span style=\"color:").append(black_text ? "#555555" : "#cccccc").append(";text-transform:capitalize\">").append(label).append(": </span>");
			else
				s.append(label).append(": ");
		else if (html && span_class != null) {
			s.append("<span class=\"").append(span_class);
			if (!"event".equals(label))
				s.append("\" style=\"color:").append(black_text ? "#555555" : "#cccccc");
			s.append("\">");
		}
		if (item != null)
			if (item.startsWith("<p>") && item.endsWith("</p>"))
				s.append(item.substring(3, item.length() - 4));
			else
				s.append(item);
		if (html) {
			if (span_class != null)
				s.append("</span>");
			if (div || show_label)
				s.append("</div>");
		}
	}

	//--------------------------------------------------------------------------

	protected void
	appendItem(String item, boolean black_text, boolean html, boolean ics, LocalDate date, StringBuilder s, Request r) {
		switch(item) {
		case "category":
			if (m_category != null)
				appendItem(item, m_category.getText(), false, "calendar_category", black_text, html, ics, s);
			return;
		case "event":
			String event_text = getEventText();
			if (event_text != null)
				appendItem(item, HTMLWriter.makeLinks(event_text), false, "calendar_event_text", black_text, html, ics, s);
			return;
		case "location":
			String location = getLocation();
			if (location != null)
				appendItem(item, location, false, "calendar_location", black_text, html, ics, s);
			return;
		case "notes":
			if (m_notes != null)
				appendItem(item, m_notes, false, "calendar_notes", black_text, html, ics, s);
			return;
		case "posted by":
			appendItem(item, Site.site.lookupName(m_owner, r.db), true, "calendar_posted_by", black_text, html, ics, s);
			return;
		case "time":
			if (m_start_time != null && !ics)
				appendItem(item, Time.formatter.getTimeRange(m_start_time, m_end_time), false, "calendar_time", black_text, html, ics, s);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	compareTo(Event event) {
		int relation = m_start_date.compareTo(event.m_start_date);
		if (relation == 0)
			relation = Time.compareTo(m_start_time, event.m_start_time);
		if (relation == 0 && m_timestamp != null && event.m_timestamp != null)
			relation = m_timestamp.compareTo(event.m_timestamp);
		return relation;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	equals(Object object) {
		if (!(object instanceof Event))
			return false;
		Event event = (Event)object;
		if (!m_start_date.equals(event.m_start_date))
			return false;
		if (!Site.areEqual(m_end_date, event.m_end_date))
			return false;
		if (!Site.areEqual(m_start_time, event.m_start_time))
			return false;
		if (!Site.areEqual(m_end_time, event.m_end_time))
			return false;
		if (!Site.areEqual(m_event_text, event.m_event_text))
			return false;
		return m_id == event.m_id;
	}

	//--------------------------------------------------------------------------

	public final SelectOption
	getCategory() {
		return m_category;
	}

	//--------------------------------------------------------------------------

	public final String
	getColor() {
		if (isOverlapping())
			return "#FFF";
		if (m_color != null)
			return m_color;
		if (m_ep.m_events_have_category && m_category != null) {
			String color = m_category.getColor();
			if (color != null)
				return color;
		}
		return m_ep.getColor();
	}

	//--------------------------------------------------------------------------

	public final LocalDate
	getEndDate() {
		return m_end_date;
	}

	//--------------------------------------------------------------------------

	public final LocalTime
	getEndTime() {
		return m_end_time;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	getEventProvider() {
		return m_ep;
	}

	//--------------------------------------------------------------------------

	public String
	getEventText() {
		return m_event_text;
	}

	//--------------------------------------------------------------------------

	public final int
	getID() {
		return m_id;
	}

	//--------------------------------------------------------------------------
	// return length in minutes or -1 if start and/or end time is not set

	public final int
	getLength() {
		if (m_start_time != null && m_end_time != null)
			return (int)ChronoUnit.MINUTES.between(m_start_time, m_end_time);
		return -1;
	}

	//--------------------------------------------------------------------------

	public final String
	getLocation() {
		if (m_locations == null && m_other_locations == null)
			return null;
		StringBuilder sb = new StringBuilder();
		if (m_locations != null)
			for (SelectOption location : m_locations)
				if (location != null) {
					if (sb.length() > 0)
						sb.append(", ");
					sb.append(location.getText());
				}
		if (m_other_locations != null) {
			if (sb.length() > 0)
				sb.append(", ");
			sb.append(m_other_locations);
		}
		if (sb.length() == 0)
			return null;
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	public final String
	getNotes() {
		return m_notes;
	}

	//--------------------------------------------------------------------------

	public final int
	getOwner() {
		return m_owner;
	}

	//--------------------------------------------------------------------------

	public final String
	getRepeat() {
		return m_repeat;
	}

	//--------------------------------------------------------------------------
	// return clone of repeating event for particular date

	public final Event
	getSingleEvent(LocalDate date) {
		Event event = null;
		if (m_exceptions != null) {
			event = m_exceptions.get(date);
			if (event != null) {
				if ("deleted".equals(m_ep.m_events_have_event ? event.m_event_text : event.m_notes))
					return null;
				return event;
			}
		}
		try {
			event = (Event)clone();
//			event.setRepeat(null);
			event.m_start_date = date;
			return event;
		} catch (CloneNotSupportedException e) {
		}
		return event;
	}

	//--------------------------------------------------------------------------

	public final LocalDate
	getStartDate() {
		return m_start_date;
	}

	//--------------------------------------------------------------------------

	public final LocalTime
	getStartTime() {
		return m_start_time;
	}

	//--------------------------------------------------------------------------

	public final String
	getTooltip(LocalDate date, Request r) {
		String[] tooltip_items = m_ep.getTooltipItems();
		if (tooltip_items == null)
			return /*r.userIsAdministrator() ? "id: " + m_id :*/ null;
		StringBuilder s = new StringBuilder();
//		if (r.userIsAdministrator())
//			s.append("id: " + m_id + "\n");
		for (String item : tooltip_items)
			appendItem(item, true, false, false, date, s, r);
		return s.toString();
	}

	//--------------------------------------------------------------------------

	public final boolean
	hasEndedBy(LocalDate date) {
		if (isRepeating()) {
			if (m_end_date == null)
				return false;
			return date.isAfter(m_end_date);
		}
		return date.isAfter(m_start_date);
	}

	//--------------------------------------------------------------------------

	final boolean
	hasLocation() {
		if (!m_ep.m_events_have_location)
			return false;
		return m_locations != null && m_locations.length > 0;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isOverlapping() {
		return m_overlaps_with != null;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isRepeating() {
		return m_repeat != null && !"never".equals(m_repeat);
	}

	//--------------------------------------------------------------------------

	protected void
	load(Rows rows) {
		for (int i=1,n=rows.getColumnCount(); i<=n; i++) {
			String column = rows.getColumnName(i);
			switch(column) {
			case "color":
				setColor(rows.getString(i));
				continue;
			case "date":
				m_start_date = rows.getDate(i);
				continue;
			case "end_date":
				m_end_date = rows.getDate(i);
				continue;
			case "end_time":
				m_end_time = rows.getTime(i);
				continue;
			case "event":
				m_event_text = rows.getString(i);
				continue;
			case "id":
				m_id = rows.getInt(i);
				continue;
			case "notes":
				m_notes = rows.getString(i);
				continue;
			case "other_locations":
				m_other_locations = rows.getString(i);
				continue;
			case "_owner_":
				m_owner = rows.getInt(i);
				continue;
			case "repeat":
				m_repeat = rows.getString(i);
				continue;
			case "start_time":
				m_start_time = rows.getTime(i);
				continue;
			case "_timestamp_":
				m_timestamp = rows.getTimestamp(i);
				continue;
			default:
				if (m_ep != null)
					if (column.equals(m_ep.getCategoryColumnName()))
						m_category = m_ep.getCategoryOption(rows);
			}
		}
		if (m_ep.m_events_have_location)
			m_locations = Locations.loadLocations(m_ep.m_events_table, m_id, m_ep.m_support_multiple_locations, rows);
	}

	//--------------------------------------------------------------------------

	public final boolean
	locationsMatch(Event event) {
		if (!m_ep.m_events_have_location || !event.m_ep.m_events_have_location)
			return false;
		if (m_locations == null || event.m_locations == null)
			return false;
		for (SelectOption one : m_locations)
			for (SelectOption two : event.m_locations)
				if (Site.areEqual(one, two))
					return true;
		return false;
	}

	//--------------------------------------------------------------------------

	public String[]
	lookupEmail(String email, DBConnection db) {
		if ("everyone".equals(email))
			return new String[] { db.lookupString(new Select("email").from("people").where("active AND email IS NOT NULL")) };
		if ("poster".equals(email))
			return new String[] { db.lookupString(new Select("email").from("people").whereIdEquals(m_owner)) };
		return new String[] { email };
	}

	//--------------------------------------------------------------------------

	public VEvent
	newVEvent(Request r) {
		VEvent vevent = new VEvent();
		StringBuilder sb = new StringBuilder();
		String[] items = m_ep.getIcalItems();
		if (items != null)
			for (String item: items)
				appendItem(item, true, false, true, null, sb, r);
		String summary = sb.toString();
		if (summary.indexOf('<') != -1) {
			String html = summary;
			summary = Jsoup.parse(summary).text();
			vevent.setSummary(summary);
			vevent.setDescription(html);
			RawProperty property = vevent.setExperimentalProperty("X-ALT-DESC", html);
			property.setParameter("FMTTYPE", "text/html");
		} else
			vevent.setSummary(summary);
		int week_num = m_start_date.get(ChronoField.ALIGNED_WEEK_OF_MONTH);
		if (m_start_time != null) {
			vevent.setDateStart(java.util.Date.from(ZonedDateTime.of(m_start_date, m_start_time, Time.zone_id).toInstant()));
			if (m_end_time != null)
				vevent.setDuration(Duration.builder().minutes(getLength()).build());
		} else
			vevent.setDateStart(java.util.Date.from(m_start_date.atStartOfDay(Time.zone_id).toInstant()), false);
		if (isRepeating()) {
			Recurrence.Builder recurrence = null;
			String repeat = getRepeat();
			if (repeat.equals("every day")) {
				if (m_start_time != null)
					recurrence = new Recurrence.Builder(Frequency.DAILY);
				else if (m_end_date != null)
					vevent.setDateEnd(java.util.Date.from(m_end_date.plusDays(1).atStartOfDay(Time.zone_id).toInstant()), false);
			} else if (repeat.endsWith("week") || repeat.endsWith("weeks"))
				recurrence = new Recurrence.Builder(Frequency.WEEKLY);
			else if (repeat.indexOf("year") != -1)
				recurrence = new Recurrence.Builder(Frequency.YEARLY);
			else if (repeat.equals("every month (same week and day)")) {
				recurrence = new Recurrence.Builder(Frequency.MONTHLY);
				java.time.DayOfWeek day_of_week = m_start_date.getDayOfWeek();
				recurrence.byDay(week_num, day_of_week == java.time.DayOfWeek.MONDAY ? DayOfWeek.MONDAY : day_of_week == java.time.DayOfWeek.TUESDAY ? DayOfWeek.TUESDAY : day_of_week == java.time.DayOfWeek.WEDNESDAY ? DayOfWeek.WEDNESDAY : day_of_week == java.time.DayOfWeek.THURSDAY ? DayOfWeek.THURSDAY : day_of_week == java.time.DayOfWeek.FRIDAY ? DayOfWeek.FRIDAY : day_of_week == java.time.DayOfWeek.SATURDAY ? DayOfWeek.SATURDAY : DayOfWeek.SUNDAY);
			} else
				recurrence = new Recurrence.Builder(Frequency.MONTHLY);
			if (recurrence != null) {
				if (m_end_date != null)
					if (m_end_time != null)
						recurrence.until(java.util.Date.from(ZonedDateTime.of(m_end_date, m_end_time, Time.zone_id).toInstant()));
					else
						recurrence.until(java.util.Date.from(m_end_date.atStartOfDay(Time.zone_id).toInstant()), false);
				if (repeat.equals("every other week") || repeat.equals("every 2 months") || repeat.equals("every 2 years"))
					recurrence.interval(2);
				else if (repeat.equals("every 3 months") || repeat.equals("every 3 weeks"))
					recurrence.interval(3);
				else if (repeat.equals("every 4 months") || repeat.equals("every 4 weeks"))
					recurrence.interval(4);
				else if (repeat.equals("every 6 months"))
					recurrence.interval(6);
				vevent.setRecurrenceRule(recurrence.build());
			}
		}
		String location = getLocation();
		if (location != null)
			vevent.setLocation(location);
		return vevent;
	}

	//--------------------------------------------------------------------------

	public final boolean
	occursOn(LocalDate date) {
		return EventProvider.occursOn(date, m_start_date, m_end_date, m_repeat);
	}

	//--------------------------------------------------------------------------

	public final boolean
	overlaps(Event e2) {
		return !equals(e2) && m_start_date.equals(e2.m_start_date) && timesOverlap(e2) && locationsMatch(e2);
	}

	//--------------------------------------------------------------------------

	public final Event
	setColor(String color) {
		m_color = color;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Event
	setEndDate(LocalDate end_date) {
		m_end_date = end_date;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Event
	setEndTime(LocalTime end_time) {
		m_end_time = end_time;
		return this;
	}

	//--------------------------------------------------------------------------

	static boolean
	setOverlaps(Event e1, Event e2) {
		if (!e1.m_ep.m_check_overlaps && !e2.m_ep.m_check_overlaps)
			return false;
		if (!e1.overlaps(e2))
			return false;
		if (e1.m_ep.m_support_waiting_list || e2.m_ep.m_support_waiting_list)
			if (e1.m_timestamp.isBefore(e2.m_timestamp)) {
				e2.m_overlaps_with = e1.m_ep;
				e2.m_waiting_list = true;
			} else {
				e1.m_overlaps_with = e2.m_ep;
				e1.m_waiting_list = true;
			}
		else {
			e1.m_overlaps_with = e2.m_ep;
			e1.m_waiting_list = false;
			e2.m_overlaps_with = e1.m_ep;
			e2.m_waiting_list = false;
		}
		return true;
	}

	//--------------------------------------------------------------------------

	public final Event
	setRepeat(String repeat) {
		m_repeat = repeat;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Event
	setStartTime(LocalTime start_time) {
		m_start_time = start_time;
		return this;
	}

	//--------------------------------------------------------------------------

	final boolean
	timesOverlap(Event e) {
		if (!m_ep.m_events_have_start_time || !e.m_ep.m_events_have_start_time)
			return true;
		if (m_start_time == null || e.m_start_time == null)
			return true;
		LocalTime end_time = m_end_time == null ? m_start_time.plusMinutes(30) : m_end_time;
		if (end_time.equals(LocalTime.MIDNIGHT))
			end_time = end_time.minusMinutes(1);
		LocalTime e_end_time = e.m_end_time == null ? e.m_start_time.plusMinutes(30) : e.m_end_time;
		if (e_end_time.equals(LocalTime.MIDNIGHT))
			e_end_time = e_end_time.minusMinutes(1);
		return m_start_time.isBefore(e_end_time) && e.m_start_time.isBefore(end_time);
	}

	//--------------------------------------------------------------------------

	final void
	write(LocalDate date, String calendar_name, Request r) {
		write(date, calendar_name, 0, 0, 0, 0, r);
	}

	//--------------------------------------------------------------------------

	final void
	write(LocalDate date, String calendar_name, double height, int width, int left, double top, Request r) {
		HTMLWriter w = r.w;
		w.addClass("event " + m_ep.getName().replace(' ', '_') + "_event");
		String on_click = m_ep.getEventEditJS(this, date, r);
		if (on_click != null)
			w.addStyle("cursor:pointer")
				.setAttribute("onclick", on_click);
		else
			w.addStyle("cursor:auto");
		String color = getColor();
		boolean black_text = Text.blackText(color);
		if (color != null)
			w.addStyle("background-color:" + color + ";color:" + (black_text ? "#000" : "#FFF"));
		if (height > 0) {
			if (color == null)
				w.addStyle("background-color:#EEE");
			w.addStyle("height:" + height + "px;left:" + left + "px;overflow:hidden;position:absolute;top:" + top + "px;width:" + width + "px");
			w.setAttribute("onmouseout", "event_mouseout(event)");
			w.setAttribute("onmouseover", "event_mouseover(event)");
		}
		if (r.getUser() != null) {
			String tooltip = getTooltip(date, r);
			if (tooltip != null)
				w.setAttribute("title", tooltip);
		}
		if (m_category != null) {
			String text = m_category.getText();
			if (text != null)
				w.setAttribute("data-category", "[\"" + text.replaceAll("\\s+", " ") + "\"]");
		}
		if (m_locations != null) {
			StringBuilder sb = new StringBuilder();
			if (m_locations != null)
				for (SelectOption location : m_locations) {
					if (sb.length() > 0)
						sb.append(",");
					else
						sb.append('[');
					sb.append('\"').append(location.getText().trim()).append('\"');
				}
			if (sb.length() > 0)
				w.setAttribute("data-location", sb.append(']').toString().replaceAll("\\s+", " "));
		}
		w.tagOpen("div");
		StringBuilder s = new StringBuilder();
		if (calendar_name != null)
			s.append("<div class=\"other_calendar_label\" style=\"color:").append(black_text ? "#555" : "#CCC").append("\">").append(calendar_name).append(" Calendar</div>");
		appendEventHTML(date, s, black_text, r);
		w.write(s.toString());
		w.tagClose();
	}
}
