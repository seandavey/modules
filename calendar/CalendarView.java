package calendar;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.PriorityQueue;

import app.Modules;
import app.Person;
import app.Request;
import app.Site;
import ui.Dropdown;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JS;

public abstract class CalendarView {
	public enum View { DAY, LIST, MONTH, WEEK }

	protected final EventProvider m_ep;

	//--------------------------------------------------------------------------

	CalendarView(EventProvider ep, Request r) {
		m_ep = ep;
	}

	//--------------------------------------------------------------------------

	protected final void
	adjustEvents(PriorityQueue<Event> events, Request r) {
		ZoneId zone_id = Time.getUserTimezone(r);
		if (zone_id == null)
			return;
		for (Event event : events)
			event.adjustTimezone(zone_id);
	}

	//--------------------------------------------------------------------------

	abstract View
	getView();

	//--------------------------------------------------------------------------

	public abstract void
	write(LocalDate date, Request r);

	//--------------------------------------------------------------------------

	private void
	writeCalendarPicker(Request r) {
		Collection<EventProvider> event_providers = Calendars.getCalendarsSorted(r);
		if (event_providers.size() <= 1)
			return;
		r.w.setId("calendar_picker");
		String display_name = m_ep.getDisplayName(r);
		r.w.setAttribute("data-main", display_name)
			.setAttribute("data-name", m_ep.getName())
			.addStyle("float:left;text-align:left");
		Dropdown dropdown = r.w.ui.dropdown(display_name, false).open();
		for (EventProvider event_provider : event_providers)
			if (!m_ep.getName().equals(event_provider.getName())) {
				r.w.setAttribute("data-name", event_provider.getName());
				dropdown.aOnClick(event_provider.getDisplayName(r), "Calendar.$().set_name(this.innerText,this.parentNode.dataset.name);");
			}
		dropdown.close();
	}

	//--------------------------------------------------------------------------

	public final void
	writeComponent(Request r) {
		writeComponent(true, r);
	}

	//--------------------------------------------------------------------------

	public final void
	writeComponent(boolean show_head, Request r) {
		HTMLWriter w = r.w;
		w.write("<div id=\"calendar\">");
		if (show_head)
			writeHead(true, r);
		Person user = r.getUser();
		boolean controls_on_top = user != null && user.testData("calendar controls on top");
		if (!(this instanceof ListView) && controls_on_top)
			writeControls("top", r);
		w.write("<div id=\"c_calendar_view\" class=\"calendar_component\">");
		if (m_ep.m_text_above_calendar != null && !(this instanceof ListView))
			r.w.write("<div style=\"margin-top:-20px;padding:10px 0;\">").write(m_ep.m_text_above_calendar).write("</div>");
		write(Time.newDate(), r);
		w.write("</div>");
		if (!(this instanceof ListView) && !controls_on_top)
			writeControls("bottom", r);
		w.write("<div id=\"calendar_after_view\"></div>");
		w.write("</div>");
		w.js("JS.get('" + Site.site.getResources().get("js", "calendar-min") + "',function(){new Calendar(" +
			JS.string(m_ep.getName()) + "," +
			JS.string(m_ep.getDisplayName(r)) + "," +
			JS.string(getView().toString()) +
			(m_ep.repeatingEventsCanBeSplit() ? ",true" : ",false") +
			(m_ep.endDateAfterStart() ? ",true" : ",false") +
			")});");
		if (r.getUser() != null) {
			String edit = r.getParameter("edit");
			if (edit != null) {
				int comma = edit.indexOf(',');
				String calendar_name = edit.substring(1, comma - 1);
				int index = calendar_name.indexOf('%');
				while (index != -1) {
					calendar_name = HTMLWriter.URLEncode(calendar_name);
					index = calendar_name.indexOf('%');
				}
				EventProvider event_provider = (EventProvider)Modules.get(calendar_name);
				if (event_provider != null)
					w.js("window.addEventListener('load',function(){Calendar.$().edit(" +
						JS.string(calendar_name) + "," +
						JS.string(event_provider.getDisplayName(r)) + "," +
						edit.substring(comma + 1) + ");});");
			}
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeControls(String position, Request r) {
		HTMLWriter w = r.w;
		w.write("<div id=\"calendar_foot\" data-position=\"").write(position).write("\">");
		if (r.getUser() != null && Calendars.isMainCalendar(m_ep)) {
			w.write("<div id=\"calendar_checkboxes\" style=\"display:flex;flex-wrap:wrap;gap:5px\">");
			Calendars.writeCheckBoxes(r);
			w.write("</div>");
		}
		w.write("<div style=\"float:right;\"><span id=\"calendar_buttons\">");
		m_ep.writeControlButtons(r);
		w.write("</span>");
		Person user = r.getUser();
		if (user != null ) {
			boolean controls_on_top = user.testData("calendar controls on top");
			w.setId("calendar_position")
				.addStyle("margin-left:2px")
				.setAttribute("title", controls_on_top ? "move controls below calendar" : "move controls above calendar")
				.ui.buttonIconOnClick(controls_on_top ? "arrow-down" : "arrow-up", "Calendar.$().move_controls()");
		}
		w.write("</div>");
		m_ep.writeFilters(r);
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	final void
	writeHead(boolean show_calendar_picker, Request r) {
		boolean is_main_calendar = Calendars.isMainCalendar(m_ep);
		if ((!show_calendar_picker || !is_main_calendar) && !m_ep.allowViewSwitching() && this instanceof ListView)
			return;

		HTMLWriter w = r.w;
		w.write("<div id=\"calendar_head\"><div style=\"align-items:center;display:flex\"><div>");
		if (show_calendar_picker)
			writeCalendarPicker(r);
		if (this instanceof MonthView)
			w.addStyle("display:none");
		w.setId("calendar_add").addStyle("margin-left:20px").ui.buttonOnClick(Text.add, "Calendar.$().add()", false);
		if (this instanceof ListView)
			w.write("</div><div></div></div><div style=\"align-items:center;display:flex\"><div>");
		else
			w.write("</div><div style=\"align-self:center;flex-grow:1;text-align:right;white-space:nobreak;\"><h1></h1></div></div><div style=\"align-items:center;display:flex\"><div style=\"align-self:center\">")
				.write("<div class=\"btn-group\" style=\"margin-right:10px;vertical-align:middle;\">")
				.ui.buttonIconOnClick("arrow-left", "Calendar.$().move(-1)")
				.ui.buttonIconOnClick("arrow-right", "Calendar.$().move(1)")
				.write("</div>")
				.setId("calendar_today")
				.addStyle("vertical-align:middle")
				.ui.buttonOutlineOnClick("Today", "Calendar.$().today()");
		w.write("</div><div style=\"flex-grow:1;text-align:right\">");
		if (m_ep.allowViewSwitching()) {
			w.setAttribute("data-active", "0").setId("calendar_view_switcher").ui.buttonGroupOpen();
			w.addClass("active").ui.buttonOnClick("Month", "Calendar.$().set_view(0).update()", false)
				.ui.buttonOnClick("Week", "Calendar.$().set_view(1).update()", false)
				.ui.buttonOnClick("Day", "Calendar.$().set_view(2).update()", false)
				.ui.buttonOnClick("List", "Calendar.$().set_view(3).update()", false)
				.tagClose();
		}
		w.write("</div></div></div>");
	}
}