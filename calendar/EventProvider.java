package calendar;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Types;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TimeZone;

import org.jsoup.Jsoup;

import app.ClassTask;
import app.Module;
import app.Modules;
import app.Person;
import app.Request;
import app.Site;
import app.Site.Message;
import app.Subscriber;
import attachments.Attachments;
import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.io.TimezoneAssignment;
import db.Categories;
import db.DBConnection;
import db.Form;
import db.NameValuePairs;
import db.OneToMany;
import db.Result;
import db.Rows;
import db.RowsSelect;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.Or;
import db.access.RecordOwnerAccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.ColorColumn;
import db.column.Column;
import db.column.DateColumn;
import db.column.EmailAddressProvider;
import db.column.EmailColumn;
import db.column.OptionsColumn;
import db.column.PersonColumn;
import db.column.TextAreaColumn;
import db.column.TimeColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.DBObject;
import db.object.JSONField;
import ui.SelectOption;
import ui.Table;
import ui.Tags;
import util.Array;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.Head;
import web.JS;

public class EventProvider extends Module implements Subscriber, EmailAddressProvider {
	private static final String[]	s_repeat_periods = new String[] { "never", "every day", "every week", "every other week", "every 3 weeks", "every 4 weeks", "every month (same day, last week)", "every month (same week and day)", "every month (same date)", "every 2 months", "every 3 months", "every 4 months", "every 6 months", "every year", "every 2 years" };
	static ReminderHandler			s_reminder_handler;

	protected AccessPolicy			m_access_policy;
	@JSONField(after="type to add items. items are shown as checkboxes on the form for adding an event and must be checked to allow adding the event.", fields = {"m_agreements_header"})
	String[]						m_agreements;
	@JSONField(after="text to show above agreement checkboxes")
	String							m_agreements_header;
	protected Attachments			m_attachments;
	@JSONField(admin_only=true,type="location")
	protected int					m_calendar_location_id;
	Categories						m_categories;
	@JSONField(fields= {"m_start_with_blank_category"})
	protected boolean				m_category_required;
	@JSONField(fields={"m_support_waiting_list"})
	protected boolean				m_check_overlaps = true;
	@JSONField(type="color")
	private String					m_color;
	@JSONField(admin_only=true)
	private int						m_daily_resolution = 30;
	@JSONField(type="time")
	protected String				m_default_end_time;
	@JSONField(admin_only=true)
	protected String				m_default_notes;
	@JSONField(type="time")
	protected String				m_default_start_time;
	@JSONField
	private CalendarView.View		m_default_view;
	@JSONField(label="show labels")
	protected boolean				m_display_item_labels;
	@JSONField(fields= {"m_display_item_labels"})
	protected String[]				m_display_items = new String[] { "time", "location", "event" };
	@JSONField
	protected String				m_end_date_label = "end date";
	@JSONField(admin_only=true)
	protected String				m_event_label = "event";
	@JSONField
	protected boolean				m_events_can_repeat;
	@JSONField(label="events can have categories",fields={"m_category_required","m_only_admins_can_edit_categories"})
	protected boolean				m_events_have_category;
	@JSONField(label="users can color events")
	private boolean					m_events_have_color;
	@JSONField(admin_only=true)
	protected boolean				m_events_have_event = true;
	@JSONField(label="events can reserve locations",fields={"m_locations","m_location_required","m_support_multiple_locations","m_support_other_locations"})
	protected boolean				m_events_have_location;
	@JSONField(label="events have time",fields={"m_default_start_time","m_default_end_time","m_resolution"})
	protected boolean				m_events_have_start_time;
	@JSONField(admin_only=true)
	public String					m_events_table;
	@JSONField
	protected String[]				m_ical_items = new String[] { "event", "notes", "posted by" };
	@JSONField(type="locations")
	int[]					m_locations;
	@JSONField(fields= {"m_start_with_blank_location"})
	protected boolean				m_location_required;
	@JSONField
	private boolean					m_only_admins_can_edit_categories;
	Reminders						m_reminders;
	@JSONField
	String[]						m_reminder_subject_items = new String[] { "location", "date" };
	@JSONField(choices = {"5","15","30","60"})
	protected int					m_resolution = 5;
	@JSONField(label="only add events with role",type="role")
	protected String				m_role;
	@JSONField(after="leave blank to let user choose",type="mail list")
	private String					m_send_announcement_to;
	@JSONField
	protected boolean				m_show_on_menu;
	@JSONField
	protected String				m_start_date_label = "date";
	@JSONField
	private boolean					m_start_with_blank_category;
	@JSONField
	private boolean					m_start_with_blank_location;
	@JSONField(fields="m_send_announcement_to")
	protected boolean				m_support_announcing_new_events;
	@JSONField
	protected boolean				m_support_attachments;
	@JSONField(label="support more than one location per event")
	protected boolean				m_support_multiple_locations;
	@JSONField(label="support non reservable locations")
	protected boolean				m_support_other_locations;
	@JSONField
	protected boolean				m_support_registrations;
	@JSONField(fields={"m_reminder_subject_items"})
	protected boolean				m_support_reminders;
	@JSONField(fields={"m_waiting_list_message"})
	protected boolean				m_support_waiting_list;
	@JSONField(type = "html")
	String							m_text_above_calendar;
	@JSONField(type = "html")
	protected String				m_text_after_form;
	@JSONField
	String[] 						m_tooltip_items;
	@JSONField(admin_only=true)
	boolean							m_tooltip_item_labels = true;
	@JSONField(label="embed")
	protected String				m_uuid;
	@JSONField
	protected String				m_waiting_list_message = "There is another event on the calendar that overlaps with this one. Your event has been added and for the days that overlap you will be on a waiting list.";

	//--------------------------------------------------------------------------

	public
	EventProvider(String name) {
		super(name);
		m_events_table = name.replaceAll("\\p{Punct}+", "").replace(' ', '_').toLowerCase() + "_events";
		m_renameable = true;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addAddresses(ArrayList<String> a) {
		a.add("everyone");
		a.add("poster");
	}

	//--------------------------------------------------------------------------

	protected final void
	addAgreements(ArrayList<String> column_names_form, ViewDef view_def) {
		if (m_agreements == null)
			return;
		column_names_form.add("agreements");
		view_def.setColumn(new Column("agreements") {
			@Override
			public String
			getLabel(View v, Request r) {
				return null;
			}
			@Override
			public void
			writeInput(View v, Form f, boolean inline, Request r) {
				r.w.addClass("ck-content doc-view").tagOpen("div");
				if (m_agreements_header != null)
					r.w.ui.formLabel(m_agreements_header);
				for (int i=0; i<m_agreements.length; i++)
					r.w.setAttribute("required", "true").ui.checkbox(null, m_agreements[i], "agreement " + (i + 1), v.getMode() != Mode.ADD_FORM, false, false);
				r.w.tagClose();
			}
			@Override
			public boolean
			writeValue(View v, Map<String,Object> data, Request r) {
				return false;
			}
		}.setDisplayName(""));
	}

	//--------------------------------------------------------------------------

	public void
	addAllEvents(ArrayList<Event> events, Request r) {
		String where = "date>=current_date-interval '1 year'";
		if (m_events_can_repeat)
			where += " OR (repeat!='never' AND (end_date IS NULL OR end_date>=current_date))";
		Select query = new Select("*").from(m_events_table).where(where);
		Rows rows = new Rows(query, r.db);
		while (rows.next())
			events.add(loadEvent(rows));
		rows.close();
	}

	//--------------------------------------------------------------------------

	public void
	addCalendarsEvents(PriorityQueue<Event> events, LocalDate from, LocalDate to, Request r) {
		addEvents(events, from, to, r);
		Collection<EventProvider> calendars = null;
		String[] checked_calendars = null;
		if (Calendars.isMainCalendar(this)) {
			calendars = Calendars.getCalendars(r);
			checked_calendars = Calendars.getCheckedCalendars(r.getUser());
			for (EventProvider ep : calendars) {
				String name = ep.getName();
				if (!name.equals(m_name))
					if (ep.isActive() && Array.indexOf(checked_calendars, name) != -1)
						ep.addEvents(events, from, to, r);
			}
		}
	}

	//--------------------------------------------------------------------------

	protected void
	addEvents(PriorityQueue<Event> events, LocalDate from, LocalDate to, Request r) {
		if (from.getYear() >= 3000) {
			r.log("EventProvider.addEvents: from.year = " + from.getYear(), false);
			return;
		}
		Select select = selectEvents(from, to, 0, r);
		if (select == null)
			return;
		Rows rows = new Rows(select, r.db);
		while (rows.next())
			events.add(loadEvent(rows));
		rows.close();
		if (m_events_can_repeat) {
			List<Event> repeating_events = loadRepeatingEvents(from, to, r);
			if (repeating_events != null)
				while (from.isBefore(to)) {
					addRepeatingEvents(events, repeating_events, from);
					from = from.plusDays(1);
				}
		}
	}

	//--------------------------------------------------------------------------

	protected final void
	addRepeatingEvents(PriorityQueue<Event> events, List<Event> repeating_events, LocalDate date) {
		for (Event event : repeating_events)
			if (event.hasEndedBy(date))
				continue;
			else if (event.occursOn(date)) {
				event = event.getSingleEvent(date);
				if (event != null)
					events.add(event);
			}
	}

	//--------------------------------------------------------------------------

	protected final void
	adjustLocation(JDBCTable table_def, DBConnection db) {
		if (!m_events_have_location || m_events_table == null)
			return;
		Locations.adjustLocation(table_def, m_events_table, m_support_multiple_locations, db);
	}

	//--------------------------------------------------------------------------

	protected final void
	adjustReminders(DBConnection db) {
		if (m_support_reminders) {
			JDBCTable table_def = new JDBCTable(getRemindersTable())
				.add(new JDBCColumn("before", Types.BOOLEAN).setDefaultValue(true))
				.add(new JDBCColumn("email", Types.VARCHAR))
				.add(new JDBCColumn("note", Types.VARCHAR))
				.add(new JDBCColumn("num", Types.INTEGER))
				.add(new JDBCColumn("_owner_", "people"))
				.add(new JDBCColumn("repeat_days", Types.VARCHAR).setDefaultValue("each event"))
				.add(new JDBCColumn("unit", Types.VARCHAR));
			if (m_events_table != null)
				table_def.add(new JDBCColumn(m_events_table));
			JDBCTable.adjustTable(table_def, true, false, db);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	adjustTables(DBConnection db) {
		if (m_events_table == null)
			return;
		JDBCTable table_def = new JDBCTable(m_events_table)
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true));
		if (m_events_can_repeat) {
			table_def.add(new JDBCColumn("repeat", Types.VARCHAR).setDefaultValue("never"));
			table_def.add(new JDBCColumn("end_date", Types.DATE));
			table_def.add(new JDBCColumn("event_id", Types.INTEGER));
		}
		if (m_events_have_category) {
			m_categories = new Categories(m_events_table, true)
				.setAllowEditing(!m_only_admins_can_edit_categories)
				.setObjectClass(Category.class);
			m_categories.adjustTables(db);
			m_categories.addJDBCColumn(table_def);
		} else
			m_categories = null;
		if (m_events_have_color)
			table_def.add(new JDBCColumn("color", Types.VARCHAR));
		if (m_events_have_event)
			table_def.add(new JDBCColumn("event", Types.VARCHAR));
		adjustLocation(table_def, db);
		if (m_support_other_locations)
			table_def.add(new JDBCColumn("other_locations", Types.VARCHAR));
		if (m_events_have_start_time) {
			table_def.add(new JDBCColumn("start_time", Types.TIME));
			table_def.add(new JDBCColumn("end_time", Types.TIME));
		}
		if (m_support_registrations)
			table_def.add(new JDBCColumn("register_people", Types.BOOLEAN));
		table_def.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, true, db);
		db.createIndex(m_events_table, "date");
		if (m_events_can_repeat)
			db.createIndex(m_events_table, "event_id");
		if (db.exists(m_events_table, "_timestamp_ IS NULL"))
			db.update(m_events_table, "_timestamp_=LEAST(now(),date) + interval '1 second' * id", "_timestamp_ IS NULL");

		if (m_support_registrations) {
			table_def = new JDBCTable(m_events_table + "_registrations")
				.add(new JDBCColumn(m_events_table))
				.add(new JDBCColumn("person", Types.INTEGER))
				.add(new JDBCColumn("note", Types.VARCHAR));
			JDBCTable.adjustTable(table_def, true, false, db);
		}
		adjustReminders(db);
		if (m_support_attachments) {
			m_attachments = new Attachments(m_events_table);
			m_attachments.init(db);
		} else
			m_attachments = null;
	}

	//--------------------------------------------------------------------------

	protected void
	adjustViewDef(ArrayList<String> column_names_form, ViewDef view_def) {
	}

	//--------------------------------------------------------------------------

	public final boolean
	allowViewSwitching() {
		return true; //m_allow_view_switching;
	}

	//--------------------------------------------------------------------------

	protected final void
	appendReminderItem(String item, String label, String text, StringBuilder sb) {
		if (text != null && text.length() > 0) {
			if (sb.length() > 0)
				sb.append(label == null ? " - " : "<br/><br/>" + label + ": ");
			if (text.indexOf('\n') != -1)
				sb.append("<br/>");
			sb.append(HTMLWriter.breakNewlines(text));
		}
	}

	//--------------------------------------------------------------------------

	protected void
	appendReminderItem(String item, String label, Reminder reminder, StringBuilder sb, DBConnection db) {
		Event event = reminder.getEvent();
		if (event == null)
			return;
		switch(item) {
		case "category":
			SelectOption category = event.getCategory();
			if (category != null)
				appendReminderItem(item, label, category.getText(), sb);
			break;
		case "date":
			appendReminderItem(item, label, buildReminderDate(event, reminder.getDate(), reminder.isLastEvent()), sb);
			break;
		case "event":
			appendReminderItem(item, label, event.getEventText(), sb);
			break;
		case "location":
			String location = event.getLocation();
			if (location != null)
				appendReminderItem(item, label, location, sb);
			break;
		case "notes":
			appendReminderItem(item, label, event.getNotes(), sb);
			break;
		case "posted by":
			appendReminderItem(item, label, Site.site.lookupName(event.getOwner(), db), sb);
			break;
		}
	}

	//--------------------------------------------------------------------------

	protected void
	appendReminderWhat(Reminder reminder, StringBuilder message, DBConnection db) {
		appendReminderItem("event", "What", reminder, message, db);
	}

	//--------------------------------------------------------------------------

	protected String
	buildReminderDate(Event event, LocalDate date, boolean last_event) {
		StringBuilder sb = new StringBuilder();
		String formatted_date = Time.formatter.getDateShort(date);
		sb.append(formatted_date);
		LocalTime start_time = event.getStartTime();
		if (start_time != null) {
			LocalTime end_time = event.getEndTime();
			if (end_time != null)
				sb.append(" from ").append(Time.formatter.getTime(start_time, true)).append(" to ").append(Time.formatter.getTime(end_time, true));
			else
				sb.append(" at ").append(Time.formatter.getTime(start_time, true));
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	protected String
	buildReminderMessage(Reminder reminder, DBConnection db) {
		String name = getDisplayName(null);
		StringBuilder m = new StringBuilder("<html><body>")
			.append(name);
		if (!name.endsWith("Calendar"))
			m.append(" Calendar");
		m.append(" Reminder");
		appendReminderWhat(reminder, m, db);
		appendReminderItem("date", "When", reminder, m, db);
		if (m_events_have_location)
			appendReminderItem("location", "Where", reminder, m, db);
		if (m_events_have_category)
			appendReminderItem("category", "Category", reminder, m, db);
		appendReminderItem("notes", "Notes", reminder, m, db);
		String note = reminder.getNote();
		if (note != null && note.length() > 0) {
			m.append("<br/><br/>Reminder notes: ");
			if (note.indexOf('\n') != -1)
				m.append("<br/>");
			m.append(HTMLWriter.breakNewlines(note));
		}
		Event event = reminder.getEvent();
		if (event != null)
			m.append("<br/><br/><a href=\"" + Site.site.absoluteURL("Calendars").set("edit", "'" + m_name + "'," + event.getID() + ",null,true") + "\">View Event</a>");
		m.append("</body></html>");
		return m.toString();
	}

	//--------------------------------------------------------------------------

	protected String
	buildReminderSubject(Reminder reminder, DBConnection db) {
		StringBuilder sb = new StringBuilder();
		if (m_reminder_subject_items != null)
			for (String item : m_reminder_subject_items)
				appendReminderItem(item, null, reminder, sb, db);
		String subject = sb.toString();
		if (subject.matches(".*<.*?>.*"))
			subject = Jsoup.parse(subject).text();
		return "Reminder - " + subject;
	}

	//--------------------------------------------------------------------------

	protected int
	countEvents(int target_max, LocalDate date, Request r) {
		if (m_events_table == null) {
			r.log(m_name + " countEvents with null events table", false);
			return 0;
		}
		String date_string = date.toString();
		Select query = new Select().from(m_events_table).where("date>='" + date_string + "'");
		if (m_events_can_repeat)
			query.andWhere("repeat='never'");
		int count = r.db.countRows(m_events_table, query.getWhere());
		if (count < target_max && m_events_can_repeat) {
			Rows rows = new Rows(new Select("date,end_date,repeat").from(m_events_table).where("(end_date IS NULL OR end_date>='" + date_string + "')").andWhere("repeat<>'never'"), r.db);
			while (rows.next()) {
				LocalDate end_date = rows.getDate(2);
				if (rows.wasNull()) {
					rows.close();
					return target_max;
				}
				LocalDate d = date;
				LocalDate end = end_date;
				while (count < target_max && d.isBefore(end)) {
					if (occursOn(d, rows.getDate(1), end, rows.getString(3)))
						count++;
					d = d.plusDays(1);
				}
			}
			rows.close();
		}
		return count;
	}

	//--------------------------------------------------------------------------

	private void
	dateClone(int event_id, Request r) {
		NameValuePairs nvp = new NameValuePairs();
		nvp.setFromParameters(r);
		nvp.setDate("date", r.getDate("db_date"));
		nvp.set("repeat", "never");
		r.db.clone(m_events_table, nvp, event_id);

	}

	//--------------------------------------------------------------------------

	private String
	dateDelete(Request r) {
		DBConnection db = r.db;
		ViewDef view_def = Site.site.getViewDef(m_name, db);
		if (view_def == null)
			return m_name + " not found";
		int event_id = r.getInt("id", 0);
		LocalDate date = r.getDate("date"); // set when deleting one date from a repeating event
		if (date != null) {
			if (m_access_policy != null && !m_access_policy.canDeleteRow(m_events_table, event_id, r) && !r.userIsAdmin()) {
				Person user = r.getUser();
				return user == null ? "user is null when deleting event" : user.getName() + " doesn't have delete rights for this calendar, delete failed";
			}
			NameValuePairs nvp = new NameValuePairs();
			Object[] row = db.readRowObjects(new Select("date,end_date,repeat").from(m_events_table).whereIdEquals(event_id));
			LocalDate end_date = (LocalDate)row[1];
			LocalDate next = next(date, end_date, (String)row[2]);
			if (date.equals(row[0])) { // deleting first date of repeating event
				if (next != null && (end_date == null || !next.isAfter(end_date))) {
					nvp.setDate("date", next);
					db.update(m_events_table, nvp, event_id);
				} else {
					view_def.delete(m_events_table, event_id, null, 0, null, r);
					db.delete(m_events_table, "event_id", event_id, false); // delete exceptions to repeating event
				}
			} else {
				if (next != null && (end_date == null || !next.isAfter(end_date))) {
					nvp.setDate("date", next);
					db.clone(m_events_table, nvp, event_id);
					nvp.clear();
				}
				nvp.setDate("end_date", date.minusDays(1));
				db.update(m_events_table, nvp, event_id);
			}
		} else {
			String error = view_def.delete(m_events_table, event_id, null, 0, null, r);
			if (error != null)
				return error;
			if (m_events_can_repeat)
				db.delete(m_events_table, "event_id", event_id, false); // delete exceptions to repeating event
		}
		return null;
	}

	//--------------------------------------------------------------------------

	private String
	dateUpdate(Request r) {
		DBConnection db = r.db;
		ViewDef view_def = Site.site.getViewDef(m_name, db);
		if (view_def == null)
			return m_name + " not found";
		int event_id = r.getPathSegmentInt(1);
		if (m_access_policy != null && !m_access_policy.canUpdateRow(m_events_table, event_id, r) && !r.userIsAdmin()) {
			Person user = r.getUser();
			return user == null ? "user is null when saving event" : user.getName() + " doesn't have update rights for this calendar, save failed";
		}
		LocalDate date = r.getDate("db_date"); // set when saving one date from a repeating event
		if (date == null)
			return view_def.update(r);
		NameValuePairs nvp = new NameValuePairs();
		Object[] row = db.readRowObjects(new Select("date,end_date,repeat").from(m_events_table).whereIdEquals(event_id));
		LocalDate end_date = (LocalDate)row[1];
		LocalDate next = next(date, end_date, (String)row[2]);
		if (date.equals(row[0])) { // saving first date of repeating event
			if (next != null && (end_date == null || !next.isAfter(end_date))) {
				Result result = db.clone(m_events_table, nvp, event_id);
				nvp.setFromParameters(r);
				nvp.set("repeat", "never");
				db.update(m_events_table, nvp, event_id);
				nvp.clear();
				nvp.setDate("date", next);
				db.update(m_events_table, nvp, result.id);
			} else {
				nvp.setFromParameters(r);
				nvp.set("repeat", "never");
				db.update(m_events_table, nvp, event_id);
			}
		} else {
			if (next != null && (end_date == null || !next.isAfter(end_date))) {
				nvp.setDate("date", next);
				db.clone(m_events_table, nvp, event_id);
				nvp.clear();
			}
			nvp.setDate("end_date", date.minusDays(1));
			db.update(m_events_table, nvp, event_id);
			dateClone(event_id, r);
		}
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	doDelete(Request r) {
		String error = dateDelete(r);
		if (error != null)
			r.response.addError(error);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		View.Mode mode = r.getEnum("db_mode", View.Mode.class);
		if (mode != null && mode != View.Mode.LIST) {
			writeEventForm(r);
			return true;
		}
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch(segment_one) {
		case "after_view":
			writeAfterView(r);
			return true;
		case "buttons":
			writeControlButtons(r);
			return true;
		case "filters":
			writeFilters(r);
			return true;
		case "public":
			writePublicCalendar(r);
			return true;
		case "DAY":
		case "WEEK":
		case "MONTH":
		case "LIST":
			if (m_text_above_calendar != null && !segment_one.equals("LIST"))
				r.w.write("<div style=\"margin-top:-20px;padding:10px 0;\">").write(m_text_above_calendar).write("</div>");
			int year = r.getPathSegmentInt(2);
			int month = r.getPathSegmentInt(3);
			int date = r.getPathSegmentInt(4);
			CalendarView.View v = CalendarView.View.valueOf(segment_one);
			if (v == null)
				v = m_default_view;
			if (v == null)
				v = CalendarView.View.MONTH;
			r.w.js("Calendar.$().set_view(" + Array.indexOf(new String[] {"MONTH", "WEEK", "DAY", "LIST"}, v.name()) + ").update_nav()");
			CalendarView calendar_view;
			if (v == CalendarView.View.LIST)
				calendar_view = new ListView(this, r);
			else if (v == CalendarView.View.DAY)
				calendar_view = new DaysView(this, 1, r);
			else if (v == CalendarView.View.WEEK)
				calendar_view = new DaysView(this, 7, r);
			else
				calendar_view = new MonthView(this, r);
			calendar_view.write(LocalDate.of(year, month, date), r);
			r.w.js("Calendar.$().repeating_events_can_be_split=" + (repeatingEventsCanBeSplit() ? "true" : "false") + ";Calendar.$().end_date_after_start=" + (endDateAfterStart() ? "true" : "false"));
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r)) {
			Site.site.newFile("calendars", m_name + ".ics").delete(); // do this here because it should be done when settings are updated and when module is removed
			return true;
		}

		String segment_two = r.getPathSegment(2);
		if (segment_two != null)
			switch(segment_two) {
			case "update":
				String error = dateUpdate(r);
				if (error != null)
					r.response.addError(error);
				return true;
			}

		String controls = r.getParameter("controls");
		if (controls != null) {
			Person user = r.getUser();
			if (user != null)
				user.setData("calendar controls on top", controls.equals("top"), r.db);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public boolean
	endDateAfterStart() {
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyFiveMinutes(LocalDateTime now, DBConnection db) {
		if (m_support_reminders) {
			if (m_reminders == null || !m_reminders.getDate().isEqual(now.toLocalDate()))
				m_reminders = new Reminders(loadReminders(false, db), getRemindersTable(), now.toLocalDate());
			m_reminders.send(now, db);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyNight(LocalDateTime now, DBConnection db) {
		if (m_support_reminders)
			new Reminders(loadReminders(true, db), getRemindersTable(), null).send(now, db);
	}

	//--------------------------------------------------------------------------

	public Attachments
	getAttachments() {
		return m_attachments;
	}

	//--------------------------------------------------------------------------

	final String
	getCategoryColumnName() {
		if (m_events_have_category)
			return m_categories.getColumnName();
		return null;
	}

	//--------------------------------------------------------------------------

	final Category
	getCategoryOption(Rows rows) {
		if (m_events_have_category)
			return (Category)m_categories.getOption(rows);
		return null;
	}
	//--------------------------------------------------------------------------

	public final String
	getColor() {
		return m_color;
	}

	//--------------------------------------------------------------------------

	public final int
	getDailyResolution() {
		return m_daily_resolution;
	}

	//--------------------------------------------------------------------------

	public String
	getDayCSSClass(LocalDate date, LocalDate today) {
		if (ChronoUnit.DAYS.between(date, today) == 0)
			return "today";
		return "day";
	}

	//--------------------------------------------------------------------------

	public final String
	getDefaultEndTime() {
		return m_default_end_time;
	}

	//--------------------------------------------------------------------------

	public final String
	getDefaultStartTime() {
		return m_default_start_time;
	}

	//--------------------------------------------------------------------------

	public final String[]
	getDisplayItems() {
		return m_display_items;
	}

	//--------------------------------------------------------------------------

	public static String
	getEmail(int people_id, DBConnection db) {
		String[] row = db.readRow(new Select("first,last,email").from("people").whereIdEquals(people_id));
		if (row == null)
			return null;
		StringBuilder email = new StringBuilder();
		if (row[0] != null)
			email.append(row[0]);
		if (row[1] != null) {
			if (row[0] != null)
				email.append(' ');
			email.append(row[1]);
			if (row[2] != null)
				email.append('<').append(row[2]).append('>');
		}
		return email.toString();
	}

	// --------------------------------------------------------------------------

	protected String
	getEventEditJS(Event event, LocalDate date, Request r) {
		Person user = r.getUser();
		if (user == null)
			return null;
//		if (m_access_policy != null && !m_access_policy.showEditButtons(null, r) && !r.userIsAdmin())
//			return null;
		boolean read_only = user.getId() != event.getOwner() && !r.userHasRole("calendar editor") && !r.userIsAdmin();
		if (read_only && m_access_policy != null)
			read_only = !m_access_policy.canUpdateRow(m_events_table, event.getID(), r);
		return "if(event.target.tagName!='A')Calendar.$().edit(" +
			JS.string(m_name) + "," +
			JS.string(getDisplayName(r)) + "," +
			event.getID() + ",'" +
			date.toString() + "'," +
			(read_only ? "true," : "false,") +
			"null," +
			(event.isRepeating() ? "true" : "false") +
			")";
	}

	//--------------------------------------------------------------------------

	@Override
	protected String[]
	getFieldOptions(String field_name) {
		if ("m_reminder_subject_items".equals(field_name))
			return getFieldOptions(new String[] { "category", "date", "event", "location", "posted by" });
		return getFieldOptions(new String[] { "category", "event", "location", "notes", "posted by", "time" });
	}

	//--------------------------------------------------------------------------

	protected final String[]
	getFieldOptions(String[] items) {
//		if (m_additional_columns == null)
			return items;
//		ArrayList<String> options = new ArrayList<String>();
//		for (String item : items)
//			options.add(item);
//		for (JDBCColumn column : m_additional_columns)
//			options.add(column.name);
//		return options.toArray(new String[options.size()]);
	}

	//--------------------------------------------------------------------------

	public final String[]
	getIcalItems() {
		return m_ical_items;
	}

	//--------------------------------------------------------------------------

	protected String
	getRemindersTable() {
		return m_events_table + "_reminders";
	}

	//--------------------------------------------------------------------------

	public final String
	getSendAnnouncementTo() {
		return m_send_announcement_to;
	}

	//--------------------------------------------------------------------------

	public final String
	getStartDateLabel() {
		return m_start_date_label;
	}

	//--------------------------------------------------------------------------

	public final String[]
	getTooltipItems() {
		return m_tooltip_items;
	}

	//--------------------------------------------------------------------------

	public final String
	getUUID(DBConnection db) {
		if (m_uuid == null) {
			m_uuid = Text.uuid();
			store(db);
		}
		return m_uuid;
	}

	//--------------------------------------------------------------------------

	private static boolean
	hasEndedBy(LocalDate date, LocalDate event_start_date, LocalDate event_end_date, String event_repeat) {
		if (event_repeat != null && !"never".equals(event_repeat)) {
			if (event_end_date == null)
				return false;
			return date.isAfter(event_end_date);
		}
		if (event_start_date == null)
			return false;
		return date.isAfter(event_start_date);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		if (m_events_table != null) {
			Site.site.subscribe(m_events_table, this);
			Site.site.subscribe(getRemindersTable(), this);
		}
		if (m_role != null)
			setAccessPolicy(new RoleAccessPolicy(m_role).add().delete().edit());
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isValidAddress(String address) {
		return "everyone".equals(address) || "poster".equals(address);
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public final void
	listReminders(Request r) {
		final String t = getRemindersTable();
		StringBuilder columns = new StringBuilder();
		columns.append(m_events_table).append(".id,").append(m_events_table).append("_id AS one_id,").append(t).append(".id AS reminder_id,num,unit,before,date,email,note");
		if (m_events_can_repeat)
			columns.append(",repeat,end_date,repeat_days");
		if (m_events_have_start_time)
			columns.append(",start_time,end_time");
		Select query = new Select(columns.toString())
			.from(m_events_table)
			.joinOn(t, "(" + m_events_table + ".id=" + t + "." + m_events_table + "_id OR " + t + "." + m_events_table + "_id IS NULL)");
		Rows rows = new Rows(query, r.db);
		Table table = r.w.ui.table().addClass("table table-condensed table-bordered");
		while (rows.next()) {
			LocalTime event_start_time = null;
			LocalTime event_end_time = null;
			if (m_events_have_start_time) {
				event_start_time = rows.getTime("start_time");
				event_end_time = rows.getTime("end_time");
			}
			table.tr().td();
			new Reminder(rows.getInt(1), event_start_time, event_end_time, this, rows).write(r);
		}
		rows.close();
		table.close();
	}

	//--------------------------------------------------------------------------

	public final void
	listReminders(LocalDate date, Table table, Request r) {
		String reminders_table = getRemindersTable();
		String columns = reminders_table + ".id," + reminders_table + ".note" + (m_events_have_event ? ",event" : ",NULL") + (m_events_can_repeat ? ",repeat,end_date" : ",NULL,NULL") + ",num,unit,before,email,date"; // date needed for new Event()
		Rows rows = new Rows(new Select(columns).from(reminders_table + " JOIN " + m_events_table + " ON (" + m_events_table + ".id=" + reminders_table + "." + m_events_table + "_id)"), r.db);
		boolean first = true;
		while (rows.next()) {
			int num_days = Reminder.numDays(rows.getInt(6), rows.getString(7), rows.getBoolean(8));
			if (date != null && num_days != 0)
				date = date.minusDays(num_days);
			if (date == null || loadEvent(rows).occursOn(date)) {
				if (first) {
					table.tr().td(10, m_name);
					first = false;
				}
				table.tr();
				for (int i=1; i<=10; i++)
					table.td(rows.getString(i));
			}
			if (date != null && num_days != 0)
				date = date.plusDays(num_days); // reset
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	protected Event
	loadEvent(Rows rows) {
		return new Event(this, rows);
	}

	//--------------------------------------------------------------------------

	protected ArrayList<Reminder>
	loadReminders(boolean nightly, DBConnection db) {
		if (m_events_table == null)
			return null;

		final ArrayList<Integer> delete_ids = new ArrayList<>();
		final ArrayList<Reminder> reminders = new ArrayList<>();
		final String table = getRemindersTable();
		final LocalDate today = Time.newDate();

		StringBuilder columns = new StringBuilder();
		columns.append(m_events_table).append(".id,").append(m_events_table).append("_id AS one_id,").append(table).append(".id AS reminder_id,num,unit,before,date,email,note");
		if (m_events_can_repeat)
			columns.append(",repeat,end_date,repeat_days");
		if (m_events_have_start_time)
			columns.append(",start_time,end_time");
		Select query = new Select(columns.toString())
			.from(m_events_table)
			.joinOn(table, "(" + m_events_table + ".id=" + table + "." + m_events_table + "_id OR " + table + "." + m_events_table + "_id IS NULL)")
			.where(nightly ? "(unit='day' OR unit='week')" : "(unit='hour' OR unit='minute')");
		Rows rows = new Rows(query, db);
		while (rows.next()) {
			int num_days = Reminder.numDays(rows.getInt("num"), rows.getString("unit"), rows.getBoolean("before"));
			LocalDate reminder_date = today.minusDays(num_days);
			LocalDate event_start_date = rows.getDate("date");
			LocalDate event_end_date = null;
			if (m_events_can_repeat)
				event_end_date = rows.getDate("end_date");
			String event_repeat = m_events_can_repeat ? rows.getString("repeat") : null;
			if (hasEndedBy(reminder_date, event_start_date, event_end_date, event_repeat)) {
				int id = rows.getInt(2);
				if (!rows.wasNull())
					delete_ids.add(id);
			} else if (Reminders.occursOn(today, reminder_date, m_events_can_repeat ? rows.getString("repeat_days") : null, event_start_date, event_end_date, event_repeat)) {
				LocalTime event_start_time = null;
				LocalTime event_end_time = null;
				if (m_events_have_start_time) {
					event_start_time = rows.getTime("start_time");
					event_end_time = rows.getTime("end_time");
				}
				reminders.add(new Reminder(rows.getInt(1), event_start_time, event_end_time, this, rows));
			}
		}
		rows.close();

		for (Integer id : delete_ids)
			db.delete(table, id, false);

		return reminders;
	}

	//--------------------------------------------------------------------------

	protected final ArrayList<Event>
	loadRepeatingEvents(LocalDate from, LocalDate to, Request r) {
		Select query = selectRepeatingEvents(from, to, r);
		if (query == null)
			return null;
		Rows rows = new Rows(query, r.db);
		if (rows.isBeforeFirst()) {
			ArrayList<Event> repeating_events = new ArrayList<>();
			while (rows.next()) {
				Event event = loadEvent(rows);
				Rows exceptions = new Rows(new Select("*").from(m_events_table).whereEquals("event_id", event.getID()), r.db);
				while (exceptions.next())
					event.addException(loadEvent(exceptions));
				exceptions.close();
				repeating_events.add(event);
			}
			rows.close();
			return repeating_events;
		}
		rows.close();
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals(m_name)) {
			ViewDef view_def = new EventViewDef(this, m_attachments);
			if (m_access_policy != null)
				view_def.setAccessPolicy(m_access_policy);
			else
				view_def.setAccessPolicy(new Or(new RecordOwnerAccessPolicy().add().delete().edit(), new RoleAccessPolicy("calendar editor")));
			view_def.setFrom(m_events_table);
			view_def.setRecordName("Event", true);
			ArrayList<String> cnf = new ArrayList<>();
			cnf.add("date");
			DateColumn date_column = new DateColumn("date").setDisplayName(m_start_date_label).setIsRequired(true);
			view_def.setColumn(date_column);
			if (m_events_have_start_time) {
				cnf.add("start_time");
				view_def.setColumn(new TimeColumn("start_time").setAdjustMeridian(true).setDateColumn("date").setDefaultValue(m_default_start_time).setResolution(m_resolution));
				date_column.setTimeColumn("start_time");
				cnf.add("end_time");
				view_def.setColumn(new TimeColumn("end_time").setDateColumn("date").setDefaultValue(m_default_end_time).setResolution(m_resolution));
			}
			if (m_events_have_event) {
				cnf.add("event");
				view_def.setColumn(new TextAreaColumn("event").setDisplayName(m_event_label).setIsRequired(true).setIsRichText(true, true));
			}
			cnf.add("notes");
			view_def.setColumn(new TextAreaColumn("notes").setDefaultValue(m_default_notes).setIsRichText(true, true));
			if (m_events_can_repeat) {
				cnf.add("repeat");
				view_def.setColumn(new OptionsColumn("repeat", s_repeat_periods)
					.setOnChange("return _.form.on_repeat_change(this)"));
				cnf.add("end_date");
				view_def.setColumn(new Column("end_date").setDisplayName(m_end_date_label));
			}
			if (m_events_have_category) {
				cnf.add(m_categories.getColumnName());
				m_categories.getOptions().setAllowNoSelection(m_start_with_blank_category);
				view_def.setColumn(m_categories.getColumn().setIsRequired(m_category_required));
			}
			setLocationColumn(view_def, cnf);
			if (m_support_other_locations) {
				cnf.add("other_locations");
				view_def.setColumn(new Column("other_locations").setDisplayName("unreserved locations"));
			}
			if (m_events_have_color) {
				cnf.add("color");
				view_def.setColumn(new ColorColumn("color"));
			}
			if (m_support_announcing_new_events && m_send_announcement_to == null) {
				cnf.add("send to");
				view_def.setColumn(new Column("send to") {
					@Override
					public void
					writeInput(View v, Form f, boolean inline, Request r) {
						new RowsSelect("db_send_to", new Select("name").from("mail_lists").where("active").orderBy("name"), "name", null, r).setAllowNoSelection(true).write(r.w);
					}
				}.setDisplayName("send email to").setHideOnForms(Mode.EDIT_FORM, Mode.READ_ONLY_FORM));
			}
			adjustViewDef(cnf, view_def);
			if (m_support_attachments)
				view_def.addRelationshipDef(new OneToMany(m_events_table + "_attachments"));
			if (m_support_reminders && m_events_table != null)
				view_def.addRelationshipDef(new OneToMany(m_name + "_reminders").setShowAddButtonOnReadOnlyForm(true));
			if (m_support_registrations) {
				cnf.add("register_people");
				view_def.setColumn(new Column("register_people").setDisplayName("allow people to register for this " + view_def.getRecordName().toLowerCase()));
				view_def.addRelationshipDef(new OneToMany(m_name + "_registrations") {
					@Override
					public boolean
					showOnForm(Mode mode, Rows data) {
						if (!m_support_registrations || mode == View.Mode.ADD_FORM)
							return false;
						return data.getBoolean("register_people");
					}
				}.setShowAddButtonOnReadOnlyForm(true));
			}
			addAgreements(cnf, view_def);
			cnf.add("_owner_");
			view_def.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by"));
			cnf.add("_timestamp_");
			view_def.setColumn(new Column("_timestamp_").setDisplayName("added").setHideOnForms(Mode.ADD_FORM).setIsReadOnly(true));
			view_def.setColumnNamesForm(cnf);
			if (m_text_after_form != null)
				view_def.setAfterForm(m_text_after_form);
			return view_def;
		}
		if (name.equals(m_events_table + "_attachments"))
			return m_attachments._newViewDef(name);
		if (m_events_have_category) {
			ViewDef view_def = m_categories.newViewDef(name);
			if (view_def != null)
				return view_def
					.setColumn(new ColorColumn("color"));
		}
		if (name.equals(m_name + "_registrations"))
			return new ViewDef(name)
				.setFrom(m_events_table + "_registrations")
				.setRecordName("Registration", true)
				.setColumnNamesTableAndForm(new String[] { "person", "note" })
				.setColumn(new PersonColumn("person", false));
		if (name.equals(m_name + "_reminders")) {
			RecordOwnerAccessPolicy access_policy = (RecordOwnerAccessPolicy) new RecordOwnerAccessPolicy().add().delete().edit();
			return new ViewDef(name)
				.setAccessPolicy(access_policy)
				.setFrom(getRemindersTable())
				.setRecordName("Reminder", true)
				.setColumnNamesForm(new String[] { "before", "note", "email" })
				.setColumnNamesTable(new String[] { "email", "before" })
				.setColumn(new EmailColumn("email").setAddressProvider(this).setDefaultToUserEmail().setDisplayName("send to").setIsRequired(true))
				.setColumn(new ReminderWhenColumn("before", m_name, m_events_table, m_events_have_start_time, m_events_can_repeat, false).setDisplayName("when"))
				.setColumn(access_policy.getColumn("person"));
		}
		if (name.equals(m_name + "_reminders auto")) {
			ViewDef v = new ViewDef(name)
				.setFrom(getRemindersTable())
				.setRecordName("Reminder", true)
				.setColumnNamesForm(new String[] { "before", "note", "email" })
				.setColumnNamesTable(new String[] { "email", "before" })
				.setColumn(new ReminderWhenColumn("before", m_name, m_events_table, m_events_have_start_time, m_events_can_repeat, true).setDisplayName("when"));
			Column email_column = new EmailColumn("email").setAddressProvider(this).setDisplayName("send to").setIsRequired(true);
			if (m_events_table != null)
				v.setBaseFilter(m_events_table + "_id IS NULL");
			v.setColumn(email_column);
			return v;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	static LocalDate
	next(LocalDate date, LocalDate end, String repeat) {
		if (repeat == null || repeat.equals("never"))
			return null;
		if (end != null && date.isAfter(end))
			return null;
		repeat = repeat.substring(6); // strip off "every "
		switch(repeat) {
		case "day":
			return date.plusDays(1);
		case "week":
			return date.plusWeeks(1);
		case "other week":
			return date.plusWeeks(2);
		case "month (same day, last week)":
			DayOfWeek dow = date.getDayOfWeek();
			LocalDate d = date.plusMonths(2).withDayOfMonth(1).minusDays(1);
			while (d.getDayOfWeek() != dow)
				d = d.minusDays(1);
			return d;
		case "month (same week and day)":
			dow = date.getDayOfWeek();
			int week = date.get(ChronoField.ALIGNED_WEEK_OF_MONTH);
			d = date.plusMonths(1).withDayOfMonth(1);
			while (d.getDayOfWeek() != dow)
				d = d.plusDays(1);
			while (d.get(ChronoField.ALIGNED_WEEK_OF_MONTH) != week)
				d = d.plusDays(7);
			return d;
		case "month (same date)":
			return date.plusMonths(1);
		case "year":
			return date.plusYears(1);
		}
		int i = Character.isDigit(repeat.charAt(0)) ? Integer.parseInt(repeat.substring(0, repeat.indexOf(' '))) : 0;
		if (repeat.endsWith("weeks"))
			return date.plusWeeks(i);
		if (repeat.endsWith("months"))
			return date.plusMonths(i);
		if (repeat.endsWith("years"))
			return date.plusYears(i);
		return null;
	}

	//--------------------------------------------------------------------------

	public static boolean
	occursOn(LocalDate date, LocalDate start, LocalDate end, String repeat) {
		if (repeat == null || repeat.equals("never"))
			return date == null || start == null ? false : date.isEqual(start);
		if (date.isBefore(start))
			return false;
		if (end != null && date.isAfter(end))
			return false;
		repeat = repeat.substring(6); // strip off "every "
		if (repeat.equals("day"))
			return true;
		if (repeat.equals("week"))
			return date.getDayOfWeek() == start.getDayOfWeek();
		if (repeat.equals("other week"))
			return ChronoUnit.DAYS.between(start, date) % 14 == 0;
		if (repeat.endsWith("weeks"))
			return date.getDayOfWeek() == start.getDayOfWeek() && ChronoUnit.WEEKS.between(start, date) % Integer.parseInt(repeat.substring(0, repeat.indexOf(' '))) == 0;
		if (repeat.equals("month (same day, last week)"))
			return date.getDayOfWeek() == start.getDayOfWeek() && date.getDayOfMonth() >= date.lengthOfMonth() - 6;
		if (repeat.equals("month (same week and day)"))
			return date.getDayOfWeek() == start.getDayOfWeek() && date.get(ChronoField.ALIGNED_WEEK_OF_MONTH) == start.get(ChronoField.ALIGNED_WEEK_OF_MONTH);
		if (repeat.equals("month (same date)"))
			return date.getDayOfMonth() == start.getDayOfMonth();
		if (repeat.endsWith("months"))
			return date.getDayOfMonth() == start.getDayOfMonth() && ChronoUnit.MONTHS.between(start, date) % Integer.parseInt(repeat.substring(0, repeat.indexOf(' '))) == 0;
		if (repeat.equals("year"))
			return date.getMonthValue() == start.getMonthValue() && date.getDayOfMonth() == start.getDayOfMonth();
		if (repeat.endsWith("years"))
			return date.getMonthValue() == start.getMonthValue() && date.getDayOfMonth() == start.getDayOfMonth() && ChronoUnit.YEARS.between(start, date) % Integer.parseInt(repeat.substring(0, repeat.indexOf(' '))) == 0;

		return false;
	}

	//--------------------------------------------------------------------------

	private EventProvider
	overlaps(LocalDate date, LocalDate end_date, LocalTime start_time, LocalTime end_time, List<Integer> locations, int event_id, DBConnection db, boolean check_others) {
		if (date == null)
			return null;
		Select query = new Select().from(m_events_table);
		if (end_date != null)
			query.where("'" + date.toString() + "' <= date AND '" + end_date.toString() + "' >= date");
		else
			query.where("'" + date.toString() + "' = date");
		if (m_events_have_start_time && start_time != null) {
			if (end_time == null)
				end_time = start_time.plusMinutes(30);
			query.andWhere("'" + start_time.toString() + "' < end_time AND '" + end_time.toString() + "' > start_time");
		}
		if (m_calendar_location_id != 0) {
			if (!locations.contains(m_calendar_location_id))
				return null;
		} else if (m_events_have_location) {
			if (locations == null || locations.size() == 0)
				return null;
			Locations.adjustOverlapsQuery(query, locations, m_events_table, m_support_multiple_locations);
		}
		if (event_id != 0)
			query.andWhere("id != " + event_id);
		if (m_events_can_repeat)
			query.andWhere("repeat IS NULL OR repeat='never'");
		if (db.exists(query))
			return this;
		if (!check_others)
			return null;
		for (EventProvider ep : Modules.getByClass(EventProvider.class)) {
			if (ep == this || !sharesLocations(ep))
				continue;
			if (ep.overlaps(date, end_date, start_time, end_time, locations, 0, db, false) != null)
				return ep;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public EventProvider
	overlaps(LocalDate date, LocalDate end_date, LocalTime start_time, LocalTime end_time, List<Integer> locations, int event_id, DBConnection db) {
		return overlaps(date, end_date, start_time, end_time, locations, event_id, db, true);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		if (((String)object).equals(m_events_table))
			Site.site.newFile("calendars", m_name + ".ics").delete();
		m_reminders = null;
	}

	//--------------------------------------------------------------------------

	public boolean
	repeatingEventsCanBeSplit() {
		return true;
	}

	//--------------------------------------------------------------------------

	protected Select
	selectEvents(LocalDate from, LocalDate to, int num_events, Request r) {
		if (m_events_table == null)
			return null;
		Select query = new Select("*").from(m_events_table).where("date>='" +from.toString() + "'").orderBy(num_events > 0 ? "date" : null);
		if (to != null)
			query.andWhere("date<'" + to.toString() + "'");
		if (m_events_can_repeat)
			query.andWhere("repeat='never'");
		if (num_events > 0)
			query.limit(num_events);
		return query;
	}

	//--------------------------------------------------------------------------

	protected Select
	selectRepeatingEvents(LocalDate from, LocalDate to, Request r) {
		StringBuilder where = new StringBuilder();
		if (to != null)
			where.append("date<'").append(to.toString()).append("' AND ");
		where.append("(end_date IS NULL OR end_date>='").append(from.toString()).append("')");
		return new Select("*").from(m_events_table).where(where.toString()).andWhere("repeat<>'never'").andWhere("event_id IS NULL");
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setAccessPolicy(AccessPolicy access_policy) {
		m_access_policy = access_policy == null ? null : new Or(access_policy, new RoleAccessPolicy("calendar editor"));
		Site.site.removeViewDef(m_name);
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setAllowViewSwitching(boolean allow_view_switching) {
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setColor(String color) {
		m_color = color;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setEventsCanRepeat(boolean events_can_repeat) {
		m_events_can_repeat = events_can_repeat;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setEventsHaveCategory(boolean events_have_category) {
		m_events_have_category = events_have_category;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setEventsHaveColor(boolean events_have_color) {
		m_events_have_color = events_have_color;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setEventsHaveLocation(boolean events_have_location) {
		m_events_have_location = events_have_location;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setEventsHaveTime(boolean events_have_start_time) {
		m_events_have_start_time = events_have_start_time;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setEventsTable(String event_table) {
		m_events_table = event_table;
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	setLocationColumn(ViewDef view_def, ArrayList<String> column_names_form) {
		if (!m_events_have_location)
			return;
		column_names_form.add("locations_id");
		if (m_support_multiple_locations)
			view_def.setColumn(new Locations.MultipleLocationsColumn(this));
		else
			view_def.setColumn(new OptionsColumn("locations_id")
				.setAllowNoSelection(m_start_with_blank_location)
				.setDisplayName("location")
				.setIsRequired(m_location_required)
				.setOptions(Locations.getLocationOptions(m_locations)));
	}

	//--------------------------------------------------------------------------

	public final void
	setOverlaps(PriorityQueue<Event> events) {
		ArrayList<Event> e = new ArrayList<>();
		for (Event t : events)
			if (t.hasLocation())
				e.add(t);
		for (int i=0; i<e.size(); i++)
			for (int j=i+1; j<e.size(); j++)
				if (Event.setOverlaps(e.get(i), e.get(j)))
					break;
	}

	//--------------------------------------------------------------------------

	final void
	setOverlapsWithOtherCalendars(PriorityQueue<Event> events, Request r) {
		LocalDate from = null;
		LocalDate to = null;
		for (Event event : events) {
			if (from == null || from.isAfter(event.m_start_date))
				from = event.m_start_date;
			if (to == null || to.isBefore(event.m_start_date))
				to = event.m_start_date;
		}
		if (from == null)
			return;
		to = to.plusDays(1); // addEvents is not inclusive
		for (EventProvider ep : Modules.getByClass(EventProvider.class)) {
			if (ep == this || !sharesLocations(ep))
				continue;
			PriorityQueue<Event> e = new PriorityQueue<>();
			ep.addEvents(e, from, to, r);
			for (Event event : events) {
				if (event.isOverlapping())
					continue;
				for (Event event2 : e)
					if (Event.setOverlaps(event, event2))
						break;
			}
		}
	}

	//--------------------------------------------------------------------------

	public static void
	setReminderHandler(ReminderHandler reminder_handler) {
		s_reminder_handler = reminder_handler;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setRole(String role) {
		m_role = role;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setShowOnMenu(boolean show_on_menu) {
		m_show_on_menu = show_on_menu;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setSupportRegistrations(boolean support_registrations) {
		m_support_registrations = support_registrations;
		return this;
	}

	//--------------------------------------------------------------------------

	public final EventProvider
	setSupportReminders() {
		m_support_reminders = true;
		return this;
	}

	//--------------------------------------------------------------------------

	private boolean
	sharesLocations(EventProvider event_provider) {
		if (m_calendar_location_id != 0 && m_calendar_location_id == event_provider.m_calendar_location_id)
			return true;
		if (!event_provider.m_events_have_location)
			return false;
		if (m_locations == null || event_provider.m_locations == null)
			return false;
		for (int i : m_locations)
			for (int j : event_provider.m_locations)
				if (i == j)
					return true;
		return false;
	}

	//--------------------------------------------------------------------------

	public boolean
	showAddButton(Request r) {
		return m_access_policy == null || m_access_policy.showAddButton(null, r);
	}

	//--------------------------------------------------------------------------

	public boolean
	showOnMenu(Request r) {
		return isActive() && m_show_on_menu;
	}

	//--------------------------------------------------------------------------

	public final boolean
	supportsAnnouncingNewEvents() {
		return m_support_announcing_new_events;
	}

	//--------------------------------------------------------------------------

	public final boolean
	supportsReminders() {
		return m_support_reminders;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		if (m_role != null)
			setAccessPolicy(null);
		DBObject.set(this, r);
		if (m_role != null)
			setAccessPolicy(new RoleAccessPolicy(m_role).add().delete().edit());
		super.updateSettings(r);
	}

	//--------------------------------------------------------------------------

	protected String
	validateTime(LocalTime start_time, LocalTime end_time) {
		if (start_time != null && end_time != null && start_time.isAfter(end_time))
			return "The end time must be after the start time.";
		return null;
	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	protected void
	writeAfterView(Request r) {
	}

	//--------------------------------------------------------------------------

	final void
	writeAutomaticReminders(Request r) {
		if (!m_support_reminders)
			return;

		HTMLWriter w = r.w;
		Select query = new Select("*").from(getRemindersTable());
		if (m_events_table != null)
			query.where(m_events_table + "_id IS NULL");
		Rows rows = new Rows(query, r.db);
		if (rows.isBeforeFirst()) {
			w.write("<tr><td>");
			w.h4("Automatic Reminders");
			Table table = w.ui.table().addClass("table").addClass("table-bordered").addClass("table-condensed").open();
			table.tr().th("send to").th("when").th("note");
			while (rows.next()) {
				table.tr().td(rows.getString("email"));
				table.td();
				int num = rows.getInt("num");
				String repeat_days = null;
				String unit = rows.getString("unit");

				if (m_events_can_repeat)
					repeat_days = rows.getString("repeat_days");
				if (num == 0 && "day".equals(unit))
					w.write(repeat_days == null ? "on the day" : "on the day of");
				else {
					w.write(num).space().write(Text.pluralize(unit, num));
					w.write(rows.getBoolean("before") ? " before" : " after");
				}
				if (m_events_can_repeat)
					if (repeat_days != null && !"never".equals(repeat_days))
						w.space().write(repeat_days);
				table.td(rows.getString("note"));
			}
			rows.close();
			table.close();
			w.write("</td></tr>");
		}
	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	public void
	writeControlButtons(Request r) {
		r.w.setAttribute("title", "calendar for printing")
			.addStyle("margin-left:2px")
			.ui.buttonIconOnClick("printer", "Calendar.print()");
	}

	//--------------------------------------------------------------------------

	private void
	writeEventForm(Request r) {
		if (m_support_reminders)
			if (r.userIsAdmin())
				ViewState.setBaseFilter(m_name + "_reminders", null, r);
			else {
				Person user = r.getUser();
				if (user != null)
					ViewState.setBaseFilter(m_name + "_reminders", "_owner_=" + user.getId(), r);
				else
					ViewState.setBaseFilter(m_name + "_reminders", "_owner_=0", r);
			}
		View v = Site.site.newView(m_name, r);
		if (v == null)
			return;
		Form f = v.newEditForm();
		if (f == null)
			return;
		if (v.getMode() == View.Mode.EDIT_FORM && m_access_policy != null && !r.userIsAdmin() && !m_access_policy.showEditButtonForRow(v.data, r))
			f.setMode(View.Mode.READ_ONLY_FORM);
		f.write();
	}

	//--------------------------------------------------------------------------

	final void
	writeFilters(Request r) {
		HTMLWriter w = r.w;
		if (m_events_have_category) {
			w.write("<span style=\"white-space:nowrap\">View categories: ")
				.addStyle("font-size:inherit");
			ui.Select select = new ui.Select("calendar_categories", new String[0]).setInline(true);
			select.addFirstOption(new ui.Option("All", "All"))
				.setOnChange("Calendar.$().set_category(this.options[this.selectedIndex].text)")
				.write(r.w);
			w.write("</span> ");
		}
		if (m_events_have_location) {
			w.write("<span style=\"white-space:nowrap\">View locations ")
				.addStyle("font-size:inherit");
			ui.Select select = new ui.Select("calendar_locations", new String[0]).setInline(true);
			select.addFirstOption(new ui.Option("All", "All"))
				.setOnChange("Calendar.$().set_location(this.options[this.selectedIndex].text)")
				.write(r.w);
			w.write("</span>");
		}
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public synchronized final void
	writeICS(Request r) {
		ICalendar ical = new ICalendar();
		String string = Site.settings.getString("time zone");
		TimeZone time_zone = TimeZone.getTimeZone(string);
		TimezoneAssignment tza = TimezoneAssignment.download(time_zone, false);
		ical.getTimezoneInfo().setDefaultTimezone(tza);
		ArrayList<Event> events = new ArrayList<>();
		addAllEvents(events, r);
		for (Event event : events) {
			if (event.isRepeating() && "deleted".equals(m_events_have_event ? event.m_event_text : event.m_notes))
				continue;
			VEvent vevent = event.newVEvent(r);
			if (vevent != null)
				ical.addEvent(vevent);
		}
		File fpsb = Site.site.newFile("calendars");
		fpsb.mkdirs();
		File file_path = Site.site.newFile("calendars", m_name);
		try {
			Biweekly.write(ical).go(file_path);
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		file_path.renameTo(Site.site.newFile("calendars", m_name + ".ics"));
	}

	//--------------------------------------------------------------------------

	private void
	writePublicCalendar(Request r) {
		new Head(Site.site.getDisplayName(), r).script("site-min").close();
		r.w.styleSheet(Site.themes.getTheme(r)).style("body{background:transparent;background-image:background-image:none;");
		Time.newDate();
		MonthView v = new MonthView(this, r);
		HTMLWriter w = r.w;
		w.write("<div id=\"calendar\" style=\"padding:0\">");
		v.writeHead(false, r);
		w.write("<div id=\"c_calendar_view\" class=\"calendar_component\">");
		v.write(Time.newDate(), r);
		w.write("</div>");
		String js = "new Calendar(" + JS.string(m_name) + "," + JS.string(getDisplayName(r)) + "," + JS.string(v.getView().toString()) + ");";
		w.js("JS.get('" + Site.site.getResources().get("js", "calendar-min") + "',function(){" + js + "});");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsFormOtherRows(ui.Form f, Request r) {
		if (!m_support_reminders)
			return;
		f.rowOpen("automatic reminders");
		Site.site.newView(m_name + "_reminders auto", r).writeComponent();
		f.rowClose();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsInput(Field field, Request r) {
		String field_name = field.getName();
		if (field_name.equals("m_display_items"))
			new Tags("display_items", m_display_items, getFieldOptions("m_display_items")).setAllowDragging(true).setRestrictToChoices(true).write(r.w);
		else if (field_name.equals("m_reminder_subject_items"))
			new Tags("reminder_subject_items", m_reminder_subject_items, getFieldOptions("m_reminder_subject_items")).setAllowDragging(true).setRestrictToChoices(true).write(r.w);
		else if (field_name.equals("m_tooltip_items"))
			new Tags("tooltip_items", m_tooltip_items, getFieldOptions("m_tooltip_items")).setAllowDragging(true).setRestrictToChoices(true).write(r.w);
		else if (field_name.equals("m_uuid"))
			r.w.aOnClick("see public url", "Dialog.alert('Public URL','To embed this calendar on a public web page, use the following URL:<br><br>https://' + domain + context + '/" + JS.escape(m_name) + "/public/" + getUUID(r.db) + "')");
		else
			super.writeSettingsInput(field, r);
	}
}
