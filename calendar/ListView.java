package calendar;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.PriorityQueue;

import app.Request;
import util.Time;
import web.HTMLWriter;

public class ListView extends CalendarView {
	public
	ListView(EventProvider event_provider, Request r) {
		super(event_provider, r);
	}

	//--------------------------------------------------------------------------

	@Override
	View
	getView() {
		return View.LIST;
	}

	//--------------------------------------------------------------------------

	@Override
	public
	void
	write(LocalDate date, Request r) {
		PriorityQueue<Event>	events = new PriorityQueue<>();
		HTMLWriter				w = r.w;

		for (int i=0; i<3; i++) {
			m_ep.addCalendarsEvents(events, date.plusDays(i * 30), date.plusDays((i + 1) * 30), r);
			if (events.size() >= 20)
				break;
		}
		if (events.size() == 0) {
			w.write("There are no upcoming events in the next 90 days");
			return;
		}

		adjustEvents(events, r);
		LocalDate previous_date = null;
		LocalDate today = Time.newDate();
		while (events.size() > 0) {
			Event event = events.poll();
			LocalDate start_date = event.getStartDate();
			if (previous_date == null || previous_date.getYear() != start_date.getYear() || previous_date.getDayOfYear() != start_date.getDayOfYear()) {
				if (previous_date != null)
					w.write("</div>");
				previous_date = start_date;
				w.write("<div class=\"calendar_date\">");
				writeDateDOW(start_date, today, r);
				w.write("</div><div class=\"calendar_events\">");
			}
			EventProvider event_provider = event.getEventProvider();
			event.write(previous_date, event_provider != m_ep ? event_provider.getDisplayName(r) :  null, r);
		}
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	private static void
	writeDateDOW(LocalDate date, LocalDate today, Request r) {
		long diff_days = ChronoUnit.DAYS.between(today, date);

		if (diff_days == 0)
			r.w.write("Today");
		else if (diff_days == 1)
			r.w.write("Tomorrow");
		else
			r.w.write(Time.formatter.getWeekdayLong(date)).space().write(Time.formatter.getMonthNameLong(date)).space().write(date.getDayOfMonth());
	}
}
