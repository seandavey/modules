package calendar;

import java.time.LocalDate;
import java.util.Collection;
import java.util.PriorityQueue;

import app.Request;
import util.Array;

public class UpcomingEventsProvider extends EventProvider {
	private boolean m_upcoming_all_calendars;

	//--------------------------------------------------------------------------

	public
	UpcomingEventsProvider(boolean upcoming_all_calendars) {
		super("upcoming");
		m_upcoming_all_calendars = upcoming_all_calendars;
		m_events_table = null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addCalendarsEvents(PriorityQueue<Event> events, LocalDate from, LocalDate to, Request r) {
		Collection<EventProvider> calendars = null;
		String[] checked_calendars = null;
		calendars = Calendars.getCalendars(r);
		if (!m_upcoming_all_calendars)
			checked_calendars = Calendars.getCheckedCalendars(r.getUser());
		for (EventProvider ep : calendars)
			if (ep.isActive() && (m_upcoming_all_calendars || checked_calendars != null && Array.indexOf(checked_calendars, ep.getName()) != -1))
				ep.addEvents(events, from, to, r);
	}
}
