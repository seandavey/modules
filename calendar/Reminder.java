package calendar;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import app.Request;
import db.DBConnection;
import db.Rows;
import db.Select;
import mail.Mail;
import util.Time;

public class Reminder {
	private boolean				m_before;
	private final LocalDate		m_date;
	private String				m_email;
	private final EventProvider	m_ep;
	private Event				m_event;
	private final LocalTime		m_event_end_time;
	private final int			m_event_id;
	private final LocalTime		m_event_start_time;
	private int					m_id;
	private boolean				m_is_automatic;
	private String				m_note;
	private int					m_num;
	private final int			m_num_days;
	private String				m_repeat_days;
	private String				m_unit;

	//--------------------------------------------------------------------------

	public
	Reminder(int event_id, LocalTime event_start_time, LocalTime event_end_time, EventProvider ep, Rows rows) {
		m_event_end_time = event_end_time;
		m_event_id = event_id;
		m_event_start_time = event_start_time;
		m_ep = ep;
		m_is_automatic = true; // for reminders without event table. for those with event table, this will be set below
		for (int i=1,n=rows.getColumnCount(); i<=n; i++) {
			String column = rows.getColumnName(i);
			switch(column) {
			case "before":
				m_before = rows.getBoolean(i);
				break;
			case "email":
				m_email = rows.getString(i);
				break;
			case "note":
				m_note = rows.getString(i);
				break;
			case "num":
				m_num = rows.getInt(i);
				break;
			case "one_id":
				m_is_automatic = rows.getString(i) == null;
				break;
			case "reminder_id":
				m_id = rows.getInt(i);
				break;
			case "repeat_days":
				m_repeat_days = rows.getString(i);
				break;
			case "unit":
				m_unit = rows.getString(i);
				break;
			}
		}
		m_num_days = numDays(m_num, m_unit, m_before);
		m_date = Time.newDate().minusDays(m_num_days);
	}

	//--------------------------------------------------------------------------

	public final LocalDate
	getDate() {
		return m_date;
	}

	//--------------------------------------------------------------------------

	public final Event
	getEvent() {
		return m_event;
	}

	//--------------------------------------------------------------------------

	private Mail
	getMail(String from, String subject, String message, DBConnection db) {
		Mail mail = new Mail(subject, message.toString())
			.replyTo(from);
		if (m_event != null) {
			if ("poster".equals(m_email)) {
				String recipient = db.lookupString(new Select("email").from("people").whereIdEquals(m_event.getOwner()).andWhere("active"));
				if (recipient == null)
					return null;
				mail.to(recipient);
			} else {
				String[] recipients = m_event.lookupEmail(m_email, db);
				if (recipients != null)
					mail.to(recipients);
			}
		} else
			mail.to(m_email);
		return mail;
	}

	//--------------------------------------------------------------------------

	public final String
	getNote() {
		return m_note;
	}

	//--------------------------------------------------------------------------

	final boolean
	isAfter(LocalDateTime now) {
		if (m_event_start_time == null)
			return false;
		if  (!m_unit.equals("hour") && !m_unit.equals("minute"))
			return false;
		LocalTime time = m_before || m_event_end_time == null ? m_event_start_time : m_event_end_time;
		if (m_unit.equals("hour"))
			if (m_before)
				time = time.minusHours(m_num);
			else
				time = time.plusHours(m_num);
		else
			if (m_before)
				time = time.minusMinutes(m_num);
			else
				time = time.plusMinutes(m_num);
		return now.toLocalTime().isAfter(time.plusMinutes(5));
	}

	//--------------------------------------------------------------------------

	final boolean
	isBefore(LocalDateTime now) {
		if (m_event_start_time == null)
			return false;
		if  (!m_unit.equals("hour") && !m_unit.equals("minute"))
			return false;
		LocalTime time = m_before || m_event_end_time == null ? m_event_start_time : m_event_end_time;
		if (m_unit.equals("hour"))
			if (m_before)
				time = time.minusHours(m_num);
			else
				time = time.plusHours(m_num);
		else
			if (m_before)
				time = time.minusMinutes(m_num);
			else
				time = time.plusMinutes(m_num);
		return now.toLocalTime().isBefore(time);
	}

	//--------------------------------------------------------------------------

	final boolean
	isLastEvent() {
		return "last event".equals(m_repeat_days);
	}

	//--------------------------------------------------------------------------

	private void
	loadEvent(DBConnection db) {
		if (m_event_id == 0)
			return;
		Rows rows = new Rows(new Select("*").from(m_ep.m_events_table).whereIdEquals(m_event_id), db);
		if (rows.next())
			m_event = m_ep.loadEvent(rows);
		rows.close();
	}

	//--------------------------------------------------------------------------

	public static int
	numDays(int num, String unit, boolean before) {
		if (unit.equals("day"))
			return num * (before ? -1 : 1);
		if (unit.equals("week"))
			return num * 7 * (before ? -1 : 1);
		return 0;
	}

	//--------------------------------------------------------------------------
	// return reminder id if it's safe to delete this reminder

	final int
	send(LocalDateTime now, DBConnection db) {
		loadEvent(db);
		String from = m_event != null ? EventProvider.getEmail(m_event.getOwner(), db) : null;
		String message = m_ep.buildReminderMessage(this, db);
		String subject = m_ep.buildReminderSubject(this, db);
		if (EventProvider.s_reminder_handler == null || !EventProvider.s_reminder_handler.handleReminder(m_ep, from, m_email, subject, message, db))
			if (m_email != null && m_email.length() != 0)
				getMail(from, subject, message, db).send();
		boolean is_not_repeating = m_event != null && !m_event.isRepeating();
		m_event = null;
		return !m_is_automatic && is_not_repeating ? m_id : 0;
	}

	//--------------------------------------------------------------------------

	public final void
	write(Request r) {
		DBConnection db = r.db;
		loadEvent(db);
		String from = m_event != null ? EventProvider.getEmail(m_event.getOwner(), db) : null;
		String message = m_ep.buildReminderMessage(this, db);
		String subject = m_ep.buildReminderSubject(this, db);
		getMail(from, subject, message, db).write(r.w);
		m_event = null;
	}
}
