package calendar;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import db.DBConnection;

public class Reminders {
	private final LocalDate				m_date;
	private final ArrayList<Reminder>	m_reminders;
	private final String				m_table;

	//--------------------------------------------------------------------------

	public
	Reminders(ArrayList<Reminder> reminders, String table, LocalDate date) {
		m_reminders = reminders;
		m_table = table;
		m_date = date;
	}

	//--------------------------------------------------------------------------

	public final LocalDate
	getDate() {
		return m_date;
	}

	//--------------------------------------------------------------------------

	public static boolean
	occursOn(LocalDate today, LocalDate reminder_date, String repeat_days, LocalDate event_start, LocalDate event_end, String event_repeat) {
		if (repeat_days != null) {
			if (repeat_days.startsWith("first ") && event_start != null)
				return today.isEqual(event_start);
			if (repeat_days.startsWith("last ") && event_end != null)
				return today.isEqual(event_end);
		}
		return EventProvider.occursOn(reminder_date, event_start, event_end, event_repeat);
	}

	//--------------------------------------------------------------------------

	public final void
	send(LocalDateTime now, DBConnection db) {
		if (m_reminders == null || m_reminders.size() == 0)
			return;
		ArrayList<Reminder> sent = new ArrayList<>();
		for (Reminder reminder : m_reminders) {
			if (reminder.isBefore(now))
				continue;
			if (!reminder.isAfter(now)) {
				int id = reminder.send(now, db);
				if (id != 0)
					db.delete(m_table, id, false);
			}
			sent.add(reminder);
		}
		m_reminders.removeAll(sent);
	}
}
