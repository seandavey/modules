package calendar;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import app.Request;
import app.Site;
import app.Site.Message;
import app.SiteModule;
import app.Subscriber;
import db.DBConnection;
import db.Form;
import db.Option;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.column.Column;
import db.column.ColumnBase;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import ui.SelectOption;
import util.Array;
import util.Text;

public class Locations extends SiteModule implements Subscriber {
	public static class MultipleLocationsColumn extends ColumnBase<MultipleLocationsColumn> {
		private EventProvider m_ep;
		public
		MultipleLocationsColumn(EventProvider ep) {
			super("locations_id");
			m_ep = ep;
			m_display_name = "reserved locations";
		}
		@Override
		public void
		writeInput(View v, Form f, boolean inline, Request r) {
			Collection<SelectOption> location_options = Locations.getLocationOptions(m_ep.m_locations);
			String first_cb_id = null;
			for (SelectOption location : location_options) {
				if (first_cb_id == null)
					first_cb_id = "db_location" + location.getValue() + "_cb";
				boolean checked = v.getMode() == Mode.EDIT_FORM ? r.db.exists(m_ep.m_events_table + "_location_links", m_ep.m_events_table + "_id=" + v.data.getInt("id") + " AND locations_id=" + location.getValue()) : false;
				r.w.ui.checkbox("db_location" + location.getValue(), location.getText(), null, checked, false, true);
			}
			if (m_is_required && first_cb_id != null)
				r.w.js("_.$('#" + first_cb_id + "').form.required_cb_name='db_location';");
		}
		@Override
		public boolean
		writeValue(View v, Map<String,Object> data, Request r) {
			List<String> locations = r.db.readValues(
				new Select("name")
					.from(m_ep.m_events_table + "_location_links")
					.joinOn("locations", "locations_id=locations.id")
					.whereEquals(m_ep.m_events_table + "_id",
				v.data.getInt("id")));
			if (m_ep.m_support_other_locations) {
				String ol = v.data.getString("other_locations");
				if (ol != null) {
					String[] ols = ol.split(",");
					for (String l : ols)
						locations.add(l.trim());
				}
			}
			locations.sort(Site.collator);
			r.w.write(Text.join(",", locations));
			return true;
		}
	}
	public static Collection<SelectOption> locations;

	//--------------------------------------------------------------------------

	public
	Locations() {
		m_description = "Master list of locations for the site. Calendars use subsets of this list.";
		m_required = true;
		Site.site.subscribe("locations", this);
	}

	//--------------------------------------------------------------------------

	public static void
	adjustLocation(JDBCTable table_def, String events_table, boolean support_multiple_locations, DBConnection db) {
		if (support_multiple_locations) {
			String links_table = events_table + "_location_links";
			db.createTable(links_table, false, events_table + "_id INTEGER REFERENCES " + events_table + "(id) ON DELETE CASCADE,locations_id INTEGER REFERENCES locations(id) ON DELETE CASCADE", null, false);
			if (db.hasColumn(events_table, "locations_id")) {
				db.execute("INSERT INTO " + links_table + "(" + events_table + "_id,locations_id) SELECT id,locations_id FROM " + events_table + " WHERE locations_id IS NOT NULL");
				db.dropColumn(events_table, "locations_id");
			}
		} else
			table_def.add(new JDBCColumn("locations").setOnDeleteSetNull(true));
	}

	//--------------------------------------------------------------------------

	public static void
	adjustOverlapsQuery(Select query, List<Integer> locations, String events_table, boolean support_multiple_locations) {
		if (support_multiple_locations) {
			query.join(events_table, events_table + "_location_links");
			if (locations.size() == 1)
				query.andWhere(events_table + "_location_links.locations_id = " + locations.get(0));
			else
				query.andWhere(events_table + "_location_links.locations_id IN (" + Text.join(",", locations) + ")");
		} else if (locations.size() == 1)
			query.andWhere("locations_id = " + locations.get(0));
		else
			query.andWhere("locations_id IN (" + Text.join(",", locations) + ")");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("locations")
			.add(new JDBCColumn("name", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, true, db);
	}

	//--------------------------------------------------------------------------

	public static synchronized Collection<SelectOption>
	getLocationOptions(int[] ids) {
		if (ids == null)
			return locations;
		ArrayList<SelectOption> a = new ArrayList<>();
		for (SelectOption o : locations) {
			int id = Integer.parseInt(o.getValue());
			if (Array.indexOf(ids, id) != -1)
				a.add(o);
		}
		return a;
	}

	// --------------------------------------------------------------------------

	public static List<Integer>
	getLocations(boolean support_multiple_locations, Request r) {
		List<Integer> locations = null;
		if (support_multiple_locations) {
			Enumeration<String> names = r.request.getParameterNames();
			while (names.hasMoreElements()) {
				String name = names.nextElement();
				if (name.startsWith("db_location") && r.getBoolean(name)) {
					if (locations == null)
						locations = new ArrayList<>();
					locations.add(Integer.valueOf(name.substring(11)));
				}
			}
		} else {
			int location_id = r.getInt("locations_id", 0);
			if (location_id != 0) {
				locations = new ArrayList<>();
				locations.add(location_id);
			}
		}
		return locations;
	}

	//--------------------------------------------------------------------------

	public static SelectOption
	getOptionByValue(int id) {
		String i = Integer.toString(id);
		for (SelectOption o : locations)
			if (i.equals(o.getValue()))
				return o;
		return null;
	}

	//--------------------------------------------------------------------------

	public static String
	getSelectColumn(String events_table, boolean support_multiple_locations) {
		return support_multiple_locations
			? "(SELECT string_agg(name, ', ') FROM " + events_table + "_location_links JOIN locations ON locations_id=locations.id WHERE " +
				events_table + "_id=" + events_table + ".id GROUP BY " + events_table + ".id) AS location"
			: "(SELECT string_agg(name, ', ') FROM locations WHERE id=locations_id GROUP BY id) AS location";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		loadLocations(db);
	}

	//--------------------------------------------------------------------------

	private void
	loadLocations(DBConnection db) {
		ArrayList<SelectOption> l = new ArrayList<>();
		try (Rows rows = new Rows(new Select("id,name").from("locations").orderBy("lower(name)"), db)) {
			while (rows.next())
				l.add(new Option(rows.getString(2), rows.getString(1)));
		}
		locations = l;
	}

	//--------------------------------------------------------------------------

	public static SelectOption[]
	loadLocations(String events_table, int id, boolean support_multiple_locations, Rows rows) {
		SelectOption[] o = null;
		if (support_multiple_locations)
			try (Rows ids = new Rows(new Select("locations_id").from(events_table + "_location_links").whereEquals(events_table + "_id", id), rows.getDB())) {
				if (ids.isBeforeFirst()) {
					ArrayList<SelectOption> l = new ArrayList<>();
					while (ids.next())
						l.add(getOptionByValue(ids.getInt(1)));
					o = l.toArray(new SelectOption[l.size()]);
					Arrays.sort(o);
				}
			}
		else {
			int location_id = rows.getInt("locations_id");
			if (location_id != 0)
				o = new SelectOption[] { getOptionByValue(location_id) };
		}
		return o;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("locations"))
			return new ViewDef(name)
				.setDefaultOrderBy("lower(name)")
				.setRecordName("Location", true)
				.setColumn(new Column("name").setIsRequired(true));
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		loadLocations(db);
	}

	// --------------------------------------------------------------------------

	public static void
	setEventLocations(String events_table, int id, Request r) {
		r.db.delete(events_table + "_location_links", events_table + "_id", id, false);
		Enumeration<String> params = r.request.getParameterNames();
		while (params.hasMoreElements()) {
			String param = params.nextElement();
			if (param.startsWith("db_location") && "true".equals(r.getParameter(param)))
				r.db.insert(events_table + "_location_links", events_table + "_id,locations_id", id + "," + param.substring(11));
		}

	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] field_names, boolean in_dialog, boolean hide_active, Request r) {
		Site.site.newView("locations", r).writeComponent();
	}

	//--------------------------------------------------------------------------

//	@ClassTask({"from id","to id 1","to id 2","to id 3","to id 4","to id 5"})
//	public static void
//	migrateLocation(int from_id, int to_id1, int to_id2, int to_id3, int to_id4, int to_id5, DBConnection db) {
//		List<String[]> referencing_tables = db.getExportedKeys("locations");
//		for (String[] referencing_table : referencing_tables) {
//			String table = referencing_table[0];
//			db.update(table, "locations_id=" + to_id1, "locations_id=" + from_id);
//			int index = table.indexOf("_location_links");
//			if (index != -1) {
//				String events_table = table.substring(0, index);
//				if (to_id2 != 0)
//					db.execute("INSERT INTO " + table + "(" + events_table + "_id,locations_id) SELECT " + events_table + "_id," + to_id2 + " FROM " + table + " WHERE locations_id=" + to_id1);
//				if (to_id3 != 0)
//					db.execute("INSERT INTO " + table + "(" + events_table + "_id,locations_id) SELECT " + events_table + "_id," + to_id3 + " FROM " + table + " WHERE locations_id=" + to_id1);
//				if (to_id4 != 0)
//					db.execute("INSERT INTO " + table + "(" + events_table + "_id,locations_id) SELECT " + events_table + "_id," + to_id4 + " FROM " + table + " WHERE locations_id=" + to_id1);
//				if (to_id5 != 0)
//					db.execute("INSERT INTO " + table + "(" + events_table + "_id,locations_id) SELECT " + events_table + "_id," + to_id5 + " FROM " + table + " WHERE locations_id=" + to_id1);
//			}
//		}
//		db.delete("locations", from_id, false);
//		DBObjects<?> locations = Site.site.getObjects("locations");
//		if (locations != null)
//			locations.clear();
//	}
}
