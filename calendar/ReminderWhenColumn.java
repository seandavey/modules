package calendar;

import java.util.Map;

import app.Request;
import db.Form;
import db.NameValuePairs;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.column.Column;
import ui.Select;
import util.Text;
import web.HTMLWriter;

class ReminderWhenColumn extends Column {
	private final boolean	m_always_can_repeat;
	private final String	m_event_provider_name;
	private final String	m_event_table;
	private final boolean	m_events_can_repeat;
	private final boolean	m_events_have_time;

	//--------------------------------------------------------------------------

	public
	ReminderWhenColumn(String name, String event_provider_name, String event_table, boolean events_have_time, boolean events_can_repeat, boolean always_can_repeat) {
		super(name);
		m_event_provider_name = event_provider_name;
		m_event_table = event_table;
		m_events_have_time = events_have_time;
		m_events_can_repeat = events_can_repeat;
		m_always_can_repeat = always_can_repeat;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		super.setValue(value, nvp, view_def, id, r);
		if (value.startsWith("on the day")) {
			nvp.set("num", 0);
			nvp.set("unit", "day");
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		boolean can_repeat = m_always_can_repeat;
		boolean repeats_every_day = false;
		Mode mode = v.getMode();
		HTMLWriter w = r.w;
		w.write("<div>");
		if (m_events_can_repeat && !can_repeat) {
			int one_id;
			if (mode == Mode.ADD_FORM) {
				ViewState state = (ViewState)r.getState(ViewState.class, m_event_provider_name);
				one_id = state.getKeyValue();
			} else
				one_id = v.data.getInt(m_event_table + "_id");
			String repeat = r.db.lookupString(new db.Select("repeat").from(m_event_table).whereIdEquals(one_id));
			can_repeat = repeat != null && !repeat.equals("never");
			repeats_every_day = "every day".equals(repeat);
		}
		int num = 0;
		if (mode != Mode.ADD_FORM)
			num = v.data.getInt("num");
		boolean on_the_day = mode == Mode.ADD_FORM || "day".equals(v.data.getString("unit")) && num == 0;

		String id = "unitselect" + r.w.newID();
		w.setId(id);
		w.ui.select("num", (String[])null, null, null, true).space();
		if (m_events_have_time)
			new Select("unit", num == 1 ? new String[] { "day", "hour", "minute", "week" } : new String[] { "days", "hours", "minutes", "weeks" }, new String[] { "day", "hour", "minute", "week" }).setSelectedOption(null, mode == Mode.ADD_FORM ? "day" : v.data.getString("unit")).setInline(true).write(r.w);
		else
			new Select("unit", num == 1 ? new String[] { "day", "week" } : new String[] { "days", "weeks" }, new String[] { "day", "week" }).setSelectedOption(null, mode == Mode.ADD_FORM ? "day" : v.data.getString("unit")).setInline(true).write(r.w);
		w.space();
		String on_the_day_label = can_repeat ? "on the day of" : "on the day";
		new Select("before", new String[] { on_the_day_label, "before", "after" }, new String[] { on_the_day_label, "true", "false" }).setSelectedOption(on_the_day ? on_the_day_label : mode == Mode.ADD_FORM ? "before" : v.data.getBoolean("before") ? "before" : "after", null).setInline(true).write(r.w);
		if (can_repeat)
			w.space().ui.select("repeat_days", repeats_every_day ? new String[] { "each day", "first day", "last day" } : new String[] { "each event", "first event", "last event" }, mode == Mode.ADD_FORM ? repeats_every_day ? "first day" : "each event" : v.data.getString("repeat_days"), null, true);
		w.js("new ReminderWhen('#" + id + "'," + (mode == Mode.ADD_FORM ? 0 : v.data.getInt("num")) + ");");
		w.write("</div>");
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getLabel(View v, Request r) {
		return getDisplayName(false);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		int num = v.data.getInt("num");
		String repeat = null;
		String unit = v.data.getString("unit");
		HTMLWriter w = r.w;

		if (m_events_can_repeat)
			repeat = r.db.lookupString(new db.Select("repeat").from(m_event_table).whereIdEquals(v.data.getInt(m_event_table + "_id")));
		if (num == 0 && "day".equals(unit))
			w.write(!m_events_can_repeat || "never".equals(repeat) ? "on the day" : "on the day of");
		else {
			w.write(num).space().write(Text.pluralize(unit, num));
			w.write(v.data.getBoolean("before") ? " before" : " after");
		}
		if (m_events_can_repeat && !"never".equals(repeat))
			w.space().write(v.data.getString("repeat_days"));
		return true;
	}
}
