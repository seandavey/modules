class Calendar {
	constructor(name, display_name, view, repeating_events_can_be_split, end_date_after_start) {
		_.$('#calendar').calendar = this
		this.date = new Date()
		this.name = name
		this.display_name = display_name
		this.end_date_after_start = end_date_after_start
		this.view = view
		this.repeating_events_can_be_split = repeating_events_can_be_split
		this.update_today()
		this.update_filters()
		this.update_nav()
		_.$('#c_calendar_view').dataset.url = this.build_url()
		app.subscribe(name, this)
	}
	static $() {
		return _.$('#calendar').calendar
	}
	add(d) {
		if (!d)
			d = _.date.to_string(this.date)
		let ed = d
		if (this.end_date_after_start)
			ed = _.date.to_string(_.date.offset_date(_.date.parse(d), 1))
		let t = 'Add Event to ' + this.display_name
		if (!t.endsWith('alendar'))
			t += ' Calendar'
		new Dialog({
			cancel: true,
			component: true,
			modal: true,
			ok: {text:_.text.add},
			owner: _.$('#c_calendar_view'),
			title: t,
			url: this.url_start() + '?date=' + d + '&end_date=' + ed + '&db_mode=ADD_FORM',
			width: 'auto'
		})
	}
	build_url() {
		return this.url_start() + '/' + this.view + '/' + this.date.getFullYear() + '/' + (this.date.getMonth() + 1) + '/' + this.date.getDate()
	}
	confirm_delete(n, rn, id, r) { // module name, record name, record id, repeating event?
		if (r) {
			let d = new Dialog({cancel:true, ok:{click:function() {
				let url = this.url_start(n) + '?id=' + id
				let params = Dialog.top().modal.querySelector('input').checked ? null : 'date=' + this.edit_date
				if (params != null)
					url += '&' + params
				net.delete(url, function() {
					Dialog.top().ok()
					Dialog.top().close()
					let c = Calendar.$()
					c.update()
					setTimeout(c.update_filters.bind(c), 100)
				})
			}.bind(this)}, title:'Delete ' + rn.toLowerCase() + '?'})
			d.body = $form({parent:d.body}) // needed for radio buttons to work
			d.tr()
			d.body = $label({parent:d.body, styles:{cursor:'pointer'}})
			d.add_input('delete', 'radio', {checked: true})
			d.body.append(' delete all dates for this event')
			d.body = d.body.parentNode
			d.tr()
			d.body = $label({parent:d.body, styles:{cursor:'pointer'}})
			d.add_input('delete', 'radio')
			d.body.append(' delete only for ' + _.date.month_date_year(_.date.parse(this.edit_date)))
			d.open()
		} else
			Dialog.confirm(null, 'Delete this ' + rn.toLowerCase() + '?', _.text.delete, function(){
				net.delete(context+'/Views/' + encodeURIComponent(n) + '?id='+id, function() {
					Dialog.top().close()
					let c = Calendar.$()
					c.update()
					setTimeout(c.update_filters.bind(c), 100)
				})
			})
	}
	confirm_save(o) {
		let d = new Dialog({cancel:true, ok:{click:function() {
			if (!_.form.validate(o.form))
				return
			let data = new FormData(o.form)
			if (!Dialog.top().modal.querySelector('input').checked)
				data.set('db_date', this.edit_date)
			net.post(this.url_start(o.n) + '/' + o.id + '/update', data, function() {
				Dialog.top().ok()
				Dialog.top().close()
				let c = Calendar.$()
				c.update()
				setTimeout(c.update_filters.bind(c), 100)
			})
		}.bind(this)}, title:'Save Changes?'})
		d.body = $form({parent:d.body}) // needed for radio buttons to work
		d.tr()
		d.body = $label({parent:d.body, styles:{cursor:'pointer'}})
		d.add_input('save', 'radio', {checked: true})
		d.body.append(' change all dates for this event')
		d.body = d.body.parentNode
		d.tr()
		d.body = $label({parent:d.body, styles:{cursor:'pointer'}})
		d.add_input('save', 'radio')
		d.body.append(' change only for ' + _.date.month_date_year(_.date.parse(this.edit_date)))
		d.open()
	}
	edit(n, dn, id, d, ro, okc, r) { // module name, display name, event id, date, read_only?, on_click for ok button, repeating event?
		this.edit_date = d
		if (!dn.endsWith('alendar'))
			dn += ' Calendar'
		new Dialog({
			cancel: !ro,
			check_for_change: !ro,
			component: true,
			confirm: this.repeating_events_can_be_split && r ? {f:this.confirm_save.bind(this), n:n, id:id} : null,
			modal: true,
			ok: ro ? null : {click:okc, text:'Save'},
			owner: _.$('#c_calendar_view'),
			title: (ro ? 'View ' : 'Edit ') + ' Event on ' + dn,
			url: this.url_start(n) + '?db_key_value=' + id + '&db_mode=' + (ro ? 'READ_ONLY_FORM' : 'EDIT_FORM')
		})
	}
	filter_events() {
		let events = _.$$('.event')
		for (const e of events)
			if (!e.classList.contains('checkbox'))
				e.style.display =
					this.category && (!e.dataset['category'] || JSON.parse(e.dataset['category']).indexOf(this.category) == -1) ||
					this.location && (!e.dataset['location'] || JSON.parse(e.dataset['location']).indexOf(this.location) == -1) ? 'none' : ''
	}
	go(y, m, d) {
		this.date = new Date(y, m-1, d)
		this.update()
	}
	ics_dialog(domain, user, uuid) {
		Dialog.alert('Subscription URL', 'To subscribe to this calendar on your phone or other calendar app, use the following URL:<br><br><span id="ics_url">https://' +
			domain + this.url_start() + '.ics?u=' + user + '&uuid=' + uuid + '</span><br><button class="btn btn-sm btn-secondary" onclick="navigator.clipboard.writeText(_.$(\'#ics_url\').innerText).then(function(){Dialog.top().close();Dialog.alert(null,\'URL copied\')})">Copy</button>');
	}
	move(amount) {
		if (this.view === 'MONTH')
			this.date.setMonth(this.date.getMonth() + amount, 1)
		else if (this.view === 'DAY')
			this.date.setDate(this.date.getDate() + amount)
		else if (this.view === 'WEEK')
			this.date.setDate(this.date.getDate() + amount * 7)
		this.update()
	}
	move_controls() {
		let c = _.$('#calendar_foot')
		c.dataset.position = c.dataset.position === 'top' ? 'bottom' : 'top'
		net.post(this.url_start(), 'controls=' + c.dataset.position, function() {
			if (c.dataset.position === 'top')
				_.$('#c_calendar_view').insertBefore(c, _.$('#c_calendar_view').firstChild)
			else
				_.$('#c_calendar_view').appendChild(c)
			let span = _.$('#calendar_position').children[0]
			_.dom.rotate(span, 180)
		});
	}
	new_item(text, name) {
		let li = $li()
		let a = $a({classes:['dropdown-item'], events:{click:function(){Calendar.$().set_name(text, name)}}, parent:li}, text)
		a.dataset.name = name
		return li
	}
	static print() {
		let d = window.open().document
		d.write('<html><head><style>')
		d.write('.btn,.db_button_cell,.dropdown-menu,.no-print,select{display:none !important}')
		d.write('#calendar_head h1{text-align:center}#calendar_foot,.week{display:none}')
		d.write('table{border: 1px solid black;border-collapse:collapse;border-spacing:0;}')
		d.write('.other_calendar_label{color: #777;font-size: xx-small;text-align: center;}')
		d.write('.calendar_location,.calendar_time{color: #777;font-size:9pt;padding-right: 5px;}')
		d.write('.event{background-color:white !important}')
		d.write('td{border: 1px solid black;vertical-align:top;}')
		d.write('a{color:black;text-decoration:none}')
		d.write('p{margin:0}')
		d.write('</style></head><body>')
		d.write(_.$('#calendar').innerHTML)
		d.write('</body></html>')
	}
	publish(message/*, data*/) {
		if (message !== 'delete')
			setTimeout(this.update_filters.bind(this), 100)
	}
	set_category(category) {
		this.category = 'All' === category ? null : category
		this.filter_events()
	}
	set_checkboxes(v) {
		for (const cb of _.$('#calendar_foot').querySelectorAll('input[type="checkbox"]'))
			cb.checked = v;
		this.update()
	}
	set_location(location) {
		this.location = 'All' === location ? null : location
		this.filter_events()
	}
	set_name(text, name) {
		app.unsubscribe(this.name, this)
		app.subscribe(name, this)
		this.name = name
		this.display_name = text
		let picker = _.$('#calendar_picker')
		let prev_name = picker.dataset.name
		picker.dataset.name = name
		let prev_text = picker.children[0].childNodes[0].nodeValue
		prev_text = prev_text.substring(0, prev_text.length)
		picker.children[0].childNodes[0].nodeValue = text
		let ul = picker.children[1]
		for (let i=0; i<ul.children.length; i++)
			if (ul.children[i].innerText === text) {
				ul.removeChild(ul.children[i])
				break
			}
		let added = false;
		for (let i=0; i<ul.children.length; i++)
			if (ul.children[i].innerText > prev_text) {
				ul.insertBefore(this.new_item(prev_text, prev_name), ul.children[i])
				added = true
				break
			}
		if (!added)
			ul.appendChild(this.new_item(prev_text, prev_name))
		let cb = _.$('#calendar_checkboxes')
		if (cb)
			cb.style.display = picker.dataset.main === text ? 'flex' : 'none'
		this.category = null
		this.location = null
		this.update()
	}
	set_view(i) {
		let cvs = _.$('#calendar_view_switcher')
		if (cvs) {
			cvs.children[cvs.dataset.active].classList.remove('active')
			cvs.dataset.active = i
			cvs.children[cvs.dataset.active].classList.add('active')
			this.view = cvs.children[cvs.dataset.active].innerText.toUpperCase()
		}
		if (i == 0) // month view
			this.date.setDate(1)
		_.$('#calendar_add').style.display = i == 0 ? 'none' : ''
		return this
	}
	today() {
		this.date = new Date()
		this.update()
	}
	update() {
		this.update_nav()
		let url = this.build_url()
		net.replace(_.$('#c_calendar_view'), url, {ignore: true, on_complete: function(){this.update_today();this.filter_events();this.update_filters()}.bind(this)})
		_.$('#c_calendar_view').dataset.url = url
		net.replace(_.$('#calendar_buttons'), this.url_start() + '/buttons')
		let av = _.$('#calendar_after_view')
		if (av)
			net.replace(av, this.url_start() + '/after_view') // , {ignore: true})
	}
	update_filter(name, prop) {
		let s = _.$('#calendar_' + name)
		if (!s)
			return
		s.options.length = 1
		let options = {}
		for (const event of _.$$('.event')) {
			let value = event.dataset[prop]
			if (value) {
				let a = JSON.parse(value)
				for (let i=0; i<a.length; i++)
					options[a[i]] = true
			}
		}
		let array = Object.keys(options).sort(function (a, b) {return a.toLowerCase().localeCompare(b.toLowerCase())})
		for (let i=0; i<array.length; i++)
			s.appendChild(new Option(array[i]))
		s.parentNode.style.display = array.length > 0 ? '' : 'none'
	}
	update_filters() {
		if (!this.category)
			this.update_filter('categories', 'category')
		if (!this.location)
			this.update_filter('locations', 'location')
	}
	update_nav() {
		let head = _.$('#calendar_head')
		if (head)
			if (this.view === 'LIST') {
				head.firstChild.children[1].firstChild.innerText = ''
				head.children[1].firstChild.style.visibility = 'hidden'
			} else {
				head.firstChild.children[1].firstChild.innerText = this.view === 'DAY' ? _.date.month_date_year(this.date) : _.date.month_year(this.date)
				head.children[1].firstChild.style.visibility = ''
			}
	}
	update_today() {
		let today = _.$('#calendar_today')
		if (today)
			today.style.display = _.$('#calendar').querySelector('.today') ? 'none' : ''
	}
	url_start(name) {
		return context + '/' + encodeURIComponent(name ? name : this.name)
	}
}
class ReminderWhen {
	constructor(id, num) {
		this.num = _.$(id);
		this.num.addEventListener('change', this.on_num_change.bind(this));
		this.unit = this.num.nextElementSibling;
		this.unit.addEventListener('change', this.on_unit_change.bind(this));
		this.before = this.unit.nextElementSibling;
		this.before.addEventListener('change', this.on_before_change.bind(this));
		this.on_before_change();
		this.on_unit_change(num);
	}
	add_nums(from, to, mult, val) {
		for (let i=from; i<=to; i++) {
			let v = i * mult;
			this.num.appendChild(new Option(v, v, null, v == val));
		}
	}
	on_before_change() {
		let on_the_day = this.before.selectedIndex === 0;
		this.num.style.display = this.unit.style.display = on_the_day ? 'none' : 'inline';
		this.num.name = on_the_day ? '' : "num";
		this.unit.name = on_the_day ? '' : "unit";
		if (!on_the_day)
			this.on_num_change();
	}
	on_num_change() {
		if (this.num.options.length === 0)
			return;
		let one = this.num.options[this.num.selectedIndex].value === "1";
		for (let i=0; i<this.unit.options.length; i++) {
			let text = this.unit.options[i].text;
			if (one) {
				if (text.endsWith('s'))
					this.unit.options[i].text = text.substring(0, text.length - 1);
			} else
				if (!text.endsWith('s'))
					this.unit.options[i].text = text + 's';
		}
	}
	on_unit_change(event) {
		let val = typeof event === 'object' ? this.num.options[this.num.selectedIndex].text : event;
		this.num.options.length = 0;
		let unit = this.unit.options[this.unit.selectedIndex].value;
		if (unit === 'day' || unit === 'week')
			this.add_nums(1, 10, 1, val);
		else if (unit === 'hour')
			this.add_nums(1, 23, 1, val);
		else
			this.add_nums(0, 3, 30, val);
		this.on_num_change();
	}
}
function day_mouseleave(event) {
//	if (!event.currentTarget.contains(event.relatedTarget)) {
		let b = event.currentTarget.querySelector('button')
		if (b)
			b.style.visibility = 'hidden'
//	}
}
function day_mouseenter(event) {
	if (!event.currentTarget.closest('table').dataset.onePerDay || event.currentTarget.childElementCount == 1) {
		let b = event.target.querySelector('button')
		if (b)
			b.style.visibility = 'visible'
	}
}
function event_mouseout(event) {
	if (!event.currentTarget.contains(event.relatedTarget))
		app.hide_popups();
}
function event_mouseover(event) {
	if (event.currentTarget.classList.contains('event') && isClipped(event.currentTarget))
		app.tooltip(event.currentTarget, event.currentTarget.innerHTML);
}
function isClipped(el) {
	let s = _.dom.size(el);
	return s.x < el.scrollWidth || s.y < el.scrollHeight;
}
