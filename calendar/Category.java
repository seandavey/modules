package calendar;

import db.Option;
import db.object.DBField;

public class Category extends Option {
	@DBField
	private String m_color;

	//--------------------------------------------------------------------------

	public String
	getColor() {
		return m_color;
	}
}
