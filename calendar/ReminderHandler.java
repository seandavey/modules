package calendar;

import db.DBConnection;

public interface ReminderHandler {
	public boolean handleReminder(EventProvider event_provider, String from, String recipient, String subject, String content, DBConnection db);
}
