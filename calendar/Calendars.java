package calendar;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

import app.Modules;
import app.Person;
import app.Request;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.object.JSONField;
import pages.Page;
import ui.Option;
import ui.Select;
import ui.SelectOption;
import util.Array;
import util.Text;
import web.HTMLWriter;

public class Calendars extends SiteModule {
	private static Calendars 	s_module;

	@JSONField
	private String m_main_calendar = "Main Calendar";

	//--------------------------------------------------------------------------

	public
	Calendars() {
		m_description = "Page for accessing the calendars for the site";
		s_module = this;
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Calendars", false) {
			@Override
			public void
			writeContent(Request r) {
				EventProvider main_calendar = Calendars.getMainCalendar();
				if (main_calendar != null) {
					new MonthView(main_calendar, r).writeComponent(r);
					return;
				}
				for (EventProvider ep : Modules.getByClass(EventProvider.class))
					if (ep.isActive()) {
						new MonthView(ep, r).writeComponent(r);
						return;
					}
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	private static void
	checkAll(String event_provider_name, Request r) {
		StringBuilder data = new StringBuilder();
		for (EventProvider calendar : Calendars.getCalendars(r)) {
			String name = calendar.getName();
			if (!name.equals(event_provider_name)) {
				if (data.length() > 0)
					data.append('\t');
				data.append(name);
			}
		}
		Person user = r.getUser();
		if (user != null)
			user.setData("checked calendars", data.toString(), r.db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String check = r.getParameter("check");
		if ("all".equals(check)) {
			checkAll(m_name, r);
			return true;
		} else if ("none".equals(check)) {
			r.getUser().setData("checked calendars", null, r.db);
			return true;
		}
		String toggle = r.getParameter("toggle");
		if (toggle != null) {
			toggleCalendar(toggle, r.getUser(), r.db);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------
	// return all EventProviders that are active and m_show_on_menu

	static Collection<EventProvider>
	getCalendars(Request r) {
		ArrayList<EventProvider> calendars = new ArrayList<>();
		for (EventProvider ep: Modules.getByClass(EventProvider.class))
			if (ep.isActive() && ep.showOnMenu(r))
				calendars.add(ep);
		return calendars;
	}

	//--------------------------------------------------------------------------

	static Collection<EventProvider>
	getCalendarsSorted(Request r) {
		TreeMap<String,EventProvider> event_providers = new TreeMap<>(Site.collator);
		for (EventProvider ep : getCalendars(r))
			event_providers.put(ep.getDisplayName(r), ep);
		return event_providers.values();
	}

	//--------------------------------------------------------------------------

	static String[]
	getCheckedCalendars(Person user) {
		if (user == null)
			return null;
		String[] checked_calendars = null;
		String data = user.getDataString("checked calendars");
		if (data != null)
			checked_calendars = data.split("\t");
		return checked_calendars;
	}

	//--------------------------------------------------------------------------

	public static EventProvider
	getMainCalendar() {
		String main_calendar = s_module.m_main_calendar;
		if (main_calendar != null) {
			EventProvider ep = (EventProvider)Modules.get(main_calendar);
			if (ep != null && ep.isActive())
				return ep;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Modules.add(new Locations().setRequired(true), db);
	}

	//--------------------------------------------------------------------------

	static boolean
	isMainCalendar(EventProvider ep) {
		return ep.equals(getMainCalendar());
	}

	//--------------------------------------------------------------------------

	private static void
	toggleCalendar(String calendar, Person user, DBConnection db) {
		if (user == null)
			return;
		String data = user.getDataString("checked calendars");
		if (data != null) {
			String[] checked_calendars = data.split("\t");
			int index = Array.indexOf(checked_calendars, calendar);
			if (index == -1)
				data += "\t" + calendar;
			else {
				checked_calendars = Array.remove(checked_calendars, index);
				data = checked_calendars.length == 0 ? null : Text.join("\t", checked_calendars);
			}
		} else
			data = calendar;
		user.setData("checked calendars", data, db);
	}

	//--------------------------------------------------------------------------

	static void
	writeCheckBoxes(Request r) {
		Collection<EventProvider> event_providers = getCalendarsSorted(r);
		if (event_providers.size() < 2)
			return;
		String[] checked_calendars = getCheckedCalendars(r.getUser());
		int i = 0;
		HTMLWriter w = r.w;
		w.write("Overlap other calendars: ");
		for (EventProvider event_provider : event_providers) {
			String name = event_provider.getName();
			if (isMainCalendar(event_provider))
				continue;
			w.addClass("event checkbox")
				.addStyle("padding:4px 8px !important;white-space:nowrap");
			String color = event_provider.getColor();
			if (color != null)
				w.addStyle("background-color:" + color).addStyle("color:" + (Text.blackText(color) ? "#000" : "#FFF"));
			w.tagOpen("div");
			w.setAttribute("onchange", "net.post(context+'/Calendars','toggle=" + HTMLWriter.URLEncode(name) + "',function(){Calendar.$().update()})")
				.ui.checkbox("e" + i++, event_provider.getDisplayName(r), null, checked_calendars == null ? false : Array.indexOf(checked_calendars, name) != -1, true, false);
			w.tagClose();
		}
		w.ui.buttonOutlineOnClick(Text.all, "net.post(context+'/Calendars','check=all',function(){Calendar.$().set_checkboxes(true)})")
			.ui.buttonOutlineOnClick(Text.none, "net.post(context+'/Calendars','check=none',function(){Calendar.$().set_checkboxes(false)})");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsInput(Field field, Request r) {
		if ("m_main_calendar".equals(field.getName())) {
			ArrayList<SelectOption> a = new ArrayList<>();
			Collection<EventProvider> calendars = getCalendarsSorted(r);
			for (EventProvider c : calendars)
				a.add(new Option(c.getDisplayName(r), c.getName()));
			new Select("main_calendar", a).setAllowNoSelection(true).setSelectedOption(null, m_main_calendar).write(r.w);
		} else
			super.writeSettingsInput(field, r);
	}
}
