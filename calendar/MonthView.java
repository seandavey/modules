package calendar;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.PriorityQueue;

import app.Request;
import util.Text;
import util.Time;
import web.HTMLWriter;

public class MonthView extends CalendarView {
	public
	MonthView(EventProvider event_provider, Request r) {
		super(event_provider, r);
	}

	//--------------------------------------------------------------------------

	@Override
	View
	getView() {
		return View.MONTH;
	}

	//--------------------------------------------------------------------------

	@Override
	public
	void
	write(LocalDate date, Request r) {
		LocalDate	d = date.withDayOfMonth(1);
		DayOfWeek	dow = d.getDayOfWeek();
		LocalDate	start = dow == DayOfWeek.SUNDAY ? d : d.minusDays(dow.getValue());
		LocalDate	end = d.plusMonths(1);
		LocalDate	today = Time.newDate();
		HTMLWriter	w = r.w;

		dow = end.getDayOfWeek();
		if (dow != DayOfWeek.SUNDAY)
			end = end.plusDays(7 - dow.getValue());

		PriorityQueue<Event> events = new PriorityQueue<>();
		m_ep.addCalendarsEvents(events, start, end, r);
		adjustEvents(events, r);
		m_ep.setOverlaps(events);
		m_ep.setOverlapsWithOtherCalendars(events, r);

		w.write("<table class=\"month_view\"><thead style=\"inset-block-start:0;position:sticky\"><tr>");
//		if (allowViewSwitching())
//			w.write("<td class=\"week\" style=\"width:10px\"><font size=\"-1\">week</font></td>");
		String[] weekdays = Time.formatter.getWeekdays();
		for (int i=1; i<=7; i++) {
			w.tagOpen("th");
			w.write(weekdays[i].substring(0, 1))
				.addClass("weekday-cdr")
				.tag("span", weekdays[i].substring(1))
				.tagClose();
		}
		w.write("</tr></thead>");

		boolean show_add = r.getUser() != null && m_ep.showAddButton(r);
		if (start.isBefore(d)) {
			w.tagOpen("tr");
			for (int i=0,n=(int)ChronoUnit.DAYS.between(start, d); i<n; i++) {
				writeDay(events, start, today, false, show_add, r);
				start = start.plusDays(1);
			}
		}
		int m = d.getMonthValue();
		do {
			if (d.getDayOfWeek() == DayOfWeek.SUNDAY)
				w.tagOpen("tr");
			writeDay(events, d, today, true, show_add, r);
			if (d.getDayOfWeek() == DayOfWeek.SATURDAY)
				w.tagClose();
			d = d.plusDays(1);
		} while (m == d.getMonthValue());

		if (d.isBefore(end)) {
			while (d.isBefore(end)) {
				writeDay(events, d, today, false, show_add, r);
				d = d.plusDays(1);
			}
			w.tagClose();
		}
		w.write("</table>");
	}

	//--------------------------------------------------------------------------

	private void
	writeDay(PriorityQueue<Event> events, LocalDate date, LocalDate today, boolean in_month, boolean show_add, Request r) {
		HTMLWriter w = r.w;
		w.write("<td class=\"");
		w.write(in_month ? m_ep.getDayCSSClass(date, today) : "non_day");
		w.write("\"");
		if (show_add)
			w.write(" onmouseleave=\"day_mouseleave(event)\" onmouseenter=\"day_mouseenter(event)\"");
		w.write("><div style=\"display:flex;justify-content:space-between;\">");
		if (m_ep.allowViewSwitching() && r.getUser() != null)
			w.addStyle("font-weight:bold;text-decoration:none")
				.aOnClick(Integer.toString(date.getDayOfMonth()), "Calendar.$().set_view(2).go(" + date.getYear() + "," + date.getMonthValue() + "," + date.getDayOfMonth() + ")");
		else
			w.write("<span style=\"font-weight:bold;\">").write(date.getDayOfMonth()).write("</span>");
		if (show_add)
			w.addClass("month_view").ui.buttonOnClick(Text.add, "Calendar.$().add('" + date.toString() + "')");
		w.write("</div>");
		if (events.size() > 0) {
			Event event = events.peek();
			while (event.occursOn(date)) {
				event = events.poll();
				EventProvider event_event_provider = event.getEventProvider();
				event.write(date, event_event_provider != m_ep ? event_event_provider.getDisplayName(r) :  null, r);
				if (events.size() == 0)
					break;
				event = events.peek();
			}
		}
		w.write("</td>");
	}
}
