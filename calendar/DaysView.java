package calendar;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import app.Request;
import ui.Table;
import util.Time;
import web.HTMLWriter;

class DaysView extends CalendarView {
	private static final double	HEIGHT_MULTIPLIER = 1.5;
	private final int			m_num_days;

	//--------------------------------------------------------------------------

	public
	DaysView(EventProvider event_provider, int num_days, Request r) {
		super(event_provider, r);
		m_num_days = num_days;
	}

	//--------------------------------------------------------------------------

	private boolean
	gather(LocalDate start, LocalDate end, PriorityQueue<Event> events, List<Event> day_events) {
		boolean has_all_day_events = false;

		for (Event event : events)
			if (event.occursOn(start)) {
				day_events.add(event);
				if (event.getStartTime() == null)
					has_all_day_events = true;
			}
		return has_all_day_events;
	}

	//--------------------------------------------------------------------------

	private int[]
	getMinuteBounds(PriorityQueue<Event> events, LocalDate start, LocalDate end) {
		int start_minutes = 1440;
		int end_minutes = 0;

		for (Event event : events) {
			if (event.getStartTime() == null)
				continue;
			int diff_days = (int)ChronoUnit.DAYS.between(start, end);
			int num_days = 0;
			for (int j=0; j<=diff_days; j++) {
				if (event.occursOn(start)) {
					int event_start_minutes = event.getStartTime().get(ChronoField.MINUTE_OF_DAY);
					if (event_start_minutes < start_minutes)
						start_minutes = event_start_minutes;
					LocalTime event_end = event.getEndTime();
					int event_end_minutes = event_end != null ? event_end.get(ChronoField.MINUTE_OF_DAY) : event_start_minutes + 30;
					if (event_end_minutes > end_minutes)
						end_minutes = event_end_minutes;
					start = start.minusDays(num_days);
					break;
				}
				start = start.plusDays(1);
				++num_days;
			}
		}
		int minute_of_hour = start_minutes % 60;
		if (minute_of_hour != 0)
			start_minutes -= minute_of_hour;
//			if (minute_of_hour >= 30)
//				start_minutes -= minute_of_hour - 30;
//			else
//				start_minutes -= minute_of_hour;
		minute_of_hour = end_minutes % 60;
		if (minute_of_hour != 0)
			end_minutes += 60 - minute_of_hour;
//			if (minute_of_hour > 30)
//				end_minutes += 60 - minute_of_hour;
//			else if (minute_of_hour < 30)
//				end_minutes += 30 - minute_of_hour;
		return new int[] {start_minutes, end_minutes};
	}

	//--------------------------------------------------------------------------

	@Override
	View getView() {
		return m_num_days == 7 ? View.WEEK : View.DAY;
	}

	//--------------------------------------------------------------------------

	@Override
	public
	void
	write(LocalDate date, Request r) {
		LocalDate	today = Time.newDate();
		LocalDate	to;
		HTMLWriter 	w = r.w;

		if (m_num_days != 1) {
			DayOfWeek dow = date.getDayOfWeek();
			if (dow != DayOfWeek.SUNDAY)
				date = date.minusDays(dow.getValue());
		}
		to = date.plusDays(m_num_days);
		PriorityQueue<Event> events = new PriorityQueue<>();
		m_ep.addCalendarsEvents(events, date, to, r);
		adjustEvents(events, r);
		m_ep.setOverlaps(events);
		m_ep.setOverlapsWithOtherCalendars(events, r);


		List<List<Event>> days_events = new ArrayList<>();
		boolean has_all_day_events = false;
		if (events.size() > 0) {
			for (int i=0; i<m_num_days; i++) {
				List<Event> event_list = new ArrayList<>();
				has_all_day_events |= gather(date, to, events, event_list);
				days_events.add(event_list);
				date = date.plusDays(1);
			}
			date = date.minusDays(m_num_days);
		}

		w.addStyle("margin:0 auto");
		int mark = w.tagOpen("table");
		w.write("<tr><td>");

		if (events.size() > 0) {
			int[] start_end = getMinuteBounds(events, date, to);
			int height = start_end[1] - start_end[0];
			Table table = w.ui.table().addClass("days_view").addStyle("margin-bottom", "20px");
			if (m_num_days != 1) {
				table.td("&nbsp;");
				for (int i=0; i<m_num_days; i++) {
					w.addStyle("text-align:center");
					table.td();
					w.aOnClickOpen("Calendar.$().set_view(2).go(" + date.getYear() + "," + date.getMonthValue() + "," + date.getDayOfMonth() + ")");
					w.write(Time.formatter.getWeekdayLong(date)).space().write(date.getDayOfMonth());
					w.tagClose();
					date = date.plusDays(1);
				}
				date = date.minusDays(m_num_days);
			}
			if (has_all_day_events) {
				table.tr();
				w.addStyle("text-align:right;padding-right:5px");
				table.td("all day");
				for (List<Event> day_events : days_events) {
					w.addStyle("vertical-align:top");
					w.addClass(ChronoUnit.DAYS.between(date, today) == 0 ? "all_day today" : "all_day");
					table.td();
					writeAllDayEvents(date, day_events, r);
					date = date.plusDays(1);
				}
				date = date.minusDays(m_num_days);
			}
			table.tr();
			w.addStyle("vertical-align:top");
			table.td();
			writeTimes(start_end[0], start_end[1], height, r);
			for (List<Event> day_events : days_events) {
				w.addStyle("vertical-align:top");
				table.td();
				writeDayEvents(date, day_events, start_end[0], start_end[1], height, m_num_days == 1 ? 300 : 150, /*m_state.splitByLocation() ? new ArrayList<String>(locations) :*/ null, ChronoUnit.DAYS.between(date, today) == 0, r);
				date = date.plusDays(1);
			}
			table.close();
		}
		w.write("</td></tr>");
		w.tagsCloseTo(mark);
	}

	//--------------------------------------------------------------------------

	private void
	writeAllDayEvents(LocalDate date, List<Event> events, Request r) {
		boolean at_least_one = false;
		for (Event event : events)
			if (event.getStartTime() == null) {
				EventProvider event_provider = event.getEventProvider();
				event.write(date, event_provider != m_ep ? event_provider.getDisplayName(r) :  null, r);
				at_least_one = true;
			}
		if (!at_least_one)
			r.w.nbsp();
	}

	//--------------------------------------------------------------------------

	private void
	writeDayEvents(LocalDate date, List<Event> events, int start_minutes, int end_minutes, int height, int box_width, List<String> locations, boolean today, Request r) {
		HTMLWriter w = r.w;

		w.write("<div style=\"position:relative;top:8px;width:");
		w.write(box_width);
		w.write("px;height:");
		w.write(height * HEIGHT_MULTIPLIER);
		w.write("px\">");
		writeDayGrid(start_minutes, end_minutes, box_width, today, w);

		int width = 0;
		for (int i=0; i<events.size(); i++) {
			Event event = events.get(i);
			if (event.getStartTime() == null)
				continue;
			LocalTime start_time = event.getStartTime();
			if (start_time == null)
				continue;
			int minutes = start_time.get(ChronoField.MINUTE_OF_DAY);
			int length = event.getLength();
			if (length < 1)
				length = 30;
			int overlaps_before = 0;
			int total_overlaps = 0;
			for (int j=0; j<events.size(); j++) {
				if (j == i)
					continue;
				Event e = events.get(j);
				if (e.getStartTime() == null)
					continue;
				if (event.timesOverlap(e)) {
					if (j < i)
						++overlaps_before;
					++total_overlaps;
				}
			}
			width = box_width / (total_overlaps + 1);
			int left = overlaps_before * width;
			EventProvider event_provider = event.getEventProvider();
			event.write(date, event_provider != m_ep ? event_provider.getDisplayName(r) :  null, length * HEIGHT_MULTIPLIER, width, left, (minutes - start_minutes) * HEIGHT_MULTIPLIER, r);
		}
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	private void
	writeDayGrid(int start_minutes, int end_minutes, int box_width, boolean today, HTMLWriter w) {
		int minutes = start_minutes;
		while (minutes < end_minutes) {
			w.write("<div");
			if (today)
				w.write(" class=\"today\"");
			w.write(" style=\"");
			if (!today)
				w.write("background-color:white;");
			w.write("width:").write(box_width).write("px;height:").write(60 * HEIGHT_MULTIPLIER).write("px;border:1px solid black;position:absolute;top:").write((minutes - start_minutes) * HEIGHT_MULTIPLIER).write("px;\">&nbsp;</div>");
			minutes += 60;
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeTimes(int start_minutes, int end_minutes, int height, Request r) {
		HTMLWriter w = r.w;
		if (start_minutes >= end_minutes)
			return;
		int minutes = start_minutes;
		w.write("<div style=\"position:relative;width:80px;height:")
			.write(height * HEIGHT_MULTIPLIER + 30)
			.write("px\">");
		while (minutes < end_minutes) {
			w.write("<div style=\"text-align:right;width:80px;height:60px;position:absolute;right:3px;top:")
				.write((minutes - start_minutes) * HEIGHT_MULTIPLIER)
				.write("px\">")
				.write(Time.formatter.getTime(minutes, true))
				.write("</div>");
			minutes += 60;
		}
		w.write("<div style=\"text-align:right;width:80px;height:60px;position:absolute;right:3px;top:")
			.write((minutes - start_minutes) * HEIGHT_MULTIPLIER)
			.write("px\">")
			.write(Time.formatter.getTime(minutes, true))
			.write("</div></div>");
	}
}
