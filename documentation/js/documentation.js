const Documentation = {
	initialize(el, pages) {
		el = _.$(el)
		this.pages = JSON.parse(pages)
		_.ui.button(_.text.add, this.add_page, {parent:el, styles:{float:'right'}})
		$h5({parent:el}, 'Table of Contents')
		JS.get('tree-min', function() {
			Documentation.tree = new Tree(Documentation.pages, { onClick:Documentation.on_click, onMove:Documentation.on_move, onToggle:Documentation.store_expansion, parent:el })	
			Documentation.load_expansion()		
		})
	},
	add_page() {
		if (Documentation.editor)
			Documentation.save(Documentation.editor)
		net.post(context+'/Documentation/add', null, function(o){
			Documentation.pages.children.push({name:'New Page Title', children:[], id:o.id})
			Documentation.tree.update(o.id)
			Documentation.save_toc()
		})
	},
	delete_page(id) {
		let pages = Documentation.pages.children
		let p = Documentation.findPage(pages, id)
		Dialog.confirm('Delete Page','Delete the page "' + p.name + '"?', _.text.delete, function(){
			net.post(context+'/Documentation/delete/' + p.id, null, function(){
				p.parent.children.splice(p.parent.children.findIndex(x => x.id === id) , 1)
				Documentation.tree.update()
				Documentation.save_toc()
		        _.$('#button_row').style.visibility = 'hidden'
		        Documentation.editor.destroy()
				Documentation.editor = null
			})
		})
	},
	edit(id) {
		if (!Documentation.editor) {
			ClassicEditor.create(_.$('#documentation_page'),{
				autosave: {save: Documentation.save},
				image: { toolbar: [ 'imageStyle:inline', 'imageStyle:wrapText', 'imageStyle:breakText', '|', 'toggleImageCaption', 'imageTextAlternative' ] },
				mediaEmbed: {previewsInData: true},
				simpleUpload: { uploadUrl: context + '/ImageUploads' },
				table: { contentToolbar: ['toggleTableCaption', 'tableColumn', 'tableRow', 'mergeTableCells', 'tableProperties', 'tableCellProperties'] },
				toolbar: _.rich_text.toolbar_full
		    })
		    .then(editor => {
		        Documentation.editor = editor
		        Documentation.load(id)
		        _.$('#button_row').style.visibility = 'visible'
		    })
		    .catch( err => {
				console.error(err)
			})
		} else
		    Documentation.load(id)
	},
	editor: null,
	findPage(a, id) {
		for (let i = 0; i<a.length; i++) {
			if (a[i].id === id)
				return a[i]
			let p = Documentation.findPage(a[i].children, id)
			if (p)
				return p
		}
		return null
	},
	load(id) {
		net.get_text(context+'/documentation/'+id+'.html?_=' + new Date().getTime(), function(t){
			Documentation.editor.setData(t)
			Documentation.editor.focus()
			Documentation.ignore = false
		})
	},
	load_expansion() {
		let s = localStorage.getItem('documentation toc expansion')
		if (s) {
			let a = JSON.parse(s)
			_.$$(".tree-leaf").forEach((l) => {
				if (a.includes(l.data.id))
					Documentation.tree.toggleExpand(l)
			})
		}
	},
	on_click(target) {
		if (Documentation.editor)
			Documentation.save(Documentation.editor)
		Documentation.o = target.data
		Documentation.ignore = true
		Documentation.edit(target.data.id)
	},
	on_move(/*target*/) {
		Documentation.save_toc()
	},
	save(editor) {
		if (Documentation.ignore)
			return
		let title = editor.plugins.get('Title').getTitle()
		if (title && title != Documentation.o.name) {
			Documentation.o.name = title
			Documentation.tree.update(Documentation.o.id)
			Documentation.save_toc()
		}
		net.post(context + '/Documentation/' + Documentation.o.id, 'data=' + encodeURIComponent(editor.getData()) + '&title=' + encodeURIComponent(title))
	},
	save_toc() {
		net.post(context + '/Documentation/toc', 'toc=' + encodeURIComponent(JSON.stringify(Documentation.pages, ['children', 'id', 'name'])))
	},
	store_expansion() {
		let a = []
		_.$$(".tree-leaf").forEach((l) => {
			if (l.data.expanded)
				a.push(l.data.id)
		})
		localStorage.setItem('documentation toc expansion', JSON.stringify(a))
	}
}
