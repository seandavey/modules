// https://github.com/davidfig/tree

const defaults = {
    move: true,
    select: true,
    indentation: 20,
    threshold: 10,
    // holdTime: 1000,
    expandOnClick: true,
    dragOpacity: 0.75,
    prefixClassName: 'tree',
    cursorName: 'grab -webkit-grab pointer',
    cursorExpand: 'pointer'
}
const icons={closed:'<?xml version="1.0" encoding="UTF-8" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="100%" height="100%" viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;"><rect id="closed" x="0" y="0" width="100" height="100" style="fill:none;"/><rect x="10" y="10" width="80" height="80" style="fill:none;stroke-width:2px;"/><path d="M25,50l50,0" style="fill:none;stroke-width:2px;"/><path d="M50,75l0,-50" style="fill:none;stroke-width:2px;"/></svg>',open:'<?xml version="1.0" encoding="UTF-8" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="100%" height="100%" viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;"><rect id="open" x="0" y="0" width="100" height="100" style="fill:none;"/><rect x="10" y="10" width="80" height="80" style="fill:none;stroke-width:2px;"/><path d="M25,50l50,0" style="fill:none;stroke-width:2px;"/></svg>'}

class Indicator {
    constructor(tree) {
        this._indicator = html()
        this._indicator.style.marginLeft = tree.indentation + 'px'
        const content = html({ parent: this._indicator })
        content.style.display = 'flex'
        this._indicator.indentation = html({ parent: content })
        this._indicator.icon = html({ parent: content, className: `${tree.prefixClassName}-expand` })
        this._indicator.icon.style.height = 0
        this._indicator.line = html({ parent: content, className: `${tree.prefixClassName}-indicator` })
    }

    get() {
        return this._indicator
    }

    set _marginLeft(value) {
        this._indicator.style.marginLeft = value + 'px'
    }
}
class Input {
    constructor(tree) {
        this._tree = tree
        this._indicator = new Indicator(tree)
        document.body.addEventListener('mousemove', e => this._move(e))
        document.body.addEventListener('touchmove', e => this._move(e))
        document.body.addEventListener('mouseup', e => this._up(e))
        document.body.addEventListener('touchend', e => this._up(e))
        document.body.addEventListener('mouseleave', e => this._up(e))
    }

    _select() {
        if (this._tree._selection) {
            this._tree._selection.name.classList.remove(`${this._tree.prefixClassName}-select`)
        }
        if (this._target) {
            this._tree._selection = this._target
            this._tree._selection.name.classList.add(`${this._tree.prefixClassName}-select`)
        }
    }

    _down(e) {
        this._target = e.currentTarget.parentNode.parentNode
        let alreadySelected = false
        if (this._tree._selection === this._target)
            alreadySelected = true
        else
            this._select()
        this._isDown = { x: e.pageX, y: e.pageY, alreadySelected }
        const pos = toGlobal(this._target)
        this._offset = { x: e.pageX - pos.x, y: e.pageY - pos.y }
        // if (this._tree.holdTime) {
        //     this._holdTimeout = window.setTimeout(() => this._hold(), this._tree.holdTime)
        // }
        e.preventDefault()
        e.stopPropagation()
    }

    // _hold() {
    //     this._holdTimeout = null
    //     this._tree.edit(this._target)
    // }

    _checkThreshold(e) {
        if (!this._tree.move) {
            return false
        } else if (this._moving) {
            return true
        } else {
            if (distance(this._isDown.x, this._isDown.y, e.pageX, e.pageY) >= this._tree._options.threshold) {
                this._moving = true
                this._pickup()
                return true
            } else {
                return false
            }
        }
    }

    _pickup() {
        // if (this._holdTimeout) {
        //     window.clearTimeout(this._holdTimeout)
        //     this._holdTimeout = null
        // }
//        this._tree.emit('move-pending', this._target, this._tree)
        const parent = this._target.parentNode
        parent.insertBefore(this._indicator.get(), this._target)
        const pos = toGlobal(this._target)
        document.body.appendChild(this._target)
        this._old = {
            opacity: this._target.style.opacity || 'unset',
            position: this._target.style.position || 'unset',
            boxShadow: this._target.name.style.boxShadow || 'unset'
        }
        this._target.style.position = 'absolute'
        this._target.name.style.boxShadow = '3px 3px 5px rgba(0,0,0,0.25)'
        this._target.style.left = pos.x + 'px'
        this._target.style.top = pos.y + 'px'
        this._target.style.opacity = this._tree.dragOpacity
        if (this._tree._getChildren(parent, true).length === 0) {
            this._tree._hideIcon(parent)
        }
    }

    _findClosest(e, entry) {
        const pos = toGlobal(entry.name)
        if (pos.y + entry.name.offsetHeight / 2 <= e.pageY) {
            if (!this._closest.foundAbove) {
                if (inside(e.pageX, e.pageY, entry.name)) {
                    this._closest.foundAbove = true
                    this._closest.above = entry
                } else {
                    const distance = distancePointElement(e.pageX, e.pageY, entry.name)
                    if (distance < this._closest.distanceAbove) {
                        this._closest.distanceAbove = distance
                        this._closest.above = entry
                    }
                }
            }
        } else if (!this._closest.foundBelow) {
            if (inside(e.pageX, e.pageY, entry.name)) {
                this._closest.foundBelow = true
                this._closest.below = entry
            } else {
                const distance = distancePointElement(e.pageX, e.pageY, entry.name)
                if (distance < this._closest.distanceBelow) {
                    this._closest.distanceBelow = distance
                    this._closest.below = entry
                }
            }
        }
        for (let child of this._tree._getChildren(entry)) {
            this._findClosest(e, child)
        }
    }

    _move(e) {
        if (this._target && this._checkThreshold(e)) {
            const element = this._tree.element
            const indicator = this._indicator.get()
            const indentation = this._tree.indentation
            indicator.remove()
            this._target.style.left = e.pageX - this._offset.x + 'px'
            this._target.style.top = e.pageY - this._offset.y + 'px'
            const x = toGlobal(this._target.name).x
            this._closest = { distanceAbove: Infinity, distanceBelow: Infinity }
            for (let child of this._tree._getChildren()) {
                this._findClosest(e, child)
            }
            if (!this._closest.above && !this._closest.below) {
                element.appendChild(indicator)
            } else if (!this._closest.above)  {
                // null [] leaf
                element.insertBefore(indicator, this._tree._getFirstChild(element))
            } else if (!this._closest.below) {
                // leaf [] null
                let pos = toGlobal(this._closest.above.name)
                if (x > pos.x + indentation) {
                    this._closest.above.insertBefore(indicator, this._tree._getFirstChild(this._closest.above, true))
                } else if (x > pos.x - indentation) {
                    this._closest.above.parentNode.appendChild(indicator)
                } else {
                    let parent = this._closest.above
                    while (parent !== element && x < pos.x) {
                        parent = this._tree._getParent(parent)
                        if (parent !== element) {
                            pos = toGlobal(parent.name)
                        }
                    }
                    parent.appendChild(indicator)
                }
            } else if (this._closest.below.parentNode === this._closest.above) {
                // parent [] child
                this._closest.above.insertBefore(indicator, this._closest.below)
            } else if (this._closest.below.parentNode === this._closest.above.parentNode) {
                // sibling [] sibling
                const pos = toGlobal(this._closest.above.name)
                if (x > pos.x + indentation) {
                    this._closest.above.insertBefore(indicator, this._tree._getLastChild(this._closest.above, true))
                } else {
                    this._closest.above.parentNode.insertBefore(indicator, this._closest.below)
                }
            } else {
                // child [] parent^
                let pos = toGlobal(this._closest.above.name)
                if (x > pos.x + indentation) {
                    this._closest.above.insertBefore(indicator, this._tree._getLastChild(this._closest.above, true))
                } else if (x > pos.x - indentation) {
                    this._closest.above.parentNode.appendChild(indicator)
                } else if (x < toGlobal(this._closest.below.name).x) {
                    this._closest.below.parentNode.insertBefore(indicator, this._closest.below)
                } else {
                    let parent = this._closest.above
                    while (parent.parentNode !== this._closest.below.parentNode && x < pos.x) {
                        parent = this._tree._getParent(parent)
                        pos = toGlobal(parent.name)
                    }
                    parent.appendChild(indicator)
                }
            }
        }
    }

    _up(e) {
        if (this._target) {
            if (!this._moving) {
                if (this._tree.expandOnClick && (!this._tree.select || this._isDown.alreadySelected)) {
                    this._tree.toggleExpand(this._target)
                }
                if (this._tree._options['onClick'])
                    this._tree._options['onClick'](this._target)
                // this._tree.emit('clicked', this._target, e, this._tree)
            } else {
                const indicator = this._indicator.get()
                indicator.parentNode.insertBefore(this._target, indicator)
                this._tree.expand(indicator.parentNode)
                this._tree._showIcon(indicator.parentNode)
                this._target.style.position = this._old.position === 'unset' ? '' : this._old.position
                this._target.name.style.boxShadow = this._old.boxShadow === 'unset' ? '' : this._old.boxShadow
                this._target.style.opacity = this._old.opacity === 'unset' ? '' : this._old.opacity
                indicator.remove()
                this._moveData()
                if (this._tree._options['onMove'])
                    this._tree._options['onMove'](this._target)
                // this._tree.emit('move', this._target, this._tree)
                // this._tree.emit('update', this._target, this._tree)
            }
            // if (this._holdTimeout) {
            //     window.clearTimeout(this._holdTimeout)
            //     this._holdTimeout = null
            // }
            this._target = this._moving = null
        }
    }

    _moveData() {
        this._target.data.parent.children.splice(this._target.data.parent.children.indexOf(this._target.data), 1)
        this._target.parentNode.data.children.splice(getChildIndex(this._target.parentNode, this._target), 0, this._target.data)
        this._target.data.parent = this._target.parentNode.data
    }

    _indicatorMarginLeft(value) {
        this._indicator.marginLeft = value
    }
}
/**
 * converts a string to an HTMLElement if necessary
 * @param {(HTMLElement|string)} element
 */
function el(element) {
    if (typeof element === 'string') {
        return document.querySelector(element)
    }
    return element

}

/**
 * measure distance between two points
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 */
function distance(x1, y1, x2, y2) {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
}

/**
 * find shortest distance from point to HTMLElement's bounding box
 * from: https://gamedev.stackexchange.com/questions/44483/how-do-i-calculate-distance-between-a-point-and-an-axis-aligned-rectangle
 * @param {number} x
 * @param {number} y
 * @param {HTMLElement} element
 */
function distancePointElement(px, py, element) {
    const pos = toGlobal(element)
    const width = element.offsetWidth
    const height = element.offsetHeight
    const x = pos.x + width / 2
    const y = pos.y + height / 2
    const dx = Math.max(Math.abs(px - x) - width / 2, 0)
    const dy = Math.max(Math.abs(py - y) - height / 2, 0)
    return dx * dx + dy * dy
}

/**
 * determine whether the mouse is inside an element
 * @param {HTMLElement} dragging
 * @param {HTMLElement} element
 */
function inside(x, y, element) {
    const pos = toGlobal(element)
    const x1 = pos.x
    const y1 = pos.y
    const w1 = element.offsetWidth
    const h1 = element.offsetHeight
    return x >= x1 && x <= x1 + w1 && y >= y1 && y <= y1 + h1
}

/**
 * determines global location of a div
 * from https://stackoverflow.com/a/26230989/1955997
 * @param {HTMLElement} e
 * @returns {PointLike}
 */
function toGlobal(e) {
    const box = e.getBoundingClientRect()

    const body = document.body
    const docEl = document.documentElement

    const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop
    const scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft

    const clientTop = docEl.clientTop || body.clientTop || 0
    const clientLeft = docEl.clientLeft || body.clientLeft || 0

    const top = box.top + scrollTop - clientTop
    const left = box.left + scrollLeft - clientLeft

    return { y: Math.round(top), x: Math.round(left) }
}

/**
 * @typedef {object} PointLike
 * @property {number} x
 * @property {number} y
 */

/**
 * combines options and default options
 * @param {object} options
 * @param {object} defaults
 * @returns {object} options+defaults
 */
function options(options, defaults) {
    options = options || {}
    for (let option in defaults) {
        options[option] = typeof options[option] !== 'undefined' ? options[option] : defaults[option]
    }
    return options
}

/**
 * set a style on an element
 * @param {HTMLElement} element
 * @param {string} style
 * @param {(string|string[])} value - single value or list of possible values (test each one in order to see if it works)
 */
// function style(element, style, value) {
//     if (Array.isArray(value)) {
//         for (let entry of value) {
//             element.style[style] = entry
//             if (element.style[style] === entry) {
//                 break
//             }
//         }
//     } else {
//         element.style[style] = value
//     }
// }

/**
 * calculate percentage of overlap between two boxes
 * from https://stackoverflow.com/a/21220004/1955997
 * @param {number} xa1
 * @param {number} ya1
 * @param {number} xa2
 * @param {number} xa2
 * @param {number} xb1
 * @param {number} yb1
 * @param {number} xb2
 * @param {number} yb2
 */
// function percentage(xa1, ya1, xa2, ya2, xb1, yb1, xb2, yb2) {
//     const sa = (xa2 - xa1) * (ya2 - ya1)
//     const sb = (xb2 - xb1) * (yb2 - yb1)
//     const si = Math.max(0, Math.min(xa2, xb2) - Math.max(xa1, xb1)) * Math.max(0, Math.min(ya2, yb2) - Math.max(ya1, yb1))
//     const union = sa + sb - si
//     if (union !== 0) {
//         return si / union
//     } else {
//         return 0
//     }
// }

function html(options) {
    options = options || {}
    const object = document.createElement(options.type || 'div')
    if (options.parent) {
        options.parent.appendChild(object)
    }
    if (options.className) {
        object.classList.add(options.className)
    }
    if (options.html) {
        object.innerHTML = options.html
    }
    if (options.id) {
        object.id = options.id
    }
    return object
}

function getChildIndex(parent, child) {
    let index = parent.data.parent ? -1 : 0
    for (let entry of parent.children) {
        if (entry === child) {
            return index
        }
        index++
    }
    return -1
}
class Tree {
    /**
     * Create Tree
     * @param {TreeData} tree - data for tree (see readme for description)
     * @param {object} [options]
     * @param {(HTMLElement|string)} [options.element] if a string then document.querySelector(element); if empty, it creates an element
     * @param {(HTMLElement|string)} [options.parent] appends the element to this parent (if string then document.querySelector(parent))
     * @param {boolean} [options.move=true] drag tree to rearrange
     * @param {boolean} [options.select=true] click to select node (if false then nodes are not selected and tree.selection is always null)
     * @param {number} [options.indentation=20] number of pixels to indent for each level
     * @param {number} [options.threshold=10] number of pixels to move to start a drag
     * @param {number} [options.holdTime=0] number of milliseconds to press and hold name before editing starts (set to 0 to disable)
     * @param {boolean} [options.expandOnClick=true] expand and collapse node on click without drag except (will select before expanding if select=true)
     * @param {number} [options.dragOpacity=0.75] opacity setting for dragged item
     * @param {string} [options.prefixClassName=tree] first part of className for all DOM objects (e.g., yy-tree, yy-tree-indicator)
     * @fires render
     * @fires clicked
     * @fires expand
     * @fires collapse
     * @fires name-change
     * @fires move
     * @fires move-pending
     * @fires update
     */
    constructor(tree, o) {
        this._options = options(o, defaults)
        this._input = new Input(this)
        if (typeof this._options.element === 'undefined') {
            /**
             * Main div holding tree
             * @type {HTMLElement}
             */
            this.element = document.createElement('div')
        } else {
            this.element = el(this._options.element)
        }
        if (this._options.parent) {
            el(this._options.parent).appendChild(this.element)
        }
        this.element.classList.add(this.prefixClassName)
        this.element.data = tree
        this.update()
    }

    /**
     * Selected data
     * @type {*}
     */
    get selection() {
        return this._selection.data
    }
    set selection(data) {
    }

    /**
     * className's prefix (e.g., "yy-tree"-content)
     * @type {string}
     */
    get prefixClassName() {
        return this._options.prefixClassName
    }
    set prefixClassName(value) {
        if (value !== this._options.prefixClassName) {
            this._options.prefixClassName = value
            this.update()
        }
    }

    /**
     * indentation for tree
     * @type {number}
     */
    get indentation() {
        return this._options.indentation
    }
    set indentation(value) {
        if (value !== this._options.indentation) {
            this._options.indentation = value
            this._input._indicatorMarginLeft = value + 'px'
            this.update()
        }
    }

    /**
     * number of milliseconds to press and hold name before editing starts (set to 0 to disable)
     * @type {number}
     */
    // get holdTime() {
    //     return this._options.holdTime
    // }
    // set holdTime(value) {
    //     if (value !== this._options.holdTime) {
    //         this._options.holdTime = value
    //     }
    // }

    /**
     * whether tree may be rearranged
     * @type {boolean}
     */
    get move() {
        return this._options.move
    }
    set move(value) {
        this._options.move = value
    }

    /**
     * expand and collapse node on click without drag except (will select before expanding if select=true)
     * @type {boolean}
     */
    get expandOnClick() {
        return this._options.expandOnClick
    }
    set expandOnClick(value) {
        this._options.expandOnClick = value
    }

    /**
     * click to select node (if false then nodes are not selected and tree.selection is always null)
     * @type {boolean}
     */
    get select() {
        return this._options.select
    }
    set select(value) {
        this._options.select = value
    }

    /**
     * opacity setting for dragged item
     * @type {number}
     */
    get dragOpacity() {
        return this._options.dragOpacity
    }
    set dragOpacity(value) {
        this._options.dragOpacity = value
    }

    _leaf(data, level) {
        const leaf = html({ className: `${this.prefixClassName}-leaf` })
        leaf.isLeaf = true
        leaf.data = data
        leaf.content = html({ parent: leaf, className: `${this.prefixClassName}-content` })
        leaf.style.marginLeft = this.indentation + 'px'
        leaf.icon = html({
            parent: leaf.content,
            html: data.expanded ? icons.open : icons.closed,
            className: `${this.prefixClassName}-expand`
        })
        leaf.name = html({ parent: leaf.content, html: data.name, className: `${this.prefixClassName}-name` })
        leaf.name.addEventListener('mousedown', e => this._input._down(e))
        leaf.name.addEventListener('touchstart', e => this._input._down(e))
        for (let child of data.children) {
            const add = this._leaf(child, level + 1)
            add.data.parent = data
            leaf.appendChild(add)
            if (!data.expanded) {
                add.style.display = 'none'
            }
        }
        if (this._getChildren(leaf, true).length === 0) {
            this._hideIcon(leaf)
        }
        leaf.icon.addEventListener('click', this.toggleExpand.bind(this, leaf))
//        clicked(leaf.icon, () => this.toggleExpand(leaf))
        // this.emit('render', leaf, this)
        return leaf
    }

    _getChildren(leaf, all) {
        leaf = leaf || this.element
        const children = []
        for (let child of leaf.children) {
            if (child.isLeaf && (all || child.style.display !== 'none')) {
                children.push(child)
            }
        }
        return children
    }

    _hideIcon(leaf) {
        if (leaf.isLeaf) {
            leaf.icon.style.opacity = 0
            leaf.icon.style.cursor = 'unset'
        }
    }

    _showIcon(leaf) {
        if (leaf.isLeaf) {
            leaf.icon.style.opacity = 1
            leaf.icon.style.cursor = this._options.cursorExpand
        }
    }

    /** Expands all leaves */
    // expandAll() {
    //     this._expandChildren(this.element)
    // }

    _expandChildren(leaf) {
        for (let child of this._getChildren(leaf, true)) {
            this.expand(child)
            this._expandChildren(child)
        }
    }

    /** Collapses all leaves */
    // collapseAll() {
    //     this._collapseChildren(this)
    // }

    _collapseChildren(leaf) {
        for (let child of this._getChildren(leaf, true)) {
            this.collapse(child)
            this._collapseChildren(child)
        }
    }

    /**
     * Toggles a leaf
     * @param {HTMLElement} leaf
     */
    toggleExpand(leaf) {
        if (leaf.icon.style.opacity !== '0') {
            if (leaf.data.expanded) {
                this.collapse(leaf)
            } else {
                this.expand(leaf)
            }
            if (this._options['onToggle'])
                this._options['onToggle'](this._target)
        }
    }

    /**
     * Expands a leaf
     * @param {HTMLElement} leaf
     */
    expand(leaf) {
        if (leaf.isLeaf) {
            const children = this._getChildren(leaf, true)
            if (children.length) {
                for (let child of children) {
                    child.style.display = 'block'
                }
                leaf.data.expanded = true
                leaf.icon.innerHTML = icons.open
                // this.emit('expand', leaf, this)
                // this.emit('update', leaf, this)
            }
        }
    }

    /**
     * Collapses a leaf
     * @param {HTMLElement} leaf
     */
    collapse(leaf) {
        if (leaf.isLeaf) {
            const children = this._getChildren(leaf, true)
            if (children.length) {
                for (let child of children) {
                    child.style.display = 'none'
                }
                leaf.data.expanded = false
                leaf.icon.innerHTML = icons.closed
                // this.emit('collapse', leaf, this)
                // this.emit('update', leaf, this)
            }
        }
    }

    /** call this after tree's data has been updated outside of this library */
    update(id) {
        const scroll = this.element.scrollTop
        while (this.element.firstChild) {
            this.element.firstChild.remove()
        }
        for (let leaf of this.element.data.children) {
            const add = this._leaf(leaf, 0)
            add.data.parent = this.element.data
            this.element.appendChild(add)
            if (add.data.id == id)
                this._input._target = add
        }
        this._input._select()
        this.element.scrollTop = scroll + 'px'
        this._input._target = null
    }

    /**
     * edit the name entry using the data
     * @param {object} data element of leaf
     */
    // editData(data) {
    //     const children = this._getChildren(null, true)
    //     for (let child of children) {
    //         if (child.data === data) {
    //             this.edit(child)
    //         }
    //     }
    // }

    /**
     * edit the name entry using the created element
     * @param {HTMLElement} leaf
     */
    // edit(leaf) {
        // this._editing = leaf
        // this._editInput = html({ parent: this._editing.name.parentNode, type: 'input', className: `${this.prefixClassName}-name` })
        // const computed = window.getComputedStyle(this._editing.name)
        // this._editInput.style.boxSizing = 'content-box'
        // this._editInput.style.fontFamily = computed.getPropertyValue('font-family')
        // this._editInput.style.fontSize = computed.getPropertyValue('font-size')
        // this._editInput.value = this._editing.name.innerText
        // this._editInput.setSelectionRange(0, this._editInput.value.length)
        // this._editInput.focus()
        // this._editInput.addEventListener('update', () => {
        //     this.nameChange(this._editing, this._editInput.value)
        //     this._holdClose()
        // })
        // this._editInput.addEventListener('keyup', (e) => {
        //     if (e.code === 'Escape') {
        //         this._holdClose()
        //     }
        //     if (e.code === 'Enter') {
        //         this.nameChange(this._editing, this._editInput.value)
        //         this._holdClose()
        //     }
        // })
        // this._editing.name.style.display = 'none'
        // this._target = null
    // }

    // _holdClose() {
    //     if (this._editing) {
    //         this._editInput.remove()
    //         this._editing.name.style.display = 'block'
    //         this._editing = this._editInput = null
    //     }
    // }

    /**
     * change the name of a leaf
     * @param {HTMLElement} leaf
     * @param {string} name
     */
    // nameChange(leaf, name) {
    //     leaf.data.name = this._input.value
    //     leaf.name.innerHTML = name
    //     // this.emit('name-change', leaf, this._input.value, this)
    //     // this.emit('update', leaf, this)
    // }

    /**
     * Find a leaf based using its data
     * @param {object} leaf
     * @param {HTMLElement} [root=this.element]
     */
    // getLeaf(leaf, root = this.element) {
    //     this.findInTree(root, data => data === leaf)
    // }

    /**
     * call the callback function on each node; returns the node if callback === true
     * @param {*} leaf data
     * @param {function} callback
     */
    // findInTree(leaf, callback) {
    //     for (const child of leaf.children) {
    //         if (callback(child)) {
    //             return child
    //         }
    //         const find = this.findInTree(child, callback)
    //         if (find) {
    //             return find
    //         }
    //     }
    // }

    _getFirstChild(element, all) {
        const children = this._getChildren(element, all)
        if (children.length) {
            return children[0]
        }
    }

    _getLastChild(element, all) {
        const children = this._getChildren(element, all)
        if (children.length) {
            return children[children.length - 1]
        }
    }

    _getParent(element) {
        element = element.parentNode
        while (element.style.display === 'none') {
            element = element.parentNode
        }
        return element
    }
}
