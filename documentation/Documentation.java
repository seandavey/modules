package documentation;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import app.ClassTask;
import app.Request;
import app.Roles;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.ViewDef;
import pages.Page;
import util.Text;
import web.JS;

public class Documentation extends SiteModule {
	private String m_previous_id;

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		String segement_one = r.getPathSegment(1);
		if (segement_one == null) {
			r.w.write("""
				<div style="display:flex">
					<div id="sidebar" style="margin-left:20px;margin-right:20px;min-width:200px"></div>
					<div class="ck-content doc-view" id="documentation_page"></div>
				</div>
				<style>
					.tree {
						margin-left: -20px;
					}
				    .tree-name {
				        padding: 1px 10px;
				        user-select: none;
				        cursor: pointer;
				    }
				    .tree-indicator {
				        background: rgb(150,150,255);
				        height: 5px;
				        width: 100px;
				        padding: 0 1em;
				    }
				    .tree-content {
				        display: flex;
				        align-items: center;
				    }
				    .tree-expand {
				        width: 15px;
				        height: auto;
				    }
				    .tree-expand svg {
					    vertical-align: initial;
				    }
				    .tree-select {
				        background: rgb(200, 200, 200);
				    }
					.ck-content .table table {
						table-layout: auto;
					}
					.ck-content .table th {
						border: none;
					}
				</style>""");
			String toc = readTOC();
			r.w.js("var toc=" + toc + "\nvar oc=function(target){net.replace('#documentation_page','" +
				Site.site.absoluteURL("documentation").toString() + 
				"/'+target.data.id+'.html')}\nJS.get('" +
				Site.site.absoluteURL("js", "tree-min.js").toString() +
				"',function(){new Tree(toc,{move:false,onClick:oc,parent:_.$('#sidebar')})})");
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (r.getUser() == null)
			return false;
		if (super.doPost(r))
			return true;
		String segement_one = r.getPathSegment(1);
		if (segement_one == null)
			return false;
		switch (segement_one) {
		case "add":
			String id = Text.uuid();
			writePage(id, "<h1>New Page Title</h1><p></p>");
			r.response.add("id", id);
			return true;
		case "delete":
			Site.site.getPath("documentation", r.getPathSegment(2) + ".html").toFile().delete();
			return true;
		case "toc":
			writeTOC(r.getParameter("toc"));
			return true;
		}
		String id = r.getPathSegment(1);
		if (id != null) {
			writePage(id, r.getParameter("data"));
			if (!id.equals(m_previous_id)) {
				if (m_previous_id != null)
					try {
						new ProcessBuilder("git commit -am " + m_previous_id).directory(Site.site.getPath("documentation").toFile()).start();
					} catch (IOException e) {
						r.log(e);
					}
				m_previous_id = id;
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		Roles.add("documentation", "can edit the documentation with this role");
		Site.site.addUserDropdownItem(new Page("Edit Documentation", this, true) {
			@Override
			public void
			writeContent(Request r) {
				r.w.write("""
					<div id="sidebar" style="left:20px;min-width:200px;position:absolute">
					</div>
					<div>
						<div id="button_row" style="margin-bottom:10px;visibility:hidden;text-align:right"><button class="btn btn-outline-primary btn-sm" onclick="Documentation.delete_page(Documentation.tree.selection.id)">delete</button></div>
						<div id="documentation_page">
					</div>
					</div>
					<style>
						.tree {
							margin-left: -20px;
						}
					    .tree-name {
					        padding: 1px 10px;
					        /* margin: 0.1em;
					           background: rgb(230,230,230); */
					        user-select: none;
					        cursor: pointer;
					    }
					    .tree-indicator {
					        background: rgb(150,150,255);
					        height: 5px;
					        width: 100px;
					        padding: 0 1em;
					    }
					    .tree-content {
					        display: flex;
					        align-items: center;
					    }
					    .tree-expand {
					        width: 15px;
					        height: auto;
					    }
					    .tree-expand svg {
						    vertical-align: initial;
					    }
					    .tree-select {
					        background: rgb(200, 200, 200);
					    }
						.ck-editor__editable {
							max-width: none;
						}
						.ck-content .table table {
							table-layout: auto;
						}
						.ck-content .table th {
							border: none;
						}
					</style>""");
				r.w.script("ckeditor", false, null);
				r.w.script("tree-min", false, null);
				String toc = readTOC();
				if (toc == null)
					toc = "{\"children\":[]}";
				r.w.script("documentation-min", true, "Documentation.initialize('#sidebar'," + JS.string(toc) + ")");
			}
		}.addRole("documentation"));
		Site.site.makeDirectory("documentation");
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("documentation"))
			return new ViewDef(name)
				.setRecordName("Page", true);
		return null;
	}

	//--------------------------------------------------------------------------

	private String
	readTOC() {
		try {
			return Files.readString(Site.site.getPath("documentation", "toc.json"));
		} catch (IOException e) {
			Site.site.log(e);
		}
		return null;
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public void
	rebuildTOC() {
		StringBuilder toc = new StringBuilder();
		try {
			Files.list(Site.site.getPath("documentation"))
				.filter(s -> s.toString().endsWith(".html"))
				.forEach(p -> {
					try {
						if (toc.length() == 0)
							toc.append("{\"children\":[");
						else
							toc.append(",");
						List<String> l = Files.readAllLines(p);
						String t = l.get(0);
						t = t.substring(t.indexOf("<h1>") + 4);
						t = t.substring(0, t.indexOf("</h1>"));
						String f = p.getFileName().toString();
						toc.append("{\"children\":[],\"id\":\"")
							.append(f.substring(0, f.length() - 5))
							.append("\",\"name\":\"")
							.append(t)
							.append("\"}");
					} catch (IOException e) {
						Site.site.log(e);
					}
				});
		} catch (IOException e) {
			Site.site.log(e);
		}
		toc.append("]}");
		writeTOC(toc.toString());
	}

	//--------------------------------------------------------------------------

	private void
	writePage(String id, String data) {
		if (data == null || data.length() == 0)
			return;
		try {
			Files.writeString(Site.site.getPath("documentation", id + ".html"), data);
		} catch (IOException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeTOC(String toc) {
		if (toc == null || toc.length() == 0)
			return;
		try {
			Files.writeString(Site.site.getPath("documentation", "toc.json"), toc);
		} catch (IOException e) {
			Site.site.log(e);
		}
	}
}
