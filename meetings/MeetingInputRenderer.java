package meetings;

import app.Modules;
import app.Person;
import app.Request;
import db.Form;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.column.ColumnBase;
import db.column.ColumnInputRenderer;
import util.Text;
import web.HTMLWriter;

class MeetingInputRenderer implements ColumnInputRenderer {
	@Override
	public void
	writeInput(View v, Form f, ColumnBase<?> column, boolean inline, Request r) {
		Mode mode = v.getMode();
		if (mode != View.Mode.ADD_FORM && mode != View.Mode.EDIT_FORM)
			return;
		Person user = r.getUser();
		if (user == null)
			return;
		HTMLWriter w = r.w;
		if (mode == View.Mode.EDIT_FORM && v.data.getInt("_owner_") == user.getId())
			w.write("<p>")
				.ui.radioButtonInline("db_mmode", null, "set my times", null, true, "meeting.setMode(1)").nbsp()
				.ui.radioButtonInline("db_mmode", null, "view overlapping times", null, false, "meeting.setMode(2)").nbsp()
				.ui.radioButtonInline("db_mmode", null, "edit available times", null, false, "meeting.setMode(0)")
				.write("</p>");
		w.setId("days_id")
			.hiddenInput("days", mode == View.Mode.EDIT_FORM ? v.data.getString(column.getName()) : "");
		w.setId("many_days_id")
			.hiddenInput("many_days", mode == View.Mode.EDIT_FORM ? r.db.lookupString("days", "meetings_people", "meetings_id=" + v.data.getInt("id") + " AND people_id=" + user.getId()) : "");
		w.write("<div id=\"meeting_div\"></div><p id=\"legend\" style=\"font-size:9pt\"></p>")
			.write("<div id=\"help\">")
			.write(((Meetings)Modules.get("Meetings")).m_help)
			.write("</div><script>meeting.initialize(")
			.write(mode == View.Mode.ADD_FORM ? 0 : 1)
			.write(",_.$('#meeting_div'),_.$('#days_id'),_.$('#many_days_id'),[");
		String user_name = null;
		if (mode == View.Mode.EDIT_FORM) {
			Rows rows = new Rows(new Select("people.id,days,first,last").from("meetings_people").joinOn("people", "people.id=people_id").where("meetings_id=" + v.data.getString("id")).orderBy("first,last"), r.db);
			while (rows.next()) {
				String name = Text.join(" ", rows.getString(3), rows.getString(4));
				if (rows.getInt(1) == user.getId()) {
					user_name = name;
					continue;
				}
				if (!rows.isFirst())
					w.write(',');
				w.write('[').jsString(name).write(',').jsString(rows.getString(2)).write(']');
			}
			rows.close();
		}
		w.write(']');
		if (user_name != null)
			w.write(',').jsString(user_name);
		w.write(")</script>");
	}
}