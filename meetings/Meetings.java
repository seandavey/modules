package meetings;

import java.sql.Types;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import app.Person;
import app.Request;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.FormHook;
import db.InsertHook;
import db.NameValuePairs;
import db.OneToMany;
import db.Rows;
import db.Select;
import db.UpdateHook;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.PersonColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import pages.Page;

public class Meetings extends SiteModule implements InsertHook, UpdateHook {
	@JSONField
	String m_help = """
		<ol style=\"width:400px\">
		<li>Add days that will contain times that people are available. Days can be named anything, e.g. Mondays, Tuesdays, 1/3/2011, 3rd Fridays, etc.</li>
		<li>To set the times for each day that will be avaiable for others, click and drag sections on red times to change them to green.</li>
		<li>Click + above or below each day if you need earlier or later times.</li>
		<li>Save the meeting when you are done and then edit it if you are one of the people trying to find common available times.</li>
		<ol>""";

	//--------------------------------------------------------------------------

	public
	Meetings() {
		m_description = "A page for finding common times to meet";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Meeting Scheduler", this, false) {
			@Override
			public void
			writeContent(Request r) {
				r.w.style("td.meeting{padding:0px 2px 0px 0px;width:100px}td.time{padding:2px 10px 2px 0px}")
					.js("JS.get('meetings-min')");
				Site.site.newView("meetings", r).writeComponent();
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("meetings")
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("days", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("meetings_people")
			.add(new JDBCColumn("meetings"))
			.add(new JDBCColumn("people"))
			.add(new JDBCColumn("color", Types.VARCHAR))
			.add(new JDBCColumn("days", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeInsert(NameValuePairs nvp, Request r) {
		nvp.remove("many_days");
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		String many_days = nvp.getString("many_days");
		if (many_days != null) {
			NameValuePairs nvp2 = new NameValuePairs();
			nvp2.set("days", many_days);
			nvp2.set("meetings_id", id);
			int user_id = r.getUser().getId();
			nvp2.set("people_id", user_id);
			r.db.updateOrInsert("meetings_people", nvp2, "meetings_id=" + id + " AND people_id=" + user_id);
		}
		nvp.remove("many_days");
		return null;
	}

	//------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("meetings"))
			return new ViewDef(name){
					@Override
					public String
					getEditButtonText(View v, Request r) {
						Person user = r.getUser();
						if (user != null && v.data.getInt("_owner_") != user.getId())
							return "set times";
						return "edit";
					}
					@Override
					public View
					newView(Request r) {
						View v = new View(this, r);
						if (View.Mode.EDIT_FORM.toString().equals(r.getParameter("db_mode"))) {
							Person user = r.getUser();
							if (user != null && r.db.lookupInt(new Select("_owner_").from("meetings").whereIdEquals(v.getID()), -1) != user.getId())
								v.setColumn(new Column("title").setIsReadOnly(true));
						}
						return v;
					}
				}
				.addFormHook(new FormHook() {
					@Override
					public void
					afterForm(int id, Rows data, ViewDef view_def, Mode mode, boolean printer_friendly, Request r) {
						Set<String> people = new TreeSet<>();
						Rows rows = new Rows(new Select("email").from("people").joinOn("meetings_people", "people.id=people_id").whereEquals("meetings_id", id).orderBy("first,last"), r.db);
						while (rows.next()) {
							String email = rows.getString(1);
							if (email != null)
								people.add(email);
						}
						rows.close();
						if (people.size() > 1) {
							StringBuilder s = new StringBuilder();
							for (String member : people) {
								if (s.length() > 0)
									s.append(',');
								s.append(member);
							}
							r.w.ui.aIcon("mailto:" + s.toString(), "envelope", "Email People");
						}
					}
				})
				.addInsertHook(this)
				.addUpdateHook(this)
				.setAccessPolicy(new RecordOwnerAccessPolicy() {
					@Override
					public boolean
					canUpdateRow(String from, int id, Request r) {
						return true;
					}
					@Override
					public boolean
					showEditButtonForRow(Rows data, Request r) {
						return true;
					}
				}.add().delete().edit())
				.setDefaultOrderBy("title")
				.setRecordName("Meeting", true)
				.setColumnNamesForm("title", "days", "_owner_")
				.setColumnNamesTable("title")
				.setColumn(new Column("days").setInputRenderer(new MeetingInputRenderer()))
				.setColumn(new Column("_owner_").setDefaultToUserId().setIsHidden(true))
				.setColumn(new Column("title").setIsRequired(true))
				.addRelationshipDef(new OneToMany("meetings_people"));
		if (name.equals("meetings_people"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setDefaultOrderBy("(SELECT first FROM people WHERE people.id=people_id),(SELECT last FROM people WHERE people.id=people_id)")
				.setRecordName("Person", true)
				.setColumnNamesForm("people_id")
				.setColumnNamesTable("color", "people_id", "email")
				.setColumn(new Column("color").setDisplayName("").setValueRenderer(new MeetingColorRenderer(), false))
				.setColumn(new PersonColumn("people_id", false))
				.setColumn(new LookupColumn("email", "people", "email").setOneColumn("people_id"));
		return null;
	}
}