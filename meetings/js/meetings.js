const meeting = {
	initialize(mode,div,input,many_input,others,user_name) {
		//mode:0=set available times,1=set personal times,2=view overlapping times
		this.div = div
		this.input = input
		this.many_input = many_input
		this.others = others
		this.user_name = user_name
		this.start = new Date()
		this.start.setHours(8)
		this.start.setMinutes(0)
		this.start.setSeconds(0)
		this.finish = new Date()
		this.finish.setHours(21)
		this.finish.setMinutes(0)
		this.finish.setSeconds(0)
		this.unavailable = '#FF4136'
		this.colors = ['#2ECC40','#001f3f','#FFDC00','#0074D9','#3D9970','#F012BE','#FF851B','#7FDBFF','#85144b','#B10DC9','#01FF70','#39CCCC']
		this.dragging=false
		this.input.validate = function() {return this.validate();}.bind(this)
		div.ondrag = function() {return false}
    	div.onselectstart = function() {return false}
		document.addEventListener('mouseup',function() {meeting.dragging=false;})
		this.selected = false
		this.setMode(mode)
		if (this.isEmpty())
			_.ui.button('add day', meeting.addDay, {parent:div})
	},
	addAddButton() {
		_.ui.button('add day', meeting.addDay, {parent:this.rows[0].insertCell(-1)})
	},
	addDay() {
		let t = prompt('enter date or day of the week','Monday')
		if (!t)
			return
		if (meeting.div.firstChild && meeting.div.firstChild.tagName != 'TABLE')
			_.dom.empty(meeting.div)
		if (meeting.isEmpty()) {
			meeting.addTimes()
			meeting.addAddButton()
		}
		meeting.newDayEditable(t, true)
	},
	addTimes() {
		let time = new Date(this.start.getTime())
		let c = ['table', 'table-borderless', 'table-hover']
		if (this.mode == 2)
			c.push('table-striped-columns')
		let t = $table({classes:c, events:{selectstart:function(){return false}}, styles:{cursor:'default',width:'min-content'}, unselectable:'on'})
		let tr = t.insertRow(-1)
		let td = tr.insertCell(-1)
		td.style.textAlign = 'center'
		if (this.mode == 0 && (this.start.getHours() || this.start.getMinutes()))
			td.innerHTML = '<a href="#" onclick="meeting.rebuild(-30,0,true)" style="text-decoration:none">+</a>'
		else
			td.innerHTML = '&nbsp;'
		while (time <= this.finish) {
			let h = time.getHours()
			let m = time.getMinutes()
			tr = t.insertRow(-1)
			td = tr.insertCell(-1)
			td.className = 'time'
			td.style.textAlign = 'right'
			td.innerHTML = (h==0?12:h>12?h-12:h)+':'+(m?m:'00')+(h>=12?'&nbsp;PM':'&nbsp;AM')
			time.setMinutes(time.getMinutes()+30)
		}
		tr = t.insertRow(-1)
		td = tr.insertCell(-1)
		td.style.textAlign = 'center'
		if (this.mode==0 && this.finish.getHours())
			td.innerHTML = '<a href="#" onclick="meeting.rebuild(0,30,true)" style="text-decoration:none">+</a>'
		else
			td.innerHTML = '&nbsp;'
		this.div.appendChild(t)
		this.rows = t.rows
	},
	buildDay(d, col, mode) { //0=add color 0 div,1=build overlap table,2=select,3=overlap select
		let selected = false
		let p = 1
		let period = d[p++].split('-')
		let hm = period[0].split(':')
		let row = 1
		let time = new Date(this.start.getTime())
		while (time < this.finish) {
			if (time.getHours() == hm[0] && time.getMinutes() == hm[1]) {
				if (selected) {
					if (p < d.length) {
						period = d[p++].split('-')
						hm = period[0].split(':')
					}
				} else
					hm = period[1].split(':')
				selected = !selected
			}
			if (selected) {
				if (mode == 0)
				this.newDiv(this.rows[row].cells[col])
				else if (mode == 1) {
					let t = $table({parent:this.rows[row].cells[col], styles:{minWidth:(this.others.length+1)*20, width:'100%'}})
					let tr = t.insertRow(-1)
					tr.insertCell(-1).innerHTML = '&nbsp;'
					for (let j=0; j<this.others.length; j++)
						tr.insertCell(-1).innerHTML = '&nbsp;'
				} else if (mode == 2) {
					let c = this.rows[row].cells[col].firstChild
					if (c) {
						c.selected = true
						c.style.backgroundColor = this.colors[0]
					}
				} else {
					let c = this.rows[row].cells[col].firstChild
					if (c) {
						c = c.rows[0].cells[this.person];
						if (c)
							c.style.backgroundColor = this.getColor(this.person)
					}
				}
			}
			++row
			time.setMinutes(time.getMinutes() + 30)
		}
	},
	buildDays() {
		let days = this.input.value.split('|')
		for (let i=0; i<days.length; i++)
			if (days[i].length) {
				if (this.isEmpty())
					this.addTimes()
				let day = days[i].split(',')
				if (this.mode == 0)
					this.newDayEditable(day[0], false)
				else {
					this.newDayEmpty(day[0])
					if (day.length > 1)
						this.buildDay(day, i+1, this.mode == 2 ? 1 : 0)
				}
			}
		if (this.mode == 0 && this.rows)
			this.addAddButton()
	},
	buildLegend() {
		let d = _.$('#legend')
		_.dom.empty(d)
		if (this.mode == 2) {
/*			$span({parent:d, styles:{backgroundColor:'#29b529',display:'inline-block',height:12,verticalAlign:'text-bottom',width:12}})
			$span({parent:d}, ' ' + this.user_name)
			for (let i=0; i<this.others.length; i++) {
				$span({parent:d, styles:{backgroundColor:this.colors[i+1],display:'inline-block',height:12,marginLeft:10,verticalAlign:'text-bottom',width:12}})
				$span({parent:d}, ' ' + this.others[i][0])	
			}*/
		} else {
			$span({parent:d, styles:{backgroundColor:'#29b529',display:'inline-block',height:12,verticalAlign:'text-bottom',width:12}})
			$span({parent:d}, ' available')
			$span({parent:d, styles:{backgroundColor:'#de1808',display:'inline-block',height:12,marginLeft:10,verticalAlign:'text-bottom',width:12}})
			$span({parent:d, html:' unavailable<br>click and drag to select available times'})
		}
	},
	calcStartFinish(s) {
		this.start.setHours(23)
		this.start.setMinutes(59)
		this.start.setSeconds(0)
		this.finish.setDate(this.finish.getDate()-1)
		this.finish.setHours(23)
		this.finish.setMinutes(59)
		this.finish.setSeconds(0)
		s=s.split('|')
		for (let i=0;i<s.length;i++) {
			let d=s[i].split(',')
			if (d.length>1) {
				let period=d[1].split('-')
				let hm=period[0].split(':')
				let t=new Date()
				t.setHours(hm[0])
				t.setMinutes(hm[1])
				t.setSeconds(0)
				if (t<this.start)
					this.start=t
				period=d[d.length-1].split('-')
				hm=period[1].split(':')
				t=new Date()
				t.setHours(hm[0])
				t.setMinutes(hm[1])
				t.setSeconds(0)
				if (t.getHours()==0)
					t.setDate(t.getDate()+1)
				if (t>this.finish)
					this.finish=t
			}
		}
	},
	deleteDay(event) {
		let col = event.target.closest('td').cellIndex
		for (let i=0; i<this.rows.length; i++)
			this.rows[i].deleteCell(col)
	},
	dragSelect(event) {
		if (this.dragging) {
			let t=event.target
			t.selected=this.selected
			t.style.backgroundColor = this.selected ? this.colors[0] : this.unavailable
		}
	},
	empty() {
		if (this.div.firstChild)
			this.div.removeChild(this.div.firstChild)
	},
	getColor(x) {
		return this.colors[x % this.colors.length]
	},
	getDay(col) {
		let d
		if (this.rows[0].cells[col].firstChild.value)
			d = this.rows[0].cells[col].firstChild.value
		else
			d = this.rows[0].cells[col].innerHTML
		let s = false
		let t = new Date(this.start.getTime())
		for (let i=1; i<this.rows.length-2; i++) {
			let c = this.rows[i].cells[col]
			if (c.firstChild&&c.firstChild.selected != s) {
				d += (s?'-':',')+t.getHours()+':'+(t.getMinutes()?t.getMinutes():'00')
				s = c.firstChild.selected
			}
			t.setMinutes(t.getMinutes() + 30)
		}
		if (s)
			d += '-'+t.getHours()+':'+(t.getMinutes()?t.getMinutes():'00')
		return d
	},
	getDays() {
		let d = ''
		if (!this.isEmpty()) {
			let c = this.rows[0].cells
			let n = c.length
			if (c[n-1].firstChild.tagName === 'BUTTON')
				--n
			for (let i=1; i<n; i++) {
				if (i>1)
					d += '|'
				d += this.getDay(i)
			}
		}
		return d
	},
	isEmpty() {
		return this.div.children.length == 0 || this.div.children[0].tagName == 'BUTTON'
	},
	newDayEditable(t) {
		let td = this.rows[0].insertCell(-1)
		let p = td.previousSibling
		if (p.firstChild.tagName == 'BUTTON') {
			td.appendChild(p.firstChild)
			td = p
		}
		$input({parent:td, styles:{width:100}, value:t})
		for (let i=1; i<this.rows.length-2; i++) {
			td = this.rows[i].insertCell(-1)
			td.className = 'meeting'
			this.newDiv(td)
		}
		td = this.rows[this.rows.length-2].insertCell(-1)
		td.style.textAlign = 'center'
		_.ui.button(_.text.delete, this.deleteDay.bind(this), {parent:td})
	},
	newDayEmpty(t) {
		let td = $th({parent:this.rows[0], styles:{textAlign:'center'}}, t)
		for (let i=1; i<this.rows.length-2; i++) {
			td = this.rows[i].insertCell(-1)
			td.className = 'meeting'
		}
	},
	newDiv(p) {
		return $div({events:{mousedown:this.swap.bind(this), mouseover:this.dragSelect.bind(this)}, parent:p, selected:false, styles:{backgroundColor:this.unavailable, height:20, width:100}})
	},
	rebuild(before, after, select) {
		if (before)
			this.start.setMinutes(this.start.getMinutes()+before)
		else if (after)
			this.finish.setMinutes(this.finish.getMinutes()+after)
		else if (this.input.value)
			this.calcStartFinish(this.input.value)
		_.dom.empty(this.div)
		this.buildDays()
		if (select)
			this.selectDays(this.input.value)
	},
	selectDays(days) {
		days = days.split('|')
		for (let i=0; i<days.length; i++)
			if (days[i].length) {
				let day = days[i].split(',')
				if (day.length > 1)
					this.buildDay(day, i+1, this.mode == 2 ? 3 : 2)
			}
	},
	setMode(mode) {
		if (!this.isEmpty())
			this.validate()
		this.mode = mode
		_.$('#help').style.display = mode == 0 ? '' : 'none'
		this.person = 0
		if (mode == 0) {
			this.rebuild(0, 0, true)
		} else if (mode == 1) {
			this.rebuild(0, 0, false)
			this.selectDays(this.many_input.value)
		} else {
			this.rebuild(0, 0, false)
			this.selectDays(this.many_input.value)
			for (let i=0; i<this.others.length; i++) {
				this.person = i
				this.selectDays(this.others[i][1])
			}
		}
		this.buildLegend()
	},
	swap(event) {
		let t = event.target
		this.selected = !t.selected
		t.selected = this.selected
		t.style.backgroundColor = t.selected ? this.colors[0] : this.unavailable
		this.dragging = true
	},
	validate() {
		if (this.mode == 0)
			this.input.value = this.getDays()
		else if (this.mode == 1)
			this.many_input.value = this.getDays()
		return true
	}
}
