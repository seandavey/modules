package meetings;

import app.Request;
import db.View;
import db.column.ColumnBase;
import db.column.ColumnValueRenderer;

class MeetingColorRenderer implements ColumnValueRenderer {
	@Override
	public void
	writeValue(View v, ColumnBase<?> column, Request r) {
		int row = v.data.getRow() - 1;
		r.w.write("<span id=\"p")
			.write(row)
			.write("\"  style=\"border-left:12px solid\" />")
			.js("_.$('#p" + row + "').style.borderColor=meeting.getColor(" + row + ");");
	}
}