package minutes;

import java.sql.Types;
import java.util.ArrayList;

import app.ClassTask;
import app.Request;
import app.Site;
import attachments.Attachments;
import attachments.ViewDefWithAttachments;
import db.DBConnection;
import db.NameValuePairs;
import db.OneToMany;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.And;
import db.access.Or;
import db.access.RecordOwnerAccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.ColumnBase;
import db.column.ColumnValueRenderer;
import db.column.TextAreaColumn;
import db.feature.Feature.Location;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.jdbc.TSVector;
import db.object.JSONField;
import social.Comments;
import social.NewsProvider;
import util.Time;
import web.HTMLWriter;

public class Minutes extends NewsProvider {
	private Attachments m_attachments = new Attachments("minutes");
	@JSONField
	private boolean		m_support_locking;

	//--------------------------------------------------------------------------

	public
	Minutes() {
		super("minutes", "minutes");
		m_description = "A place to store meeting minutes; interfaces with NewsFeed module";
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("minutes")
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("summary", Types.VARCHAR))
			.add(new JDBCColumn("text", Types.VARCHAR))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new TSVector());
		if (m_support_locking)
			table_def.add(new JDBCColumn("locked", Types.BOOLEAN));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		int minutes_id = r.getPathSegmentInt(1);
		if (minutes_id == 0)
			return false;
		HTMLWriter w = r.w;
		Rows rows = new Rows(new Select("text").from("minutes").whereIdEquals(minutes_id), r.db);
		if (rows.next()) {
			String text = rows.getString(1);
			if (text != null)
				w.write("<div id=\"c_\" class=\"ck-content doc-view\">").writeWithLinks(text).ui.buttonIconOnClick("printer", "_.open_print_window(_.c(this))").write("</div>");
		}
		rows.close();
		View v = new View(Site.site.getViewDef("minutes_attachments", r.db), r);
		ColumnBase<?> column = v.getColumn("filename");
		v.select(new Select("*").from("minutes_attachments").whereEquals("minutes_id", minutes_id));
		if (v.data.isBeforeFirst()) {
			w.hr().h4("Attachments");
			while (v.next()) {
				column.writeValue(v, null, r);
				w.br();
			}
			w.br();
		}
		Comments.writeComments("minutes", minutes_id, r);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		return "posted minutes";
	}

	//--------------------------------------------------------------------------

	protected String
	getItemTitle(int item_id, DBConnection db) {
		return getMainMeetingLabel();
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return Site.site.absoluteURL("Minutes").set("v", item_id).toString();
	}

	//--------------------------------------------------------------------------

	public final String
	getMainMeetingLabel() {
		String title = Site.settings.getString("main meeting");
		if (title == null)
			title = "Business Meeting";
		return title;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		m_attachments.init(db);
		NameValuePairs nvp = new NameValuePairs();
		Rows rows = new Rows(new Select("id,summary,text").from("minutes").where("tsvector IS NULL"), db);
		while (rows.next()) {
			Site.site.log("tsvector null for minute " + rows.getInt(1), false);
			nvp.set("tsvector", rows.getString(2) + "\n" + rows.getString(3));
			db.update("minutes", nvp, rows.getInt(1));
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@ClassTask({"id", "only_nulls"})
	public final void
	loadTokens(int id, boolean only_nulls, DBConnection db) {
		Select query = new Select("id,summary,text").from("minutes");
		if (only_nulls)
			query.where("tsvector IS NULL");
		Rows d = new Rows(query, db);
		while (d.next()) {
			StringBuilder tsvector = new StringBuilder();
			tsvector.append(d.getString(2)).append("\n").append(d.getString(3)).append("\n");
			if (m_attachments != null)
				m_attachments.appendTokens(d.getInt(1), tsvector, db);
			db.update("minutes", "tsvector=to_tsvector('" + SQL.escape(tsvector.toString()) + "')", d.getInt(1));
		}
		d.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("minutes")) {
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("date");
			column_names.add("summary");
			column_names.add("text");
			addColumnNames(column_names);
			ViewDef view_def = new ViewDefWithAttachments(name, m_attachments);
			addColumnSendEmail(column_names, view_def);
			addColumnShowOnNewsFeed(column_names, view_def);
			if (m_support_locking)
				column_names.add("locked");
			AccessPolicy access_policy = new Or(new RecordOwnerAccessPolicy().add().delete().edit().view(), new RoleAccessPolicy("minutes"));
			if (m_support_locking)
				access_policy = new And(new LockedAccessPolicy().add().delete().edit().view(), access_policy);
			return addHooks(view_def
				.addFeature(new YearFilter("minutes", Location.TOP_LEFT))
				.setAccessPolicy(access_policy)
				.setDefaultOrderBy("date")
				.setRecordName("Minute", true)
				.setShowDoneLink(true)
				.setTimestampRecords(true)
				.setTSVectorColumns("summary", "text")
				.setColumnNamesForm(column_names)
				.setColumnNamesTable("date", "summary")
				.setColumn(new Column("date").setDefaultToDateNow().setDisplayName("meeting date").setIsRequired(true).setValueRenderer(new ColumnValueRenderer(){
					@Override
					public void writeValue(View v, ColumnBase<?> column, Request r) {
						r.w.write(Time.formatter.getMonthDate(v.data.getDate(column.getName())));
					}
				}, true))
				.setColumn(new Column("show_on_news_feed").setHideOnForms(Mode.READ_ONLY_FORM))
				.setColumn(new Column("summary").setIsRequired(true))
				.setColumn(new TextAreaColumn("text").setIsRichText(true, false))
				.addRelationshipDef(new OneToMany("minutes_attachments")));
		}
		if (name.equals("minutes_attachments"))
			return m_attachments._newViewDef(name);
		return null;
	}

	//--------------------------------------------------------------------------

	public final boolean
	supportsLocking() {
		return m_support_locking;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		super.updateSettings(r);
		Site.site.removeViewDef("minutes");
	}

	//--------------------------------------------------------------------------

	public final void
	writeLink(int id, String label, HTMLWriter w) {
		w.write("<a href=\"#\" onclick=\"new Dialog({url:context+'/")
			.write(m_name).write("/").write(id)
			.write("',title:'").write(label).write(" Minutes',overflow:'auto'});return false\">")
			.write(label)
			.write("</a>");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		String date = Time.formatter.getDateShort(db.lookupDate(new Select("date").from("minutes").whereIdEquals(item_id)));
		String label = getItemTitle(item_id, db) + " " + date;
		if (for_mail)
			w.a(label, Site.site.absoluteURL("Minutes").set("v", item_id).set("label", label).toString());
		else
			writeLink(item_id, label, w);
		w.write("<div style=\"max-height:250px;overflow:auto;\">").write(db.lookupString("summary", "minutes", item_id)).write("</div>");
		if (!for_mail)
			m_attachments.write(item_id, w, db);
	}
}
