package minutes;

import app.Modules;
import app.Request;
import app.Site;
import db.View;
import db.ViewDef;
import util.Time;

public class MinutesView extends View {
	protected final Minutes m_minutes;

	//--------------------------------------------------------------------------

	public
	MinutesView(ViewDef view_def, Request r) {
		super(view_def, r);
		m_minutes = (Minutes)Modules.get("Minutes");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeColumnHeadsRow(String[] columns, boolean include_popup_menu) {
		if (m_mode != Mode.READ_ONLY_LIST)
			super.writeColumnHeadsRow(columns, include_popup_menu);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeRow(String[] columns) {
		if (m_mode != Mode.READ_ONLY_LIST) {
			super.writeRow(columns);
			return;
		}
		m_w.write("<div class=\"mb-3\">");
		m_minutes.writeLink(data.getInt("id"), Time.formatter.getDateLong(data.getDate("date")), m_w);
		String summary = data.getString("summary");
		if (summary != null)
			m_w.tag("div", summary);
		m_w.addStyle("font-size:smaller")
			.tag("div", "posted by " + Site.site.lookupName(data.getInt("_owner_"), m_r.db));
		m_w.write("</div>");
		++m_num_rows;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	write() {
		if (!m_request_processed) {
			processRequest(this, m_r).write();
			return;
		}
		if (getMode() != Mode.READ_ONLY_LIST)
			m_w.style("div [data-view=\"minutes\"] table.table td:first-child{white-space:nowrap}");
		super.write();
	}
}