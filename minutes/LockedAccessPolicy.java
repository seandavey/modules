package minutes;

import app.Request;
import db.Rows;
import db.Select;
import db.access.AccessPolicy;

public class LockedAccessPolicy extends AccessPolicy {
	@Override
	public boolean
	canUpdateRow(String from, int id, Request r) {
		return !r.db.lookupBoolean(new Select("locked").from(from).whereIdEquals(id));
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtonForRow(Rows data, Request r) {
		return !data.getBoolean("locked");
	}
}
