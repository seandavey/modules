var documents = {
	add_dialog(el, viewdef, allow_links, allow_richtext, allow_folders) {
		let title = 'Add Document'
		if (allow_folders)
			title += ' or Folder'
		var d = new Dialog({title:title})
		var div = $div({parent:d.body, styles:{display:'flex', gap:20}})
		function a(text, kind, title) {
			_.ui.button(text, function() {
				Dialog.top().close()
				_.table.dialog_add(el, viewdef, context + '/Views/' + viewdef + '/component?db_mode=ADD_FORM&kind='+kind, title)
			}, {parent:div, styles:{display:'block', marginBottom:5, width:'100%'}})
		}
		if (allow_links)
			a('add link', 'link', 'Add Link')
		if (allow_richtext)
			a('create new document', 'richtext', 'Create Document')
		if (allow_folders)
			a('create new folder', 'folder', 'Create Folder')
		a('upload file', 'upload', 'Upload a File')
		d.open()
	},
	for_folder(el, f) {
		let depth = parseInt(el.dataset['depth'] || '0')
		el = el.nextElementSibling
		if (!el)
			return
		let d = parseInt(el.dataset['depth'] || '0')
		while (el && d > depth) {
			if (f(el, d))
				return true
			el = el.nextElementSibling
			if (el)
				d = parseInt(el.dataset['depth'] || '0')
		}
		return false
	},
	hide_empty_folders(el) {
		while (el) {
			if ('f' === el.dataset.kind) {
				let d = el.dataset.depth || 0
				let e = el.nextElementSibling
				let has_doc = false
				while (e && e.dataset.depth > d) {
					if ('f' !== e.dataset.kind) {
						has_doc = true
						break
					}
					e = e.nextElementSibling
				}
				if (!has_doc)
					el.style.display = 'none'
			}
			el = el.nextElementSibling
		}
	},
	record_name(id) {
		let row = _.$('[data-id="' + id + '"]')
		if (row && 'f' === row.dataset.kind)
			return 'folder'
		return 'document'
	},
	toggle_folder(el) {
		let closed = _.ui.icon_toggle(el.firstChild.firstChild,'folder','folder-open')
		el = el.closest('tr')
		let depth = parseInt(el.dataset['depth'] || '0') + 1
		documents.for_folder(el, (e, d) => {
			if (d === depth || closed)
				e.style.display = closed ? 'none' : ''
			if (closed && 'f' === e.dataset.kind)
				_.ui.icon_set(e.querySelector('svg[data-icon]'), 'folder')
			return false
		})
	}
}
app.set('edit documents:record name',documents.record_name)