package documents;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Types;
import java.util.ArrayList;

import app.ClassTask;
import app.Request;
import app.Roles;
import app.Site;
import attachments.Attachments;
import db.DBConnection;
import db.NameValuePairs;
import db.OneToMany;
import db.Options;
import db.Rows;
import db.SQL;
import db.Select;
import db.View.Mode;
import db.ViewDef;
import db.access.RecordOwnerAccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.DocumentColumn;
import db.column.FileColumn;
import db.column.FolderColumn;
import db.column.PersonColumn;
import db.column.TimestampColumn;
import db.feature.Feature.Location;
import db.feature.TSVectorSearch;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.jdbc.TSVector;
import db.object.JSONField;
import pages.Page;
import social.NewsProvider;
import web.HTMLWriter;
import web.Head;

public class Documents extends NewsProvider {
	@JSONField
	protected boolean		m_allow_folders = true;
//	@JSONField
//	private boolean			m_allow_sort_by_date;
	protected Attachments	m_attachments;
	private String			m_dir_column;
	@JSONField
	private boolean			m_only_admins_can_edit_types;
	@JSONField(after="set to blank to allow anyone to edit documents",label="role needed for editing on main Documents page",type="role")
	private String			m_role = "docs";
	@JSONField
	private boolean			m_show_date_in_table;
	@JSONField
	private boolean			m_support_attachments;

	//--------------------------------------------------------------------------

	public
	Documents() {
		super("documents", "document");
		m_description = "Organize documents that are created online or uploaded or linked to other sites";
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("documents")
			.add(new JDBCColumn("filename", Types.VARCHAR))
			.add(new JDBCColumn("folder", "documents").setOnDeleteSetNull(true))
			.add(new JDBCColumn("kind", Types.CHAR, 1))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("type", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP))
			.add(new TSVector());
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, true, db);
		db.createTable("documents_types", "text VARCHAR");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		if (!"f".equals(nvp.getString("kind")))
			super.afterInsert(id, nvp, r);
	}

	//--------------------------------------------------------------------------

	public final boolean
	allowFolders() {
		return m_allow_folders;
	}

	//--------------------------------------------------------------------------

	private ViewDef
	documentsViewDef(String name, boolean badge_column) {
		ViewDef view_def = new DocumentsViewDef(name, m_allow_folders, m_attachments)
			.setDefaultOrderBy("title")
			.setFrom("documents")
			.setRecordName("Document", true)
			.setTimestampRecords(true)
			.setTSVectorColumns("title");
		if (m_role != null)
			view_def.setAccessPolicy(new RoleAccessPolicy("docs").add().delete().edit());
		else
			view_def.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit());

		ArrayList<String> column_names = new ArrayList<>();
		column_names.add("filename");
		if (m_show_date_in_table) {
			column_names.add("_timestamp_");
			view_def.setColumn(new TimestampColumn("_timestamp_").setDisplayName("date").setJustDate(true));
		}
		view_def.setColumnNamesTable(column_names);

		column_names.clear();
		column_names.add("title");
		if (m_show_date_in_table)
			column_names.add("_timestamp_");
		column_names.add("type");
		column_names.add("filename");
		if (m_allow_folders) {
			column_names.add("folder");
			view_def.setColumn(new FolderColumn("documents"));
			view_def.setBaseFilter("folder IS NULL");
		}
		column_names.add("_owner_");
		addColumnNames(column_names);
		if (m_add_new_items_to_news_feed)
			column_names.add("show_on_news_feed");
		view_def.setColumnNamesForm(column_names);

		FileColumn c = new DocumentColumn("filename", "documents", "documents")
			.setAllowFolders(m_allow_folders)
			.setAllowEditing(true)
			.setAllowURLs(true)
			.setDisplayName("document")
			.setIsRequired(true)
			.setPrintTemplate("/Documents/{id}/print")
			.setTitleColumn("title")
			.setUseGit(true);
		if (badge_column)
			c.setBadgeColumn("type");
		if (m_dir_column != null)
			c.setDirColumn(m_dir_column);
		view_def.setColumn(c)
			.setColumn(new PersonColumn("_owner_", false).setAllowNoSelection(true).setDefaultToUserId().setDisplayName("posted by"))
			.setColumn(new Column("show_on_news_feed").setHideOnForms(Mode.READ_ONLY_FORM))
			.setColumn(new Column("title"){
				@Override
				public String
				validate(ViewDef v_d, String value, int id, Request r) {
					if (r.getBoolean("db_filename_unzip"))
						return null;
					return super.validate(v_d, value, id, r);
				}
			}.setIsRequired(true).setOrderBy("lower(title)"))
			.setColumn(((Options)Site.site.getObjects("documents_types")).setAllowEditing(!m_only_admins_can_edit_types).newColumn("type"));
		if (m_support_attachments)
			view_def.addRelationshipDef(new OneToMany("documents_attachments"));
		return addHooks(view_def);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		int segment_one = r.getPathSegmentInt(1);
		if (segment_one == 0)
			return false;
		Rows data = new Rows(new Select("*").from("documents").whereIdEquals(segment_one), r.db);
		if (data.next()) {
			if ("print".equals(r.getPathSegment(2)))
				new Head(data.getString("title"), r).close();
			FileColumn column = (FileColumn)Site.site.getViewDef("documents", r.db).getColumn("filename");
			File file = column.getFilePath(data).toFile();
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line = br.readLine();
				while (line != null) {
					r.w.write(line);
					line = br.readLine();
				}
				br.close();
			} catch (IOException e) {
				r.abort(e);
			}
		} else
			r.w.write("document not found");
		data.close();
		r.close();
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch(segment_one) {
		case "folder":
			if (m_allow_folders) {
				int id = r.getPathSegmentInt(2);
				if (id != 0 && "delete".equals(r.getPathSegment(3))) {
					r.db.delete("documents", id, false);
					return true;
				}
				if ("add".equals(r.getPathSegment(2))) {
					r.db.insert("documents", "title,kind", SQL.string(r.getParameter("title")) + ",'f'");
					return true;
				}
			}
			break;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		return "posted a document";
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return Site.site.absoluteURL("Documents").set("v=", item_id).toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Site.site.addObjects(new Options(new Select("*").from("documents_types").orderBy("text")).setAllowEditing(true).setTextIsValue(true));
		Roles.add("docs", "can edit documents on the site with this role");
		Site.site.addUserDropdownItem(new Page("Edit Documents", this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("edit documents", r).writeComponent();
			}
		}.addRole("docs"));
		if (m_support_attachments) {
			m_attachments = new Attachments("documents");
			m_attachments.init(db);
		}
	}

	//--------------------------------------------------------------------------

	@ClassTask({"id", "only_nulls"})
	public final void
	loadTokens(int id, boolean only_nulls, DBConnection db) {
		String where = id != 0 ? "id=" + id : "filename IS NOT NULL";
		if (only_nulls)
			where += " AND tsvector IS NULL";
		Rows d = new Rows(new Select("id,filename,title,groups_id,kind").from("documents").where(where), db);
		while (d.next()) {
			String kind = d.getString(5);
			if ("f".equals(kind))
				continue;
			StringBuilder tsvector = new StringBuilder();
			tsvector.append(d.getString(3)).append("\n");
			if ("l".equals(kind)) {
				db.update("documents", "tsvector=to_tsvector('" + SQL.escape(tsvector.toString()) + "')", d.getInt(1));
				continue;
			}
			String filename = d.getString(2);
			int groups_id = d.getInt(4);
			Path path = groups_id == 0 ? Site.site.getPath("documents", filename) : Site.site.getPath("documents", Integer.toString(groups_id), filename);
			if (!path.toFile().exists()) {
				System.out.println(path + " not found");
				continue;
			}
			tsvector.append(Site.site.loadTokens(path)).append("\n");
			if (m_attachments != null)
				m_attachments.appendTokens(d.getInt(1), tsvector, db);
			db.update("documents", "tsvector=to_tsvector('" + SQL.escape(tsvector.toString()) + "')", d.getInt(1));
		}
		d.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("documents"))
			return documentsViewDef(name, true);
		if (name.equals("documents_attachments") && m_attachments != null)
			return m_attachments._newViewDef(name);
		if (name.equals("documents_types"))
			return Site.site.getObjects(name).newViewDef()
				.setRecordName("Type", true)
				.setShowNumRecords(false)
				.setColumn(new Column("text").setIsRequired(true));
		if (name.equals("edit documents"))
			return documentsViewDef(name, false)
				.addFeature(new TSVectorSearch(null, Location.TOP_RIGHT))
				.setAccessPolicy(new RoleAccessPolicy("docs").add().delete().edit());

		return null;
	}

	//--------------------------------------------------------------------------

	public final Documents
	setDirColumn(String dir_column) {
		m_dir_column = dir_column;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		super.updateSettings(r);
		if (m_support_attachments) {
			if (m_attachments == null) {
				m_attachments = new Attachments("documents");
				m_attachments.init(r.db);
			}
		} else
			m_attachments = null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		FileColumn file_column = (FileColumn)Site.site.getViewDef("documents", db).getColumn("filename");
		Rows rows = new Rows(new Select("*").from("documents").whereIdEquals(item_id), db);
		if (rows.next())
			file_column.writeValue(rows, w);
		rows.close();
		if (m_attachments != null)
			m_attachments.write(item_id, w, db);
	}
}
