package documents;

import app.Request;
import app.Stringify;
import attachments.Attachments;
import attachments.ViewDefWithAttachments;
import db.NameValuePairs;

@Stringify
public class DocumentsViewDef extends ViewDefWithAttachments {
	private boolean m_allow_folders;

	//--------------------------------------------------------------------------

	public
	DocumentsViewDef(String name, boolean allow_folders, Attachments attachments) {
		super(name, attachments);
		m_allow_folders = allow_folders;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeInsert(NameValuePairs nvp, Request r) {
		if ("link".equals(r.getParameter("db_filename_type")))
			nvp.set("url", true);
		return super.beforeInsert(nvp, r);
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getAddButtonOnClick(String url) {
		return "documents.add_dialog(this,'" + m_name + "',true,true," + (m_allow_folders ? "true" : "false") + ")";
	}

	//--------------------------------------------------------------------------

	@Override
	public db.View
	newView(Request r) {
		return new DocumentsView(this, m_allow_folders, r);
	}
}
