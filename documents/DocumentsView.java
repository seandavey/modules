package documents;

import java.util.ArrayList;
import java.util.Stack;

import app.Modules;
import app.Request;
import db.Form;
import db.Rows;
import db.Select;
import db.View;
import db.ViewDef;

public class DocumentsView extends View {
	private final boolean		m_allow_folders;
	private final Stack<Rows>	m_data_stack = new Stack<>();

	// --------------------------------------------------------------------------

	public
	DocumentsView(ViewDef view_def, boolean allow_folders, Request r) {
		super(view_def, r);
		m_allow_folders = allow_folders;
	}

	// --------------------------------------------------------------------------

	@Override
	public Form
	newEditForm() {
		Form f = super.newEditForm();
		if (m_mode == Mode.ADD_FORM && "folder".equals(m_r.getParameter("kind")) || m_mode == Mode.EDIT_FORM && "f".equals(data.getString("kind"))) {
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("filename");
			if (m_allow_folders)
				column_names.add("folder");
			Modules.get("Documents").addColumnNames(column_names);
			setColumnNamesForm(column_names);
		}
		return f;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	componentOpen() {
		if (m_mode == Mode.LIST)
			m_w.js("JS.get('documents-min')");
		return super.componentOpen();
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	writeCells(String[] columns) {
		for (String column : columns) {
			getColumn(column);
			m_w.tagOpen("td");
			if ("filename".equals(column) && m_data_stack.size() > 0)
				m_w.write("<span style=\"display:inline-block;width:" + m_data_stack.size() * 20 + "px\"></span>");
			if (!writeColumnHTML(column))
				m_w.nbsp();
			m_w.tagClose();
		}
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	writeRow(String[] columns) {
		if (m_allow_folders && m_data_stack.size() == 0 && data.getInt("folder") != 0)
			return;
		boolean is_folder = m_allow_folders && "f".equals(data.getString("kind"));
		if (is_folder)
			m_w.setAttribute("data-kind", "f");
		super.writeRow(columns);
		if (is_folder) {
			int id = data.getInt("id");
			--m_num_rows;
			m_data_stack.push(data);
			data = new Rows(new Select("*").from("documents").whereEquals("folder", id).andWhere(m_state.getFilter()).orderBy(m_state.getOrderBy().toQueryString(this, m_r)), m_r.db);
			while (data.next()) {
				m_w.addStyle("display:none")
					.setAttribute("data-depth", m_data_stack.size());
				writeRow(columns);
			}
			data.close();
			data = m_data_stack.pop();
		}
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	writeRows(String[] columns) {
		super.writeRows(columns);
		if (m_state.getFilter() != null && m_view_def.getName().equals("docs"))
			m_w.js("function hide_empty_folders(){if(typeof documents === 'undefined')setTimeout(hide_empty_folders,50);else documents.hide_empty_folders(_.$('table[data-view=\"docs\"]').querySelector('tr'))}hide_empty_folders()");
	}
}
